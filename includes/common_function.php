<?php

// COMMON VARIABLES & VAlUES

$mand = "<span class='mand'> *</span>";
//$Select = "Select";
$Select="<option  value='0'>Select</option>";
$errlbl = "<p class='error'>Incomplete / Invalid entries for<br>";

function encrypt($plaintext)
{
	$cypher = 'blowfish';
	$mode = 'cfb';
	$key = '1a2s3d4f5g6h';

	$td = mcrypt_module_open($cypher,'',$mode,'');
	$iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
	mcrypt_generic_init($td, $key, $iv);
	$crypttext = mcrypt_generic($td, $plaintext);
	mcrypt_generic_deinit($td);
	return $iv.$crypttext;
}
function decrypt($crypttext)
{
	$plaintext = "";
	$td = mcrypt_module_open($cypher, '', $mode, '');
	$ivsize = mcrypt_enc_get_iv_size($td);
	$iv = substr($crypttext, 0, $ivsize);
	$crypttext = substr($crypttext, $ivsize);
	if ($iv)
	{
		mcrypt_generic_init($td, $key, $iv);
		$plaintext = mdecrypt_generic($td, $crypttext);
	}
	return $plaintext;
}

function Strcheck($var,&$errmsg,&$errflag,$msg)
{	
	if(strlen($var)>0)
		return 0;
	else 
	{	
		if($errflag==1) $errmsg.=",".$msg; else $errmsg = $msg;
		$errflag=1;
		return 1;
	}
}

/* function Strcheck1($var,&$errmsg,&$errflag,$msg,$name)
{	
	if(strlen($var)>0)
		return 0;
	else 
	{	
		if($errflag==1) $errmsg.=",".$msg; else $errmsg = $msg;
		$errflag=1;
		return 1;
		 echo "<script> document.getElementById('".$name."').focus();</script>";
		
	}
} */
function Zerocheck($var,&$errmsg,&$errflag,$msg)
{	
	if( strlen($var) >0 and ( (is_numeric($var) and $var>0) or (!is_numeric($var) and $var<>'0' ) ))
		return 0;
	else 
	{	
		if($errflag==1) $errmsg.=",".$msg; else $errmsg = $msg;
		$errflag=1;
		return 1;
	}
}

function Numcheck($var,&$errmsg,&$errflag,$msg)
{
	if(is_numeric($var))
		return 0;
	else
	{
		if($errflag==1) $errmsg.= ",".$msg; else $errmsg = $msg;
		$errflag=1;
		return 1;	
		
	}
}


// Check only Email Format
function Emailcheck($var,&$errmsg,&$errflag,$msg) 
{ 
if (!eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,5})$", $var)){
 	  if(strlen($errmsg)>0) $errmsg.= ",".$msg; else $errmsg = $msg;
	  $errflag=1;
	  return 1;
 }
 return 0;
}

// Check only Email Domain Format
/*function Emailcheck($var,&$errmsg,&$errflag,$msg) {
 if(preg_match("/^( [a-zA-Z0-9] )+( [a-zA-Z0-9\._-] )*@( [a-zA-Z0-9_-] )+( [a-zA-Z0-9\._-] +)+$/" , $var)){
  list($username,$domain)=split('$',$var);
  if(!checkdnsrr($domain,'MX')) {
   if(strlen($errmsg)>0) $errmsg.= "Email not in this domain"; else $errmsg = "Email not in this domain";
   $errflag=1;
   return false;
  }
  return true;
 }
 if(strlen($errmsg)>0) $errmsg.= ",".$msg; else $errmsg = $msg;
 $errflag=1;
 return false;
}
*/

function Min_lengthcheck($var,&$errmsg,&$errflag,$msg,$cnt)
{
if(strlen($var)>=$cnt){
 return 0;
}
else
	{
		if($errflag==1) $errmsg.= ",".$msg; else $errmsg = $msg;
		$errflag=1;
		return 1;	
		
	}
}
function Min_lengthcheck1($var,&$errmsg,&$errflag,$msg,$cnt)
{
if(strlen($var)>=$cnt){
 return 0;
}
else
	{
		if($errflag==1) $errmsg.= ",".$msg; else $errmsg = $msg;
		$errflag=1;
		return 1;	
		
	}
}

function FromTo_Valcheck($var,$var1,&$errmsg,&$errflag,$msg)
{
    if($var > $var1) {
        if($errflag==1) $errmsg.= ",".$msg; else $errmsg = $msg;
		$errflag=1;
		return 1;
    } else return 0;
}
function MaxMin_Valcheck($var,&$errmsg,&$errflag,$msg,$var1,$var2)
{
    if($var >= $var1 && $var <= $var2 )
        return 0;
    else
    {
        if($errflag==1) $errmsg.= ",".$msg; else $errmsg = $msg;
		$errflag=1;
		return 1;
    }
}
function DateCheck($start_date,$end_date,&$errmsg,&$errflag,$msg)
{
  $start = strtotime($start_date);
  $end = strtotime($end_date);
  if (($start-$end) > 0) 
  {
    if($errflag==1) $errmsg.= ",".$msg; else $errmsg = $msg;
    $errflag=1;
    return 1;
  } else 
   return 0;
}
?>