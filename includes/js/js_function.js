function markonly(obj)
{ 	
	var txt="#"+obj;
	$(txt).filter_input({regex:'([1-9]|[1]+[0]'});
	
}

function numberonly_mark(obj)
{ 	
 	var txt="#"+obj;
	$(txt).filter_input({regex:'[.0-9]'}); 
}
function numberonly(obj)
{ 	
 	var txt="#"+obj;
	$(txt).filter_input({regex:'[0-9]'}); 
}
function periodonly(obj)
{ 	
 	var txt="#"+obj;
	$(txt).filter_input({regex:'[0-9 -]'}); 
}
function alphaonly(obj)
{ 
 	var txt="#"+obj;
	$(txt).filter_input({regex:'[a-zA-Z ]'});
	
}
function alphagrade(obj)
{ 
 	var txt="#"+obj;
	$(txt).filter_input({regex:'[a-zA-Z+]'});
	
}
function alphanumeric(obj)
{ 
 	var txt="#"+obj;
	$(txt).filter_input({regex:'[a-zA-Z .0-9]'});
	
}
function phonecode(obj)
{
	var txt="#"+obj; 
	$(txt).filter_input({regex:'[0-9+]'}); 
}
function lengthcheck(len,divid,txtid)
{ 
	var maxLen = (len)- ($(txtid).val().length);
	var msg = "Character Left : "+maxLen;
	
	$(divid).html(msg);
}			

function Nameonly(obj)
{ 
 	var txt="#"+obj;
	$(txt).filter_input({regex:'[a-zA-Z]'});
	
}
function Rptgrade(obj)
{ 
 	var txt="#"+obj;
	$(txt).filter_input({regex:'[a-eA-E+0-9]'});
	
}