<?php session_start(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Isha Vidhya</title>
<?php include("header.php"); ?>
</head>
<body style="margin:5px; background: #DFE9E8;" >
<table width="90%" border="0" cellspacing="0" align="center" cellpadding="0">
  <tr>
    <td width="13"><img src="images/bg_1.png" width="13" height="14" /></td>
    <td style="background:url(images/bg_2.png) repeat-x;"></td>
    <td width="15"><img src="images/bg_3.png" width="13" height="14" /></td>
</tr>
  <tr>
    <td style="background:url(images/bg_8.png) repeat-y;">&nbsp;</td>
    <td style="height:800px; background:#FFFFFF;" valign="top" >
    
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><table width="964" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td ><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td style="background:url(images/bg.gif) repeat-y; width:15px;">&nbsp;</td>
            <td><table width="938" border="0" align="center" cellpadding="0" cellspacing="3">
              <tr>
                <td align="center"><table width="100%" border="0" cellspacing="5" cellpadding="0">
                    <tr>
                      <td><img src="images/logo.jpg" width="197" height="150" /></td>
                      <td align="right"><img src="images/top-right.jpg" width="218" height="146" /></td>
                    </tr>
                    <tr>
                      <td class="title_td" colspan="2">&nbsp;</td>
                    </tr>
                </table></td>
              </tr>
              <tr>
                <td width="100%"><table width="100%" height="500" align="center" border="0" cellspacing="0" cellpadding="0" class="table_border">
                    <tr>
                      <td><table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" >
                          <tr>
                            <td align="center"><img src="imgs/thanks.jpg" alt=""/></td>
                          </tr>
                          <tr>
                            <td align="center" colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;<span class='hr_message'>Thank you for Using this Service.</span></td>
                          </tr>
                          <tr>
                            <td align="center" colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;<a href="index.php" target="_parent"><u><span class="silver">Login</span></u></a></td>
                          </tr>
                          <tr>
                            <td height="3"></td>
                          </tr>
                      </table></td>
                    </tr>
                    <tr>
                      <td height="5">&nbsp;</td>
                    </tr>
                    <tr>
                      <td align="center" class="foot" style="border-bottom: dashed 1px #000000;">&nbsp;</td>
                    </tr>
                    <tr>
                      <td align="center" class="foot">Isha Vidhya &copy; <?php echo date("Y"); ?>. All rights reserved.</td>
                    </tr>
                    <tr>
                      <td colspan="3" align="center" style="padding-top: 5px;"><span class="style6" id="map"><a href="http://www.ishavidhya.org/Sitemap.isa">Site Map</a> &bull; <a href="mailto:info@ishavidhya.org">Feedback</a> &bull; <a href="http://www.ishavidhya.org/contact-us.html">Contact Us</a> &bull;</span><span class="style6" id="view"> <a href="http://www.ishafoundation.org/Copyright.isa">View our Copyright and Privacy Policy</a></span></td>
                    </tr>
                </table></td>
              </tr>
            </table></td>
            <td style="background:url(images/bg.gif) repeat-y; width:15px;">&nbsp;</td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp; </td>
  </tr>
</table>

    <td style="background:url(images/bg_4.png) repeat-y;">&nbsp;</td>
  </tr>
  <tr>
    <td valign="top"><img src="images/bg_7.png" width="13" height="14" /></td>
    <td style="background:url(images/bg_6.png) repeat-x"></td>
    <td valign="top"><img src="images/bg_5.png" width="13" height="14" /></td>
</tr>
</table>
<?php 	session_destroy();
		session_unset();	
	//	setcookie("PHPSESSID","",time()-3600,"/");	 
	?>
</body>
</html>