<?php session_start(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>Isha Foundation - A Non-profit Organization</title>

<?php include("../Includes/header.php"); ?>

<table width="70%" border="0" align="center" cellpadding="3" cellspacing="1">
<tr><td class="head">You are here: Org Structure - <span class="text">Mas_School</span></td></tr>
</table><br />
<script language="javascript" type="text/javascript">
	function getCity(obj)
	{	
		if(obj=='') return false;
	    url = "../includes/ajax.php?listcode=1&State="+obj;
		$('#citydiv').load(url);
	}
	function getCity_Edit(obj,obj1,obj2)
	{	
		if(obj=='') return false;
	    url = "../includes/ajax.php?listcode=1&State="+obj1+"&City_Code="+obj2;
		var cdiv = '#citydiv'+obj;
		$(cdiv).load(url);
	}

function editval(val)
{	
	if($('#editcnt').val()!="")
		$('#editcnt').val() = $('#editcnt').val()+','+ $('#editcnt').val(val);
	else
		$('#editcnt').val(val)
}
	
function adddata()
 { 
 	$('#school_name1').show();
	$('#short_name1').show();
	$('#location1').show();
	$('#address1').show();
	$('#state_code1').show();
	$('#citydiv').show();
	$('#Principal_Code').show();		
	$('#contact_no11').show();
	$('#contact_no21').show();
	$('#email1').show();
	$('#active1').show();
	
	$('#mode1').val('ADD');
} 


function editdata(val,val1,val2)
{
	editval(val);
	$('#school_name_'+val).show();
	$('#short_name_'+val).show();
	$('#location_'+val).show();
	$('#address_'+val).show();
	$('#location_'+val).show();
	$('#statecode'+val).show();
	$('#citydiv'+val).show();	
	$('#principal_code_'+val).show();
	$('#contact_no1_'+val).show();
	$('#contact_no2_'+val).show();
	$('#email_'+val).show();
	$('#active_'+val).show();
	
	var $mode ='EDIT';
	$('#mode').val($mode);
	getCity_Edit(val,val1,val2);

}
function deldata(val)
{ 
	var msg="Do you want to delete?";
	if(!confirm(msg)){
		return false;
	}	 
	else {
		$('#school_id').val(val);
		$('#mode').val('DELETE');
		$('#myform').submit();
	 }
}
</script>
</head>

<?php 
	$errmsg="";
	$errflag=0;
	$dummy=0;
	$mode		=	$_POST['mode'];
	$mode1		=	$_POST['mode1'];
	$editcnt 	=	split(',',$_POST['editcnt']);		

	if($_POST['Cancel']=="Cancel") { $mode=""; $mode1="";	}
	
	If($mode == "EDIT")
	{ 
		$action=2;
	for($i=0;$i<count($editcnt);$i=$i+1) 
	{ 
		$school_id	    =	$_POST['school_id'.$editcnt[$i]];	
		$school_name  	=	$_POST['school_name_'.$editcnt[$i]];
		$short_name		=	$_POST['short_name_'.$editcnt[$i]];
		$location		=	$_POST['location_'.$editcnt[$i]];
		$address		=	$_POST['address_'.$editcnt[$i]];
		$state_code		=	$_POST['state_code'.$editcnt[$i]];
		$city_code		=	$_POST['city_code'];
		$principal_code	=	$_POST['principal_code_'.$editcnt[$i]];
		$contact_no1	=	$_POST['contact_no1_'.$editcnt[$i]];
		$contact_no2	=	$_POST['contact_no2_'.$editcnt[$i]];
		$email			=	$_POST['email_'.$editcnt[$i]];
		$active			=	$_POST['active_'.$editcnt[$i]];
	
		$j=$i+1;
		$dummy = Strcheck($school_name,$errmsg,$errflag,"School".$j);
		$dummy = Strcheck($short_name,$errmsg,$errflag,"Short Name".$j);
		$dummy = Strcheck($location,$errmsg,$errflag,"Location".$j);
		$dummy = Strcheck($principal_code,$errmsg,$errflag,"Principal".$j);
		//$dummy = CheckEmail($email,$errmsg,$errflag,"Email ID".$j);

		if($errflag==0)
		{
			
			$query = mssql_init('sp_School',$mssql);
			mssql_bind($query,'@School_Id',$school_id,SQLINT4,false,false,5);
			mssql_bind($query,'@School_Name',$school_name,SQLVARCHAR,false,false,50);
			mssql_bind($query,'@Short_Name',$short_name,SQLVARCHAR,false,false,25);
			mssql_bind($query,'@Location',$location,SQLVARCHAR,false,false,50);
			mssql_bind($query,'@Address',$address,SQLVARCHAR,false,false,50);
			mssql_bind($query,'@State_Code',$state_code,SQLINT4,false,false,5);
			mssql_bind($query,'@City_Code',$city_code,SQLINT4,false,false,5);
			mssql_bind($query,'@Principal_Code',$principal_code,SQLINT4,false,false,5);
			mssql_bind($query,'@Contact_No1',$contact_no1,SQLVARCHAR,false,false,5);
			mssql_bind($query,'@Contact_No2',$contact_no2,SQLVARCHAR,false,false,5);
			mssql_bind($query,'@Email_Id',$email,SQLVARCHAR,false,false,5);
			mssql_bind($query,'@Active',$active,SQLVARCHAR,false,false,1);
			mssql_bind($query,'@Action',$action,SQLINT4,false,false,1);
			$result = @mssql_execute($query);
			mssql_free_statement($query);
			if($result==1)
				echo "<p class='mesg'>School has been Updated</p>";
			else {
				$errmsg1=mssql_get_last_message();
				$errflag=2;
				$errcnt=1;
			}
		}
		else {
			If ($errcnt == 0) { $errcnt = 1;} else { $errcnt = $errcnt + 1; }
			if ($errval == "") { $errval = $editcnt[$i];	 } else { $errval = $errval.",".$editcnt[$i]; } 
		}
	}	
	}
  
	if($mode=="DELETE")
	{	$action=3;
  		$school_id	=	$_POST['school_id'];
       
		$query = mssql_init('sp_School',$mssql);		
		    mssql_bind($query,'@School_Id',$school_id,SQLINT4,false,false,5);
			mssql_bind($query,'@School_Name',$school_name,SQLVARCHAR,false,false,50);
			mssql_bind($query,'@Short_Name',$short_name,SQLVARCHAR,false,false,25);
			mssql_bind($query,'@Location',$location,SQLVARCHAR,false,false,50);
			mssql_bind($query,'@Address',$address,SQLVARCHAR,false,false,50);
			mssql_bind($query,'@State_Code',$state_code,SQLINT4,false,false,5);
			mssql_bind($query,'@City_Code',$city_code,SQLINT4,false,false,5);
			mssql_bind($query,'@Principal_Code',$principal_code,SQLINT4,false,false,5);
			mssql_bind($query,'@Contact_No1',$contact_no1,SQLVARCHAR,false,false,5);
			mssql_bind($query,'@Contact_No2',$contact_no2,SQLVARCHAR,false,false,5);
			mssql_bind($query,'@Email_Id',$email,SQLVARCHAR,false,false,5);
			mssql_bind($query,'@Active',$active,SQLVARCHAR,false,false,1);
			mssql_bind($query,'@Action',$action,SQLINT4,false,false,1);
		$result = @mssql_execute($query);
		mssql_free_statement($query);
		if($result==1)
			echo "<p class='mesg'>School has been Deleted</p>";
		else {
			$errmsg1=mssql_get_last_message();
			
			$errflag=2;
			}
	}
	if($mode1 == "ADD")
	{	$action=1;
	
        $school_name	=	Trim($_POST['school_name1']);
		$short_name		=	Trim($_POST['short_name1']);
		$location		=	Trim($_POST['location1']);
		$address	    =	Trim($_POST['address1']);
		$state_code		=	Trim($_POST['state_code1']);
		$city_code		=	Trim($_POST['city_code']);
			//echo "=".$city_code;
		$principal_code	=	Trim($_POST['Principal_Code']);
		$contact_no1	=	Trim($_POST['contact_no11']);
		$contact_no2     =	Trim($_POST['contact_no21']);
		$email	        =	Trim($_POST['email1']);
		$active		    =	Trim($_POST['active1']);
		

	         //error message//
				 		

		$dummy = Strcheck($school_name,$errmsg,$errflag,"School");
		//$dummy = CheckEmail($email,$errmsg,$errflag,"Email ID");
		$dummy = Strcheck($short_name,$errmsg,$errflag,"Short Name");
		$dummy = Strcheck($location,$errmsg,$errflag,"Location");
		$dummy = Strcheck($principal_code,$errmsg,$errflag,"Principal Code");
		if($errflag==0){

			$query = mssql_init('sp_School',$mssql);
		    mssql_bind($query,'@School_Id',$school_id,SQLINT4,false,false,5);
			mssql_bind($query,'@School_Name',$school_name,SQLVARCHAR,false,false,50);
			mssql_bind($query,'@Short_Name',$short_name,SQLVARCHAR,false,false,25);
			mssql_bind($query,'@Location',$location,SQLVARCHAR,false,false,50);
			mssql_bind($query,'@Address',$address,SQLVARCHAR,false,false,50);
			mssql_bind($query,'@State_Code',$state_code,SQLINT4,false,false,5);
			mssql_bind($query,'@City_Code',$city_code,SQLINT4,false,false,5);
		
			mssql_bind($query,'@Principal_Code',$principal_code,SQLINT4,false,false,5);
			mssql_bind($query,'@Contact_No1',$contact_no1,SQLVARCHAR,false,false,5);
			mssql_bind($query,'@Contact_No2',$contact_no2,SQLVARCHAR,false,false,5);
			mssql_bind($query,'@Email_Id',$email,SQLVARCHAR,false,false,5);
			mssql_bind($query,'@Active',$active,SQLVARCHAR,false,false,1);
			mssql_bind($query,'@Action',$action,SQLINT4,false,false,1);
			
			$result = @mssql_execute($query);
			mssql_free_statement($query);
		if($result==1)
			echo "<p class='mesg'>School has been Added</p>";
		else {
			$errmsg1=mssql_get_last_message();
			$errflag=2;
			$inserr=1;
			}
		} else $inserr=1;
	}
	if($errflag==1) 
		echo "<p class='error'>Incomplete / Invalid entried for<br>".$errmsg;
	if($errflag==2) 
		echo "<p class='error'>".$errmsg1;
	
?>
<body style="margin:0;">

<form name="myform" id="myform" method="post" action="Mas_Schooll.php">
<input type="hidden" name="editcnt" id="editcnt"/>
<input type="hidden" name="school_id" id="school_id"/>
<input type="hidden" name="mode" id="mode"/>
<input type="hidden" name="mode1" id="mode1"/>


<table width="75%" height="450" border="0" align="center" cellpadding="3" cellspacing="3">
<tr><td valign="top">
<table width="100%" border="0" align="center" cellpadding="3" cellspacing="1">
<colgroup><col width=3%><col width=3%><col width=20%><col width=20%><col width=10%><col width=5%><col width=5%></colgroup>
<tr align="center">
<thead>
<th>&nbsp;</th>
<th>&nbsp;</th>
<th align="center">School Name</th>
<th>Short Name</th>
<th>Location</th>
<th>Address</th>
<th>State Name</th>
<th>City Name</th>
<th>Principal Code</th>
<th>Contact No1</th>
<th>Contact No2</th>
<th>Email Id</th>
<th>Active</th>
</thead></tr>


<?php   // New Record Insert 	 


     $colorflag+=1; ?>
	<tr class=<?php if($colorflag%2==0) { echo "row2"; } else { echo "row1"; } ?> valign="center">
	
	
	<td align="center" ><img src="../images/new.gif" title="Add New" onclick="adddata();"/></td><td>&nbsp;</td>
	
	<td align="center"><div id ="school_name1" style="display:none;">
    <input type="text" name="school_name1" id="school_name1"  size="20" maxlength="50" value="<?php echo $school_name1 ?>" onkeydown="return alphaonly('school_name1')" ><span class="mand"> *</span></div></td>

	 <td align="center"><div id ="short_name1" style="display:none;"><input type="text" name="short_name1" id="short_name1"  size="5" maxlength="5" onkeydown="return alphaonly('short_name1')" value="<?php echo $short_name; ?>"><span class="mand">*</span></div></td>

	<td align="center"><div id ="location1" style="display:none;"><input type="text" name="location1" id="location1"  size="15" maxlength="15" onkeydown="return alphaonly('location1')" value="<?php echo $location; ?>"><span class="mand">*</span></div></td>

	<td align="center"><div id ="address1" style="display:none;"><input type="text" name="address1" id="address1"  size="25" maxlength="25" value="<?php echo $address1; ?>"><span class="mand"> *</span></div></td>


	   <?php 	//SHOW State DROPDOWN
		$query = mssql_init('sp_GetState',$mssql);
		$result = mssql_execute($query);
		mssql_free_statement($query);	?> 
        <td align="center">
        <div id ="state_code1" style="display:none;">           
        	   <select name="state_code1" id="state_code1" onchange="getCity(this.value)">
         <option>Select State</option>
         <?php	while($field = mssql_fetch_array($result)) {  ?>
         <option value="<?php echo $field['State_Code']?>"><?php echo $field['State']?></option>
         <?php } ?>
       </select></td>


	 <td ><div id="citydiv" style="display:none;">
	   <select name="city">
         <option>Select City</option>
       </select>
	 </div></td>
<?php
	$query = mssql_init('sp_getteacher',$mssql);
		$result = mssql_execute($query);
		mssql_free_statement($query);	?>
        
    <td align="center"><div id ="Principal_Code" style="display:none;">
   <select id="Principal_Code" name="Principal_Code">
   <?php while($field = mssql_fetch_array($result)) 
   {  ?>	<option value="<?php echo $field['Teacher_Code']?>">
			<?php echo $field['Teacher_Name']?></option>
	 <?php } ?>
     </select></div></td>
   
    
    <td align="center"><div id ="contact_no11" style="display:none;"><input type="text" name="contact_no11" id="contact_no11"  size="15" maxlength="15" value="<?php echo $contact_no11; ?>" onkeydown="return phonecode('contact_no11')"><span class="mand"> *</span></div></td>
    
    <td align="center"><div id ="contact_no21" style="display:none;"><input type="text" name="contact_no21" id="contact_no21"  size="15" maxlength="15" value="<?php echo $contact_no21; ?>" onkeydown="return phonecode('contact_no21')"><span class="mand"> *</span></div></td>
    
    <td align="center"><div id ="email1" style="display:none;"><input type="text" name="email1" id="email1"  size="25" maxlength="25" value="<?php echo $email; ?>"><span class="mand"> *</span></div></td>
    
	<td align="center" valign="top"><div id ="active1" style="display:none;"><select name="active1" id="active1"><option size="5" maxlength="10" value="Y">Yes</option><option value="N">No</option></select>	</div></td>






 <?php 	// UPDATE & SHOW RECORDS
		$query = mssql_init('sp_GetSMSschool',$mssql);
		$result = mssql_execute($query);
		mssql_free_statement($query);
		$rs_cnt = mssql_num_rows($result);
		$colorflag = 0;
		$i = 0;
		while($field = mssql_fetch_array($result))
		{	$i +=1;	$colorflag+=1;
			$tot_rec = $i;	?>
		<tr class=<?php if($colorflag%2==0) { echo "row1"; } else { echo "row2"; } ?> valign="center">

		<input type="hidden" name="school_id<?php echo $i ?>" id ="school_id<?php echo $i ?>" value="<?php echo $field['School_Id']?>"	 />
		
		<td align="center"><img src="../images/delete_d.gif" title="Remove" onClick="deldata('<?php echo $field['School_Id'];?>');" />&nbsp;</td>	 

		<td align="center"><img src="../images/edit.gif" title="Edit" onClick="editdata('<?php echo $i;?>',<?php echo $field['State_Code']; ?>,<?php echo $field['City_Code']; ?>);" />&nbsp;</td>

		<td ><?php echo $field['School_Name'] ?>
				<div id ="school_name_<?php echo $i;?>" style="display:none;"><input type="text" name="school_name_<?php echo $i ?>" id="school_name_<?php echo $i ?>"  size="15" maxlength="25" value="<?php echo $field['School_Name']?>" onkeydown="return alphaonly('school_name_<?php echo $i ?>')"></div></td>
				
                
                <td align="center"><?php echo $field['Short_Name'] ?>
				<div id ="short_name_<?php echo $i;?>" style="display:none;">
				<input type="text" name="short_name_<?php echo $i ?>" id="short_name_<?php echo $i ?>"  size="5" maxlength="5" value="<?php echo $field['Short_Name']?>" onkeydown="return alphaonly('short_name_<?php echo $i ?>')"></div></td>


				<td align="center"><?php echo $field['Location'] ?>
				<div id ="location_<?php echo $i;?>" style="display:none;"><input type="text" name="location_<?php echo $i ?>" id="location_<?php echo $i ?>"  size="15" maxlength="15" value="<?php echo $field['Location']?>" onkeydown="return alphaonly('location_<?php echo $i ?>')"></div></td>
                
               <td align="center"><?php echo $field['Address'] ?>
				<div id ="address_<?php echo $i;?>" style="display:none;"><input type="text" name="address_<?php echo $i ?>" id="location_<?php echo $i ?>"  size="15" maxlength="25" value="<?php echo $field['Address']; ?>"onkeydown="return alphaonly('location_<?php echo $i ?>')"></div></td>
                

<!-- 			    <td align="center"><?php echo $field['Address'] ?>
				<div id ="address_<?php echo $i;?>" style="display:none;">
				<input type="textarea" name="address_<?php echo$i ?>"id location_<?php echo $i ?>" cols="5" rows="5"> > value="<?php echo $field['Address']; ?>"onkeydown="return alphaonly('location_<?php echo $i ?>')"></textarea></div></td>
 -->
                
                
	<?php 	//SHOW STATE DROPDOWN
		$query = mssql_init('sp_GetState',$mssql);
		$result1 = mssql_execute($query);
		mssql_free_statement($query);	?>
	<td align="center"><?php echo $field['State'] ?>    
	<div id="statecode<?php echo $i ?>" style="display:NONE;">
    
	      <select name="state_code<?php echo $i ?>" id="state_code<?php echo $i ?>" onchange="getCity_Edit('<?php echo $i;?>',this.value)" >
<?php	while($field1 = mssql_fetch_array($result1)) {  ?>

			<option value="<?php echo $field1['State_Code']?>" <?php if($field['State_Code']==$field1['State_Code']) echo "Selected";?>><?php echo $field1['State']?></option>
	 <?php } ?>
	 </select></div></td>
     
    <td align="center"><?php echo $field['City']  ?>      
    <div id="citydiv<?php echo $i ?>" 
    style="display:none;">
	   <select name="city_code_<?php echo $i;?>" >
         <option>Select City </option>
       </select>
	 </div></td>
     <?PHP
$query = mssql_init('sp_getteacher',$mssql);
		$result1 = mssql_execute($query);
		mssql_free_statement($query);	?>

<td align="center"><?php echo $field['Teacher_Name'] ?>
				<div id ="principal_code_<?php echo $i ?>" style="display:none;">               
                 <select name="principal_code_<?php echo $i ?>" id="principal_code_<?php echo $i ?>">
<?php	while($field1 = mssql_fetch_array($result1)) {  ?>
			<option value="<?php echo $field1['Teacher_Code']?>" <?php if($field['Principal_Code']==$field1['Teacher_Code']) echo "Selected";?>><?php echo $field1['Teacher_Name']?></option>
	 <?php } ?>
	 </select></div></td>
                
           <td align="center"><?php echo $field['Contact_No1'] ?>
				<div id ="contact_no1_<?php echo $i;?>" style="display:none;"><input type="text" name="contact_no1_<?php echo $i ?>" id="contact_no1_<?php echo $i ?>"  size="12" maxlength="12" value="<?php echo $field['Contact_No1']; ?>"  onkeydown="return phonecode('contact_no1_<?php echo $i ?>')"></div></td>

               
           <td align="center"><?php echo $field['Contact_No2'] ?>
				<div id ="contact_no2_<?php echo $i;?>" style="display:none;"><input type="text" name="contact_no2_<?php echo $i ?>" id="contact_no2_<?php echo $i ?>"  size="12" maxlength="12" value="<?php echo $field['Contact_No2']; ?> "  onkeydown="return phonecode('contact_no2_<?php echo $i ?>')"></div></td>
                
                      
           <td align="center"><?php echo $field['Email_Id'] ?>
				<div id ="email_<?php echo $i;?>" style="display:none;"><input type="text" name="email_<?php echo $i ?>" id="email_<?php echo $i ?>"  size="50" maxlength="25" value="<?php echo $field['Email_Id']; ?>"></div></td>


								
		<td align="center"><?php if(strtoupper($field['Active'])=="Y") echo "Yes"; else echo "No";?>
		<div id ="active_<?php echo $i;?>" style="display:none;">
			<select id="active_<?php echo $i ?>" name="active_<?php echo $i ?>">
            
		<option size="5" maxlength="5" value="Y" <?php if($active=="Y")  echo "selected" ?>>Yes</option>
		<option size="5" maxlength="5" value="N" <?php if($active=="N")  echo "selected" ?>>No</option>
		</select></div></td>
		</tr> 
	<?php } 
	if ($errcnt > 0) {
		for($i=0;$i<=$errcnt-1;$i=$i+1){
		$editcnt = split(',',$_POST['editcnt']);?>
			<script>editdata(<?php echo $editcnt[$i];?>);</script>
		<?php } 
		} ?>	
		<input type="hidden" name="tot_rec" value="<?php echo $tot_rec ?>" />
	<tr><td colspan="13" align="right">
		<input type="submit" name="save" value="Save" class="winbutton_go" />&nbsp;&nbsp;&nbsp;
		<input type="submit" name="Cancel" value="Cancel" class="winbutton_go"/></td></tr>

<?php	if($inserr == 1) { ?><script>adddata();</script><?php } ?>
</table></td></tr></table>
</form></body></html>
<?php include("../Includes/copyright.php"); ?>

