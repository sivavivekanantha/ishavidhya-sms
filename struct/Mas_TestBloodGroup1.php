<?php session_start(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>Isha Foundation - A Non-profit Organization</title>
<?php include("../includes/header.php");
 title('Org Structure','BloodGroup',1,0,1);
?>

<script>

// display all the text box fields for add religion
function adddata()
 { 
 	$('#bloodgroup_name').show();
	$('#active').show();
	$('#displayorder').show();
	var $mode1 ='ADD';
	$('#mode1').val($mode1);
} 

// display all the text box fields for update religion
function editdata(val)
{
	if($('#editcnt').val() >0 ) return false;
    $('#editcnt').val(val);

	$('#bloodgroup_name'+val).show();
	$('#active'+val).show();
	$('#displayorder'+val).show();	
	var $mode ='EDIT';
	$('#mode').val($mode);

}

// delete the religion
function deldata(val)
{ 
	var msg="Do you want to delete?";
	if(!confirm(msg)){
		return false;
	}	 
	else {
		$('#bloodgroup_code').val(val);
		var $mode ='DELETE';
		$('#mode').val($mode);
		$('#myform').submit();
	 }
}
</script>
</head>

<?php 
			$errmsg="";
			$errflag=0;
			$dummy=0;
			$mode		=	$_POST['mode'];
			$mode1		=	$_POST['mode1'];
			$editcnt 	=	split(',',$_POST['editcnt']);		
		
			if($_POST['Cancel']=="Cancel") 
			{ 
				$mode=""; 
				$mode1="";	
			}
			if($mode == "EDIT")
			{
			 		$action=2;
					
					for($i=0;$i<count($editcnt);$i=$i+1) 
					{ 
							$bloodgroup_code	=	Trim($_POST['bloodgroup_code'.$editcnt[$i]]);
                            $bloodgroup_name	=	Trim($_POST['bloodgroup_name'.$editcnt[$i]]);
                            $active		    	=	$_POST['active'.$editcnt[$i]];
                            $displayorder		=	Trim($_POST['displayorder'.$editcnt[$i]]);
                           
							$j=$i+1;
							
							//error message
							$dummy = Strcheck($bloodgroup_name,$errmsg,$errflag,"Blood Group");
							$dummy = Numcheck($displayorder,$errmsg,$errflag,"Display Order");
						
							if($errflag==0)
							{	
									$query = mssql_init('sp_BloodGroup',$mssql);
									mssql_bind($query,'@BloodGroup_Code',$bloodgroup_code,SQLINT4,false,false,5);
									mssql_bind($query,'@BloodGroup_Name',$bloodgroup_name,SQLVARCHAR,false,false,50);
									mssql_bind($query,'@Active',$active,SQLVARCHAR,false,false,1);
									mssql_bind($query,'@Display_Order',$displayorder,SQLINT4,false,false,5);
									mssql_bind($query,'@Action',$action,SQLINT4,false,false,1);
									$result = @mssql_execute($query);
									mssql_free_statement($query);
									
									if($result==1)
									{
										echo "<p class='mesg'>Blood Group has been Updated</p>";
									}
									else 
									{
										$errmsg1=mssql_get_last_message();
										$errflag=2;
									}
							}
							else
							{
									if ($errcnt == 0) 
										$errcnt = 1;
									else 									 
										$errcnt = $errcnt + 1; 
									
									if ($errval == "") 
										$errval = $editcnt[$i];	 
									else 
										$errval = $errval.",".$editcnt[$i]; 
									 
							}
					
					}//end for
					
						
			}//end if
			
			//delete the Blood Group		
			if($mode=="DELETE")
			{	
				
				$action=3;
				$bloodgroup_code	=	$_POST['bloodgroup_code'];
		        
                $query = mssql_init('sp_BloodGroup',$mssql);
				
				mssql_bind($query,'@BloodGroup_Code',$bloodgroup_code,SQLINT4,false,false,5);
				mssql_bind($query,'@BloodGroup_Name',$bloodgroup_name,SQLVARCHAR,false,false,50);
				mssql_bind($query,'@Active',$active,SQLVARCHAR,false,false,1);
				mssql_bind($query,'@Display_Order',$displayorder,SQLINT4,false,false,5);
				mssql_bind($query,'@Action',$action,SQLINT4,false,false,1);
				$result = @mssql_execute($query);
				mssql_free_statement($query);
				if($result==1)
				{
					echo "<p class='mesg'>Blood Group has been Deleted</p>";
				}
				else 
				{
					$errmsg1=mssql_get_last_message();
					$errflag=2;
				}
				
			}
			
			//add the Blood Group
			if($mode1 == "ADD")
			{	
				$action=1;
				$bloodgroup_name	=	Trim($_POST['bloodgroup_name']);			
				$active			=	$_POST['active'];
				$displayorder		=	Trim($_POST['displayorder']);
				
				//error messge//
				$dummy = Strcheck($bloodgroup_name,$errmsg,$errflag,"Blood Group");
				$dummy = Numcheck($displayorder,$errmsg,$errflag,"Display Order");
				if($errflag==0)
				{
		
					$query = mssql_init('sp_BloodGroup',$mssql);
					mssql_bind($query,'@BloodGroup_Code',$bloodgroup_code,SQLINT4,false,false,5);
					mssql_bind($query,'@BloodGroup_Name',$bloodgroup_name,SQLVARCHAR,false,false,100);
					mssql_bind($query,'@Active',$active,SQLVARCHAR,false,false,1);
					mssql_bind($query,'@Display_Order',$displayorder,SQLINT4,false,false,5);
					mssql_bind($query,'@Action',$action,SQLINT4,false,false,1);					
					$result = @mssql_execute($query);
					mssql_free_statement($query);
					
						if($result==1) 
						{
							echo "<p class='mesg'>Blood Group has been Added</p>";
							$bloodgroup_name=$active=$displayorder="";
							
						}
						else
						{
							$errmsg1=mssql_get_last_message();
							$errflag=2;
							$inserr=1;
						}
				}  
				else $inserr=1;
			}
			if($errflag==1) 
			{
				echo $errlbl.$errmsg."</p>";
			}
			if($errflag==2) 
			{
				echo "<p class='error'>".$errmsg1;
			}
			
?>
<body style="margin:0;">

        <form name="myform" id="myform" method="post" action="mas_testbloodgroup1.php">
        <input type="hidden" name="editcnt" id="editcnt"/>
        <input type="hidden" name="bloodgroup_code" id="bloodgroup_code"/>
        <input type="hidden" name="mode" id="mode"/>	
        <input type="hidden" name="mode1" id="mode1"/>

        <table width="70%" height="450" border="0" align="center" cellpadding="3" cellspacing="3">
        <tr><td valign="top">
                <table width="100%" border="0" align="center" cellpadding="3" cellspacing="1">
                <?php titleheader(BloodGroup,0); ?>
                <tr align="center">
                <thead><colgroup><col width=3%><col width=3%><col width=10%><col width=10%><col width=10%></colgroup>
                <th>&nbsp;</th><th>&nbsp;</th><th align="center">Blood Group</th>
                <th>Active</th><th>Display Order</th>
                </thead></tr>
        
      		    <?php   // New Record Insert
                $colorflag+=1; ?>
           		<tr class=<?php if($colorflag%2==0) { echo "row2"; } else { echo "row1"; } ?> valign="center">            
            	<td align="center" ><img src="../images/new.gif" title="Add New" onclick="adddata();"/></td><td>&nbsp;</td>            
            	<td align="center"><div id ="bloodgroup_name" style="display:none;"><input type="text" name="bloodgroup_name" id="bloodgroup_name" 
                 size="25" maxlength="25"  value="<?php echo $bloodgroup_name ?>"><?php echo $mand; ?></div></td>                
            	<td align="center"><div id ="active" style="display:none;"><select name="active" id="active"><option value="Y">Yes</option><option value="N">No</option></select></div></td>           
            	<td align="center"><div id ="displayorder" style="display:none;"><input type="text" name="displayorder" id="displayorder" 
                 size="5" maxlength="5" value="<?php echo $displayorder ?>" onkeydown="return numberonly('displayorder')"><?php echo $mand; ?></div></td>
        
				<?php 	// UPDATE & SHOW RECORDS
                        $query = mssql_init('sp_Getbloodgroup',$mssql);
                        $result = mssql_execute($query);
                mssql_free_statement($query);
                $rs_cnt = mssql_num_rows($result);
                $colorflag = 0;
                $i = 0;
                while($field = mssql_fetch_array($result))
                {	$i  +=1;	$colorflag+=1;
                    $tot_rec = $i;	?>
                    
                    <tr class=<?php if($colorflag%2==0) { echo "row1"; } else { echo "row2"; } ?> valign="center">
                    <input type="hidden" name="bloodgroup_code<?php echo $i ?>" id ="bloodgroup_code<?php echo $i ?>" value="<?php echo $field['BloodGroup_Code']?>"/>		
                    <td align="center"><img src="../images/delete_d.gif" title="Remove" onclick="deldata('<?php echo $field['BloodGroup_Code'];?>');" />&nbsp;</td>	
                    <td align="center"><img src="../images/edit.gif" title="Edit" onclick="editdata('<?php echo $i;?>');" />&nbsp;</td>
                    <td ><?php echo $field['BloodGroup_Name'] ?>
                    <div id ="bloodgroup_name<?php echo $i;?>" style="display:none;"><input type="text"
                     name="bloodgroup_name<?php echo $i ?>" id="bloodgroup_name<?php echo $i ?>"  size="25" maxlength="25" value="<?php echo $field['BloodGroup_Name']?>"></div></td>								
                    <td align="center"><?php if(strtoupper($field['Active'])=="Y") echo "Yes"; else echo "No";?>
                    <div id ="active<?php echo $i;?>" style="display:none;">
                    <select id="active<?php echo $i ?>" name="active<?php echo $i ?>">
                    <option value="Y" <?php if(strtoupper($field['Active'])=="Y")  echo "selected" ?>>Yes</option>
                    <option value="N" <?php if(strtoupper($field['Active'])=="N")  echo "selected" ?>>No</option>
                    </select></div></td>
                    <td align="center"><?php echo $field['Display_Order'] ?>
                    <div id ="displayorder<?php echo $i;?>" style="display:none;"><input type="text" name="displayorder<?php echo $i ?>" id="displayorder<?php echo $i ?>" 
                     size="5" maxlength="5" value="<?php echo $field['Display_Order']?>" onkeydown="return numberonly('displayorder<?php echo $i ?>')"></div></td>
                    </tr>
                <?php 
                } 
                if ($errcnt > 0) 
                {
                    for($i=0;$i<=$errcnt-1;$i=$i+1){
                    $editcnt = split(',',$_POST['editcnt']);?>
                    <script>editdata(<?php echo $editcnt[$i];?>);</script>
                <?php 
                } 
			} ?>	
                <input type="hidden" name="tot_rec" value="<?php echo $tot_rec ?>" />
                <tr><td colspan="5" align="right">
                <input type="submit" name="save" value="Save" class="winbutton_go" />&nbsp;&nbsp;&nbsp;
                <input type="submit" name="Cancel" value="Cancel" class="winbutton_go"/></td></tr>
				<?php	if($inserr == 1) { ?><script>adddata();</script><?php } ?>
                </table>
                </td>
                </tr></table>
        </form>
 </body></html>
<?php include("../includes/copyright.php"); ?>\