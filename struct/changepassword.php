<?php
session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<?php
if($_GET['frm'] == 1) include ("../includes/header.php");
else  include ("../includes/header.php");
?>
<head>
<?php
if($_POST['Save'] == "Change My Password") {
    $errmsg1 = 0;
    $Currentpwd = $_POST['Currentpwd'];
    $Newpwd = $_POST['Newpwd'];
    $confirmpwd = $_POST["confirmpwd"];
    $Userid = $_SESSION["UserID"];
    $UserName = $_SESSION["UserName"];
    $dummy = Strcheck($Currentpwd, $errmsg, $errflag, "Current Password");
    $dummy = Strcheck($Newpwd, $errmsg, $errflag, "New Password");
    $dummy = Strcheck($confirmpwd, $errmsg, $errflag, "Confirm Password");
    if($errflag == 0) {
        if($Newpwd == $Currentpwd) {
            echo "<p class='error'>Choose a password you haven't previously used with this account.</p>";
            $Newpwd = "";
            $Currentpwd = $confirmpwd = "";
            $errmsg1 = 1;
        } elseif($Newpwd != $confirmpwd) {
            echo "<p class='error'>New Password & Confirm New Password do not match.</p>";
            $Newpwd = "";
            $Currentpwd = $confirmpwd = "";
            $errmsg1 = 1;
        }
        if($errmsg1 == 0) {
			
			mssql_free_result($result);
            $query = @mssql_init('Sp_Changepassword_Save', $mssql);
            mssql_bind($query, '@Currentpwd', $Currentpwd, SQLVARCHAR, false, false, 50);
            mssql_bind($query, '@Newpwd', $Newpwd, SQLVARCHAR, false, false, 50);
            mssql_bind($query, '@Userid', $Userid, SQLINT4, false, false, 5);
            mssql_bind($query, '@Username', $UserName, SQLVARCHAR, false, false, 50);
            $result = @mssql_execute($query);
            mssql_free_statement($query);
            if($result == 1) {
                echo "<p class='mesg'>Password  has been Changed Successfully</p>";
                $Currentpwd = $Newpwd = $confirmpwd = "";
                $_SESSION["Pwd"] = 0;
                echo "<p align='center'><a href='../main/index.php'>Go to Home</a></p>";
                exit();
            } else {
                $errmsg1 = mssql_get_last_message();
                echo "<p class='error'>" . $errmsg1 . " </p>";
            }
        }
    } else {
        echo $errlbl . $errmsg;
        $Currentpwd = $Newpwd = $confirmpwd = "";
    }
}
?>

<form id="Changepassword" name="Changepassword" action="changepassword.php" method="POST">
<table width="100%"><tr align="center"><td>
<p>&nbsp;</p>
<table>
<tr><td colspan="2" ><img src='../images/arrow_skip.png' width='16' height='16' />&nbsp;&nbsp;<span class='view_tit_text1'>Change Password</span></td></tr>
<tr>
<td>
Current Password
</td>
<td><input type="password" name="Currentpwd" autocomplete="off" maxlength="20" /><td>
</tr>
<tr>
<td>
New Password 
</td>
<td>
<input type="password" name="Newpwd" autocomplete="off" maxlength="20"  />
</td>
</tr>
<tr>
<td>
Confirm New Password
</td>
<td>
<input type="password" name="confirmpwd" maxlength="20" autocomplete="off"  />
</td>
</tr>
<tr>
<td colspan="2" align="right"><input type="submit" class = "winbutton_go" name="Save"  value="Change My Password"/></td>
</tr>
</table>
</td></tr> </table>
</form>
<?php
include ("../includes/copyright.php");
?>
</body>
</html>
