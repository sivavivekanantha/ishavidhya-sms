<?php session_start(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php include("../includes/header.php"); 
	title('Org Structure','Category',1,1,0); 	
?>
<script>

function adddata()
 { 
 	$('#category_name1').show();
	$('#active1').show();
	$('#disporder1').show();
	$('#mode1').val('ADD');
} 


function editdata(val)
{ 
	if($('#editcnt').val() >0 ) return false;
    $('#editcnt').val(val);

	$('#category_name_'+val).show();
	$('#active_'+val).show();
	$('#disporder_'+val).show();	
	var $mode ='EDIT';
	$('#mode').val($mode);

}
function deldata(val)
{ 
	var msg="Do you want to delete?";
	if(!confirm(msg)){
		return false;
	}	 
	else {
		$('#category_code').val(val);
		var $mode ='DELETE';
		$('#mode').val($mode);
		$('#myform').submit();
	 }
}
</script>
</head>

<?php 
	$errmsg="";
	$errflag=0;
	$dummy=0;
	$mode		=	$_POST['mode'];
	$mode1		=	$_POST['mode1'];
	$editcnt 	=	split(',',$_POST['editcnt']);		

	if($_POST['Cancel']=="Cancel") { $mode=""; $mode1="";	}
	
	If($mode == "EDIT")
	{ $action=2;

	for($i=0;$i<count($editcnt);$i++) 
	{ 	
		$category_code	=	$_POST['category_code_'.$editcnt[$i]];
		$category	=	$_POST['category_name_'.$editcnt[$i]];
		$active			=	$_POST['active_'.$editcnt[$i]];
		$disporder		=	$_POST['disporder_'.$editcnt[$i]];
		$j=$i+1;
		$dummy = Strcheck($category,$errmsg,$errflag,"Category-".$j);
		
		$dummy = Strcheck($disporder,$errmsg,$errflag,"Display Order-".$j);
		if($errflag==0)
		{   
		    mssql_free_result($result);
			$query = mssql_init('sp_Category',$mssql);
			mssql_bind($query,'@Category_Code',$category_code,SQLINT4,false,false,5);
			mssql_bind($query,'@Category_Name',$category,SQLVARCHAR,false,false,50);
			mssql_bind($query,'@Active',$active,SQLVARCHAR,false,false,1);
			mssql_bind($query,'@Disp_Order',$disporder,SQLINT4,false,false,5);
			mssql_bind($query,'@Action',$action,SQLINT4,false,false,1);
			$result = @mssql_execute($query);
			mssql_free_statement($query);
			if($result==1)
				echo "<p class='mesg'>Category has been Updated</p>";
			else {
				$errmsg1=mssql_get_last_message();
				$errflag=2;
			If ($errcnt == 0) { $errcnt = 1;} else { $errcnt = $errcnt + 1; }
			if ($errval == "") { $errval = $editcnt[$i];	 } else { $errval = $errval.",".$editcnt[$i]; }
			}
		}
		else {
			If ($errcnt == 0) { $errcnt = 1;} else { $errcnt = $errcnt + 1; }
			if ($errval == "") { $errval = $editcnt[$i];	 } else { $errval = $errval.",".$editcnt[$i]; } 
		}
	}	
	}

	if($mode=="DELETE")
	{	$action=3;
		$category_code	=	$_POST['category_code'];
        
            mssql_free_result($result);
		    $query = mssql_init('sp_Category',$mssql);
		    mssql_bind($query,'@Category_Code',$category_code,SQLINT4,false,false,5);
			mssql_bind($query,'@Category_Name',$category,SQLVARCHAR,false,false,50);
			mssql_bind($query,'@Active',$active,SQLVARCHAR,false,false,1);
			mssql_bind($query,'@Disp_Order',$disporder,SQLINT4,false,false,5);
			mssql_bind($query,'@Action',$action,SQLINT4,false,false,1);
		$result = @mssql_execute($query);
		mssql_free_statement($query);
		if($result==1)
			echo "<p class='mesg'>Category has been Deleted</p>";
		else {
			$errmsg1=mssql_get_last_message();
			$errflag=2;
			}
	}
	if($mode1 == "ADD")
	{	$action=1;   
	//Get Values From Form
		$category1	=	Trim($_POST['category_name1']);
		$active1			=	$_POST['active1'];
		$disporder1		=	Trim($_POST['disporder1']);

//Validate the Inputs
		$dummy = Strcheck($category1,$errmsg,$errflag,"Category");
		$dummy = Strcheck($disporder1,$errmsg,$errflag,"Display Order");
		
		if($errflag==0){
            
            mssql_free_result($result);
			$query = mssql_init('sp_Category',$mssql);
			mssql_bind($query,'@Category_Code',$category_code,SQLINT4,false,false,5);
			mssql_bind($query,'@Category_Name',$category1,SQLVARCHAR,false,false,50);
			mssql_bind($query,'@Active',$active1,SQLVARCHAR,false,false,1);
			mssql_bind($query,'@Disp_Order',$disporder1,SQLINT4,false,false,5);
			mssql_bind($query,'@Action',$action,SQLINT4,false,false,1);
			
			$result = @mssql_execute($query);
			mssql_free_statement($query);
		if($result==1) {
			echo "<p class='mesg'>Category has been Added</p>";
            $category1 = $active1=$disporder1="";
            }
		else {
			$errmsg1=mssql_get_last_message();
			$errflag=2;
			$inserr=1;
			}
		} else $inserr=1;
	}
	if($errflag==1) 
		echo $errlbl.$errmsg;
	if($errflag==2) 
		echo "<p class='error'>".$errmsg1;
	
?>
<body style="margin:0;">

<form name="myform" id="myform" method="post" action="mas_category.php">
<input type="hidden" name="editcnt" id="editcnt"/>
<input type="hidden" name="category_code" id="category_code"/>
<input type="hidden" name="mode" id="mode"/>	
<input type="hidden" name="mode1" id="mode1"/>
<table width="70%" height="450" border="0" align="center" cellpadding="3" cellspacing="3">
<tr><td valign="top">
<table width="100%" border="0" align="center" cellpadding="3" cellspacing="1">
<?php titleheader (Category,0);?>
<thead><colgroup><col width=3%><col width=3%><col width=20%><col width=10%><col width=10%><col width=5%><col width=5%></colgroup>
<th>&nbsp;</th><th>&nbsp;</th>
<th>Category</th>
<th>Active</th><th>Display Order</th>
</thead></tr>

<?php   // New Record Insert	?>
	<tr class="row1" valign="center">
	
	<td align="center" ><img src="../images/new.gif" title="Add New" onclick="adddata();"/></td><td>&nbsp;</td>
	
	<td align="center"><div id ="category_name1" style="display:none;">
	<input type="text" name="category_name1" id="category_name1"  size="25" maxlength="35" onkeydown="return alphaonly('category_name1')" value="<?php echo $category1 ?>">
	<?php echo $mand; ?></div></td>

	<td align="center"><div id ="active1" style="display:none;"><select name="active1" id="active1"><option value="Y">Yes</option><option value="N">No</option></select></div></td>
	
	<td align="center"><div id ="disporder1" style="display:none;"><input type="text" name="disporder1" id="disporder1"  size="5" maxlength="5" value="<?php echo $disporder1 ?>" onkeydown="return numberonly('disporder1')"><?php echo $mand; ?></div></td>

<?php 	// UPDATE & SHOW RECORDS
        
        mssql_free_result($result);
		$query = mssql_init('sp_GetCategory_Master',$mssql);
		$result = mssql_execute($query);
		mssql_free_statement($query);
		$rs_cnt = mssql_num_rows($result);
		$colorflag = 0;
		$i = 0;
		while($field = mssql_fetch_array($result))
		{	$i  +=1;	$colorflag+=1;
			$tot_rec = $i;	
			$category_code = $field['Category_Code'];
			$category = $field['Category'];
			$active = strtoupper($field['Active']);
			$disp_order = $field['Display_Order'];		?>
		<tr class=<?php if($colorflag%2==0) { echo "row1"; } else { echo "row2"; } ?> valign="center">

		<input type="hidden" name="category_code_<?php echo $i ?>" id ="category_code_<?php echo $i ?>" value="<?php echo $category_code?>"	 />
		
		<td align="center"><?php /*<img src="../images/delete_d.gif" title="Remove" onClick="deldata('<?php echo $field['Category_Code'];?>');" />*/ ?>&nbsp;</td>	  

		<td align="center"><?php /*<img src="../images/edit.gif" title="Edit" onClick="editdata('<?php echo $i;?>');" />*/ ?>&nbsp;</td>

		<td ><?php echo $field['Category'] ?>
				<div id ="category_name_<?php echo $i;?>" style="display:none;"><input type="text" name="category_name_<?php echo $i ?>" id="category_name_<?php echo $i ?>"  size="25" maxlength="35" value="<?php echo $category; ?>"  
                onkeydown="return alphaonly('category_name_<?php echo $i ?>')"></div></td>

		<td align="center"><?php if(strtoupper($field['Active'])==
			"Y") echo "Yes"; else echo "No";?>
		<div id ="active_<?php echo $i;?>" style="display:none;">
			<select id="active_<?php echo $i ?>" name="active_<?php echo $i ?>">
            
		<option value="Y" <?php if($active=="Y")  echo "selected" ?>>Yes</option>
		<option value="N" <?php if($active=="N")  echo "selected" ?>>No</option>
		</select></div></td>

		<td align="center"><?php echo $field['Display_Order'] ?>
		<div id ="disporder_<?php echo $i;?>" style="display:none;"><input type="text" name="disporder_<?php echo $i ?>" id="disporder_<?php echo $i ?>"  size="5" maxlength="5" value="<?php echo $disp_order?>" onkeydown="return numberonly('disporder_<?php echo $i ?>')"></div></td>
		</tr>
	<?php } 
	if ($errcnt > 0) {
		for($i=0;$i<=$errcnt-1;$i=$i+1){
		$editcnt = split(',',$_POST['editcnt']);?>
			<script>editdata(<?php echo $editcnt[$i];?>);</script>
		<?php } 
		} ?>	
		<input type="hidden" name="tot_rec" value="<?php echo $tot_rec ?>" />
	<tr><td colspan="5" align="right">
		<input type="submit" name="save" value="Save" class="winbutton_go" />&nbsp;&nbsp;&nbsp;
		<input type="submit" name="Cancel" value="Cancel" class="winbutton_go"/></td></tr>

<?php	if($inserr == 1) { ?><script>adddata();</script><?php } ?>
</table></td></tr></table>
</form></body></html>
<?php include("../Includes/copyright.php"); ?>
