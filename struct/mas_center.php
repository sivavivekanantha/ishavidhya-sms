<?php session_start(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Isha Foundation - A Non-profit Organization</title>


<?php include("../includes/header.php"); 
 title('Org Structure','Center',1,0,1);

?>

<script>
function adddata()
{
	$('#center_name1').show();
	$('#short_name1').show();
	$('#City1').show();
	$('#active1').show();
	$('#displayorder1').show();
	$('#mode1').val('ADD');
}


function editdata(val)
{ 
	if($('#editcnt').val() >0 ) return false;
    $('#editcnt').val(val);

	$('#center_name_'+val).show();
	$('#short_name_'+val).show();
	$('#city_name_'+val).show();
	$('#active_'+val).show();
	$('#displayorder_'+val).show();	
	$('#mode').val('EDIT');

}


function deldata(val)
{ 
	var msg="Do you want to delete?";
	if(!confirm(msg)){
		return false;
	}	 
	else {
		$('#center_code').val(val);
		$('#mode').val('DELETE');
		$('#myform').submit();
	 }
}
</script>

</head>

<?php 
	$errmsg="";
	$errflag=0;
	$dummy=0;
	$mode		=	$_POST['mode'];
	$mode1		=	$_POST['mode1'];
	$editcnt 	=	split(',',$_POST['editcnt']);	
	if($_POST['Cancel']=="Cancel") { $mode=""; $mode1="";	}	
	
	if($mode1 == "ADD")
	{	
		$action=1;
        
		$center_code =Trim($_POST['center_code1']);
		$center1  	 =Trim($_POST['center_name1']);
		$short_name1 =Trim($_POST['short_name1']);
		$City1 		 =$_POST['City1'];
		$active1	 =$_POST['active1'];
		$disporder1  =Trim($_POST['displayorder1']);
		
		
		
		//error messge//
		$dummy = Strcheck($center1,$errmsg,$errflag,"Center");
		$dummy = Strcheck($short_name1,$errmsg,$errflag,"Short Name");
		//$dummy = Strcheck($City1,$errmsg,$errflag,"City");
		//$dummy = Numcheck($phone_code1,$errmsg,$errflag,"Phone Code");
		$dummy = Strcheck($disporder1,$errmsg,$errflag,"Display Order");
		
		if($errflag==0)
		{
            mssql_free_result($result);
			$query = mssql_init('sp_Center',$mssql);
			mssql_bind($query,'@Center_Code',$center_code,SQLINT4,false,false,5);
			mssql_bind($query,'@Center',$center1,SQLVARCHAR,false,false,50);
			mssql_bind($query,'@Short_Name',$short_name1,SQLVARCHAR,false,false,25);
			mssql_bind($query,'@Display_Order',$disporder1,SQLINT4,false,false,5);
			mssql_bind($query,'@Active',$active1,SQLVARCHAR,false,false,1);
			mssql_bind($query,'@City_Code',$City1,SQLINT4,false,false,1);
			mssql_bind($query,'@Action',$action,SQLINT4,false,false,1);
			
			$result = @mssql_execute($query);
			
			mssql_free_statement($query);
		if($result==1) {
			echo "<p class='mesg'>Center has been Added</p>";
            $center_code=$center1=$short_name1=$City1=$active1=$disporder1="";
            }
		else {
			$errmsg1=mssql_get_last_message();
			$errflag=2;
			$inserr=1;
			}
		} else $inserr=1;
	}
	
	
	If($mode == "EDIT")
	{ $action=2;
	for($i=0;$i<count($editcnt);$i++) 
	{  
		
		$center_code    = 	Trim($_POST['center_code_'.$editcnt[$i]]);
		
		$center_name  	=	Trim($_POST['center_name_'.$editcnt[$i]]);
		$short_name	    =	Trim($_POST['short_name_'.$editcnt[$i]]);
		$City 		    =	$_POST['city_name_'.$editcnt[$i]];
		$active		    =	$_POST['active_'.$editcnt[$i]];
		$disporder	    =	Trim($_POST['displayorder_'.$editcnt[$i]]);
		
		$j=$i+1;
		

		$dummy = Strcheck($center_name,$errmsg,$errflag,"Center");
		$dummy = Strcheck($short_name,$errmsg,$errflag,"Short Name");
		$dummy = Strcheck($disporder,$errmsg,$errflag,"Display Order");
		
		if($errflag==0)
		{
		    mssql_free_result($result);
			$query = mssql_init('sp_Center',$mssql);
			mssql_bind($query,'@center_code',$center_code,SQLINT4,false,false,5);
			mssql_bind($query,'@Center',$center_name ,SQLVARCHAR,false,false,50);
			mssql_bind($query,'@Short_Name',$short_name,SQLVARCHAR,false,false,25);
			mssql_bind($query,'@Display_Order',$disporder,SQLINT4,false,false,5);
			mssql_bind($query,'@Active',$active,SQLVARCHAR,false,false,1);
			mssql_bind($query,'@City_Code',$City,SQLINT4,false,false,1);
			mssql_bind($query,'@Action',$action,SQLINT4,false,false,1);
			$result = @mssql_execute($query);
			mssql_free_statement($query);
			if($result==1)
				echo "<p class='mesg'>Center has been Updated</p>";
			else {
				$errmsg1=mssql_get_last_message();
				$errflag=2;
				If ($errcnt == 0) { $errcnt = 1;} else { $errcnt = $errcnt + 1; }
			if ($errval == "") { $errval = $editcnt[$i];	 } else { $errval = $errval.",".$editcnt[$i]; } 
			}
		}
		else {
			If ($errcnt == 0) { $errcnt = 1;} else { $errcnt = $errcnt + 1; }
			if ($errval == "") { $errval = $editcnt[$i];	 } else { $errval = $errval.",".$editcnt[$i]; } 
		}
	}	
	}
	
	if($mode=="DELETE")
	{	$action=3;
		$center_code= 	$_POST['center_code'];

		    mssql_free_result($result);
			$query = mssql_init('sp_Center',$mssql);
			mssql_bind($query,'@center_code',$center_code,SQLINT4,false,false,5);
			mssql_bind($query,'@Center',$center_name ,SQLVARCHAR,false,false,50);
			mssql_bind($query,'@Short_Name',$short_name,SQLVARCHAR,false,false,25);
			mssql_bind($query,'@Display_Order',$disporder,SQLINT4,false,false,5);
			mssql_bind($query,'@Active',$active,SQLVARCHAR,false,false,1);
			mssql_bind($query,'@City_Code',$City,SQLINT4,false,false,1);
			mssql_bind($query,'@Action',$action,SQLINT4,false,false,1);
			$result = @mssql_execute($query);
		mssql_free_statement($query);
		if($result==1)
			echo "<p class='mesg'>Center has been Deleted</p>";
		else {
			$errmsg1=mssql_get_last_message();
			$errflag=2;
			}
	}
	

			
	if($errflag==1) 
		echo $errlbl.$errmsg."</p>";
	if($errflag==2) 
		echo "<p class='error'>".$errmsg1."</p>";	
?>

<body style="margin:0;">

<form name="myform" id="myform" method="post" action="mas_center.php">
<input type="hidden" name="editcnt" id="editcnt"/>
<input type="hidden" name ="editval" id ="editval" />
<input type="hidden" name="center_code" id="center_code"/>
<input type="hidden" name="mode" id="mode"/>	
<input type="hidden" name="mode1" id="mode1"/>

<table width="100%" height="450" border="0" align="center" cellpadding="3" cellspacing="3">

<tr><td valign="top">
<table width="70%" border="0" align="center" cellpadding="3" cellspacing="1">
<?php titleheader(Center,0); ?>
<tr align="center">
<thead>
<colgroup>
<col width=3%>
<col width=1%>
<col width=13%>
<col width=8%>
<col width=15%>
<col width=3%>
<col width=3%>
</colgroup>
<th>&nbsp;</th>
<th>&nbsp;</th>
<th>Center</th>
<th>Short Name</th>
<th>City</th>
<th>Active</th>
<th>Display Order</th>
</thead></tr>

<?php   // New Record Insert
		$colorflag+=1; ?>
	<tr class=<?php if($colorflag%2==0) { echo "row2"; } else { echo "row1"; } ?> valign="center">
			
        
	<td align="center"><img src="../images/new.gif" title="Add New" onclick="adddata();"/>
    </td>
    <td>&nbsp;</td>
	
	<td align="center">
    	<div id ="center_name1" style="display:none;">
    	  <input type="text" name="center_name1" id="center_name1"  size="25" maxlength="50"  
   		  value="<?php echo $center1 ?>"  onkeydown="return alphaonly('center_name1')"><?php echo $mand; ?>
    	</div>
    </td>
	
	<td align="center">
    	<div id ="short_name1" style="display:none;">
   		  <input type="text" name="short_name1" id="short_name1"  size="15" maxlength="15" onkeydown="return alphaonly('short_name1')" 
    	  value="<?php echo $short_name1 ?>">
         <?php echo $mand; ?>
       </div>
    </td>
    
    <?php 	//SHOW CITY DROPDOWN
        
        mssql_free_result($result);
		$query = mssql_init('sp_FetchCity',$mssql);
		$result = mssql_execute($query);
		mssql_free_statement($query);	?>
        
    <td align="center"><div id ="City1" style="display:none;">
	
       <select name="City1"  id="City1" >
	   
       		 <?php echo $Select;
			 
			while($field = mssql_fetch_array($result)) 
      		 {  ?>
		<option value="<?php echo $field['City_Code']?>"<?php if($City1== $field['City_Code']) echo "selected" ?> >
			<?php 
			echo $field['City']?>
        </option>
	 		<?php } ?>
       </select><?php echo $mand; ?></div>
       </td>
		
	<td align="center">
       <div id ="active1" style="display:none;">
   		 <select name="active1" id="active1">
  		   <option value="Y">Yes</option>
   		   <option value="N">No</option>
         </select>
       </div><?php echo $mand; ?>
    </td>
	
	<td align="center">
    	<div id ="displayorder1" style="display:none;">
   		  <input type="text" name="displayorder1" id="displayorder1" onkeydown="return numberonly('displayorder1')"  size="4" maxlength="5"
    	  value="<?php echo $disporder1 ?>" >
       <?php echo $mand; ?>
     </div>
     </td>
    </tr>
    
    <?php 	// UPDATE & SHOW RECORDS
    
        mssql_free_result($result);
		$query = mssql_init('sp_GetCenter',$mssql);
		$result = mssql_execute($query);
		mssql_free_statement($query);
		$rs_cnt = mssql_num_rows($result);
		$colorflag = 0;
		$i = 0;
		while($field = mssql_fetch_array($result))
		{	$i  +=1;	$colorflag+=1;
			$tot_rec = $i;	?>
		<tr class=<?php if($colorflag%2==0) { echo "row1"; } else { echo "row2"; } ?> valign="center">

		<input type="hidden" name="center_code_<?php echo $i ?>" id ="center_code_<?php echo $i ?>" 
        value="<?php echo $field['Center_Code']?>"	 />		
        
		<td align="center"><img src="../images/delete_d.gif" title="Remove" 
          onclick="deldata('<?php echo $field['Center_Code'];?>');" />&nbsp;
        </td>
        
		<td align="center"><img src="../images/edit.gif" title="Edit" 
           onclick="editdata('<?php echo $i;?>');" />&nbsp;
        </td>	
        	
        <td >
			<?php echo $field['Center'] ?>
	    	<div id ="center_name_<?php echo $i;?>" style="display:none;">
        	 <input type="text" id="center_name_<?php echo $i ?>" name="center_name_<?php echo $i ?>"  
             size="25" maxlength="50" value="<?php echo $field['Center']?>" 
             onkeydown="return alphaonly('center_name_<?php echo $i ?>')"></div>
        </td> 
    
		<td align="center">
			<?php echo $field['Short_Name'] ?>
				<div id ="short_name_<?php echo $i;?>" style="display:none;">
                <input type="text" name="short_name_<?php echo $i ?>" id="short_name_<?php echo $i ?>"  
                size="25" maxlength="50" value="<?php echo $field['Short_Name']?>"  onkeydown="return alphaonly('short_name_<?php echo $i ?>')"></div>
        </td>
                
           <?php 	//SHOW State DROPDOWN
            mssql_free_result($result1);
			$query = mssql_init('sp_FetchCity',$mssql);
			$result1 = mssql_execute($query);
			mssql_free_statement($query);	?>  
                		
        <td align="center">
			<?php echo $field['City'] ?>
        		<div id ="city_name_<?php echo $i;?>" style="display:none;">
                <select name="city_name_<?php echo $i ?>" id="city_name_<?php echo $i ?>">
					<?php	
					while($field1 = mssql_fetch_array($result1))
					 {  ?>
				    <option value="<?php echo $field1['City_Code']?>" 
					<?php if($field['City_Code']==$field1['City_Code']) 
					echo "Selected";?>><?php echo $field1['City']?>
           		    </option>
				    <?php } ?>
	 		    </select></div>
        </td>								
		<td align="center">
				<?php if(strtoupper($field['Active'])=="Y") echo "Yes"; else echo  "No";?>
				<div id ="active_<?php echo $i;?>" style="display:none;">
			<select id="active_<?php echo $i ?>" name="active_<?php echo $i ?>">
            
				<option value="Y" <?php if(strtoupper($field['Active'])==="Y")  echo "selected" ?>>Yes</option>
				<option value="N" <?php if(strtoupper($field['Active'])==="N")  echo "selected" ?>>No</option>
			</select>
            	</div>
        </td>

		<td align="center">
				<?php echo $field['Display_Order'] ?>
				<div id ="displayorder_<?php echo $i;?>" style="display:none;">
                	<input type="text" name="displayorder_<?php echo $i ?>" id="displayorder_<?php echo $i ?>"  
                    size="5" maxlength="5" value="<?php echo $field['Display_Order']?>" 
                    onkeydown="return numberonly('displayorder<?php echo $i ?>')">
                </div>
         </td>
				
	</tr>      
         <?php } 
	if ($errcnt > 0) {
		for($i=0;$i<=$errcnt-1;$i=$i+1){
		$editcnt = split(',',$_POST['editcnt']);?>
			<script>editdata(<?php echo $editcnt[$i];?>);</script>
		<?php } 
		} ?>       
          
    <input type="hidden" name="tot_rec" value="<?php echo $tot_rec ?>" />
	<tr>
   		 <td colspan="7" align="right">
			<input type="submit" name="save" value="Save" class="winbutton_go" />&nbsp;&nbsp;&nbsp;
			<input type="submit" name="Cancel" value="Cancel" class="winbutton_go"/>
        </td>
   	</tr>

<?php	if($inserr == 1) { ?><script>adddata();</script><?php } ?>

</table></td></tr></table>

</body>
</form>
</html>
<?php include("../includes/copyright.php"); ?>