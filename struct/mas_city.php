<?php session_start(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>Isha Foundation - A Non-profit Organization</title>

<?php include("../includes/header.php"); 
 title('Org Structure','City',1,0,1);
?>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<style type="text/css">
<!--
.style1 {color: #CCFFFF}
-->
</style>

<script>
function adddata()
 { 
 	$('#City_name').show();
	$('#State_name').show();
	$('#short_name').show();
	$('#active').show();
	$('#displayorder').show();
	var $mode1 ='ADD';
	$('#mode1').val($mode1);
}
function editdata(val)
{
	
	if($('#editcnt').val() >0 ) return false;
    $('#editcnt').val(val);
	
	$('#City_name'+val).show();
	$('#State_name'+val).show();
	$('#short_name'+val).show();
	$('#active'+val).show();
	$('#displayorder'+val).show();	
	var $mode ='EDIT';
	$('#mode').val($mode);

}
function deldata(val)
{ 
	var msg="Do you want to delete?";
	if(!confirm(msg)){
		return false;
}	 
else
 { 
		$('#City_code').val(val);
		var $mode ='DELETE';
		$('#mode').val($mode);
		$('#myform').submit();
 }
}
</script>
</head>
<?php                //VARIABLE DECLARATION
	$errmsg="";
	$errflag=0;
	$dummy=0;  
    // GET MODE VALUE FOR WHICH FUNCTION PERFORMED (ADD / EDIT/ DELETE)
	$mode		=	$_POST['mode'];
	$mode1		=	$_POST['mode1'];
	$editcnt 	=	split(',',$_POST['editcnt']);		

	if($_POST['Cancel']=="Cancel") { $mode=""; $mode1="";	}
	//RECORD EDIT PART
	If($mode == "EDIT")	
	{ $action=2;
	for($i=0;$i<count($editcnt);$i=$i+1) 
	{  
	   //EDIT ROW VALUES ARE GETTING
		$City_Code	    =	 Trim($_POST['City_code'.$editcnt[$i]]);		
		$City_name	    =	 Trim($_POST['City_name'.$editcnt[$i]]);		
		$short_name	    =	 Trim($_POST['short_name'.$editcnt[$i]]);
		$state_name     =    Trim( $_POST['state_name'.$editcnt[$i]]);		
		$active			=	$_POST['active'.$editcnt[$i]];	
		$displayorder	=	Trim($_POST['displayorder'.$editcnt[$i]]);	
		$j=$i+1;
        //VALIDATION INPUTS HERE
		$dummy = Strcheck($City_name,$errmsg,$errflag,"City name");		
		$dummy = Strcheck($short_name,$errmsg,$errflag,"Short name");	
		$dummy = Strcheck($active,$errmsg,$errflag,"Active");	
		$dummy = Strcheck($displayorder,$errmsg,$errflag,"Displayorder");
        // ALL THE INPUTS ARE CORRECTED AND GO TO SP

		if($errflag==0)
		{   
		    mssql_free_result($result);
			$query = mssql_init('sp_CitySave',$mssql);
			mssql_bind($query,'@City_Code',$City_Code,SQLINT4,false,false,5);
			mssql_bind($query,'@City',$City_name,SQLVARCHAR,false,false,50);
			mssql_bind($query,'@Short_name',$short_name,SQLVARCHAR,false,false,25);
			mssql_bind($query,'@Display_Order',$displayorder,SQLVARCHAR,false,false,5);
			mssql_bind($query,'@Active',$active,SQLVARCHAR,false,false,1);
			mssql_bind($query,'@State_Code',$state_name,SQLINT4,false,false,5);
			mssql_bind($query,'@Action',$action,SQLINT4,false,false,1);
			$result = @mssql_execute($query);    //SP EXCUTED HER
			mssql_free_statement($query);        //FREE THE QUERY VARIABLE HERE
			if($result==1)
			     echo "<p class='mesg'>City has been Updated</p>";
			else {
				$errmsg1=mssql_get_last_message();  ///ERROR MESSAGE GET FROM HERE					
				$errflag=2;
				If ($errcnt == 0) { $errcnt = 1;} else { $errcnt = $errcnt + 1; }
			if ($errval == "") { $errval = $editcnt[$i];	 } else { $errval = $errval.","                       .$editcnt[$i]; }
			     }
		}
        //INPUT VALUES ARE WRONG 
		else {
			If ($errcnt == 0) { $errcnt = 1;} else { $errcnt = $errcnt + 1; }
			if ($errval == "") { $errval = $editcnt[$i];	 } else { $errval = $errval.","                     .$editcnt[$i]; } 
		      }
	}	
	}
    //DELETE PART HERE 
	if($mode=="DELETE")
	{	$action=3;
		$City_code	=	$_POST['City_code'];
            //DELETE PART SP EXECUTION 
        mssql_free_result($result);
		$query = mssql_init('sp_CitySave',$mssql);
		mssql_bind($query,'@City_Code',$City_code,SQLINT4,false,false,5);
		mssql_bind($query,'@City',$City_name,SQLVARCHAR,false,false,50);
		mssql_bind($query,'@Short_name',$short_name,SQLVARCHAR,false,false,25);
		mssql_bind($query,'@Display_Order',$displayorder,SQLVARCHAR,false,false,5);
		mssql_bind($query,'@Active',$active,SQLVARCHAR,false,false,1);
		mssql_bind($query,'@State_Code',$State_name,SQLINT4,false,false,5);
		mssql_bind($query,'@Action',$action,SQLINT4,false,false,1);
		$result = @mssql_execute($query);
		mssql_free_statement($query);
		if($result==1)
			echo "<p class='mesg'>City has been Deleted</p>";
		else {
			$errmsg1=mssql_get_last_message();				
			$errflag=2;
			}
	}
         //NEW RECORD HERE GET INPUTS 
	if($mode1 == "ADD")
	{	$action=1;
		$City_name1	=	Trim($_POST['City_name']);		
		$short_name1	=	Trim($_POST['short_name']);		
		$state_name1 =Trim($_POST['state_name']);		
		$displayorder1=	Trim($_POST['displayorder']);			
		$active1	=	Trim($_POST['active']);		
			
        //EMPTY VALIDATION//
		$dummy = Strcheck($City_name1,$errmsg,$errflag,"City name");
		$dummy = Strcheck($short_name1,$errmsg,$errflag,"Short name");
		$dummy = Zerocheck($state_name1,$errmsg,$errflag,"State name");
		$dummy = Numcheck($displayorder1,$errmsg,$errflag,"Display Order");
		$dummy = Strcheck($active1,$errmsg,$errflag,"Active");
	      	
		if($errflag==0)
		{
            //RECORD INFORMATION ADD SP
            mssql_free_result($result);
			$query = mssql_init('sp_CitySave',$mssql);		
			mssql_bind($query,'@City_Code',$City_code1,SQLINT4,false,false,5);
			mssql_bind($query,'@City',$City_name1,SQLVARCHAR,false,false,50);
			mssql_bind($query,'@Short_name',$short_name1,SQLVARCHAR,false,false,25);
			mssql_bind($query,'@Display_Order',$displayorder1,SQLVARCHAR,false,false,5);
			mssql_bind($query,'@Active',$active1,SQLVARCHAR,false,false,1);
			mssql_bind($query,'@State_Code',$state_name1,SQLINT4,false,false,5);
			mssql_bind($query,'@Action',$action,SQLINT4,false,false,1);				
			$result = @mssql_execute($query);			
			mssql_free_statement($query);			
            if($result==1) {
			 echo "<p class='mesg'>City has been Added</p>";
             $City_name1=$short_name1=$state_name1=$displayorder1=$active1="";
                            }
            else {
		
			$errmsg1=mssql_get_last_message();			
			$errflag=2;
			$inserr=1;
			}
	           	} else $inserr=1;
	       }
		if($errflag==1) 
	    echo $errlbl.$errmsg."</p>";
	    if($errflag==2) 
		echo "<p class='error'>".$errmsg1;
	
        ?>
<body style="margin:0;">
<form name="myform" id="myform" method="post" action="mas_city.php">
<input type="hidden" name="City_code" id="City_code"/>
<input type="hidden" name="mode" id="mode"/>	
<input type="hidden" name="mode1" id="mode1"/>
<input type="hidden" name="editcnt" id="editcnt"/>
<table width="100%" height="450" border="0" align="center" cellpadding="3" cellspacing="3">
  <tr>
    <td valign="top"><table width="90%" border="0" align="center" cellpadding="3" cellspacing="1">
	<?php titleheader(City,0); ?>
      <colgroup>
        <col width="5%" />
        <col width="5%" />
        <col width="25%" />
        <col width="25%" />
        <col width="15%" />
        <col width="15%" />
        <col width="10%" />
        </colgroup>
        <tr align="center"> </tr>
        <thead>
        </thead>
        <tr>
        <th>&nbsp;</th>
        <th>&nbsp;</th>
        <th>CityName</th>        
        <th>Short Name</th>
        <th>StateName</th>
        <th>Active</th>
        <th>Display Order</th>
        <td></thead></td>
 </tr>
      <?php   // New Record Insert
		$colorflag+=1; ?>
	   <tr class=<?php if($colorflag%2==0) { echo "row2"; } else { echo "row1"; } ?> 
       valign="center">	
       <!-- NEW ICON-->
	   <td align="center" ><img src="../images/new.gif" title="Add New" onclick="adddata();"/>
       </td><td>&nbsp;</td>	
       <!-- CITY NAME-->
	   <td align="center"><div id ="City_name" style="display:none;" >
       <input type="text" name="City_name" id="City_name"  size="20" maxlength="50" 
       value="<?php echo $City_name1 ?>" 
	   onkeydown="return alphaonly('City_name')" /><?php echo $mand;?></div></td>  
       <!-- SHORT NAME--> 
        <td align="center"><div id="short_name" style="display:none;">
        <input type="text" id="short_name" name="short_name" size="10" maxlength="5" 
        value="<?php echo $short_name1 ?>" 
	   onkeydown="return alphaonly('short_name')" /> <?php echo $mand;?></div></td>    
    
        <?php 	//SHOW STATE DROPDOWN
        mssql_free_result($result);
		$query = mssql_init('sp_FetchState',$mssql);
		$result = mssql_execute($query);
		mssql_free_statement($query);	?>
        <!-- STATE NAME-->
       <td align="center"><div id ="State_name" style="display:none;">
       <select id="state_name" name="state_name">
       <?php echo $Select;?>
       <?php 
       while($field = mssql_fetch_array($result)) 
       {  ?>
	   <option value="<?php echo $field['State_Code']?>"  
       <?php if($state_name1==$field['State_Code']) echo "Selected"; ?>>
	   <?php echo $field['State']?></option>
	   <?php } ?>
       </select>
       <?php echo $mand; ?></div></td> 
       <!-- ACTIVE-->  
       <td align="center"><div id="active" style="display:none;">
       <select name="active" id="active"><option value="Y">Yes</option><option value="N">No
       </option></select></div></td>  
       <!-- DISPLAY ORDER-->  
        <td  align="center"><div id="displayorder" style="display:none;"><input type="text" 
        id="displayorder" name="displayorder" size="5" maxlength="5" 
        value="<?php echo      $displayorder1 ?>" onkeydown="return numberonly('displayorder')"/>
        <?php echo $mand;?>
        </div></td>        
        </tr>
        <?php 	// UPDATE & SHOW RECORDS
                //SHOW CITY DROPDOWN
                mssql_free_result($result);
		$query = mssql_init('sp_GetCity',$mssql);
		$result = mssql_execute($query);
		mssql_free_statement($query);
		$rs_cnt = mssql_num_rows($result);
		$colorflag = 0;
		$i = 0;
		while($field = mssql_fetch_array($result))
		{	$i  +=1;	$colorflag+=1;
			$tot_rec = $i;	?>            
 		<tr class=<?php if($colorflag%2==0) { echo "row1"; } else { echo "row2"; } ?> 
         valign="center">
		<input type="hidden" name="City_code<?php echo $i ?>" id ="City_code<?php echo $i ?>" value="<?php echo $field['City_Code']?>"/>	
        <!-- DELETE ICON-->	
		<td align="center"><img src="../images/delete_d.gif" title="Remove" 
        onclick="deldata('<?php echo $field['City_Code'];?>');" />&nbsp;</td>
        <!-- EDIT ICON-->
		<td align="center"><img src="../images/edit.gif" title="Edit" 
        onclick="editdata('<?php echo $i;?>');" />&nbsp;</td>		
        <td ><?php echo $field['City'] ?>
        <!-- CITY NAME-->
        <div id ="City_name<?php echo $i;?>" style="display:none;"><input type="text" 
        name="City_name<?php echo $i ?>" id="City_name<?php echo $i ?>"  size="25" maxlength="50" value="<?php echo $field['City']?>"  
        onkeydown="return alphaonly('City_name<?php echo $i ?>')"/></div></td> 
        <!-- SHORT NAME-->
		<td align="center"><?php echo $field['Short_name'] ?>
		<div id ="short_name<?php echo $i;?>" style="display:none;">
        <input type="text" name="short_name<?php echo $i ?>" id="short_name<?php echo $i ?>"  
        size="25" maxlength="5" value="<?php echo $field['Short_name']?>" 
        onkeydown="return alphaonly('short_name<?php echo $i ?>')"/></div></td>                
        <?php 	//SHOW State DROPDOWN
        mssql_free_result($result1);
		$query = mssql_init('sp_FetchState',$mssql);
		$result1 = mssql_execute($query);
		mssql_free_statement($query);	?>                		
        <td align="center"><?php echo $field['State'] ?> 
        <!-- STATE NAME-->       
		<div id ="State_name<?php echo $i;?>" style="display:none;">
        <select name="state_name<?php echo $i ?>" id="state_name<?php echo $i ?>">
        <?php	while($field1 = mssql_fetch_array($result1)) {  ?>
		<option value="<?php echo $field1['State_Code']?>" <?php if($field['State_Code']==$field1['State_Code']) echo "Selected";?>><?php echo $field1['State']?>
        </option>
	    <?php } ?>
	   </select></div></td>								
		<td align="center"><?php if(strtoupper($field['Active'])=="Y") echo "Yes"; 
        else echo  "No";?> <!-- IF ENTER SMALL LETTER AUTOMATICALLY CHANGE TO UPPER -->
		<div id ="active<?php echo $i;?>" style="display:none;">
		<select id="active<?php echo $i ?>" name="active<?php echo $i ?>">            
		<option value="Y" <?php if(strtoupper($field['Active'])==="Y")  
        echo "selected" ?>>Yes</option>
		<option value="N" <?php if(strtoupper($field['Active'])==="N")  
        echo "selected" ?>>No</option>
		</select></div></td>
        <!-- DISPLAY ORDER-->
        <td align="center"><?php echo $field['Display_Order'] ?>
		<div id ="displayorder<?php echo $i;?>" style="display:none;"><input type="text" 
        name="displayorder<?php echo $i ?>" id="displayorder<?php echo $i ?>"  size="5" 
        maxlength="5" value="<?php echo $field['Display_Order']?>" 
        onkeydown="return numberonly('displayorder<?php echo $i ?>')"/></div></td>				
		</tr>      
        <?php } 
        	//IF ANY ERROR IN ADD, SHOW THAT ROW
	    if ($errcnt > 0) {	      
		for($i=0;$i<=$errcnt-1;$i=$i+1){
		$editcnt = split(',',$_POST['editcnt']);?>
		<script>editdata(<?php echo $editcnt[$i];?>);</script>
		<?php } 
		} ?>    
        <input type="hidden" name="tot_rec" value="<?php echo $tot_rec ?>" />
        <tr><td colspan="7" align="right">
		<input type="submit" name="save" value="Save" class="winbutton_go" />&nbsp;&nbsp;&nbsp;
		<input type="submit" name="Cancel" value="Cancel" class="winbutton_go"/></table></td></tr>
        </table> 
        <!--IF ANY ERROR IN ADD, SHOW THAT ROW--!>
         <?php	if($inserr == 1) { ?><script>adddata();</script><?php } ?>
         
</body>
</html>
        <?php include("../includes/copyright.php"); ?>

