<?php session_start(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php include("../includes/header.php");
 title('Org Structure','Country',1,0,1); ?>
<script>
$(document).ready(function(){
    $('#scrollbar1').tinyscrollbar();	
});
function adddata()                  
{ 
 	$('#country_name1').show();
	$('#short_name1').show();
	$('#phone_code1').show();
	$('#active1').show();
	$('#disporder1').show();
	var $mode1 ='ADD';
	$('#mode1').val($mode1);
} 
function editdata(val)
{ 
    if($('#editcnt').val() >0 ) return false;
    $('#editcnt').val(val);
	$('#country_name_'+val).show();
	$('#short_name_'+val).show();
	$('#phone_code_'+val).show();
	$('#active_'+val).show();
	$('#disporder_'+val).show();	
	var $mode ='EDIT';
	$('#mode').val($mode);
}
function deldata(val)
{ 
	var msg="Do you want to delete?";
	if(!confirm(msg)){
		return false;
	}	 
	else {
		$('#country_code').val(val);
		var $mode ='DELETE';
		$('#mode').val($mode);
		$('#myform').submit();
	 }
}
</script>
</head>
<?php   //VARIABLE DECLARATION
			$errmsg="";
			$errflag=0;
			$dummy=0;
			// GET MODE VALUE FOR WHICH FUNCTION PERFORMED (ADD / EDIT/ DELETE)
			$mode		=	$_POST['mode'];
			$mode1		=	$_POST['mode1'];
			$editcnt 	=	$_POST['editcnt'];		// ROW ID ( SERIAL NO) IS GETTING
			//RESET FORM
			if($_POST['Cancel']=="Cancel") { $mode=""; $mode1="";	}
			         //RECORD EDIT PART
			If($mode == "EDIT")
			{ 
			$action=2;
					//EDIT ROW VALUES ARE GETTING
			$country_code	=	$_POST['country_code_'.$editcnt];   
			$country_name	=	TRIM($_POST['country_name_'.$editcnt]);
			$short_name 	=	TRIM($_POST['short_name_'.$editcnt]);
			$phone_code		=	TRIM($_POST['phone_code_'.$editcnt]);
			$active			=	$_POST['active_'.$editcnt];
			$disporder		=	TRIM($_POST['disporder_'.$editcnt]);
			//VALIDATE THE INPUTS
			$dummy = Strcheck($country_name,$errmsg,$errflag,"Country");
			$dummy = Strcheck($short_name,$errmsg,$errflag,"Short Name");
			$dummy = Numcheck($phone_code,$errmsg,$errflag,"Phone Code");
			//ADDED + SYMBOL IN PHONE VALUE IF USER NOT GIVEN
			if($dummy==0){
				if(substr($phone_code,0,1) <> "+" ) $phone_code = "+".$phone_code;
			}
			$dummy = Strcheck($disporder,$errmsg,$errflag,"Display Order");
			//ALL INPUTS ARE CORRECTED, THEN GOTO SP
			if($errflag==0)
			{
			 mssql_free_result($result);
			$query = mssql_init('sp_Country',$mssql);
			mssql_bind($query,'@Country_Code',$country_code,SQLINT4,false,false,5);
			mssql_bind($query,'@Country_Name',$country_name,SQLVARCHAR,false,false,50);
			mssql_bind($query,'@Short_Name',$short_name,SQLVARCHAR,false,false,25);
			mssql_bind($query,'@Phone_Code',$phone_code,SQLVARCHAR,false,false,5);
			mssql_bind($query,'@Active',$active,SQLVARCHAR,false,false,1);
			mssql_bind($query,'@Disp_Order',$disporder,SQLINT4,false,false,5);
			mssql_bind($query,'@Action',$action,SQLINT4,false,false,1);
			$result = @mssql_execute($query);    // SP EXECUTE HERE
			mssql_free_statement($query);        // FREE THE $QUERY VARIABLE
            //SP EXECUTE SUCCESSFULLY
			if($result==1)
				echo "<p class='mesg'>Country has been Updated</p>";
			else 
            {
				$errmsg1=mssql_get_last_message();          //  ERROR MESSAGE GET FROM SP
				$errflag=2;
    			If ($errcnt == 0) { $errcnt = 1;} else { $errcnt = $errcnt + 1; }
    			if ($errval == "") { $errval = $editcnt[$i]; } 
			}
			}
			//INPUT VALUES ARE WRONG
			else 
			{
				If ($errcnt == 0) { $errcnt = 1;} else { $errcnt = $errcnt + 1; }
				if ($errval == "") { $errval = $editcnt[$i]; } 
			}
		}	
    //RECORD DELETE  PART 
	if($mode=="DELETE")
	{	
			$action=3;
			$country_code	=	$_POST['country_code'];
            mssql_free_result($result);
			$query = mssql_init('sp_Country',$mssql);
			mssql_bind($query,'@Country_Code',$country_code,SQLINT4,false,false,5);
			mssql_bind($query,'@Country_Name',$country_name,SQLVARCHAR,false,false,50);
			mssql_bind($query,'@Short_Name',$short_name,SQLVARCHAR,false,false,25);
			mssql_bind($query,'@Phone_Code',$phone_code,SQLVARCHAR,false,false,5);
			mssql_bind($query,'@Active',$active,SQLVARCHAR,false,false,1);
			mssql_bind($query,'@Disp_Order',$disporder,SQLINT4,false,false,5);
			mssql_bind($query,'@Action',$action,SQLINT4,false,false,1);
			$result = @mssql_execute($query);
			mssql_free_statement($query);
			if($result==1)
				echo "<p class='mesg'>Country has been Deleted</p>";
			else 
        {
			$errmsg1=mssql_get_last_message();
			$errflag=2;
		}
	}
    //RECORD ADDED PART
	if($mode1 == "ADD")
	{	
			$action=1;
			$country_name1	=	Trim($_POST['country_name1']);
			$short_name1	=	Trim($_POST['short_name1']);
			$phone_code1	=	Trim($_POST['phone_code1']);		
			$active1		=	$_POST['active1'];
			$disporder1		=	Trim($_POST['disporder1']);
			$dummy = Strcheck($country_name1,$errmsg,$errflag,"Country");
			$dummy = Strcheck($short_name1,$errmsg,$errflag,"Short Name");
			$dummy = Numcheck($phone_code1,$errmsg,$errflag,"Phone Code");
			if($dummy==0){
            if(substr($phone_code1,0,1) <> "+" ) 
                $phone_code1 = "+".$phone_code1;
        }
		$dummy = Strcheck($disporder1,$errmsg,$errflag,"Display Order");
		if($errflag==0){
            mssql_free_result($result);
    		$query = mssql_init('sp_Country',$mssql);
    		mssql_bind($query,'@Country_Code',$country_code,SQLINT4,false,false,5);
    		mssql_bind($query,'@Country_Name',$country_name1,SQLVARCHAR,false,false,50);
    		mssql_bind($query,'@Short_Name',$short_name1,SQLVARCHAR,false,false,25);
    		mssql_bind($query,'@Phone_Code',$phone_code1,SQLVARCHAR,false,false,5);
    		mssql_bind($query,'@Active',$active1,SQLVARCHAR,false,false,1);
    		mssql_bind($query,'@Disp_Order',$disporder1,SQLINT4,false,false,5);
    		mssql_bind($query,'@Action',$action,SQLINT4,false,false,1);
    		$result = @mssql_execute($query);
    		mssql_free_statement($query);
		if($result==1){
			echo "<p class='mesg'>Country has been Added</p>";
			$country_name1=$short_name1=$phone_code1=$disporder1="";
			}
		else {
			$errmsg1=mssql_get_last_message();
			$errflag=2;
			$inserr=1;
			}
		} else $inserr=1;
	}	  if($errflag==1) 
		  echo $errlbl.$errmsg."</p>";
		  if($errflag==2) 
		  echo "<p class='error'>".$errmsg1;        ?>
		  <body style="margin:0;">
		  <form name="myform" id="myform" method="post" action="mas_country.php">
		  <?PHP //HIDDEN VALUES   ?>
		  <input type="hidden" name="editcnt" id="editcnt"/>
		  <input type="hidden" name="country_code" id="country_code"/>
		  <input type="hidden" name="mode" id="mode"/>	
		  <input type="hidden" name="mode1" id="mode1"/>
		  <table width="100%" height="450" border="0" align="center" cellpadding="3" cellspacing="3">
		  <tr><td>
          <table width="65%" border="0" align="center" cellpadding="3" cellspacing="1">
          <?php titleheader(Country,0); ?>
          <tr><td>
          <thead><colgroup><col width=3%><col width=3%><col width=10%><col width=10%><col width=10%><col width=5%><col width=5%></colgroup>
          <th>&nbsp;</th><th>&nbsp;</th><th>Country</th><th>Short Name</th><th>Phone Code</th><th>Active</th><th>Display Order</th></thead>
            
          <?php   // New Record Insert
           $colorflag+=1; ?>
            <!-- NEW ICON-->
          <tr class="row1">
    	  <td align="center"><img src="../images/new.gif" title="Add New" onclick="adddata();"/></td><td>&nbsp;</td>
            <!-- COUNTRY NAME -->
          <td align="center">
          <div id ="country_name1" style="display:none;">
    	  <input type="text" name="country_name1" id="country_name1"  size="25" maxlength="50" onkeydown="return alphaonly('country_name1')" value="<?php echo $country_name1 ?>"><?php echo $mand;?>
          </div>
          </td>
            <!-- SHORT NAME-->
          <td align="center">
          <div id ="short_name1" style="display:none;">
          <input type="text" name="short_name1" id="short_name1"  size="5" maxlength="10" onkeydown="return alphaonly('short_name1')"value="<?php echo $short_name1 ?>"><?php echo $mand;?>
          </div>
          </td>
            <!-- PHONE CODE-->
          <td align="center">
          <div id ="phone_code1" style="display:none;">
          <input type="text" name="phone_code1" id="phone_code1" size="5" maxlength="10" value=" <?php echo $phone_code1 ?>"  onkeydown="return phonecode('phone_code1')"><?php echo $mand;?>
          </div>
          </td>
            <!-- ACTIVE-->
              <td align="center">
              <div id ="active1" style="display:none;">
              <select name="active1" id="active1">
              <option value="Y" <?php if($active1=='Y') echo "Selected"; ?>>Yes</option>
              <option value="N" <?php if($active1=='N') echo "Selected"; ?>>No</option>
              </select>
              </div>
              </td>
            <!-- DISPLAY ORDER-->
          <td align="center">
          <div id ="disporder1" style="display:none;">
          <input type="text" name="disporder1" id="disporder1"  size="5" maxlength="3" value="<?php echo $disporder1 ?>" onkeydown="return numberonly('disporder1')"><?php echo $mand;?>
          </div>
          </td>
          <?php 	// UPDATE & SHOW RECORDS
            mssql_free_result($result);
    		$query = mssql_init('sp_GetCountry',$mssql);
    		$result = mssql_execute($query);
    		mssql_free_statement($query);
    		$rs_cnt = mssql_num_rows($result);
    		$colorflag = 0; $i=0;
    		while($field = mssql_fetch_array($result))
    		{	$colorflag+=1; $i=$i+1;
    			$country_code = $field['Country_Code'];
    			$country_name = $field['Country'];
    			$short_name = $field['Short_name'];
    			$phone_code = $field['Phone_Code'];
    			$active = strtoupper($field['Active']);
    			$disp_order = $field['Display_Order'];       ?>
          <tr class=<?php if($colorflag%2==0) { echo "row1"; } else { echo "row2"; } ?>>
          <input type="hidden" name="country_code_<?php echo $i ?>" id ="country_code_<?php echo $i ?>" 
          value="<?php echo $country_code?>"	 />
            <!-- DELETE ICON-->
          <td align="center"><img src="../images/delete_d.gif" title="Remove" onclick="deldata('<?php echo $field['Country_Code'];?>');" />&nbsp;</td>	 
            <!-- EDIT ICON-->
          <td align="center"><img src="../images/edit.gif" title="Edit" onclick="editdata('<?php echo $i;?>');" />&nbsp;</td>
            <!-- COUNTRY-->
          <td>
		  <?php echo $field['Country'] ?>
    	  <div id ="country_name_<?php echo $i;?>" style="display:none;">
          <input type="text" name="country_name_<?php echo $i ?>" id="country_name_<?php echo $i ?>"  size="25" maxlength="50" value="<?php echo $country_name; ?>"  onkeydown="return alphaonly('country_name_<?php echo $i ?>')">
          </div>
          </td>
            <!-- SHORT NAME-->
          <td align="center"><?php echo $field['Short_name'] ?>
    	  <div id ="short_name_<?php echo $i;?>" style="display:none;">
          <input type="text" name="short_name_<?php echo $i ?>" id="short_name_<?php echo $i ?>"  size="5" maxlength="10" value="<?php echo $short_name; ?>"
    	  onkeydown="return alphaonly('short_name_<?php echo $i ?>')"/>
          </div>
          </td>
            <!-- PHONE CODE-->		
          <td align="center"><?php echo $field['Phone_Code'] ?>
    	  <div id ="phone_code_<?php echo $i ?>" style="display:none;"><input type="text" 
          name="phone_code_<?php echo $i ?>" id="phone_code_<?php echo $i ?>"  size="5" 
          maxlength="10" value="<?php echo $phone_code; ?>"  
          onkeydown="return phonecode('phone_code_<?php echo $i ?>')"/>
          </div>
          </td>
            <!-- ACTIVE-->
          <td align="center"><?php if(strtoupper($field['Active'])=="Y") echo "Yes"; else echo "No";?>
          <div id ="active_<?php echo $i;?>" style="display:none;">
          <select id="active_<?php echo $i ?>" name="active_<?php echo $i ?>">
          <option value="Y" <?php if($active=="Y")  echo "selected" ?>>Yes</option>
          <option value="N" <?php if($active=="N")  echo "selected" ?>>No</option>
          </select>
          </div>
          </td>
            <!-- DISPLAY ORDER-->
          <td align="center"><?php echo $field['Display_Order'] ?>
          <div id ="disporder_<?php echo $i;?>" style="display:none;">
          <input type="text" name="disporder_<?php echo $i ?>" id="disporder_<?php echo $i ?>"  size="5" maxlength="5" value="<?php echo $disp_order; ?>" onkeydown="return numberonly('disporder_<?php echo $i ?>')">
          </div>
          </td>
          </tr>
          <?php } 
          //IF ANY ERROR IN EDIT, SHOW THAT ROW
          if ($errcnt > 0) {  ?>
                <script>editdata(<?php echo $editcnt;?>);</script>
    	  <?php }  ?>
          <!-- bUTTONS-->	
          <tr>
          <td colspan="7" align="right">
          <input type="submit" name="save" value="Save" class="winbutton_go" />&nbsp;&nbsp;&nbsp;
          <input type="submit" name="Cancel" value="Cancel" class="winbutton_go"/>
          </td>
          </tr>
          <?php	//IF ANY ERROR IN ADD, SHOW THAT ROW
            if($inserr == 1) { ?><script>adddata();</script><?php } ?>
          </table>    
		  </td></tr></table>
		  </form>
		  <?php include("../includes/copyright.php"); ?>
		  </body>
		  </html>