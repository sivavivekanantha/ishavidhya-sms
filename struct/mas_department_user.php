<?php session_start(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php include("../includes/header.php"); 
	title('Org Structure','Department User',1,1,0); 	?>
<script>

function adddata()
 {  
  	$('#Category1').show();
 	$('#type1').show();
	$('#Username').show();
	$('#incharge_name1').show();
	$('#f_mobile').show();
	$('#s_mobile').show();
	$('#email1').show();
	$('#active1').show();
	$('#disporder1').show();
	$('#department_name1').show();
	
	$('#mode1').val('ADD');
} 

function getdepartment()
 { 
  	if($('#Category').val()==6)
	{
	
 	$('#department_name1').show();
	}
	else
	{
	
	$('#department_name1').hide();
	}
 }
function geteditdepartment(obj)
{ 
	
	if($("option:selected", $('#Category_name_'+obj)).val()==6)
	{
	$('#department_name_'+obj).show();
	}
	else
	{
	$('#department_name_'+obj).hide();
	}  
  }
function editdata(val)
{ 
	 if($('#editcnt').val() >0 ) return false;
    $('#editcnt').val(val);
	//alert('#Category_name_'+val);
	
	$('#department_name_'+val).show();
	$('#Category_name_'+val).show();
	$('#Username_'+val).show();
    $('#incharge_name_'+val).show();
	$('#f_mobile_'+val).show();
	$('#s_mobile_'+val).show();
	$('#email_'+val).show();
	$('#active_'+val).show();
	$('#disporder_'+val).show();	
	var $mode ='EDIT';
	$('#mode').val($mode);

}
function deldata(val)
{ 
	var msg="Do you want to delete?";
	if(!confirm(msg)){
		return false;
	}	 
	else {
		$('#Department_User_Code').val(val);
		
		var $mode ='DELETE';
		$('#mode').val($mode);
			$('#myform').submit();
	 }
}
</script>
</head>
<?php 
	$errmsg="";
	$errflag=0;
	$dummy=0;
	$mode		=	$_POST['mode'];
	$mode1		=	$_POST['mode1'];
	$editcnt 	=	split(',',$_POST['editcnt']);		

	if($_POST['Cancel']=="Cancel") { $mode=""; $mode1="";	}
	
	If($mode == "EDIT")
	{ $action=2;
	
     
	for($i=0;$i<count($editcnt);$i++) 
	{ 	$j=$i+1;
	
	  	$Department_User_Code	=	$_POST['Department_User_Code_'.$editcnt[$i]];
		
		$incharge_name			=	$_POST['incharge_name_'.$editcnt[$i]];
		$mobile 				=	$_POST['f_mobile_'.$editcnt[$i]];
		$email					=	$_POST['email_'.$editcnt[$i]];
		$Category				=	$_POST['Category_name_'.$editcnt[$i]];
		$Username				=	$_POST['Username_'.$editcnt[$i]];
		$active					=	$_POST['active_'.$editcnt[$i]];
		$disporder				=	$_POST['disporder_'.$editcnt[$i]];
		$department_code		=	$_POST['department_name_'.$editcnt[$i]];
		$dummy = Strcheck($incharge_name,$errmsg,$errflag,"Incharge Name");
		$dummy = Strcheck($mobile,$errmsg,$errflag,"Mobile");
		$dummy = Min_lengthcheck($mobile,$errmsg,$errflag,"Mobile No. must be 10 digits",10);	
		$dummy = Strcheck($email,$errmsg,$errflag,"Email".$j);
		$dummy = Strcheck($Category,$errmsg,$errflag,"Category".$j);
		if($Category==6)
		{
		
		$dummy = Zerocheck($department_code,$errmsg,$errflag,"Department");
		}
		$dummy = Strcheck($Username,$errmsg,$errflag,"User Name");
		$dummy = Strcheck($disporder,$errmsg,$errflag,"Display Order");
		
		
		if($errflag==0)	
		{	
		    mssql_free_result($result);
			$query = mssql_init('sp_Department_User_Save',$mssql);
			mssql_bind($query,'@Department_User_Code',$Department_User_Code,SQLINT4,false,false,5);
			mssql_bind($query,'@Department_Code',$department_code,SQLINT4,false,false,50);
			mssql_bind($query,'@Incharge_Name',$incharge_name,SQLVARCHAR,false,false,5);			
			mssql_bind($query,'@Mobile',$mobile,SQLVARCHAR,false,false,5);
			mssql_bind($query,'@Email',$email,SQLVARCHAR,false,false,50);
			mssql_bind($query,'@Category_Code',$Category,SQLVARCHAR,false,false,1);
			mssql_bind($query,'@Username ',$Username,SQLVARCHAR,false,false,1);
			mssql_bind($query,'@Active',$active,SQLVARCHAR,false,false,1);
			mssql_bind($query,'@Disp_Order',$disporder,SQLINT4,false,false,5);
			mssql_bind($query,'@Action',$action,SQLINT4,false,false,1);
			
			$result = @mssql_execute($query);
			
			mssql_free_statement($query);
			
			if($result==1)
				echo "<p class='mesg'>Department has been Updated</p>";
			else {
				$errmsg1=mssql_get_last_message();
				$errflag=2;
			If ($errcnt == 0) { $errcnt = 1;} else { $errcnt = $errcnt + 1; }
			if ($errval == "") { $errval = $editcnt[$i];	 } else { $errval = $errval.",".$editcnt[$i]; }
			}
		}
		else {
			If ($errcnt == 0) { $errcnt = 1;} else { $errcnt = $errcnt + 1; }
			if ($errval == "") { $errval = $editcnt[$i];	 } else { $errval = $errval.",".$editcnt[$i]; } 
		}
	}	
	}

	if($mode=="DELETE")
	{	$action=3;

		   $Department_User_Code	=	$_POST['Department_User_Code'];
           
            mssql_free_result($result);
			$query = mssql_init('sp_Department_User_Save',$mssql);
			mssql_bind($query,'@Department_User_Code',$Department_User_Code,SQLINT4,false,false,5);
			mssql_bind($query,'@Department_Code',$department_code1,SQLINT4,false,false,50);
			mssql_bind($query,'@Incharge_Name',$incharge_name1,SQLVARCHAR,false,false,5);			
			mssql_bind($query,'@Mobile',$mobile1,SQLVARCHAR,false,false,5);
			mssql_bind($query,'@Email',$email1,SQLVARCHAR,false,false,50);
			mssql_bind($query,'@Category_Code',$Category1,SQLVARCHAR,false,false,1);
			mssql_bind($query,'@Username ',$Username1,SQLVARCHAR,false,false,1);
			mssql_bind($query,'@Active',$active1,SQLVARCHAR,false,false,1);
			mssql_bind($query,'@Disp_Order',$disporder1,SQLINT4,false,false,5);
			mssql_bind($query,'@Action',$action,SQLINT4,false,false,1);
		
		$result = @mssql_execute($query);
		mssql_free_statement($query);
		if($result==1)
			echo "<p class='mesg'>Department has been Deleted</p>";
		else {
			$errmsg1=mssql_get_last_message();
			$errflag=2;
			}
	}
	if($mode1 == "ADD")
	{	$action=1;   
	//Get Values From Form
	
		$department_code1	=	$_POST['department_name1'];	
		$Category1	=	$_POST['Category'];
		$Username1=$_POST['Username'];
		$incharge_name1=Trim($_POST['incharge_name']);	
		$mobile1=($_POST['f_mobile']);		
		$email1=($_POST['email1']);
		$active1			=	$_POST['active1'];
		$disporder1		=	Trim($_POST['disporder1']);
	
		//Validate the Inputs
		$dummy = Strcheck($incharge_name1,$errmsg,$errflag,"Incharge Name");
		$dummy = Strcheck($mobile1,$errmsg,$errflag,"Mobile");
		$dummy = Min_lengthcheck($mobile1,$errmsg,$errflag,"Mobile No. must be 10 digits",10);	
		$dummy = Emailcheck($email1,$errmsg,$errflag,"Email");
		$dummy = Zerocheck($Category1,$errmsg,$errflag,"Category");
		if($Category1==6)
    		$dummy = Zerocheck($department_code1,$errmsg,$errflag,"Department");
	
		$dummy = Strcheck($Username1,$errmsg,$errflag,"Username");
		$dummy = Strcheck($disporder1,$errmsg,$errflag,"Display Order");
		
		
		
			
		
		
		if($errflag==0)
		{	
		    mssql_free_result($result);
			$query = mssql_init('sp_Department_User_Save',$mssql);
			mssql_bind($query,'@Department_User_Code',$Department_User_Code,SQLINT4,false,false,5);
			mssql_bind($query,'@Department_Code',$department_code1,SQLINT4,false,false,50);
			mssql_bind($query,'@Incharge_Name',$incharge_name1,SQLVARCHAR,false,false,5);			
			mssql_bind($query,'@Mobile',$mobile1,SQLVARCHAR,false,false,5);
			mssql_bind($query,'@Email',$email1,SQLVARCHAR,false,false,50);
			mssql_bind($query,'@Category_Code',$Category1,SQLVARCHAR,false,false,1);
			mssql_bind($query,'@Username ',$Username1,SQLVARCHAR,false,false,1);
			mssql_bind($query,'@Active',$active1,SQLVARCHAR,false,false,1);
			mssql_bind($query,'@Disp_Order',$disporder1,SQLINT4,false,false,5);
			mssql_bind($query,'@Action',$action,SQLINT4,false,false,1);
			$result = @mssql_execute($query);
			mssql_free_statement($query);
			
			if($result==1)
			{
				echo "<p class='mesg'>Department has been Added</p>";
				
				$department_code1	="";
				$Category1	="";
				$Username1		="";
				$incharge_name1	="";
				$mobile1		="";
				$email1			="";
				$active1		="";
				$disporder1		="";
				
				
			}
				
			else 
			{
				$errmsg1=mssql_get_last_message();
				$errflag=2;
				$inserr=1;
			}
		
		} 
		else $inserr=1;
	}
	if($errflag==1) 
		echo $errlbl.$errmsg;
	if($errflag==2) 
		echo "<p class='error'>".$errmsg1;
	
?>
<body style="margin:0;">
<form name="myform" id="myform" method="post" action="mas_department_user.php">
  <input type="hidden" name="editcnt" id="editcnt"/>
  <input type="hidden" name="Department_User_Code" id="Department_User_Code"/>
  <input type="hidden" name="mode" id="mode"/>
  <input type="hidden" name="mode1" id="mode1"/>
    <table width="100%" height="450" border="0" align="center" cellpadding="3" cellspacing="3">
    <tr>
   	  <td valign="top"><table width="100%" border="0" align="center" cellpadding="3" cellspacing="1">
	  <?php titleheader('Department  User',0);?>
        
        <thead>
        </thead>
   	    <th>&nbsp;</th>
   	        <th>&nbsp; </th>
   	      <th align="center">Incharge</th>
   	      <th align="center">Mobile</th>
   	      <th align="center">Email</th>
   	      <th align="center">Category</th>
   	      <th align="center">Department</th>
   	      <th align="center">User Name</th>
   	      <th align="center">Active</th>
   	      <th >Display Order</th>
 	      </tr>
        <?php   // New Record Insert	?>
        <tr class="row1" valign="center">
          <td align="center" ><img src="../images/new.gif" title="Add New" onclick="adddata();"/></td>
          <td>&nbsp;</td>
          <td><div id ="incharge_name1" style="display:none;" >
              <input type="text" name="incharge_name" id="incharge_name"  size="15" maxlength="50" value="<?php echo $incharge_name1?>"  onkeydown="return alphaonly('incharge_name1')" />
            <?php echo $mand; ?> </div></td>
          <td><div id ="f_mobile" style="display:none;" >
              <input type="text" name="f_mobile" id="f_mobile" value="<?php echo $mobile1 ?>"  size="15" maxlength="15" onkeydown="return numberonly('f_mobile')"/>
            <?php echo $mand; ?> </div></td>
          <td><div id ="email1" style="display:none;" >
              <input type="text" name="email1" id="email1"  size="25" maxlength="50" value="<?php echo $email1?>" />
            <?php echo $mand; ?> </div></td>
          <!-- <td><div id ="type1" style="display:none;"  >
                <select name="Type" id="Type" ?php echo $field1['Department']?>>
                  <option value="0">Select</option>s
                  <option value="1">Academic </option>
                  <option value="2">Non Academic</option>
                </select>
              </div></td>-->
          <?php 	// //SHOW CATEGORY DROPDOWN
          
                mssql_free_result($result);
				$query = mssql_init('sp_FetchCategory',$mssql);
				$result = mssql_execute($query);
				mssql_free_statement($query);
				$rs_cnt = mssql_num_rows($result);
				?>
          <td><div id ="Category1" style="display:none;" >
              <select id ="Category" name="Category" >
             <!-- <select id ="Category" name="Category"  onchange="getdepartment()">-->

                <?php 
				echo $Select;
				while($field = mssql_fetch_array($result)) 
   				{   ?>
                <option value="<?php echo $field['Category_Code']?>" <?php if($Category1==$field['Category_Code']) echo "selected"; ?>> <?php echo $field['Category']; }?> </option>
              </select>
              <?php echo $mand; ?> </div></td>
          <?php 	//SHOW DEPARTMENT DROPDOWN
                   
                    mssql_free_result($result);
					$query = mssql_init('sp_FetchDepartment',$mssql);
					$result = mssql_execute($query);
					mssql_free_statement($query);?>
          <td align="center"><div id ="department_name1" style="display:none;">
            <select id ="department_name2" name="department_name1"  >
              <?php 
				echo $Select;
				while($field = mssql_fetch_array($result)) 
				   {?>
              <option value="<?php echo $field['Department_Code']?>" <?php if($department_code1==$field['Department_Code']) echo "selected"; ?>> <?php echo $field['Department']?></option>
              <?php } ?>
            </select>
            <?php echo $mand; ?> </div></td>
          <td><div id ="Username" style="display:none;" >
            <input type="text" name="Username" id="Username2" size="12"  maxlength="30" value="<?php echo $Username1 ?>" />
            <?php echo $mand; ?> </div></td>
          <td align="center"><div id ="active1" style="display:none;"  >
              <select name="active1" id="active1">
                <option value="Y">Yes</option>
                <option value="N">No</option>
              </select>
            <?php echo $mand; ?> </div></td>
          <td align="center"><div id ="disporder1" style="display:none;">
              <input type="text" name="disporder1" id="disporder1"  size="2" maxlength="5" value="<?php echo $disporder1 ?>" 
                onkeydown="return numberonly('disporder1')" />
            <?php echo $mand; ?> </div></td>
            
               <?php                   
                mssql_free_result($result);
                $query = mssql_init('[SP_GetDepartmentUser]',$mssql);
        		$result = mssql_execute($query);
        		mssql_free_statement($query);
        		$rs_cnt = mssql_num_rows($result);
        		$colorflag = 0;
        		$i = 0;
        //		 ,, Mobile_No, ,  Category_Code, Department_Code,UserName ,Active,Display_Order 
        		while($field = mssql_fetch_array($result))
        		{	$i  +=1;	$colorflag+=1;
        			$tot_rec = $i;	
			
			
				$Department_User_Code = $field['Teacher_Code'];?>
        </tr>
   	    <tr class=<?php  if($colorflag%2==0) { echo "row1"; } else { echo "row2"; } ?> valign="center">
         
          <input type="hidden" name="Department_User_Code_<?php echo $i ?>" id ="Department_User_Code_<?php echo $i ?>" value="<?php echo $field['Teacher_Code'];?>" />
          <td align="center"><img src="../images/delete_d.gif" title="Remove" onclick="deldata('<?php echo $field['Teacher_Code'];?>');" />&nbsp;</td>
   	      <td align="center"><img src="../images/edit.gif" title="Edit" onclick="editdata('<?php echo $i;?>');" />&nbsp;</td>
   	      <td><?php echo $field['Teacher_Name'] ?>
              <div id ="incharge_name_<?php echo $i;?>" style="display:none;">
                <input type="text" name="incharge_name_<?php echo $i ?>" id="incharge_name_<?php 
                echo $i ?>" size="25" maxlength="50" value="<?php echo $field['Teacher_Name'];?>" onkeydown="return alphaonly('incharge_name_<?php echo $i ?>')" />
            </div></td>
   	      <td><?php echo $field['Mobile_No'] ?>
              <div id ="f_mobile_<?php echo $i;?>" style="display:none;">
                <input type="text" name="f_mobile_<?php echo $i ?>" id="f_mobile_<?php 
                echo $i ?>" size="25" maxlength="15" value="<?php echo $field['Mobile_No'];?>" onkeydown="return numberonly('f_mobile_<?php echo $i ?>')" />
            </div></td>
   	      <td><?php echo $field['Email_Id'] ?>
              <div id ="email_<?php echo $i;?>" style="display:none;">
                <input type="text" name="email_<?php echo $i ?>" id="email_<?php echo $i ?>"  size="25" maxlength="50" value="<?php echo $field['Email_Id']; ?>" />
            </div></td>
   	      <?php 	//SHOW DEPARTMENT DROPDOWN
                    mssql_free_result($result1);
					$query = mssql_init('sp_FetchCategory',$mssql);
					$result1 = mssql_execute($query);
					mssql_free_statement($query);?>
          <td><?php echo $field['Category'] ?>
              <div id ="Category_name_<?php echo $i;?>" style="display:none;">
                <select id="Category_name_<?php echo $i ?>" name="Category_name_<?php echo $i ?>" >
                  <?php	while($field1 = mssql_fetch_array($result1)) {?>
                  <option value="<?php echo $field1['Category_Code']?>" <?php if($field['Category']==$field1['Category']) echo "Selected";?>> <?php echo $field1['Category']?></option>
                  <?php } ?>
                </select>
            </div></td>
   	      <?php 	//SHOW State DROPDOWN
             mssql_free_result($result1);
		$query = mssql_init('sp_FetchDepartment',$mssql);
		$result1 = mssql_execute($query);
		mssql_free_statement($query);?>
          <td ><?php echo $field['Department'] ?>
              <div id ="department_name_<?php echo $i;?>"  style="display:none;">
                <select name="department_name_<?php echo $i ?>" id="department_name_<?php echo $i ?>"  >
                 <?php	echo $Select;
				 while($field1 = mssql_fetch_array($result1)) {  ?>
                  <option value="<?php echo $field1['Department_Code']?>" <?php if($field['Department_Code']==$field1['Department_Code']) echo "Selected";?>><?php echo $field1['Department']?></option>
                  <?php } ?>
                </select>
            </div></td>
   	      <td><?php echo $field['UserName']?>
              <div id ="Username_<?php echo $i;?>" style="display:none;">
                <input type="text" name="Username_<?php echo $i ?>" id="Username_<?php echo $i ?>"  size="25" maxlength="30" value="<?php echo $field['UserName']; ?>" />
            </div></td>
   	      <td align="center"><?php if(strtoupper($field['Active'])=="Y") echo "Yes"; else echo "No";?>
              <div id ="active_<?php echo $i;?>" style="display:none;">
                <select id="active_<?php echo $i ?>" name="active_<?php echo $i ?>">
                  <option value="Y" <?php if(strtoupper($field['Active'])=="Y")  echo "selected" ?>>Yes</option>
                  <option value="N" <?php if(strtoupper($field['Active'])=="N")   echo "selected" ?>>No</option>
                </select>
            </div></td>
   	      <td align="center"><?php echo $field['Display_Order'] ?>
              <div id ="disporder_<?php echo $i;?>" style="display:none;">
                <input type="text" name="disporder_<?php echo $i ?>" id="disporder_<?php echo $i ?>"  size="5" maxlength="5" value="<?php echo $field['Display_Order'];?>" onkeydown="return numberonly('disporder_<?php echo $i ?>')" />
            </div></td>
 	      </tr>
        <?php } 
	if ($errcnt > 0) {
		for($i=0;$i<=$errcnt-1;$i=$i+1){
		$editcnt = split(',',$_POST['editcnt']);?>
        <script>editdata(<?php echo $editcnt[$i];?>);</script>
        <?php } 
		} ?>
        <input type="hidden" name="tot_rec" value="<?php echo $tot_rec ?>" />
        <tr>
          <td colspan="10" align="right"><input type="submit" name="save" value="Save" class="winbutton_go" />
            <input type="submit" name="Cancel" value="Cancel" class="winbutton_go"/></td>
        </tr>
        <?php	if($inserr == 1) { ?>
        <script>adddata();</script>
        <?php } ?>
      </table></td>
    </tr>
  </table>
</form>
<?php  if($errflag==1 and $Category==6 ) echo "<script> getdepartment()</script>" ?>
</body>
</html>
<?php include("../includes/copyright.php"); ?>
