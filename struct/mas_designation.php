<?php session_start(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>Isha Foundation - A Non-profit Organization</title>
<?php include("../includes/header.php");
 title('Org Structure','Designation',1,0,1);
 ?>
<script>

	// display all the text box fields for add designation
	function adddata()
	 { 
		$('#designation_name1').show();
		$('#active1').show();
		$('#disporder1').show();
		var $mode1 ='ADD';
		$('#mode1').val($mode1);
	} 
	
	// display all the text box fields for update designation
	function editdata(val)
	{
		if($('#editcnt').val() >0 ) return false;
		$('#editcnt').val(val);
		$('#designation_name_'+val).show();
		$('#active_'+val).show();
		$('#disporder_'+val).show();	
		var $mode ='EDIT';
		$('#mode').val($mode);
	
	}
	
	// delete the designation
	function deldata(val)
	{ 
		var msg="Do you want to delete?";
		if(!confirm(msg)){
			return false;
		}	 
		else {
			$('#designation_code').val(val);
			var $mode ='DELETE';
			$('#mode').val($mode);
			$('#myform').submit();
		 }
	}
</script>
</head>

<?php 
			$errmsg="";
			$errflag=0;
			$dummy=0;
			$mode		=	$_POST['mode'];
			$mode1		=	$_POST['mode1'];
			$editcnt 	=	split(',',$_POST['editcnt']);		
		
			//cancel the display mode 
			if($_POST['Cancel']=="Cancel") 
			{ 
				$mode="";
				$mode1="";	
			}
			
			//edit the designation
			if($mode == "EDIT")
			{ 
				$action=2;
				
						for($i=0;$i<count($editcnt);$i=$i+1) 
						{ 
									$designation_code	=	Trim($_POST['designation_code_'.$editcnt[$i]]);
									$designation_name	=	Trim($_POST['designation_name_'.$editcnt[$i]]);
									$active			=	$_POST['active_'.$editcnt[$i]];
									$disporder		=	Trim($_POST['disporder_'.$editcnt[$i]]);
									
									$j=$i+1;
									
									//error message
									$dummy = Strcheck($designation_name,$errmsg,$errflag,"Designation");
									$dummy = Numcheck($disporder,$errmsg,$errflag,"Display Order");
								
									if($errflag==0)
									{           
									             mssql_free_result($result);
												$query = mssql_init('sp_Designation',$mssql);
												mssql_bind($query,'@Designation_Code',$designation_code,SQLINT4,false,false,5);
												mssql_bind($query,'@Designation_Name',$designation_name,SQLVARCHAR,false,false,50);
												mssql_bind($query,'@Active',$active,SQLVARCHAR,false,false,1);
												mssql_bind($query,'@Disp_Order',$disporder,SQLINT4,false,false,5);
												mssql_bind($query,'@Action',$action,SQLINT4,false,false,1);
												$result = @mssql_execute($query);
												mssql_free_statement($query);
												
												if($result==1)
												{
													echo "<p class='mesg'>Designation has been Updated</p>";
													
												}
												else 
												{
													$errmsg1=mssql_get_last_message();
													$errflag=2;
												}
									
									}
									else 
									{
											if ($errcnt == 0) 
											{
												$errcnt = 1;
											} 
											else 
											{ 
												$errcnt = $errcnt + 1; 
											}
											
											if ($errval == "") 
											{ 
												$errval = $editcnt[$i];	 
											} 
											else 
											{
												$errval = $errval.",".$editcnt[$i]; 
											} 
									}
						}//end for	



			}//end if
		
			//delete the designation
			if($mode=="DELETE")
			{	
					$action=3;
					$designation_code	=	$_POST['designation_code'];
			         
                    mssql_free_result($result);
					$query = mssql_init('sp_Designation',$mssql);
					mssql_bind($query,'@Designation_Code',$designation_code,SQLINT4,false,false,5);
					mssql_bind($query,'@Designation_Name',$designation_name,SQLVARCHAR,false,false,50);
					mssql_bind($query,'@Active',$active,SQLVARCHAR,false,false,1);
					mssql_bind($query,'@Disp_Order',$disporder,SQLINT4,false,false,5);
					mssql_bind($query,'@Action',$action,SQLINT4,false,false,1);
					$result = @mssql_execute($query);
					mssql_free_statement($query);
					
					if($result==1)
					{
						echo "<p class='mesg'>Designation has been Deleted</p>";
					}
					else 
					{
						$errmsg1=mssql_get_last_message();
						$errflag=2;
					}
			}
	
			//add the designation
			if($mode1 == "ADD")
			{	
					$action=1;
					$designation_name1	=	Trim($_POST['designation_name1']);
					$active1			=	$_POST['active1'];
					$disporder1		    =	Trim($_POST['disporder1']);
				
					//error messge
					$dummy = Strcheck($designation_name1,$errmsg,$errflag,"Designation");
					$dummy = Numcheck($disporder1,$errmsg,$errflag,"Display Order");
				
					if($errflag==0)
					{
		                     mssql_free_result($result);
							$query = mssql_init('sp_Designation',$mssql);	//add the designation		
							mssql_bind($query,'@Designation_Code',$designation_code,SQLINT4,false,false,5);
							mssql_bind($query,'@Designation_Name',$designation_name1,SQLVARCHAR,false,false,50);
							mssql_bind($query,'@Active',$active1,SQLVARCHAR,false,false,1);
							mssql_bind($query,'@Disp_Order',$disporder1,SQLINT4,false,false,5);
							mssql_bind($query,'@Action',$action,SQLINT4,false,false,1);
						
							$result = @mssql_execute($query);
							mssql_free_statement($query);
		
							if($result==1) 
							{
								echo "<p class='mesg'>Designation has been Added</p>";
								$designation_name1=$active1=$disporder1="";
							}
							else 
							{
								$errmsg1=mssql_get_last_message();
								$errflag=2;
								$inserr=1;
							}
					} 
					else 
					{
							$inserr=1;
					}
			}
			
			
			if($errflag==1) 
			{	
					echo $errlbl.$errmsg."</p>";
			}
			if($errflag==2) 
			{
					echo "<p class='error'>".$errmsg1;
			}
	
?>
        <body style="margin:0;">
        
        <form name="myform" id="myform" method="post" action="mas_designation.php">
        <input type="hidden" name="editcnt" id="editcnt"/>
        <input type="hidden" name="designation_code" id="designation_code"/>
        <input type="hidden" name="mode" id="mode"/>	
        <input type="hidden" name="mode1" id="mode1"/>

        <table width="70%" height="450" border="0" align="center" cellpadding="3" cellspacing="3">
        
        <tr>
        <td valign="top">
        
	        <table width="100%" border="0" align="center" cellpadding="3" cellspacing="1">
    	    <?php titleheader(Designation,0); ?>
           
            <tr align="center">
            <thead>
            <colgroup><col width=3%><col width=3%><col width=10%><col width=10%><col width=10%></colgroup>
            <th>&nbsp;</th>
            <th>&nbsp;</th>
            <th align="center">Designation</th>
            <th>Active</th>
            <th>Display Order</th>
            </thead>
            </tr>
        
     	   <?php   // New Record Insert
            $colorflag+=1; ?>
            
            <tr class=<?php if($colorflag%2==0) { echo "row2"; } else { echo "row1"; } ?> valign="center">
            <td align="center" ><img src="../images/new.gif" title="Add New" onclick="adddata();"/></td>
            <td>&nbsp;</td>
            <td align="center"><div id ="designation_name1" style="display:none;"><input type="text" name="designation_name1" id="designation_name1"  size="25" maxlength="50" onkeydown="return alphaonly('designation_name1')" value="<?php echo $designation_name1 ?>"><?php echo $mand; ?></div></td>
            <td align="center"><div id ="active1" style="display:none;"><select name="active1" id="active1"><option value="Y">Yes</option><option value="N">No</option></select></div></td>
            <td align="center"><div id ="disporder1" style="display:none;"><input type="text" name="disporder1" id="disporder1"  size="5" maxlength="5" value="<?php echo $disporder1 ?>" onkeydown="return numberonly('disporder1')"><?php echo $mand; ?></div></td>
            <?php 	// UPDATE & SHOW RECORDS
            
                 mssql_free_result($result);
        	    $query = mssql_init('sp_GetDesignation',$mssql);
                $result = mssql_execute($query);
                mssql_free_statement($query);
                $rs_cnt = mssql_num_rows($result);
                $colorflag = 0;
                $i = 0;
                while($field = mssql_fetch_array($result))
                {	$i  +=1;	$colorflag+=1;
                    $tot_rec = $i;	?>
                
                <tr class=<?php if($colorflag%2==0) { echo "row1"; } else { echo "row2"; } ?> valign="center">
               		 <input type="hidden" name="designation_code_<?php echo $i ?>" id ="designation_code_<?php echo $i ?>" value="<?php echo $field['Des_Code']?>"	 />		
                    <td align="center"><?php if($field['Des_Code']>1 ) { ?>
                    <img src="../images/delete_d.gif" title="Remove" onclick="deldata('<?php echo $field['Des_Code'];?>');" />&nbsp;<?php } ?></td>
                    <td align="center"><?php if($field['Des_Code']>1 ) { ?><img src="../images/edit.gif" title="Edit" onclick="editdata('<?php echo $i;?>');" />&nbsp;<?php } ?></td>
                    <td ><?php echo $field['Designation'] ?>
                    <div id ="designation_name_<?php echo $i;?>" style="display:none;"><input type="text" name="designation_name_<?php echo $i ?>" id="designation_name_ 					<?php echo $i ?>"  size="25" maxlength="50" value="<?php echo $field['Designation']?>"  onkeydown="return alphaonly('designation_name_<?php echo $i ?>')"></div></td>
                                            
                    <td align="center"><?php if(strtoupper($field['Active'])=="Y") echo "Yes"; else echo "No";?>
                    <div id ="active_<?php echo $i;?>" style="display:none;">
                    <select id="active_<?php echo $i ?>" name="active_<?php echo $i ?>">
                    <option value="Y" <?php if(strtoupper($field['Active'])=="Y")  echo "selected" ?>>Yes</option>
                    <option value="N" <?php if(strtoupper($field['Active'])=="N")  echo "selected" ?>>No</option>
                    </select></div></td>
            
                    <td align="center"><?php echo $field['Display_Order'] ?>
                    <div id ="disporder_<?php echo $i;?>" style="display:none;"><input type="text" name="disporder_<?php echo $i ?>" id="disporder_<?php echo $i ?>"  size="5" maxlength="5" value="<?php echo $field['Display_Order']?>" onkeydown="return numberonly('disporder_<?php echo $i ?>')"></div></td>
                        
                </tr>
                
            <?php } 
            if ($errcnt > 0) {
                for($i=0;$i<=$errcnt-1;$i=$i+1){
                $editcnt = split(',',$_POST['editcnt']);?>
                    <script>editdata(<?php echo $editcnt[$i];?>);</script>
                <?php } 
                } ?>	
                <input type="hidden" name="tot_rec" value="<?php echo $tot_rec ?>"/>
                <tr><td colspan="5" align="right">
                <input type="submit" name="save" value="Save" class="winbutton_go" />&nbsp;&nbsp;&nbsp;
                <input type="submit" name="Cancel" value="Cancel" class="winbutton_go"/>
                </td></tr>
        
       		<?php	if($inserr == 1) { ?><script>adddata();</script><?php } ?>
        
        </table></td></tr></table>
		</form>
</body>
</html>
<?php include("../includes/copyright.php"); ?>