<?php session_start(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>Isha Foundation - A Non-profit Organization</title>

<?php include("../includes/header.php"); 
 title('Student Management','Scholarship',2,1,0);

?>

<script>

function adddata()
 { 
 	$('#scholarship1').show();
	$('#active1').show();
	var $mode1 ='ADD';
	$('#mode1').val($mode1);
} 


function editdata(val)
{
	if($('#editcnt').val() >0 ) return false;
    $('#editcnt').val(val);

	$('#scholarship_'+val).show();
	$('#active_'+val).show();
	var $mode ='EDIT';
	$('#mode').val($mode);

}
function deldata(val)
{ 
	var msg="Do you want to delete?";
	if(!confirm(msg)){
		return false;
	}	 
	else {
		$('#scholarship_code').val(val);
		var $mode ='DELETE';
		$('#mode').val($mode);
		$('#myform').submit();
	 }
}
</script>
</head>

<?php 
	$errmsg="";
	$errflag=0;
	$dummy=0;
	$mode		=	$_POST['mode'];
	$mode1		=	$_POST['mode1'];
	$editcnt 	=	split(',',$_POST['editcnt']);		

	if($_POST['Cancel']=="Cancel") { $mode=""; $mode1="";	}
	
	If($mode == "EDIT")
	{ 
	$action=2;
	for($i=0;$i<count($editcnt);$i=$i+1) 
	{ 
		$scholarship_code	=	Trim($_POST['scholarship_code_'.$editcnt[$i]]);
		$scholarship	    =	Trim($_POST['scholarship_'.$editcnt[$i]]);		
		$active			    =	$_POST['active_'.$editcnt[$i]];         
		$j=$i+1;
		$dummy = Strcheck($scholarship,$errmsg,$errflag,"Scholarship");		
		
		if($errflag==0)
		{    
            mssql_free_result($result);
			$query = mssql_init('sp_Scholarship',$mssql);
			mssql_bind($query,'@Scholarship_Code',$scholarship_code,SQLINT4,false,false,5);
			mssql_bind($query,'@Scholarship',$scholarship,SQLVARCHAR,false,false,50);
			mssql_bind($query,'@Active',$active,SQLVARCHAR,false,false,1);
			mssql_bind($query,'@Action',$action,SQLINT4,false,false,5);
			$result = @mssql_execute($query);
			mssql_free_statement($query);
			if($result==1)
				echo "<p class='mesg'>Scholarship has been Updated</p>";
			else {
				$errmsg1=mssql_get_last_message();
				$errflag=2;
			}
		}
		else {
			If ($errcnt == 0) { $errcnt = 1;} else { $errcnt = $errcnt + 1; }
			if ($errval == "") { $errval = $editcnt[$i];	 } else { $errval = $errval.",".$editcnt[$i]; } 
		}
	}	
	}

	if($mode=="DELETE")
	{	$action=3;
		$scholarship_code	=	$_POST['scholarship_code'];
        mssql_free_result($result);
		$query = mssql_init('sp_Scholarship',$mssql);
			mssql_bind($query,'@Scholarship_Code',$scholarship_code,SQLINT4,false,false,5);
			mssql_bind($query,'@Scholarship',$scholarship,SQLVARCHAR,false,false,50);
			mssql_bind($query,'@Active',$active,SQLVARCHAR,false,false,1);
			mssql_bind($query,'@Action',$action,SQLINT4,false,false,5);
			$result = @mssql_execute($query);
		
		mssql_free_statement($query);
		if($result==1)
			echo "<p class='mesg'>Scholarship has been Deleted</p>";
		else {
			$errmsg1=mssql_get_last_message();
			$errflag=2;
			}
	}
	if($mode1 == "ADD")
	{	$action=1;
		$scholarship1	=	Trim($_POST['scholarship1']);
		$active1			=	$_POST['active1'];
	
		//error messge//
		$dummy = Strcheck($scholarship1,$errmsg,$errflag,"Scholarship");
	
		
		if($errflag==0){
		  
        mssql_free_result($result);  
		   $query = mssql_init('sp_Scholarship',$mssql);
			mssql_bind($query,'@Scholarship_Code',$scholarship_code,SQLINT4,false,false,5);
			mssql_bind($query,'@Scholarship',$scholarship1,SQLVARCHAR,false,false,50);			
			mssql_bind($query,'@Active',$active1,SQLVARCHAR,false,false,1);
			mssql_bind($query,'@Action',$action,SQLINT4,false,false,5);
			$result = @mssql_execute($query);			
			mssql_free_statement($query);
		if($result==1)
		{
			echo "<p class='mesg'>Scholarship has been Added</p>";
			$scholarship1="";
			
			
			
			}
		else {
			$errmsg1=mssql_get_last_message();
			$errflag=2;
			$inserr=1;
			
			}
		} else $inserr=1;
	}
	if($errflag==1) 
		echo $errlbl.$errmsg."</p>";
	if($errflag==2) 
		echo "<p class='error'>".$errmsg1;
	
?>
<body style="margin:0;">

<form name="myform" id="myform" method="post" action="mas_scholarship.php">
<input type="hidden" name="editcnt" id="editcnt"/>
<input type="hidden" name="scholarship_code" id="scholarship_code"/>
<input type="hidden" name="mode" id="mode"/>	
<input type="hidden" name="mode1" id="mode1"/>

<table width="70%" height="450" border="0" align="center" cellpadding="3" cellspacing="3">
<tr><td valign="top">
<table width="100%" border="0" align="center" cellpadding="3" cellspacing="1">
<?php titleheader(Scholarship,0); ?>
<tr align="center">
<thead><colgroup><col width=3%><col width=3%><col width=20%><col width=10%></colgroup>
<th>&nbsp;</th><th>&nbsp;</th><th>Scholarship</th>
<th>Active</th>
</thead></tr>

<?php   // New Record Insert
		$colorflag+=1; ?>
	<tr class=<?php if($colorflag%2==0) { echo "row2"; } else { echo "row1"; } ?> valign="center">
	
	<td align="center" ><img src="../images/new.gif" title="Add New" onclick="adddata();"/></td><td>&nbsp;</td>
	
	<td align="center"><div id ="scholarship1" style="display:none;"><input type="text" name="scholarship1" id="scholarship1"  size="50" maxlength="50" value="<?php echo $scholarship1 ?>"/><?php echo $mand; ?></div></td>
	
	<td align="center"><div id ="active1" style="display:none;"><select name="active1" id="active1"><option value="Y">Yes</option><option value="N">No</option></select></div></td>
	


<?php 	// UPDATE & SHOW RECORDS
        
        mssql_free_result($result);
		$query = mssql_init('sp_Getscholarship',$mssql);
		$result = mssql_execute($query);
		mssql_free_statement($query);
		$rs_cnt = mssql_num_rows($result);
		$colorflag = 0;
		$i = 0;
		while($field = mssql_fetch_array($result))
		{	$i  +=1;	$colorflag+=1;
			$tot_rec = $i;	?>
		<tr class=<?php if($colorflag%2==0) { echo "row1"; } else { echo "row2"; } ?> valign="center">

		<input type="hidden" name="scholarship_code_<?php echo $i ?>" id ="scholarship_code_<?php echo $i ?>" value="<?php echo $field['Scholarship_Code']?>"	 />
		
		<td align="center"><img src="../images/delete_d.gif" title="Remove" onclick="deldata('<?php echo $field['Scholarship_Code'];?>');" />&nbsp;</td>	 

		<td align="center"><img src="../images/edit.gif" title="Edit" onclick="editdata('<?php echo $i;?>');" />&nbsp;</td>

		<td ><?php echo $field['Scholarship'] ?>
				<div id ="scholarship_<?php echo $i;?>" style="display:none;"><input type="text" name="scholarship_<?php echo $i ?>" id="scholarship_<?php echo $i ?>"  size="50" maxlength="50" value="<?php echo $field['Scholarship']?>"  )"/></div></td>		
								
		<td align="center"><?php if(strtoupper($field['Active'])=="Y") echo "Yes"; else echo "No";?>
		<div id ="active_<?php echo $i;?>" style="display:none;">
			<select id="active_<?php echo $i ?>" name="active_<?php echo $i ?>">
		<option value="Y" <?php if(strtoupper($field['Active'])=="Y")  echo "selected" ?>>Yes</option>
		<option value="N" <?php if(strtoupper($field['Active'])=="N")  echo "selected" ?>>No</option>
		</select></div></td>	
				
		</tr>
	<?php } 
	if ($errcnt > 0) {
		for($i=0;$i<=$errcnt-1;$i=$i+1){
		$editcnt = split(',',$_POST['editcnt']);?>
			<script>editdata(<?php echo $editcnt[$i];?>);</script>
		<?php } 
		} ?>	
		<input type="hidden" name="tot_rec" value="<?php echo $tot_rec ?>" />
	<tr><td colspan="5" align="right">
		<input type="submit" name="save" value="Save" class="winbutton_go" />&nbsp;&nbsp;&nbsp;
		<input type="submit" name="Cancel" value="Cancel" class="winbutton_go"/></td></tr>

<?php	if($inserr == 1) { ?><script>adddata();</script><?php } ?>

</table></td></tr></table>
</form></body></html>
<?php include("../includes/copyright.php"); ?>