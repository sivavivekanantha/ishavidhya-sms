<?php session_start(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include("../includes/header.php"); ?>
<?php title('Student Management','School',2,1,0); ?>
<script>

//load the city when we select state
function make_ajax1(obj,obj1)
	{
		if(obj=='') return false;
		if(obj1=='') obj1 = 0;
		url = "../includes/ajax.php?listcode=1&State="+obj+"&City="+obj1;
    	$('#Citydiv').load(url);
	}
	
//load the city when we select state
function make_ajax2(obj,obj1,obj2)
	{	
		if(obj=='') return false;
		if(obj2=='') obj2=0;
	    url = "../includes/ajax.php?listcode=1&State="+obj1+'&City='+obj2;
		var cdiv = '#Citydiv_'+obj;
		$(cdiv).load(url);
	}

// display all the text box fields for add school
function adddata()
 { 
	$('#school_name1').show();
	$('#short_name1').show();
	$('#location1').show();
	$('#address1').show();
	$('#state_code1').show();
	$('#Citydiv').show();
	$('#contact_no11').show();
	$('#contact_no21').show();
	$('#email1').show();
	$('#active1').show();	
	$('#mode1').val('ADD');
} 


// display all the text box fields for update school
function editdata(val,val1,val2)
{
	if($('#editcnt').val() >0 ) return false;
    $('#editcnt').val(val);
	
	$('#school_name_'+val).show();
	$('#short_name_'+val).show();
	$('#location_'+val).show();
	$('#address_'+val).show();
	$('#location_'+val).show();
	$('#statecode_'+val).show();
	$('#Citydiv_'+val).show();
	$('#contact_no1_'+val).show();
	$('#contact_no2_'+val).show();
	$('#email_'+val).show();
	$('#active_'+val).show();
	
	var $mode ='EDIT';
	$('#mode').val($mode);
}

// delete the school

function deldata(val)
{ 
	var msg="Do you want to delete?";
	if(!confirm(msg)){
		return false;
	}	 
	else {
		$('#school_id').val(val);
		$('#mode').val('DELETE');
		$('#myform').submit();
	 }
}
</script>
</head>

<?php 
			$errmsg="";
			$errflag=0;
			$dummy=0;
			$mode		=	trim($_POST['mode']);
			$mode1		=	trim($_POST['mode1']);
			$editcnt 	=	split(',',$_POST['editcnt']);		
		
			//cancel the display mode 
			if($_POST['Cancel']=="Cancel") { $mode=""; $mode1="";	}
			
			//edit the school
			If($mode == "EDIT")
			{ 
					$action=2;
					for($i=0;$i<count($editcnt);$i=$i+1) 
					{  
						$school_id	    =	trim($_POST['school_id'.$editcnt[$i]]);	
						$school_name  	=	strtoupper(trim($_POST['school_name_'.$editcnt[$i]]));
						$short_name		=	strtoupper(trim($_POST['short_name_'.$editcnt[$i]]));
						$location		=	trim($_POST['location_'.$editcnt[$i]]);
						$address		=	trim($_POST['address_'.$editcnt[$i]]);
						$state_code		=	trim($_POST['state_code_'.$editcnt[$i]]);
						$city_code		=	trim($_POST['City_Code_'.$editcnt[$i]]);
				
						if(strlen($city_code)==0)
							
							$city_code = $_POST['City_Code'];
							$contact_no1	=	trim($_POST['contact_no1_'.$editcnt[$i]]);
							$contact_no2	=	trim($_POST['contact_no2_'.$editcnt[$i]]);
							$email			=	trim($_POST['email_'.$editcnt[$i]]);
							$active			=	trim($_POST['active_'.$editcnt[$i]]);
						
							$j=$i+1;
							
							//error message
							
							$dummy = Strcheck($school_name,$errmsg,$errflag,"School Name");							
							$dummy = Strcheck($short_name,$errmsg,$errflag,"Short Name");
							if(dummy==0)
							$dummy = Min_lengthcheck($short_name,$errmsg,$errflag,
            				"School short name must 3 chars",3);
							$dummy = Strcheck($location,$errmsg,$errflag,"Location");
							$dummy = Strcheck($address,$errmsg,$errflag,"Address");
							$dummy = Strcheck($state_code,$errmsg,$errflag,"State Name");
							$dummy = Strcheck($city_code,$errmsg,$errflag,"City Name");
							$dummy = Numcheck($contact_no1,$errmsg,$errflag,"Contact No-1");
							
							if($dummy==0)
								$dummy = Min_lengthcheck($contact_no1,$errmsg,$errflag,"Contact No-1 must be 9 digits",9);            
							if(strlen($contact_no2)>0)
								$dummy = Min_lengthcheck($contact_no2,$errmsg,$errflag,"Contact No-2 must be 9 digits",9);  
							if(strlen($email)>0)
						  		$dummy = Emailcheck($email,$errmsg,$errflag,"Email Id");
								$dummy = Strcheck($active,$errmsg,$errflag,"Active");
				
				
						//$dummy = CheckEmail($email,$errmsg,$errflag,"Email ID".$j);
				
							if($errflag==0)
							{
							        mssql_free_result($result);
									$query = mssql_init('sp_School',$mssql);
									mssql_bind($query,'@School_Id',$school_id,SQLINT4,false,false,5);
									mssql_bind($query,'@School_Name',$school_name,SQLVARCHAR,false,false,50);
									mssql_bind($query,'@Short_Name',$short_name,SQLVARCHAR,false,false,25);
									mssql_bind($query,'@Location',$location,SQLVARCHAR,false,false,50);
									mssql_bind($query,'@Address',$address,SQLVARCHAR,false,false,50);
									mssql_bind($query,'@State_Code',$state_code,SQLINT4,false,false,5);
									mssql_bind($query,'@City_Code',$city_code,SQLINT4,false,false,5);
									mssql_bind($query,'@Contact_No1',$contact_no1,SQLVARCHAR,false,false,5);
									mssql_bind($query,'@Contact_No2',$contact_no2,SQLVARCHAR,false,false,5);
									mssql_bind($query,'@Email_Id',$email,SQLVARCHAR,false,false,5);
									mssql_bind($query,'@Active',$active,SQLVARCHAR,false,false,1);
									mssql_bind($query,'@Action',$action,SQLINT4,false,false,1);
									$result = @mssql_execute($query);
									mssql_free_statement($query);
									if($result==1)
										echo "<p class='mesg'>School has been Updated</p>";
									else 
									{
										$errmsg1=mssql_get_last_message();
										$errflag=2;
										$errcnt=1;
									}
							}
							else 
							{
								If ($errcnt == 0) 
									{ $errcnt = 1;} 
								else
									{ $errcnt = $errcnt + 1; }
								if ($errval == "") 
								{ 	$errval = $editcnt[$i];	 
								}
								else 
								{ $errval = $errval.",".$editcnt[$i]; } 
							
							}
				
					}//end for	
		
			}
			// delete the school
  
			if($mode=="DELETE")
			{	
				$action=3;
  				$school_id	=	trim($_POST['school_id']);
                mssql_free_result($result);
       			$query = mssql_init('sp_School',$mssql);		
				mssql_bind($query,'@School_Id',$school_id,SQLINT4,false,false,5);
				mssql_bind($query,'@School_Name',$school_name,SQLVARCHAR,false,false,50);
				mssql_bind($query,'@Short_Name',$short_name,SQLVARCHAR,false,false,25);
				mssql_bind($query,'@Location',$location,SQLVARCHAR,false,false,50);
				mssql_bind($query,'@Address',$address,SQLVARCHAR,false,false,50);
				mssql_bind($query,'@State_Code',$state_code,SQLINT4,false,false,5);
				mssql_bind($query,'@City_Code',$city_code,SQLINT4,false,false,5);
				mssql_bind($query,'@Contact_No1',$contact_no1,SQLVARCHAR,false,false,5);
				mssql_bind($query,'@Contact_No2',$contact_no2,SQLVARCHAR,false,false,5);
				mssql_bind($query,'@Email_Id',$email,SQLVARCHAR,false,false,5);
				mssql_bind($query,'@Active',$active,SQLVARCHAR,false,false,1);
				mssql_bind($query,'@Action',$action,SQLINT4,false,false,1);
				$result = @mssql_execute($query);
				mssql_free_statement($query);
			
				if($result==1)
						echo "<p class='mesg'>School has been Deleted</p>";
				else 
				{
						$errmsg1=mssql_get_last_message();			
						$errflag=2;
				}
			
			}
			
			//add the school	
			if($mode1 == "ADD")
			{	
				
				$action=1;			
				$school_name1	=	trim($_POST['school_name1']);
				{
				$school_name1=strtoupper($school_name1);
				
				}
				$short_name1	=	trim($_POST['short_name1']);
				{
					$short_name1=strtoupper($short_name1);
				}
				$location1		=	trim($_POST['location1']);
				$address1	    =	trim($_POST['address1']);
				$State	  		=	trim($_POST['State']);
				$City_Code		=	trim($_POST['City_Code']);
				$contact_no11	=	trim($_POST['contact_no11']);
				$contact_no21     =	trim($_POST['contact_no21']);
				$email1	        =	trim($_POST['email1']);
				$active1	    =	trim($_POST['active1']);			
		
				//error message//								
		
				$dummy = Strcheck($school_name1,$errmsg,$errflag,"School Name");
					$dummy = Strcheck($short_name1,$errmsg,$errflag,"Location");
				if(dummy==0)
					$dummy = Min_lengthcheck($short_name1,$errmsg,$errflag,
            		"School short name must 3 chars",3);
				
				$dummy = Strcheck($location1,$errmsg,$errflag,"Location");
				$dummy = Strcheck($address1,$errmsg,$errflag,"Address");
				$dummy = Strcheck($State,$errmsg,$errflag,"State Name");
				$dummy = Strcheck($City_Code,$errmsg,$errflag,"City Name");
				$dummy = Numcheck($contact_no11,$errmsg,$errflag,"Contact No-1");
				
				if($dummy==0)
					$dummy = Min_lengthcheck($contact_no11,$errmsg,$errflag,"Contact No-1 must be 9 digits",9);            
				if(strlen($contact_no21)>0)
					$dummy = Min_lengthcheck($contact_no21,$errmsg,$errflag,"Contact No-2 must be 9 digits",9);  
				if(strlen($email1)>0)
				 	$dummy = Emailcheck($email1,$errmsg,$errflag,"Email Id");
					$dummy = Strcheck($active1,$errmsg,$errflag,"Active");
		
			
				if($errflag==0)
				{
		                mssql_free_result($result);
						$query = mssql_init('sp_School',$mssql);
						mssql_bind($query,'@School_Id',$school_id,SQLINT4,false,false,5);
						mssql_bind($query,'@School_Name',$school_name1,SQLVARCHAR,false,false,50);
						mssql_bind($query,'@Short_Name',$short_name1,SQLVARCHAR,false,false,25);
						mssql_bind($query,'@Location',$location1,SQLVARCHAR,false,false,50);
						mssql_bind($query,'@Address',$address1,SQLVARCHAR,false,false,50);
						mssql_bind($query,'@State_Code',$State,SQLINT4,false,false,5);
						mssql_bind($query,'@City_Code',$City_Code,SQLINT4,false,false,5);
						mssql_bind($query,'@Contact_No1',$contact_no11,SQLVARCHAR,false,false,5);
						mssql_bind($query,'@Contact_No2',$contact_no21,SQLVARCHAR,false,false,5);
						mssql_bind($query,'@Email_Id',$email1,SQLVARCHAR,false,false,5);
						mssql_bind($query,'@Active',$active1,SQLVARCHAR,false,false,1);
						mssql_bind($query,'@Action',$action,SQLINT4,false,false,1);
						
						$result = @mssql_execute($query);
						mssql_free_statement($query);
						
						if($result==1) 
						{
								echo "<p class='mesg'>School has been Added</p>";
								$school_name1=$short_name1=$location1=$address1=$State=$City_Code=$contact_no11=$contact_no21=$email1=$active1="";
						}
						else 
						{
								$errmsg1=mssql_get_last_message();
								$errflag=2;
								$inserr=1;
						}
				}
				else $inserr=1;
			}
			if($errflag==1) 
					echo $errlbl.$errmsg;
			if($errflag==2) 
					echo "<p class='error'>".$errmsg1;
	
?>
            <body style="margin:0;">
            <form name="myform" id="myform" method="post" action="mas_school.php">
            <input type="hidden" name="editcnt" id="editcnt"/>
            <input type="hidden" name="school_id" id="school_id"/>
            <input type="hidden" name="mode" id="mode"/>
            <input type="hidden" name="mode1" id="mode1"/>

            <table width="100%" border="0" align="center" cellpadding="3" cellspacing="3">
            
            <tr>
            <td valign="top">
            <table width="100%" border="0" align="center" cellpadding="3" cellspacing="1">
            <?php titleheader(School,0);?>
            <tr><td>
            <thead><colgroup><col width="3%" /><col width="3%"/><col width="25%"/><col width="5%"/><col width="8%"/><col width="10%"/><col width="8%"/>
            <col width="8%"/><col width="8%"/><col width="8%"/><col width="8%"/><col width="3%"/></colgroup>
            <th>&nbsp;</th>
            <th>&nbsp;</th>
            <th align="center">School Name</th>
            <th>Short Name</th>
            <th>Location</th>
            <th>Address</th>
            <th>State Name</th>
            <th>City Name</th>
            <th>Contact No1</th>
            <th>Contact No2</th>
            <th>Email Id</th>
            <th>Active</th>
            </thead></td></tr>
            <?php   // New Record Insert 	 
                 $colorflag+=1; ?>
            
            <tr class=<?php if($colorflag%2==0) { echo "row2"; } else { echo "row1"; } ?> valign="center" >
            <td align="center" ><img src="../images/new.gif" title="Add New" onclick="adddata();"/></td><td>&nbsp;</td>
            <td align="center"><div id ="school_name1" style="display:none;">
            <input type="text" name="school_name1" id="school_name1"  size="10" maxlength="100" value="<?php echo $school_name1 ?>" onkeydown="return alphaonly('school_name1')" ><?php echo $mand; ?></div></td>
            <td align="center"><div id ="short_name1" style="display:none;"><input type="text" name="short_name1" id="short_name1"  size="5" maxlength="3" onkeydown="return alphaonly('short_name1')" value="<?php echo $short_name1; ?>"><?php echo $mand; ?></div></td>
            <td align="center"><div id ="location1" style="display:none;"><input type="text" name="location1" id="location1"  size="10" maxlength="1000" onkeydown="return alphaonly('location1')" value="<?php echo $location1; ?>"><?php echo $mand; ?></div></td>
            <td align="center"><div id ="address1" style="display:none;"><textarea rows="1" cols="20" name="address1" id="address1" maxlength="5000"><?php echo $address1; ?></textarea><?php echo $mand; ?></div></td>
            <?php 	//SHOW State DROPDOWN
                    mssql_free_result($result);
                    $query = mssql_init('sp_FetchState',$mssql);
                    $result = mssql_execute($query);
                    mssql_free_statement($query);	?> 
                    <td align="center">
                    <div id ="state_code1" style="display:none;">           
                     <select name="State" id="State"  onchange="make_ajax1(this.value);" >
                     <?php echo $Select; ?>	
                     <?php	while($field = mssql_fetch_array($result)) {  ?>
                     <option value="<?php echo $field['State_Code']?>" <?php if($State==$field['State_Code']) echo "selected";?>><?php echo $field['State']?></option>
                     <?php }?>
                   </select>
                	 </div> </td>
                    
            	    <td><div id="Citydiv" style="display:none;"><select name="City_Code" id="City_Code"> <?php echo $Select; ?></select></div></td>
 		            <?php  if($State>0) { ?> <script>make_ajax1(<?php echo $State; ?>,<?php echo $City_Code; ?>)</script><?php } ?>
         			<td align="center"><div id ="contact_no11" style="display:none;"><input type="text" name="contact_no11" id="contact_no11"  size="10" maxlength="15" value="<?php echo $contact_no11; ?>" onkeydown="return phonecode('contact_no11')"><?php echo $mand; ?></div></td>                
                	<td align="center"><div id ="contact_no21" style="display:none;"><input type="text" name="contact_no21" id="contact_no21"  size="10" maxlength="15" value="<?php echo $contact_no21; ?>" onkeydown="return phonecode('contact_no21')" > </div></td>                
               		 <td align="center"><div id ="email1" style="display:none;"><input type="text" name="email1" id="email1"  size="10" maxlength="50" 
                 		value="<?php echo $email1; ?>" ></div></td>               
                	 <td align="center" valign="top"><div id ="active1" style="display:none;">
                    <select name="active1" id="active1">
                     <option value="Y" <?php if(active1=="Y") echo "Selected"; ?> >Yes</option>
                     <option value="N" <?php if(active1=="N") echo "Selected"; ?> >No</option>
                     </select>	</div></td>
            
            
             <?php 	// UPDATE & SHOW RECORDS
                    mssql_free_result($result);
                    $query = mssql_init('sp_GetSMSschool',$mssql);
                    $result = mssql_execute($query);
                    mssql_free_statement($query);
                    $rs_cnt = mssql_num_rows($result);
                    $colorflag = 0;
                    $i = 0;
                    while($field = mssql_fetch_array($result))
                    {	$i +=1;	$colorflag+=1;
                        $tot_rec = $i;	?>
                    <tr class=<?php if($colorflag%2==0) { echo "row1"; } else { echo "row2"; } ?> valign="center">            
                    <input type="hidden" name="school_id<?php echo $i ?>" id ="school_id<?php echo $i ?>" value="<?php echo $field['School_Id']?>"	 />                 
                    <td align="center"><img src="../images/delete_d.gif" title="Remove" onclick="deldata('<?php echo $field['School_Id'];?>');" />&nbsp;</td>	                    <td align="center"><img src="../images/edit.gif" title="Edit" onclick="editdata('<?php echo $i;?>',<?php echo $field['State_Code']; ?>,<?php echo $field['City_Code']; ?>);" />&nbsp;</td>            
                    <td ><?php echo $field['School_Name'] ?>
                    <div id ="school_name_<?php echo $i;?>" style="display:none;"><input type="text" name="school_name_<?php echo $i ?>" id="school_name_<?php echo $i ?>"  size="10" maxlength="100" value="<?php echo $field['School_Name']?>" onkeydown="return alphaonly('school_name_<?php echo $i ?>')"></div></td>
                     <td align="center"><?php echo $field['Short_Name'] ?>
                     <div id ="short_name_<?php echo $i;?>" style="display:none;">
                     <input type="text" name="short_name_<?php echo $i ?>" id="short_name_<?php echo $i ?>"  size="5" maxlength="10" value="<?php echo $field['Short_Name']?>" onkeydown="return alphaonly('short_name_<?php echo $i ?>')"></div></td>
            		 <td align="center"><?php echo $field['Location'] ?>
                     <div id ="location_<?php echo $i;?>" style="display:none;"><input type="text" name="location_<?php echo $i ?>" id="location_<?php echo $i ?>"  size="10" maxlength="1000" value="<?php echo $field['Location']?>" onkeydown="return alphaonly('location_<?php echo $i ?>')"></div></td>
                     <td align="center"><?php echo $field['Address'] ?>
                     <div id ="address_<?php echo $i;?>" style="display:none;"><textarea rows="1" cols="20"  name="address_<?php echo $i ?>" id="location_<?php echo $i ?>"  size="10" maxlength="5000" onkeydown="return alphaonly('location_<?php echo $i ?>')"><?php echo $field['Address']; ?></textarea></div></td>
                     <?php 	//SHOW STATE DROPDOWN
                     mssql_free_result($result1);
                    $query = mssql_init('sp_FetchState',$mssql);
                    $result1 = mssql_execute($query);
                    mssql_free_statement($query);	?>
                    <td align="center"><?php echo $field['State'] ?>    
                
                    <div id="statecode_<?php echo $i ?>" style="display:none;">
                          <select name="state_code_<?php echo $i ?>" id="state_code_<?php echo $i ?>"  onchange="make_ajax2(<?php echo $i; ?>, this.value)" >
                			<?php	while($field1 = mssql_fetch_array($result1)) {  ?>
                            <option value="<?php echo $field1['State_Code']?>" <?php if( ($field['State_Code']==$field1['State_Code']) or ($errflag==1 and $state_code==$field1['State_Code'])) echo "Selected";?>><?php echo $field1['State']?></option>
                     <?php } ?>
                     </select></div></td>
                 
               		<td align="center"><?php echo $field['City']  ?>  
                    <div id="Citydiv_<?php echo $i ?>" style="display:none;">
               		 <?php 	//SHOW STATE DROPDOWN
                     mssql_free_result($result1);
                    $query = mssql_init('sp_Getcityview',$mssql);		
                    mssql_bind($query,'@State_Code',$field['State_Code'],SQLINT4,false,false,5);
                    $result1 = @mssql_execute($query);
                    mssql_free_statement($query); ?>
                	<select name="City_Code_<?php echo $i ?>" id="City_Code_<?php echo $i ?>" >
                  	<?php	while($field1 = mssql_fetch_array($result1)) {  ?>
                  	<option value="<?php echo $field1['City_Code']?>" <?php if($field['City_Code']==$field1['City_Code']) echo "Selected";?>><?php echo $field1['City']?></option>
                  <?php } ?>
               		</select></div> </td>               
                
   					<?php if($state_code>0 and $errflag==1) 
					{ ?> <script>make_ajax2(<?php echo $i ?>, <?php echo $state_code ?>, <?php echo $city_code ?>)</script><?php } ?>        
         			<td align="center"><?php echo $field['Contact_No1'] ?>
					<div id ="contact_no1_<?php echo $i;?>" style="display:none;"><input type="text" name="contact_no1_<?php echo $i ?>" id="contact_no1_<?php echo $i ?>"  size="10" maxlength="15" value="<?php echo $field['Contact_No1']; ?>"  onkeydown="return phonecode('contact_no1_<?php echo $i ?>')"></div></td>
		            <td align="center"><?php echo $field['Contact_No2'] ?>
					<div id ="contact_no2_<?php echo $i;?>" style="display:none;"><input type="text" name="contact_no2_<?php echo $i ?>" id="contact_no2_<?php echo $i ?>"  size="10" maxlength="15" value="<?php echo $field['Contact_No2']; ?> "  onkeydown="return phonecode('contact_no2_<?php echo $i ?>')"></div></td>
                    <td align="center"><?php echo $field['Email_Id'] ?>
					<div id ="email_<?php echo $i;?>" style="display:none;"><input type="text" name="email_<?php echo $i ?>" id="email_<?php echo $i ?>"  size="10" maxlength="50" value="<?php echo $field['Email_Id']; ?>"></div></td>
					<td align="center"><?php if(strtoupper($field['Active'])=="Y") echo "Yes"; else echo "No";?>
		<div id ="active_<?php echo $i;?>" style="display:none;">
					<select id="active_<?php echo $i ?>" name="active_<?php echo $i ?>">            
                    <option value="Y" <?php if($active=="Y")  echo "selected" ?>>Yes</option>
                    <option value="N" <?php if($active=="N")  echo "selected" ?>>No</option>
                    </select></div></td>
                    </tr> 
                                    
                    <?php } 
                    if ($errcnt > 0)
					{
							for($i=0;$i<=$errcnt-1;$i=$i+1) 
							{
								$editcnt = split(',',$_POST['editcnt']);
								$svar = "#state_code_".($i +1).".val()"; ?>
								<script>editdata(<?php echo $editcnt[$i]; ?>);</script>
						<?php 
							}  
                     
					 } ?>	
                <input type="hidden" name="tot_rec" value="<?php echo $tot_rec ?>" />
                <tr><td colspan="13" align="right">
                <input type="submit" name="save" value="Save" class="winbutton_go"  />
                <input type="submit" name="Cancel" class="winbutton_go" value="Cancel" /></td></tr>	
                <?php	if($inserr == 1) { ?><script>adddata();</script> <?php  } ?>
                </table>
                </td>
                </tr>
                </table>
                </form>
	
    </body>
    </html>
<?php include("../includes/copyright.php"); ?>

