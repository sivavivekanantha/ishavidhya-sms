<?php session_start();?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>Isha Foundation - A Non-profit Organization</title>
<?php include("../includes/header.php"); 
title('Org Structure','State',1,0,1);
?>
<script>
//Under This Coding Using JQuery Concepts
//Data are showing in Form This Functions Used
function adddata()
{ 
    	$('#state1').show();
    	$('#short_name1').show();
    	$('#disporder1').show();
    	$('#active1').show();
    	$('#country_code1').show();
	
	    $('#mode1').val('ADD');
} 
function editdata(val)
{
        if($('#editcnt').val() >0 ) return false;
        $('#editcnt').val(val);

	    $('#state_'+val).show();
    	$('#short_name_'+val).show();
     	$('#disporder_'+val).show();
	    $('#active_'+val).show();
	    $('#country_code_'+val).show();	

    	$('#mode').val('EDIT');
}

function deldata(val)
{ 
        var msg="Do you want to delete?";
        if(!confirm(msg)){
	    	return false;
	}	 
	else {
	    $('#state_code').val(val);
		$('#mode').val('DELETE');
		$('#myform').submit();
	 }
}
</script>
</head>
                 
     <?php 
	 //VARIABLE DECLARATION
	    $errmsg="";
	    $errflag=0;
     	$dummy=0;  
		// GET MODE VALUE FOR WHICH FUNCTION PERFORMED (ADD / EDIT/ DELETE)
        $mode		=	$_POST['mode']; 
     	$mode1		=	$_POST['mode1'];
	    $editcnt 	=	split(',',$_POST['editcnt']);	// ROW ID ( SERIAL NO) IS GETTING
		//RESET FORM
	if($_POST['Cancel']=="Cancel") { $mode=""; $mode1="";	}//when we click the cancel button all data are cleared
	 //RECORD EDIT PART
	If($mode == "EDIT")
	{ 
    $action=2;//stored procedure action 2 is update query written
	 //EDIT ROW VALUES ARE GETTING
	for($i=0;$i<count($editcnt);$i=$i+1) 

	{ 
	                        //unwanted spaces are trim	  
    $state_code		=	Trim($_POST['state_code'.$editcnt[$i]]);
	$state_name		=	Trim($_POST['state_'.$editcnt[$i]]);
	$short_name		=	Trim($_POST['short_name_'.$editcnt[$i]]);
	$disporder		=	Trim($_POST['disporder_'.$editcnt[$i]]);
	$active			=	$_POST['active_'.$editcnt[$i]];	
	$country_code	=	Trim($_POST['country_code_'.$editcnt[$i]]);
	$j=$i+1;
                 //VALIDATE THE INPUTS
	$dummy = Strcheck($state_name,$errmsg,$errflag,"State");
	$dummy = Strcheck($short_name,$errmsg,$errflag,"Short Name");
	$dummy = Zerocheck($country_code,$errmsg,$errflag,"Country");
	$dummy = Strcheck($disporder,$errmsg,$errflag,"Display Order");
               //ALL INPUTS ARE CORRECTED, THEN GOTO SP
	if($errflag==0)
		{     
                         //stored procedure purpose initialize
    mssql_free_result($result);
	$query = mssql_init('sp_State',$mssql);
	mssql_bind($query,'@State_Code',$state_code,SQLINT4,false,false,5);
	mssql_bind($query,'@State',$state_name,SQLVARCHAR,false,false,100);
	mssql_bind($query,'@Short_Name',$short_name,SQLVARCHAR,false,false,25);
	mssql_bind($query,'@Display_Order',$disporder,SQLINT4,false,false,5);
	mssql_bind($query,'@Active',$active,SQLVARCHAR,false,false,1);
	mssql_bind($query,'@Country_Code',$country_code,SQLVARCHAR,false,false,5);
	mssql_bind($query,'@Action',$action,SQLINT4,false,false,1);
    $result = @mssql_execute($query);// SP EXECUTE HERE
	mssql_free_statement($query); // FREE THE $QUERY VARIABLE
			 //SP EXECUTE SUCCESSFULLY
	if($result==1)
	echo "<p class='mesg'>State has been Updated</p>";
			  //INPUT VALUES ARE WRONG
	else {
				$errmsg1=mssql_get_last_message(); //  ERROR MESSAGE GET FROM SP
				$errflag=2;
				$errcnt=1;
			     }
		}
	else {
	     	  If ($errcnt == 0) { $errcnt = 1;} else { $errcnt = $errcnt + 1; }
			  if ($errval == "") { $errval = $editcnt[$i];	 } else { $errval = $errval.",".$editcnt[$i]; } 
		     }
	}	
	}
   //RECORD DELETE  PART 
	if($mode=="DELETE")
	{	 
	$action=3;//action 3 is delete written stored procedure
  	$state_code	=	$_POST['state_code'];
    mssql_free_result($result);
    $query = mssql_init('sp_State',$mssql);		
	mssql_bind($query, '@State_Code',$state_code,SQLINT4,false,false,5);
	mssql_bind($query,'@State',$state,SQLVARCHAR,false,false,100);
	mssql_bind($query,'@Short_Name',$short_name,SQLVARCHAR,false,false,25);
	mssql_bind($query,'@Display_Order',$disporder,SQLINT4,false,false,5);
	mssql_bind($query,'@Active',$active,SQLVARCHAR,false,false,1);
    mssql_bind($query,'@Country_Code',$country_code,SQLVARCHAR,false,false,5);
	mssql_bind($query,'@Action',$action,SQLINT4,false,false,1);
	$result = @mssql_execute($query);
	mssql_free_statement($query);
	if($result==1)
	echo "<p class='mesg'>State has been Deleted</p>";
	else {
			$errmsg1=mssql_get_last_message();
			$errflag=2;
			}
	}
	 //RECORD ADD PART
	if($mode1 == "ADD")
	{	$action=1;
                              //unwanted spaces are trim	  
	$state_name1		=	Trim($_POST['state1']);
	$short_name1		=	Trim($_POST['short_name1']);
	$disporder1		    =	Trim($_POST['disporder1']);
	$active1			=	$_POST['active1'];
	$country_code1	    =	Trim($_POST['country_code1']);
		                  //error message
                         //checking the textbox validation
    $dummy = Strcheck($state_name1,$errmsg,$errflag,"State");
	$dummy = Strcheck($short_name1,$errmsg,$errflag,"Short Name");
    $dummy = Zerocheck($country_code1,$errmsg,$errflag,"Country");
	$dummy = Strcheck($disporder1,$errmsg,$errflag,"Display Order");
	if($errflag==0){
		              //Stored Procedure 
    mssql_free_result($result);
	$query = mssql_init('sp_State',$mssql);
	mssql_bind($query,'@State_Code',$state_code,SQLINT4,false,false,5);
	mssql_bind($query,'@State',$state_name1,SQLVARCHAR,false,false,100);
	mssql_bind($query,'@Short_Name',$short_name1,SQLVARCHAR,false,false,25);
	mssql_bind($query,'@Display_Order',$disporder1,SQLINT4,false,false,5);
	mssql_bind($query,'@Active',$active1,SQLVARCHAR,false,false,1);
	mssql_bind($query,'@Country_Code',$country_code1,SQLVARCHAR,false,false,5);
	mssql_bind($query,'@Action',$action,SQLINT4,false,false,1);
			
			$result = @mssql_execute($query);//SP EXECUTE HERE
			mssql_free_statement($query); //$QUERY FREE HERE
	     	if($result==1) {
			echo "<p class='mesg'>State has been Added</p>";
            $state_name1=$short_name1=$disporder1=$active1=$country_code1="";
            }
   else {
     		$errmsg1=mssql_get_last_message();
			$errflag=2;
			$inserr=1;
			 }
        		        }
   else $inserr=1;
	}
	if($errflag==1) 
		  echo $errlbl.$errmsg."</p>";
	if($errflag==2) 
		echo "<p class='error'>".$errmsg1;
	
        ?>
              <!--Form Desining--><!--HIDDEN VALUES--> 
           <body style="margin:0;">
           <form name="myform" id="myform" method="post" action="mas_state.php">
           <input type="hidden" name="editcnt" id="editcnt"/>
           <input type="hidden" name="state_code" id="state_code"/>
           <input type="hidden" name="mode" id="mode"/>
           <input type="hidden" name="mode1" id="mode1"/>


           <table width="70%" height="450" border="0" align="center" cellpadding="3" cellspacing="3">
           <?php titleheader(State,0); ?>
           <tr><td valign="top">
           <table width="100%" border="0" align="center" cellpadding="3" cellspacing="1">
           <colgroup><col width=3%><col width=3%><col width=10%><col width=10%><col width=10%><col width=5%><col width=5%></colgroup>
   <tr align="center">
           <thead>
           <th>&nbsp;</th>
           <th>&nbsp;</th>
           <th align="center">State</th>
           <th>Short Name</th>
           <th>Country</th>
           <th>Active</th>
           <th>Display Order</th>
           </thead>
   </tr>

   <?php   // New Record Insert 	 ?>
	       <tr class="row1" valign="center">
	       <!--NEW ICON-->
	       <td align="center" ><img src="../images/new.gif" title="Add New" onclick="adddata();"/></td><td>&nbsp;</td>
	
	       <td align="center"><div id ="state1" style="display:none;">
           <input type="text" name="state1" id="state1"  size="25" maxlength="50" value="<?php echo $state_name1 ?>" onkeydown="return alphaonly('state1')" ><span class="mand"> *</span></div></td>
           <td align="center"><div id ="short_name1" style="display:none;">
       	   <input type="text" name="short_name1" id="short_name1"  size="5" maxlength="15" value="<?php echo $short_name1 ?>"
	       onkeydown="return alphaonly('short_name1')"><?php echo $mand;?></div></td>
           <?php 	//SHOW COUNTRY DROPDOWN
           mssql_free_result($result);
		   $query = mssql_init('sp_FetchCountry',$mssql);
		   $result = mssql_execute($query);
		   mssql_free_statement($query);	?>
	       <td align="center"><div id ="country_code1" style="display:none;">
	       <select name="country_code1" id="cuntry_code1">          
           <?php echo $Select;?>
   <?php   while($field = mssql_fetch_array($result)) {  ?>
	       <option value="<?php echo $field['Country_Code']?>" <?php if($country_code1==$field['Country_Code']) echo "Selected"; ?> ><?php echo $field['Country']?></option>
   <?php } ?>
	       </select><?php echo $mand;?></div>
	       </td>
	       <td align="center"><div id ="active1" style="display:none;"><select name="active1" id="active1"><option value="Y">Yes</option><option value="N">No</option></select></div></td>
	       <td align="center"><div id ="disporder1" style="display:none;"><input type="text" name="disporder1" id="disporder1"  size="5" maxlength="5" value="<?php echo $disporder1 ?>" onkeydown="return numberonly('disporder1')" ><span class="mand"> *</span></div></td>


   <?php 	// UPDATE & SHOW RECORDS
           mssql_free_result($result);
  	       $query = mssql_init('sp_GetState',$mssql);
		   $result = mssql_execute($query);
		   mssql_free_statement($query);
		   $rs_cnt = mssql_num_rows($result);
		   $colorflag = 0;
		   $i = 0;
    	   while($field = mssql_fetch_array($result))
		{  $i +=1;	$colorflag+=1;
		   $tot_rec = $i;	?>
		   <tr class=<?php if($colorflag%2==0) { echo "row1"; } else { echo "row2"; } ?> valign="center">

		   <input type="hidden" name="state_code<?php echo $i ?>" id ="state_code<?php echo $i ?>" value="<?php echo $field['State_Code']?>"	 />
	       <!-- DELETE ICON-->
           <td align="center"><img src="../images/delete_d.gif" title="Remove" onclick="deldata('<?php echo $field['State_Code'];?>');" />&nbsp;</td>	 
           <!-- EDIT ICON-->
		   <td align="center"><img src="../images/edit.gif" title="Edit" onclick="editdata('<?php echo $i;?>');" />&nbsp;</td>

		   <td ><?php echo $field['State'] ?>
           <div id ="state_<?php echo $i;?>" style="display:none;"><input type="text" name="state_<?php echo $i ?>" id="state_<?php echo $i ?>"  size="25" maxlength="50" value="<?php echo $field['State']?>" onkeydown="return alphaonly('state_<?php echo $i ?>')"></div></td>
		   <td align="center"><?php echo $field['Short_Name'] ?>
           <div id ="short_name_<?php echo $i;?>" style="display:none;">
           <input type="text" name="short_name_<?php echo $i ?>" id="short_name_<?php echo $i ?>"  size="25" maxlength="50" value="<?php echo $field['Short_Name']?>" onkeydown="return alphaonly('short_name_<?php echo $i ?>')"></div></td>

                
                
	<?php 	//SHOW COUNTRY DROPDOWN
           mssql_free_result($result1);
		   $query = mssql_init('sp_FetchCountry',$mssql);
		   $result1 = mssql_execute($query);
		   mssql_free_statement($query);	?>
	       <td align="center"><?php echo $field['Country'] ?>
	       <div id="country_code_<?php echo $i ?>" style="display:none;">
	       <select name="country_code_<?php echo $i ?>" id="cuntry_code_<?php echo $i ?>">
   <?php   while($field1 = mssql_fetch_array($result1)) {  ?>
	       <option value="<?php echo $field1['Country_Code']?>" <?php if($field['Country_Code']==$field1['Country_Code']) echo "Selected";?>><?php echo $field1['Country']?></option>
	 <?php } ?>
	 </select></div>
	 </td>
          <td align="center"><?php if(strtoupper($field['Active'])==="Y") echo "Yes"; else echo "No";?>
		  <div id ="active_<?php echo $i;?>" style="display:none;">
          <select id="active_<?php echo $i ?>" name="active_<?php echo $i ?>">
		  <option value="Y" <?php if(strtoupper($field['Active'])==="Y")  echo "selected" ?>>Yes</option>
		  <option value="N" <?php if(strtoupper($field['Active'])==="N")  echo "selected" ?>>No</option>
		  </select></div></td>

          <td align="center"><?php echo $field['Display_Order'] ?>
		  <div id ="disporder_<?php echo $i;?>" style="display:none;"><input type="text" name="disporder_<?php echo $i ?>" id="disporder_<?php echo $i ?>"  size="5" maxlength="5" value="<?php echo $field['Display_Order']?>" onkeydown="return numberonly('disporder_<?php echo $i ?>')"></div></td>
		  </tr>
	<?php } 
		  //IF ANY ERROR IN EDIT, SHOW THAT ROW
	if ($errcnt > 0) {
	      for($i=0;$i<=$errcnt-1;$i=$i+1){
		  $editcnt = split(',',$_POST['editcnt']);?>
	      <script>editdata(<?php echo $editcnt[$i];?>);</script>
		<?php } 
		} ?>	
		  <input type="hidden" name="tot_rec" value="<?php echo $tot_rec ?>" />
        
	      <tr><td colspan="7" align="right">
		  <input type="submit" name="save" value="Save" class="winbutton_go" />&nbsp;&nbsp;&nbsp;
		  <input type="submit" name="Cancel" value="Cancel" class="winbutton_go"/></td></tr>
    <?php //IF ANY ERROR IN ADD, SHOW THAT ROW ?>
    <?php	if($inserr == 1) { ?><script>adddata();</script><?php } ?>

          </table></td></tr></table>
          </form></body></html>
          <?php include("../includes/copyright.php"); ?>

