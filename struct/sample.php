<?php session_start(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>Isha Foundation - A Non-profit Organization</title>

<?php include("../Includes/header.php"); ?>

<table width="70%" border="0" align="center" cellpadding="3" cellspacing="1">
<tr><td class="head">You are here: Org Structure - <span class="text">Scholrship_Criteria</span></td></tr>
</table><br />
<script>
 
function editval(val)
{	
	if($('#editcnt').val()!="") {
	 var el = $('#editcnt').val()+","+val;
	 $('#editcnt').val(el);

	}
	else { 
		$('#editcnt').val(val)
		}
}

function adddata()
 { 
 	$('#criteria1').show();
	$('#no_of_criteria1').show();
	$('#cost_criteria1').show();
	$('#active1').show();
	var $mode1 ='ADD';
	$('#mode1').val($mode1);
} 


function editdata(val)
{
	editval(val);
	$('#criteria_'+val).show();
	$('#no_of_criteria_'+val).show();
	$('#cost_criteria_'+val).show();
	$('#active_'+val).show();

	var $mode ='EDIT';
	$('#mode').val($mode);

}
function deldata(val)
{ 
	var msg="Do you want to delete?";
	if(!confirm(msg)){
		return false;
	}	 
	else {
		$('#criteria_code').val(val);
		var $mode ='DELETE';
		$('#mode').val($mode);
		$('#myform').submit();
	 }
}
</script>
</head>

<?php 
	$errmsg="";
	$errflag=0;
	$dummy=0;
	$mode		=	$_POST['mode'];
	$mode1		=	$_POST['mode1'];
	$editcnt 	=	split(',',$_POST['editcnt']);		

	if($_POST['Cancel']=="Cancel") { $mode=""; $mode1="";	}
	
	If($mode == "EDIT")
	{ $action=2;
	for($i=0;$i<count($editcnt);$i=$i+1) 
	{ 
		$criteria_code	=	$_POST['criteria_code_'.$editcnt[$i]];
		$criteria     	=	$_POST['criteria_'.$editcnt[$i]];
		$no_of_criteria =   $_POST['no_of_criteria_'.$editcnt[$i]];
        $cost_criteria  =   $_POST['cost_criteria_'.$editcnt[$i]];
		$active			=	$_POST['active_'.$editcnt[$i]];
		$j=$i+1;
		$dummy = Strcheck($criteria,$errmsg,$errflag,"Criteria-".$j);
//		$dummy = Numcheck($no_of_criteria,$errmsg,$errflag,"No of Criteria-".$j);
//		$dummy = Numcheck($cost_criteria,$errmsg,$errflag,"Cost Criteria-".$j);
		if(strlen($no_of_criteria)==0 and strlen($cost_criteria)==0)
		{	$val="";
 			$dummy = Numcheck($val,$errmsg,$errflag,"No.of Criteria / Cost Criteria -".$j);	
		}
			
		if($errflag==0)
		{
			$query = mssql_init('sp_SMS_Scholrship_Criteria',$mssql);
			mssql_bind($query,'@Criteria_Code',$criteria_code,SQLINT4,false,false,5);
			mssql_bind($query,'@Criteria',$criteria,SQLVARCHAR,false,false,50);
	        mssql_bind($query,'@No_of_Criteria',$no_of_criteria,SQLINT4,false,false,5);
		    mssql_bind($query,'@Cost_Criteria',$cost_criteria,SQLINT4,false,false,5);
			mssql_bind($query,'@Active',$active,SQLVARCHAR,false,false,1);
			mssql_bind($query,'@Action',$action,SQLINT4,false,false,1);
			$result = @mssql_execute($query);
			mssql_free_statement($query);
			if($result==1)
				echo "<p class='mesg'>Criteria has been Updated</p>";
			else {
				$errmsg1=mssql_get_last_message();
				$errflag=2;
			}
		}
		else {
			If ($errcnt == 0) { $errcnt = 1;} else { $errcnt = $errcnt + 1; }
			if ($errval == "") { $errval = $editcnt[$i];	 } else { $errval = $errval.",".$editcnt[$i]; } 
		}
	}	
	}

	if($mode=="DELETE")
	{	$action=3;
		$criteria_code	=	$_POST['criteria_code'];

		$query = mssql_init('sp_SMS_Scholrship_Criteria',$mssql);
			mssql_bind($query,'@Criteria_Code',$criteria_code,SQLINT4,false,false,5);
			mssql_bind($query,'@Criteria',$criteria,SQLVARCHAR,false,false,50);
	        mssql_bind($query,'@No_of_Criteria',$no_of_criteria,SQLINT4,false,false,5);
		    mssql_bind($query,'@Cost_Criteria',$cost_criteria,SQLINT4,false,false,5);
			mssql_bind($query,'@Active',$active,SQLVARCHAR,false,false,1);
			mssql_bind($query,'@Action',$action,SQLINT4,false,false,1);
		$result = @mssql_execute($query);
		mssql_free_statement($query);
		if($result==1)
			echo "<p class='mesg'>Criteria has been Deleted</p>";
		else {
			$errmsg1=mssql_get_last_message();
			$errflag=2;
			}
	}
	if($mode1 == "ADD")
	{	$action=1;
		$criteria    =	Trim($_POST['criteria1']);
		$no_of_criteria = Trim($_POST['no_of_criteria1']);
        $cost_criteria = Trim($_POST['cost_criteria1']);
		$active		=	$_POST['active1'];
		                    //error messge//
		$dummy = Strcheck($criteria,$errmsg,$errflag,"Criteria");
//		$dummy = Numcheck($no_of_criteria,$errmsg,$errflag,"No_of_Criteria");
//		$dummy = Numcheck($cost_criteria,$errmsg,$errflag,"Cost_Criteria");
		if(strlen($no_of_criteria)==0 and strlen($cost_criteria)==0)
		{	$val="";
 			$dummy = Numcheck($val,$errmsg,$errflag,"No.of Criteria / Cost Criteria");	
		}		
		
		if($errflag==0){
			$query = mssql_init('sp_SMS_Scholrship_Criteria',$mssql);
			mssql_bind($query,'@Criteria_Code',$criteria_code,SQLINT4,false,false,5);
			mssql_bind($query,'@Criteria',$criteria,SQLVARCHAR,false,false,50);
	        mssql_bind($query,'@No_of_Criteria',$no_of_criteria,SQLINT4,false,false,5);
		    mssql_bind($query,'@Cost_Criteria',$cost_criteria,SQLINT4,false,false,5);
			mssql_bind($query,'@Active',$active,SQLVARCHAR,false,false,1);
			mssql_bind($query,'@Action',$action,SQLINT4,false,false,1);
			
			$result = @mssql_execute($query);
			mssql_free_statement($query);
		if($result==1) {
			echo "<p class='mesg'>Criteria has been Added</p>";
				$action= $criteria  =	$no_of_criteria = $cost_criteria = $active= "";
		}
		else {
			$errmsg1=mssql_get_last_message();
			$errflag=2;
			$inserr=1;
			}
		} else $inserr=1;
	}
	if($errflag==1) 
		echo "<p class='error'>Incomplete / Invalid entried for<br>".$errmsg;
	if($errflag==2) 
		echo "<p class='error'>".$errmsg1;
	
?>
<body style="margin:0;">

<form name="myform" id="myform" method="post" action="SMS_Scholrship_Criteria.php">
    <input type="hidden" name="editcnt" id="editcnt"/>
	<input type="hidden" name="criteria_code" id="criteria_code"/>
    <input type="hidden" name="mode" id="mode"/>	
    <input type="hidden" name="mode1" id="mode1"/>

<table width="70%" height="450" border="0" align="center" cellpadding="3" cellspacing="3">
<tr><td valign="top">
<table width="100%" border="0" align="center" cellpadding="3" cellspacing="1">
<colgroup><col width=3%><col width=3%><col width=11%><col width=10%><col width=10%><col width=5%><col width=5%></colgroup>
<tr align="center">
<thead><th>&nbsp;</th><th>&nbsp;</th><th align="center">Criteria Name</th><th>No. of Criteria</th><th>Cost Criteria</th><th>Active</th></thead></tr>

<?php   // New Record Insert 		 ?>
	<tr class="row1" valign="center">
	
	<td align="center" ><img src="../images/new.gif" title="Add New" onclick="adddata();"/></td><td>&nbsp;</td>
	
	<td align="center"><div id ="criteria1" style="display:none;">
	<input type="text" name="criteria1" id="criteria1"  size="25" maxlength="50" onkeydown="return alphaonly('criteria1')" value="<?php echo $criteria?>">
	<span class="mand"> *</span></div></td>

	<td align="center"><div id ="no_of_criteria1" style="display:none;">
	<input type="text" name="no_of_criteria1" id="no_of_criteria1"  size="5" maxlength="5" value="<?php echo $no_of_criteria ?>" onkeydown="return numberonly('no_of_criteria1')" >
	<span class="mand"> *</span></div></td>

	<td align="center"><div id ="cost_criteria1" style="display:none;">
	<input type="text" name="cost_criteria1" id="cost_criteria1"  size="5" maxlength="5" value="<?php echo $cost_criteria ?>" 
	onkeydown="return numberonly('cost_criteria1')" >
	<span class="mand"> *</span></div></td>

	<td align="center"><div id ="active1" style="display:none;"><select name="active1" id="active1"><option value="Y">Yes</option><option value="N">No</option></select></div></td>
	
	
<?php 	// UPDATE & SHOW RECORDS
		$query = mssql_init('sp_GetScholrship_Criteria',$mssql);
		$result = mssql_execute($query);
		mssql_free_statement($query);
		$rs_cnt = mssql_num_rows($result);
		$colorflag = 0;
		$i = 0;
		while($field = mssql_fetch_array($result))
		{	$i  +=1;	$colorflag+=1;
			$tot_rec = $i;	?>
		<tr class=<?php if($colorflag%2==0) { echo "row1"; } else { echo "row2"; } ?> valign="center">

		<input type="hidden" name="criteria_code_<?php echo $i ?>" id ="criteria_code_<?php echo $i ?>" value="<?php echo $field['Criteria_Code']?>"	 />
		
		<td align="center"><img src="../images/delete_d.gif" title="Remove" onClick="deldata('<?php echo $field['Criteria_Code'];?>');" />&nbsp;</td>	 

		<td align="center"><img src="../images/edit.gif" title="Edit" onClick="editdata('<?php echo $i;?>');" />&nbsp;</td>

		<td ><?php echo $field['Criteria'] ?>
				<div id ="criteria_<?php echo $i;?>" style="display:none;"><input type="text" name="criteria_<?php echo $i ?>" id="criteria_   <?php echo $i ?>"  size="25" maxlength="50" value="<?php echo $field['Criteria']?>"  onkeydown="return alphaonly('criteria_<?php echo $i ?>')"></div></td>

	           <td ><?php echo $field['No_of_Criteria'] ?>
				<div id ="no_of_criteria_<?php echo $i;?>" style="display:none;"><input type="text" name="no_of_criteria_<?php echo $i ?>" id="no_of_criteria_<?php echo $i ?>"  size="25" maxlength="50" value="<?php echo $field['No_of_Criteria']?>"  onkeydown="return numberonly('no_of_criteria_<?php echo $i ?>')"></div></td>

					<td ><?php echo $field['Cost_Criteria'] ?>
				<div id ="cost_criteria_<?php echo $i;?>" style="display:none;"><input type="text" name="cost_criteria_<?php echo $i ?>" id="cost_criteria_<?php echo $i ?>"  size="25" maxlength="50" value="<?php echo $field['Cost_Criteria']?>"  onkeydown="return numberonly('cost_criteria_<?php echo $i ?>')"></div></td>
								
		<td align="center"><?php if(strtoupper($field['Active'])=="Y") echo "Yes"; else echo "No";?>
		<div id ="active_<?php echo $i;?>" style="display:none;">
			<select id="active_<?php echo $i ?>" name="active_<?php echo $i ?>">
		<option value="Y" <?php if(strtoupper($field['Active'])=="Y")  echo "selected" ?>>Yes</option>
		<option value="N" <?php if(strtoupper($field['Active'])=="N")  echo "selected" ?>>No</option>
		</select></div></td>

		
		</tr>
	<?php } 
	if ($errcnt > 0) {
		for($i=0;$i<=$errcnt-1;$i=$i+1){
		$editcnt = split(',',$_POST['editcnt']);?>
			<script>editdata(<?php echo $editcnt[$i];?>);</script>
		<?php } 
		} ?>	
		<input type="hidden" name="tot_rec" value="<?php echo $tot_rec ?>" />
	<tr><td colspan="6" align="right">
		<input type="submit" name="save" value="Save" class="winbutton_go" />&nbsp;&nbsp;&nbsp;
		<input type="submit" name="Cancel" value="Cancel" class="winbutton_go"/></td></tr>

<?php	if($inserr == 1) { ?><script>adddata();</script><?php } ?>

</table></td></tr></table>
</form></body></html>
<?php include("../Includes/copyright.php"); ?>