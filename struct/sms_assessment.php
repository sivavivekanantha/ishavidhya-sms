<?php session_start(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>Isha Foundation - A Non-profit Organization</title>

<?php include("../includes/header.php");
title('Student Management','Assessment',2,1,0);
 ?>


<script>

// display all the text box fields for add assessment
function adddata()
 { 
 	$('#E_Assessment1').show();
	$('#T_Assessment1').show();
	$('#active1').show();
	$('#disporder1').show();
	var $mode1 ='ADD';
	$('#mode1').val($mode1);
} 

// display all the text box fields for update assessment
function editdata(val)
{   
	 if($('#editcnt').val() >0 ) return false;
    $('#editcnt').val(val);
	$('#e_assessment_'+val).show();
	$('#t_assessment_'+val).show();
	$('#active_'+val).show();
	$('#disporder_'+val).show();	
	var $mode ='EDIT';
	$('#mode').val($mode);

}

// delete the assessment
function deldata(val)
{ 
	var msg="Do you want to delete?";
	if(!confirm(msg)){
		return false;
	}	 
	else {
		$('#ass_code').val(val);
		var $mode ='DELETE';
		$('#mode').val($mode);
		$('#myform').submit();
	 }
}
</script>
</head>

<?php 
	$errmsg="";
	$errflag=0;
	$dummy=0;
	$mode		=	$_POST['mode'];
	$mode1		=	$_POST['mode1'];
	$editcnt 	=	split(',',$_POST['editcnt']);		

	//cancel the display mode 
	if($_POST['Cancel']=="Cancel") 
	{ 
		$mode=""; $mode1="";
	}
	
	//edit the assessment
	if($mode == "EDIT")
	{ 
			$action=2;

			for($i=0;$i<count($editcnt);$i++) 
			{ 	
				$ass_code	    =	trim($_POST['ass_code_'.$editcnt[$i]]);
				$e_assessment	=	trim($_POST['e_assessment_'.$editcnt[$i]]);
				$t_assessment	=	trim($_POST['t_assessment_'.$editcnt[$i]]);
				$active			=	trim($_POST['active_'.$editcnt[$i]]);
				$disporder		=	trim($_POST['disporder_'.$editcnt[$i]]);
				
				$j=$i+1;
				
				//error message
				$dummy = Strcheck($e_assessment,$errmsg,$errflag,"English Assessment-".$j);
				$dummy = Strcheck($t_assessment,$errmsg,$errflag,"Tamil Assessment-".$j);
				$dummy = Strcheck($disporder,$errmsg,$errflag,"Display Order-".$j);
				
				if($errflag==0)
				{    
				     mssql_free_result($result);
					$query = mssql_init('sp_Assessment',$mssql);
					mssql_bind($query,'@Ass_Code',$ass_code,SQLINT4,false,false,5);
					mssql_bind($query,'@E_Assessment',$e_assessment,SQLVARCHAR,false,false,1000);
					mssql_bind($query,'@T_Assessment',$t_assessment,SQLTEXT,false,false,1000);
					mssql_bind($query,'@Active',$active,SQLVARCHAR,false,false,1);
					mssql_bind($query,'@Disp_Order',$disporder,SQLINT4,false,false,5);
					mssql_bind($query,'@Action',$action,SQLINT4,false,false,1);
					$result = @mssql_execute($query);
					mssql_free_statement($query);
					if($result==1)
						echo "<p class='mesg'>Assessment has been Updated</p>";
					else {
						$errmsg1=mssql_get_last_message();
						$errflag=2;
					If ($errcnt == 0) { $errcnt = 1;} else { $errcnt = $errcnt + 1; }
					if ($errval == "") { $errval = $editcnt[$i];	 } else { $errval = $errval.",".$editcnt[$i]; }
					}
				}
				else {
					If ($errcnt == 0) { $errcnt = 1;} else { $errcnt = $errcnt + 1; }
					if ($errval == "") { $errval = $editcnt[$i];	 } else { $errval = $errval.",".$editcnt[$i]; } 
				}
			}	
	}

	//delete the assessment
	if($mode=="DELETE")
	{	$action=3;
		$ass_code	=	trim($_POST['ass_code']);
        mssql_free_result($result);
		$query = mssql_init('sp_Assessment',$mssql);
		mssql_bind($query,'@Ass_Code',$ass_code,SQLINT4,false,false,5);
		mssql_bind($query,'@E_Assessment',$e_assessment,SQLVARCHAR,false,false,50);
		mssql_bind($query,'@T_Assessment',$t_assessment,SQLTEXT,false,false,25);
		mssql_bind($query,'@Active',$active,SQLVARCHAR,false,false,1);
		mssql_bind($query,'@Disp_Order',$disporder,SQLINT4,false,false,5);
		mssql_bind($query,'@Action',$action,SQLINT4,false,false,1);
		$result = @mssql_execute($query);
		mssql_free_statement($query);
		if($result==1)
			echo "<p class='mesg'>Assessment has been Deleted</p>";
		else {
			$errmsg1=mssql_get_last_message();
			$errflag=2;
			}
	}
	
	//add the assessment
	if($mode1 == "ADD")
	{	$action1=1;
		$e_assessment1	=trim($_POST['E_Assessment1']);
		
		$t_assessment1	=trim($_POST['T_Assessment1']);
        
        $active1		=trim($_POST['active1']);
		$disporder1		=trim($_POST['disporder1']);

		$dummy = Strcheck($e_assessment1,$errmsg,$errflag,"English Assessment");
		$dummy = Strcheck($t_assessment1,$errmsg,$errflag,"Tamil Assessment");
		$dummy = Strcheck($disporder1,$errmsg,$errflag,"Display Order");
		
		if($errflag==0){
             
            mssql_free_result($result);
			$query = mssql_init('sp_Assessment',$mssql);
			mssql_bind($query,'@Ass_Code',$ass_code1,SQLINT4,false,false,5);
			mssql_bind($query,'@E_Assessment',$e_assessment1,SQLVARCHAR,false,false,1000);
			mssql_bind($query,'@T_Assessment',$t_assessment1,SQLTEXT,false,false,1000);			
			mssql_bind($query,'@Active',$active1,SQLVARCHAR,false,false,1);
			mssql_bind($query,'@Disp_Order',$disporder1,SQLINT4,false,false,5);
			mssql_bind($query,'@Action',$action1,SQLINT4,false,false,1);
			
			$result = @mssql_execute($query);
			mssql_free_statement($query);
		if($result==1)
		{
			echo "<p class='mesg'>Assessment has been Added</p>";
			$e_assessment1	="";
			$t_assessment1	=""; 
			$disporder1		="";
			}
		else {
			$errmsg1=mssql_get_last_message();
			$errflag=2;
			$inserr=1;
			}
		} else $inserr=1;
	}
	if($errflag==1) 
		echo $errlbl.$errmsg;
	if($errflag==2) 
		echo "<p class='error'>".$errmsg1;
	
?>
<body style="margin:0;">

<form name="myform" id="myform" method="post" action="sms_assessment.php">
<input type="hidden" name="editcnt" id="editcnt"/>
<input type="hidden" name="ass_code" id="ass_code"/>
<input type="hidden" name="mode" id="mode"/>	
<input type="hidden" name="mode1" id="mode1"/>

<table width="90%" height="450" border="0" align="center" cellpadding="3" cellspacing="3">
<tr><td valign="top">
<table width="100%" border="0" align="center" cellpadding="3" cellspacing="1">
<?php titleheader(Assessment,0);?>
<tr align="center">
<thead><colgroup><col width=3%><col width=3%><col width=20%><col width=20%><col width=10%><col width=10%><col width=5%></colgroup>
<th>&nbsp;</th><th>&nbsp;</th><th align="left">English Assessment</th><th>Tamil Assessment</th>
<th>Active</th><th>Display Order</th>
</thead></tr>

<?php   // New Record Insert
		$colorflag+=1; ?>
	<tr class=<?php if($colorflag%2==0) { echo "row2"; } else { echo "row1"; } ?> valign="center">
	
	<td align="center" ><img src="../images/new.gif" title="Add New" onclick="adddata();"/></td><td>&nbsp;</td>
	
	<td><div id ="E_Assessment1" style="display:none;">
      <textarea name="E_Assessment1" id="E_Assessment1" rows="5" cols="45"><?php echo trim(str_replace("\r\n","\n",$e_assessment1));?></textarea>    
<?php echo $mand; ?></div></td>
	<td><div id ="T_Assessment1" style="display:none;">
    <textarea name="T_Assessment1" id="T_Assessment1" rows="5" cols="45"><?php echo trim(str_replace("\r\n","\n",$t_assessment1));?>
    </textarea><?php echo $mand; ?></div></td>
	
	<td align="center" valign="top"><div id ="active1" style="display:none;"><select name="active1" id="active1"><option value="Y">Yes</option><option value="N">No</option></select></div></td>
	
	<td align="center" valign="top"><div id ="disporder1" style="display:none;"><input type="text" name="disporder1" id="disporder1"  size="5" maxlength="5" value="<?php echo $disporder1 ?>" onkeydown="return numberonly('disporder1')"><?php echo $mand; ?></div></td>

<?php 	// UPDATE & SHOW RECORDS
        mssql_free_result($result);
		$query = mssql_init('sp_GetAssessment',$mssql);
		$result = mssql_execute($query);
		mssql_free_statement($query);
		$rs_cnt = mssql_num_rows($result);
		$colorflag = 0;
		$i = 0;
		while($field = mssql_fetch_array($result))
		{	$i  +=1;	$colorflag+=1;
			$tot_rec = $i;	
			$ass_code = $field['Ass_Code'];
			$e_assessment = $field['E_Assessment'];
			$t_assessment = $field['T_Assessment'];
			$active = strtoupper($field['Active']);
			$disp_order = $field['Display_Order'];		?>
			
		<tr class=<?php if($colorflag%2==0) { echo "row1"; } else { echo "row2"; } ?> valign="center">

		<input type="hidden" name="ass_code_<?php echo $i ?>" id ="ass_code_<?php echo $i ?>" value="<?php echo $ass_code; ?>"	 />
		
		<td align="center"><img src="../images/delete_d.gif" title="Remove" onclick="deldata('<?php echo $field['Ass_Code'];?>');" />&nbsp;</td>	 

		<td align="center"><img src="../images/edit.gif" title="Edit" onclick="editdata('<?php echo $i;?>');" />&nbsp;</td>

		<td ><?php echo  trim($field['E_Assessment']) ?>
				<div id ="e_assessment_<?php echo $i;?>" style="display:none;">                
        <textarea name="e_assessment_<?php echo $i ?>" id="e_assessment_<?php echo $i ?>" cols="45" rows="5"><?php echo trim($field['E_Assessment']);?>              
        </textarea>     
        </div></td>
		<td><?php echo $field['T_Assessment'] ?>        
				<div id ="t_assessment_<?php echo $i;?>" style="display:none;">
                <textarea name="t_assessment_<?php echo $i ?>" id="t_assessment_<?php echo $i ?>" cols="45" rows="5"><?php echo trim($field['T_Assessment']);?> 
               </textarea>                    
         </div></td>
	
		<td align="center"><?php if(strtoupper($field['Active'])==="Y") echo "Yes"; else echo "No";?>
		<div id ="active_<?php echo $i;?>" style="display:none;">
			<select id="active_<?php echo $i ?>" name="active_<?php echo $i ?>">
            
		<option value="Y" <?php if($active=="Y")  echo "selected" ?>>Yes</option>
		<option value="N" <?php if($active=="N")  echo "selected" ?>>No</option>
		</select></div></td>

		<td align="center"><?php echo $field['Display_Order'] ?>
		<div id ="disporder_<?php echo $i;?>" style="display:none;"><input type="text" name="disporder_<?php echo $i ?>" id="disporder_<?php echo $i ?>"  size="5" maxlength="5" value="<?php echo $disp_order?>" onkeydown="return numberonly('disporder_<?php echo $i ?>')"></div></td>
				
		</tr>
	<?php } 
	if ($errcnt > 0) {
		for($i=0;$i<=$errcnt-1;$i=$i+1){
		$editcnt = split(',',$_POST['editcnt']);?>
			<script>editdata(<?php echo $editcnt[$i];?>);</script>
		<?php } 
		} ?>	
		<input type="hidden" name="tot_rec" value="<?php echo $tot_rec ?>" />
	<tr><td colspan="6" align="right">
		<input type="submit" name="save" value="Save" class="winbutton_go" />&nbsp;&nbsp;&nbsp;
		<input type="submit" name="Cancel" value="Cancel" class="winbutton_go"/></td></tr>

<?php	if($inserr == 1) { ?><script>adddata();</script><?php } ?>

</table></td></tr></table>
</form></body></html>
<?php include("../Includes/copyright.php"); ?>