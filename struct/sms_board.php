<?php session_start(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>Isha Foundation - A Non-profit Organization</title>

<?php include("../includes/header.php");
title('Student Management','Board',2,1,0);
 ?>

<script> 
function adddata()
 { 
 	$('#board1').show();
	$('#active1').show();
	$('#displayorder1').show();
	var $mode1 ='ADD';
	$('#mode1').val($mode1);
} 
function editdata(val)
{
	if($('#editcnt').val() >0 ) return false;
    $('#editcnt').val(val);
	$('#board_'+val).show();
	$('#active_'+val).show();
	$('#displayorder_'+val).show();	
	var $mode ='EDIT';
	$('#mode').val($mode);

}
function deldata(val)
{ 
	var msg="Do you want to delete?";
	if(!confirm(msg)){
		return false;
	}	 
	else {
		$('#board_code').val(val);
		var $mode ='DELETE';
		$('#mode').val($mode);
		$('#myform').submit();
	 }
}
</script>
</head>
<?php 
    //VARIABLE DECLARATION HERE
	$errmsg="";
	$errflag=0;
	$dummy=0;
	// GET MODE VALUE FOR WHICH FUNCTION PERFORMED (ADD / EDIT/ DELETE)
	$mode		=	trim($_POST['mode']);
	$mode1		=	trim($_POST['mode1']);
	$editcnt 	=	split(',',$_POST['editcnt']);		

	if($_POST['Cancel']=="Cancel") { $mode=""; $mode1="";	}
	//RECORD EDIT HERE
	If($mode == "EDIT")
	{
	  $action=2;
	 //EDIT ROW VALUES ARE GETTING
	for($i=0;$i<count($editcnt);$i=$i+1) 
	{ 
		$board_code   	=	trim($_POST['board_code_'.$editcnt[$i]]);
		$board			=	trim($_POST['board_'.$editcnt[$i]]);
		$active			=	trim($_POST['active_'.$editcnt[$i]]);
		$displayorder	=	trim($_POST['displayorder_'.$editcnt[$i]]);
		$j=$i+1;
		//INPUT VALIDATE HERE
		$dummy = Strcheck($board,$errmsg,$errflag,"Board-".$j);
		$dummy = Strcheck($displayorder,$errmsg,$errflag,"Display Order-".$j);
	    //ALL INPUTS ARE CORRECTED, THEN GOTO SP
		if($errflag==0)
		{    
            mssql_free_result($result);
			$query = mssql_init('sp_Board',$mssql);
			mssql_bind($query,'@Board_Code',$board_code,SQLINT4,false,false,5);
			mssql_bind($query,'@Board',$board,SQLVARCHAR,false,false,500);
			mssql_bind($query,'@Active',$active,SQLVARCHAR,false,false,1);
			mssql_bind($query,'@Display_Order',$displayorder,SQLINT4,false,false,5);
			mssql_bind($query,'@Action',$action,SQLINT4,false,false,1);
			$result = @mssql_execute($query);//SP EXECUTE HERE
			mssql_free_statement($query);//$QUERY FREE HERE
			if($result==1)
				echo "<p class='mesg'>Board has been Updated</p>";
			else {
				$errmsg1=mssql_get_last_message();
				$errflag=2;
			}
		}
		else {
			If ($errcnt == 0) { $errcnt = 1;} else { $errcnt = $errcnt + 1; }
			if ($errval == "") { $errval = $editcnt[$i];	 } else { $errval = $errval.",".$editcnt[$i]; } 
		}
	}	
	}
        //RECORD DELTE HERE
	    if($mode=="DELETE")
		{	$action=3;
			$board_code	=	$_POST['board_code'];
            
             mssql_free_result($result);
			$query = mssql_init('sp_Board',$mssql);
			mssql_bind($query,'@Board_Code',$board_code,SQLINT4,false,false,5);
			mssql_bind($query,'@Board',$board,SQLVARCHAR,false,false,50);
			mssql_bind($query,'@Active',$active,SQLVARCHAR,false,false,1);
			mssql_bind($query,'@Display_Order',$displayorder,SQLINT4,false,false,5);
			mssql_bind($query,'@Action',$action,SQLINT4,false,false,1);
			$result = @mssql_execute($query);//SP EXECUTE HERE
			mssql_free_statement($query);//$QUERY FREE HERE
			if($result==1)
				echo "<p class='mesg'>Board has been Deleted</p>";
			else {
			$errmsg1=mssql_get_last_message();
			$errflag=2;
			}
	}
	    //RECORD ADD HERE
	    if($mode1 == "ADD")
		{	$action1=1;
			$board1      	    =	trim($_POST['board1']);
			$active1			=	trim($_POST['active1']);
			$displayorder1	    =	trim($_POST['displayorder1']);
			//VALIDATE THE INPUT
			$dummy = Strcheck($board1,$errmsg,$errflag,"Board");
			$dummy = Strcheck($displayorder1,$errmsg,$errflag,"Display Order");
		
		if($errflag==0){
             //SP INITIALIZED AND BACKEND CONNECTION
            mssql_free_result($result);
			$query = mssql_init('sp_Board',$mssql);
			mssql_bind($query,'@Board_Code',$board_code1,SQLINT4,false,false,5);
			mssql_bind($query,'@Board',$board1,SQLVARCHAR,false,false,50);
			mssql_bind($query,'@Active',$active1,SQLVARCHAR,false,false,1);
			mssql_bind($query,'@Display_Order',$displayorder1,SQLINT4,false,false,5);
			mssql_bind($query,'@Action',$action1,SQLINT4,false,false,1);
			
			$result = @mssql_execute($query);//SP EXECUTE HERE
			mssql_free_statement($query);//$QUERY FREE HERE
		if($result==1){
			echo "<p class='mesg'>Board has been Added</p>";
			$board1 ="";
			$displayorder1="";}
		else {
			$errmsg1=mssql_get_last_message();//GET ERROR MESSAGE FROM SP
			$errflag=2;
			$inserr=1;
			}
		} else $inserr=1;
	}
	if($errflag==1) 
		echo $errlbl.$errmsg;
	if($errflag==2) 
		echo "<p class='error'>".$errmsg1;
	
    ?>
			<!--FORM DESIGN AND HIDDEN VALUES-->
			<body style="margin:0;">
			<form name="myform" id="myform" method="post" action="sms_board.php">
			<input type="hidden" name="editcnt" id="editcnt"/>
			<input type="hidden" name="board_code" id="board_code"/>
			<input type="hidden" name="mode" id="mode"/>
			<input type="hidden" name="mode1" id="mode1"/>
			<table width="70%" height="450" border="0" align="center" cellpadding="3" cellspacing="3">
			<tr><td valign="top">
			<table width="100%" border="0" align="center" cellpadding="3" cellspacing="1">
			<?php titleheader(Board,0);?>
			<tr align="center">
			<thead><colgroup><col width=3%><col width=3%><col width=20%><col width=10%><col width=10%><col width=5%><col width=5%></colgroup>
			<th>&nbsp;</th><th>&nbsp;</th><th align="center">Board</th>
			<th>Active</th><th>Display Order</th>
			</thead></tr>

			<?php   // New Record Insert
				$colorflag+=1; ?>
			<tr class=<?php if($colorflag%2==0) { echo "row2"; } else { echo "row1"; } ?> valign="center">
			<td align="center" >
			<!--ADD NEW ICON-->
			<img src="../images/new.gif" title="Add New" onclick="adddata();"/></td><td>&nbsp;</td>
			<td align="center"><div id ="board1" style="display:none;"><input type="text" name="board1" id="board1"  size="25" maxlength="30" onkeydown="return alphaonly('board1')" value="<?php echo $board1 ?>"><?php echo $mand; ?></div></td>
			<td align="center"><div id ="active1" style="display:none;"><select name="active1" id="active1"><option  value="Y">Yes</option><option value="N">No</option></select></div></td>
			<td align="center"><div id ="displayorder1" style="display:none;"><input type="text" name="displayorder1" id="displayorder1"  size="5" maxlength="3" value="<?php echo $displayorder1 ?>" onkeydown="return numberonly('displayorder1')"><?php echo $mand; ?></div></td>
			<?php 	// UPDATE & SHOW RECORDS
            mssql_free_result($result);
			$query = mssql_init('sp_GetBoard',$mssql);
			$result = mssql_execute($query);
			mssql_free_statement($query);
			$rs_cnt = mssql_num_rows($result);
			$colorflag = 0;
			$i = 0;
			while($field = mssql_fetch_array($result))
			{	$i  +=1;	$colorflag+=1;
				$tot_rec = $i;	?>
			<tr class=<?php if($colorflag%2==0) { echo "row1"; } else { echo "row2"; } ?> valign="center">
			<input type="hidden" name="board_code_<?php echo $i ?>" id ="board_code_<?php echo $i ?>" value="<?php echo $field['Board_Code']?>"	 />
			<td align="center">
			<!--DELETE ICON-->
			<img src="../images/delete_d.gif" title="Remove" onclick="deldata('<?php echo $field['Board_Code'];?>');" />&nbsp;</td>	 
			<td align="center">
			<!-- EDIT ICON-->
			<img src="../images/edit.gif" title="Edit" onclick="editdata('<?php echo $i;?>');" />&nbsp;</td>
			<td ><?php echo $field['Board'] ?>
			<div id ="board_<?php echo $i;?>" style="display:none;"><input type="text" name="board_<?php echo $i ?>" id="board_ <?php echo $i ?>"  size="25" maxlength="30" value="<?php echo $field['Board']?>"  onkeydown="return alphaonly('board_<?php echo $i ?>')"></div></td>
			<td align="center"><?php if(strtoupper($field['Active'])=="Y") echo "Yes"; else echo "No";?>
			<div id ="active_<?php echo $i;?>" style="display:none;">
			<select id="active_<?php echo $i ?>" name="active_<?php echo $i ?>">
			<option value="Y" <?php if(strtoupper($field['Active'])=="Y")  echo "selected" ?>>Yes</option>
			<option value="N" <?php if(strtoupper($field['Active'])=="N")  echo "selected" ?>>No</option>
			</select></div></td>
			<td align="center"><?php echo $field['Display_Order'] ?>
			<div id ="displayorder_<?php echo $i;?>" style="display:none;"><input type="text" name="displayorder_<?php echo $i ?>" id="displayorder_<?php echo $i ?>"  size="5" maxlength="3" value="<?php echo $field['Display_Order']?>" onkeydown="return numberonly('displayorder_<?php echo $i ?>')"></div></td>
			</tr>
			<?php } 
			//IF ANY ERROR IN EDIT, SHOW THAT ROW
			if ($errcnt > 0) {
			for($i=0;$i<=$errcnt-1;$i=$i+1){
			$editcnt = split(',',$_POST['editcnt']);?>
			<script>editdata(<?php echo $editcnt[$i];?>);</script>
			<?php } 
			} ?>	
			<input type="hidden" name="tot_rec" value="<?php echo $tot_rec ?>" />
			<tr><td colspan="5" align="right">
			<input type="submit" name="save" value="Save" class="winbutton_go" />&nbsp;&nbsp;&nbsp;
			<input type="submit" name="Cancel" value="Cancel" class="winbutton_go"/></td></tr>
			<?php //IF ANY ERROR IN ADD, SHOW THAT ROW ?>
			<?php	if($inserr == 1) { ?><script>adddata();</script><?php } ?>
			</table></td></tr></table>
			</form></body></html>
			<?php include("../includes/copyright.php"); ?>