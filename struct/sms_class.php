<?php session_start();?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Isha Foundation - A Non-profit Organization</title>
<?php include("../includes/header.php");
 title('Student Management','Class',2,1,0); ?>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<style type="text/css">
<!--
.style1 {color: #CCFFFF}
-->
</style>
<script>

function adddata(){
$('#Class').show();
$('#displayorder').show();
$('#active').show();
var $mode1 ='ADD';
$('#mode1').val($mode1);
}
function editdata(val)
{	
	if($('#editcnt').val() >0 ) return false;
    $('#editcnt').val(val);
	
	$('#Class'+val).show();
	$('#displayorder'+val).show();
	$('#active'+val).show();	
	var $mode ='EDIT';
	$('#mode').val($mode);

}
function deldata(val)
{ 

	var msg="Do you want to delete?";
	if(!confirm(msg)){
		return false;
	}	 
	else { 
		$('#Class_code').val(val);
		
		var $mode ='DELETE';
		$('#mode').val($mode);
		$('#myform').submit();
	 }
}


</script>
</head>
<?php 
	$errmsg="";
	$errflag=0;
	$dummy=0;
	$mode		=	$_POST['mode'];
	$mode1		=	$_POST['mode1'];
	$editcnt 	=	split(',',$_POST['editcnt']);
	if($_POST['Cancel']=="Cancel") { $mode=""; $mode1="";	
	}
	
	
	if($mode1 == "ADD")
	{	$action1=1;
		$Class1	=Trim($_POST['Class']);		
		$Display_Order1	=Trim($_POST['displayorder']);		
		$Active1=Trim($_POST['active']);
			
		$dummy = Strcheck($Class1,$errmsg,$errflag,"Class");		
		$dummy = Strcheck($Display_Order1,$errmsg,$errflag,"Display Order");	
		$dummy = Strcheck($Active1,$errmsg,$errflag,"Active");
		
		if($errflag==0)
		{
		    mssql_free_result($result);
			$query = mssql_init('[sp_SmsClass]',$mssql);		
			mssql_bind($query,'@Class_Code',$Class_code1,SQLINT4,false,false,5);
			mssql_bind($query,'@Class',$Class1,SQLVARCHAR,false,false,50);		
			mssql_bind($query,'@Display_Order',$Display_Order1,SQLINT4,false,false,5);
			mssql_bind($query,'@Active',$Active1,SQLVARCHAR,false,false,1);			
			mssql_bind($query,'@Action',$action1,SQLINT4,false,false,1);
				
			$result = @mssql_execute($query);		
			
			mssql_free_statement($query);
			
		if($result==1)
		{
			echo "<p class='mesg'>Class has been Added</p>";
			$Class1="";
			$Display_Order1="";
		}
		else {
		
			$errmsg1=mssql_get_last_message();
			$errflag=2;
			$inserr=1;
			}
		} else $inserr=1;
	}
	if($mode == "EDIT")	
	{ $action=2;
	for($i=0;$i<count($editcnt);$i=$i+1) 
	{ 
		$Class_code=Trim($_POST['Class_code'.$editcnt[$i]]);	
		$Class  =Trim($_POST['Class'.$editcnt[$i]]);			
		$active	=Trim($_POST['active'.$editcnt[$i]]);
	
		$displayorder		=Trim($_POST['displayorder'.$editcnt[$i]]);	
		$j=$i+1;
				
		$dummy = Strcheck($Class,$errmsg,$errflag,"Class".$j);	
		$dummy = Strcheck($active,$errmsg,$errflag,"Active".$j);	
		$dummy = Strcheck($displayorder,$errmsg,$errflag,"Display order".$j);

		if($errflag==0)
		{
		    mssql_free_result($result);
			$query = mssql_init('sp_SmsClass',$mssql);       
			mssql_bind($query,'@Class_Code',$Class_code,SQLINT4,false,false,5);
			mssql_bind($query,'@Class',$Class,SQLVARCHAR,false,false,50);		
			mssql_bind($query,'@Display_Order',$displayorder,SQLINT4,false,false,5);
			mssql_bind($query,'@Active',$active,SQLVARCHAR,false,false,1);			
			mssql_bind($query,'@Action',$action,SQLINT4,false,false,1);
			
			$result = @mssql_execute($query);
			mssql_free_statement($query);
			if($result==1)
				echo "<p class='mesg'>Class has been Updated</p>";
			else {
				$errmsg1=mssql_get_last_message();
				$errflag=2;
				If ($errcnt == 0) { $errcnt = 1;} else { $errcnt = $errcnt + 1; }
			if ($errval == "") { $errval = $editcnt[$i];	 } else { $errval = $errval.",".$editcnt[$i]; } 
				}
		}
		else {
			If ($errcnt == 0) { $errcnt = 1;} else { $errcnt = $errcnt + 1; }
			if ($errval == "") { $errval = $editcnt[$i];	 } else { $errval = $errval.",".$editcnt[$i]; } 
		}
	}	
	}
	
	
	if($mode=="DELETE")
	{	
		$action=3;
		$Class_code=$_POST['Class_code'];
        mssql_free_result($result);
		$query = mssql_init('[sp_SmsClass]',$mssql);		
			mssql_bind($query,'@Class_Code',$Class_code,SQLINT4,false,false,5);
			mssql_bind($query,'@Class',$Class,SQLVARCHAR,false,false,50);		
			mssql_bind($query,'@Display_Order',$Display_Order,SQLINT4,false,false,5);
			mssql_bind($query,'@Active',$Active,SQLVARCHAR,false,false,1);			
			mssql_bind($query,'@Action',$action,SQLINT4,false,false,1);
				
			
			$result = @mssql_execute($query);
		mssql_free_statement($query);
		if($result==1)
			echo "<p class='mesg'>Class has been Deleted</p>";
		else {

			$errmsg1=mssql_get_last_message();	
			echo "Class_code=".$Class_code;		
			$errflag=2;
			}
	}
	
	if($errflag==1) 
		echo $errlbl.$errmsg."</p>";
	if($errflag==2) 
		echo "<p class='error'>".$errmsg1."</p>";
?>	
	
		
<body style="margin:0;">

<form name="myform" id="myform" method="post" action="sms_class.php">
<input type="hidden" name="Class_code" id="Class_code"/>
<input type="hidden" name="mode" id="mode"/>	
<input type="hidden" name="mode1" id="mode1"/>
<input type="hidden" name="editcnt" id="editcnt"/>

<table width="100%" height="450" border="0" align="center" cellpadding="3" cellspacing="3">
<tr>
    <td valign="top"><table width="70%" border="0" align="center" cellpadding="3" cellspacing="1">
	<?php titleheader('Class',0);?>
	   
      <tr align="center"> </tr>
      <thead><colgroup>
        <col width="15%" />
        <col width="15%" />
        <col width="30%" />
        <col width="20%" />
        <col width="20%" />
      
        </colgroup>
    </thead>
      <tr>
      <thead>
        <th>&nbsp;</th>
        <th>&nbsp;</th>
        <th >Class</th>              
        <th>Display Order</th>
        <th>Active</th>
        <td></thead>
        </td></tr>
           <?php   // New Record Insert
		$colorflag+=1; ?>
        <tr class=<?php if($colorflag%2==0) { echo "row2"; } else { echo "row1"; } ?> valign="center"> 
         <td align="center" ><img src="../images/new.gif" title="Add New" onclick="adddata();"/></td>
         <td>&nbsp;</td>
         <td align="center"><div id="Class" style="display:none;" ><input type="text" name="Class" id="Class" size="20" maxlength="10" value="<?php echo $Class1 ?>"onkeydown="return alphaonly('Class')" ><?php echo $mand; ?> </div></td>
         <td align="center"><div id="displayorder" style="display:none;"><input type="text" id="displayorder" name="displayorder" size="5" maxlength="3" value="<?php echo $Display_Order1 ?>" onkeydown="return numberonly('displayorder')"/><?php echo $mand; ?></div></td>
   <td align="center"><div id="active" style="display:none;"><select name="active" id="active"><option value="Y">Yes</option><option value="N">No</option></select></div></td>
     
        
        
        </tr>
         <?php 	// UPDATE & SHOW RECORDS
        mssql_free_result($result);
		$query = mssql_init('sp_GetSmsClass',$mssql);
		$result = mssql_execute($query);
		mssql_free_statement($query);
		$rs_cnt = mssql_num_rows($result);
		$colorflag = 0;
		$i = 0;
		while($field = mssql_fetch_array($result))
		{	
		$i  +=1;	$colorflag+=1;
			$tot_rec = $i;	?> 
		      
        
        <tr class=<?php if($colorflag%2==0) { echo "row1"; } else { echo "row2"; } ?> valign="center">
		<input type="hidden" name="Class_code<?php echo $i ?>" id ="Class_code<?php echo $i ?>" value="<?php echo $field['Class_Code']?>"	 />	        	
		<td align="center">
	<?php /*	<img src="../images/delete_d.gif" title="Remove" onClick="deldata('<?php echo $field['Class_Code'];?>');" /> */ ?>
		&nbsp;</td>
        <td align="center"><img src="../images/edit.gif" title="Edit" onclick="editdata('<?php echo $i;?>');" />&nbsp;</td>
        
       <td align="center"><?php echo $field['Class'] ?>       
        	<div id ="Class<?php echo $i;?>" style="display:none;"><input type="text" name="Class<?php echo $i ?>" id="Class<?php echo $i ?>"  size="25" maxlength="10" value="<?php echo $field['Class']?>"  onkeydown="return alphaonly('Class<?php echo $i ?>')"></div></td> 
        
        <td align="center"><?php echo $field['Display_Order'] ?>
		<div id ="displayorder<?php echo $i;?>" style="display:none;"><input type="text" name="displayorder<?php echo $i ?>" id="displayorder<?php echo $i ?>"  size="5" maxlength="3" value="<?php echo $field['Display_Order']?>" onkeydown="return numberonly('displayorder<?php echo $i ?>')"></div></td>
        
          <td align="center"><?php if(strtoupper($field['Active'])=="Y") echo "Yes"; else echo  "No";?>
		<div id ="active<?php echo $i;?>" style="display:none;">
			<select id="active<?php echo $i ?>" name="active<?php echo $i ?>">
            
		<option value="Y" <?php if(strtoupper($field['Active'])==="Y")  echo "selected" ?>>Yes</option>
		<option value="N" <?php if(strtoupper($field['Active'])==="N")  echo "selected" ?>>No</option>
		</select></div></td>    
        
        </tr>  
          <?php } 
	if ($errcnt > 0) {
		for($i=0;$i<=$errcnt-1;$i=$i+1){
		$editcnt = split(',',$_POST['editcnt']);?>
			<script>editdata(<?php echo $editcnt[$i];?>);</script>
		<?php } 
		} ?>    
        
        <input type="hidden" name="tot_rec" value="<?php echo $tot_rec ?>" />
            <tr><td colspan="7" align="right">
		<input type="submit" name="save" value="Save" class="winbutton_go" />&nbsp;&nbsp;&nbsp;
		<input type="submit" name="Cancel" value="Cancel" class="winbutton_go"/></td></tr>
       
        </table>        
        </td></tr>
        
        </table>
 <?php	if($inserr == 1) { ?><script>adddata();</script><?php } ?>

</body>
</html>
<?php include("../includes/copyright.php"); ?>