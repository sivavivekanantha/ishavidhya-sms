<?php session_start();?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Isha Foundation - A Non-profit Organization</title>
<?php include("../includes/header.php"); 
 title('Student Management','Grade',2,1,0);

?>
<script>
function adddata()
{
	$('#grade1').show();
	$('#tmarkfrom1').show();
	$('#tmarkto1').show();
   	$('#fmarkfrom1').show();
	$('#fmarkto1').show();
    $('#smarkfrom1').show();
	$('#smarkto1').show();
	$('#ecomment1').show();
	$('#tcomment1').show();
	$('#displayorder1').show();
	$('#mode1').val('ADD');
}


function editdata(val)
{ 
	if($('#editcnt').val() >0 ) return false;
    $('#editcnt').val(val);

	$('#grade_'+val).show();
	$('#tmarkfrom_'+val).show();
	$('#tmarkto_'+val).show();
   	$('#fmarkfrom_'+val).show();
    $('#fmarkto_'+val).show();
    $('#smarkfrom_'+val).show();
	$('#smarkto_'+val).show();
	$('#ecomment_'+val).show();
	$('#tcomment_'+val).show();
	$('#displayorder_'+val).show();	
	$('#mode').val('EDIT');

}


function deldata(val)
{ 
	var msg="Do you want to delete?";
	if(!confirm(msg)){
		return false;
	}	 
	else {
		$('#grade_code').val(val);
		$('#mode').val('DELETE');
		$('#myform').submit();
	 }
}



</script>



</head>

<?php
	$errmsg="";
	$errflag=0;
	$dummy=0;
	$mode		=	$_POST['mode'];
	$mode1		=	$_POST['mode1'];
	$editcnt 	=	split(',',$_POST['editcnt']);	
	if($_POST['Cancel']=="Cancel") { $mode=""; $mode1="";	}	
	
	if($mode1 == "ADD")
	{	
		$action1=1;
		$grade1=trim($_POST['grade1']);
		$tmarkfrom1=trim($_POST['tmarkfrom1']);
		$tmarkto1=trim($_POST['tmarkto1']);
       	$fmarkfrom1=trim($_POST['fmarkfrom1']);
		$fmarkto1=trim($_POST['fmarkto1']);
       	$smarkfrom1=trim($_POST['smarkfrom1']);
		$smarkto1=trim($_POST['smarkto1']);
		$ecomment1=trim($_POST['ecomment1']);
		$tcomment1=trim($_POST['tcomment1']);
		$displayorder1=trim($_POST['displayorder1']);
				
		$dummy = Strcheck($grade1,$errmsg,$errflag,"Grade");
		$dummy = Strcheck($tmarkfrom1,$errmsg,$errflag,"Tmark From");
        if($dummy==0)
            $dummy = MaxMin_Valcheck($tmarkfrom1,$errmsg,$errflag,"Tmark From must be 1-100",1,100);
		$dummy = Strcheck($tmarkto1,$errmsg,$errflag,"Tmark To");
        if($dummy==0)
            $dummy = MaxMin_Valcheck($tmarkto1,$errmsg,$errflag,"Tmark To must be 1-100",1,100);
        if($dummy==0) {
            $dummy = FromTo_Valcheck($tmarkfrom1,$tmarkto1,$errmsg,$errflag,"Tmark From must be greater than or equal to Tmark To");
        }
        
        $dummy = Strcheck($fmarkfrom1,$errmsg,$errflag,"Fmark From");
        if($dummy==0)
            $dummy = MaxMin_Valcheck($fmarkfrom1,$errmsg,$errflag,"Fmark From must be 1-100",1,100);
		$dummy = Strcheck($fmarkto1,$errmsg,$errflag,"Fmark To");
        if($dummy==0)
            $dummy = MaxMin_Valcheck($fmarkto1,$errmsg,$errflag,"Fmark To must be 1-100",1,100);
        if($dummy==0) {
            $dummy = FromTo_Valcheck($fmarkfrom1,$fmarkto1,$errmsg,$errflag,"Fmark From must be greater than or equal to Fmark To");
        }
        
        $dummy = Strcheck($smarkfrom1,$errmsg,$errflag,"Smark From");
        if($dummy==0)
            $dummy = MaxMin_Valcheck($smarkfrom1,$errmsg,$errflag,"Smark From must be 1-100",1,100);
		$dummy = Strcheck($smarkto1,$errmsg,$errflag,"Smark To");
        if($dummy==0)
            $dummy = MaxMin_Valcheck($smarkto1,$errmsg,$errflag,"Smark To must be 1-100",1,100);
        if($dummy==0) {
            $dummy = FromTo_Valcheck($smarkfrom1,$smarkto1,$errmsg,$errflag,"Smark From must be greater than or equal to Smark To");
        }
		//$dummy = Strcheck($ecomment1,$errmsg,$errflag,"Grade English Comment");
		//$dummy = Strcheck($tcomment1,$errmsg,$errflag,"Grade Tamil Comment");
		$dummy=Strcheck($displayorder1,$errmsg,$errflag,"Display Order");
		
		if($errflag==0)
		{
            mssql_free_result($result);
			$query = mssql_init('SP_GradeSave',$mssql);
			mssql_bind($query,'@Grade_Code',$grade_code1,SQLINT4,false,false,5);
			mssql_bind($query,'@Grade',$grade1,SQLVARCHAR,false,false,50);
			mssql_bind($query,'@Tmark_From',$tmarkfrom1,SQLINT4,false,false,10);
			mssql_bind($query,'@Tmark_To',$tmarkto1,SQLINT4,false,false,10);
           	mssql_bind($query,'@Fmark_From',$fmarkfrom1,SQLINT4,false,false,10);
			mssql_bind($query,'@Fmark_To',$fmarkto1,SQLINT4,false,false,10);
            mssql_bind($query,'@Smark_From',$smarkfrom1,SQLINT4,false,false,10);
			mssql_bind($query,'@Smark_To',$smarkto1,SQLINT4,false,false,10);
			mssql_bind($query,'@Grade_Ecomment',$ecomment1,SQLVARCHAR,false,false,50);
			mssql_bind($query,'@Grade_Tcomment',$tcomment1,SQLVARCHAR,false,false,50);
			mssql_bind($query,'@Display_Order',$displayorder1,SQLINT4,false,false,5);
			mssql_bind($query,'@Action',$action1,SQLINT4,false,false,1);
			
			$result = @mssql_execute($query);
			
			mssql_free_statement($query);
		if($result==1){
			echo "<p class='mesg'>Grade has been Added</p>";
			
			$grade1="";
			$tmarkfrom1="";
			$tmarkto1="";
            $fmarkfrom1="";
			$fmarkto1="";
           	$smarkfrom1="";
			$smarkto1="";
			$ecomment1="";
			$tcomment1="";
			$displayorder1="";
			
			}
		else {
			$errmsg1=mssql_get_last_message();
			$errflag=2;
			$inserr=1;
			}
		} else $inserr=1;
	}
//echo "==".$mode."<br>";
	if($mode == "EDIT")
	{ 
	
	$action=2;
	
	for($i=0;$i<count($editcnt);$i++) 
	{  
		
		$grade_code=trim($_POST['grade_code_'.$editcnt[$i]]);
		$grade=trim($_POST['grade_'.$editcnt[$i]]);
		$tmarkfrom=trim($_POST['tmarkfrom_'.$editcnt[$i]]);
		$tmarkto=trim($_POST['tmarkto_'.$editcnt[$i]]);
        $fmarkfrom=trim($_POST['fmarkfrom_'.$editcnt[$i]]);
		$fmarkto=trim($_POST['fmarkto_'.$editcnt[$i]]);
        $smarkfrom=trim($_POST['smarkfrom_'.$editcnt[$i]]);
		$smarkto=trim($_POST['smarkto_'.$editcnt[$i]]);
		$ecomment=trim($_POST['ecomment_'.$editcnt[$i]]);
		$tcomment=trim($_POST['tcomment_'.$editcnt[$i]]);
		$displayorder=trim($_POST['displayorder_'.$editcnt[$i]]);
				$j=$i+1;
				
		$dummy = Strcheck($grade,$errmsg,$errflag,"Grade");
		$dummy = Strcheck($tmarkfrom,$errmsg,$errflag,"Tmark From");
        if($dummy==0)
            $dummy = MaxMin_Valcheck($tmarkfrom,$errmsg,$errflag,"Tmark From must be 1-100",1,100);    
		$dummy = Strcheck($tmarkto,$errmsg,$errflag,"Tmark To");
        if($dummy==0)
            $dummy = MaxMin_Valcheck($tmarkto,$errmsg,$errflag,"Tmark To must be 1-100",1,100);
        if($dummy==0) 
            $dummy = FromTo_Valcheck($tmarkfrom,$tmarkto,$errmsg,$errflag,"Tmark From must be greater than or equal to Tmark To");
            
            	$dummy = Strcheck($fmarkfrom,$errmsg,$errflag,"Fmark From");
        if($dummy==0)
            $dummy = MaxMin_Valcheck($fmarkfrom,$errmsg,$errflag,"Fmark From must be 1-100",1,100);    
		$dummy = Strcheck($fmarkto,$errmsg,$errflag,"Fmark To");
        if($dummy==0)
            $dummy = MaxMin_Valcheck($fmarkto,$errmsg,$errflag,"Fmark To must be 1-100",1,100);
        if($dummy==0) 
            $dummy = FromTo_Valcheck($fmarkfrom,$fmarkto,$errmsg,$errflag,"Fmark From must be greater than or equal to Fmark To");
            
            $dummy = Strcheck($smarkfrom,$errmsg,$errflag,"Smark From");
        if($dummy==0)
            $dummy = MaxMin_Valcheck($smarkfrom,$errmsg,$errflag,"Smark From must be 1-100",1,100);    
		$dummy = Strcheck($smarkto,$errmsg,$errflag,"Smark To");
        if($dummy==0)
            $dummy = MaxMin_Valcheck($smarkto,$errmsg,$errflag,"Smark To must be 1-100",1,100);
        if($dummy==0) 
            $dummy = FromTo_Valcheck($smarkfrom,$smarkto,$errmsg,$errflag,"Smark From must be greater than or equal to Smark To");
            
        
	//	$dummy = Strcheck($ecomment,$errmsg,$errflag,"Grade English Comment");
	//	$dummy = Strcheck($tcomment,$errmsg,$errflag,"Grade Tamil Comment");
		$dummy=Strcheck($displayorder,$errmsg,$errflag,"Display Order");
				
		//echo "flag".$errflag;	
		if($errflag==0)
		{
		    mssql_free_result($result);
			$query = mssql_init('SP_GradeSave',$mssql);
			mssql_bind($query,'@Grade_Code',$grade_code,SQLINT4,false,false,5);
			mssql_bind($query,'@Grade',$grade,SQLVARCHAR,false,false,50);
			mssql_bind($query,'@Tmark_From',$tmarkfrom,SQLINT4,false,false,10);
			mssql_bind($query,'@Tmark_To',$tmarkto,SQLINT4,false,false,10);
           	mssql_bind($query,'@Fmark_From',$fmarkfrom,SQLINT4,false,false,10);
			mssql_bind($query,'@Fmark_To',$fmarkto,SQLINT4,false,false,10);
           	mssql_bind($query,'@Smark_From',$smarkfrom,SQLINT4,false,false,10);
			mssql_bind($query,'@Smark_To',$smarkto,SQLINT4,false,false,10);
			mssql_bind($query,'@Grade_Ecomment',$ecomment,SQLVARCHAR,false,false,50);
			mssql_bind($query,'@Grade_Tcomment',$tcomment,SQLVARCHAR,false,false,50);
			mssql_bind($query,'@Display_Order',$displayorder,SQLINT4,false,false,5);
			mssql_bind($query,'@Action',$action,SQLINT4,false,false,1);
			
			$result = @mssql_execute($query);
			mssql_free_statement($query);
			if($result==1)
				echo "<p class='mesg'>Grade has been Updated</p>";
			else {
				$errmsg1=mssql_get_last_message();
				$errflag=2;
				If ($errcnt == 0) { $errcnt = 1;} else { $errcnt = $errcnt + 1; }
			    if ($errval == "") { $errval = $editcnt[$i];	 } else { $errval = $errval.",".$editcnt[$i]; } 
			}
		}
		else {
			if ($errcnt == 0) { $errcnt = 1;} else { $errcnt = $errcnt + 1; }
			if ($errval == "") { $errval = $editcnt[$i];	 } else { $errval = $errval.",".$editcnt[$i]; } 
		}
	}	
	}
	//echo "==".$mode;
	if($mode=="DELETE")
	{	
		$action=3;
		$grade_code= 	trim($_POST['grade_code']);

		    mssql_free_result($result);
			$query = mssql_init('SP_GradeSave',$mssql);			
			mssql_bind($query,'@Grade_Code',$grade_code,SQLINT4,false,false,5);
			mssql_bind($query,'@Grade',$grade,SQLVARCHAR,false,false,50);
			mssql_bind($query,'@Tmark_From',$tmarkfrom,SQLINT4,false,false,5);
			mssql_bind($query,'@Tmark_To',$tmarkto,SQLINT4,false,false,5);
           	mssql_bind($query,'@Fmark_From',$fmarkfrom,SQLINT4,false,false,5);
			mssql_bind($query,'@Fmark_To',$fmarkto,SQLINT4,false,false,5);
           	mssql_bind($query,'@Smark_From',$smarkfrom,SQLINT4,false,false,5);
			mssql_bind($query,'@Smark_To',$smarkto,SQLINT4,false,false,5);
			mssql_bind($query,'@Grade_Ecomment',$ecomment,SQLVARCHAR,false,false,50);
			mssql_bind($query,'@Grade_Tcomment',$tcomment,SQLVARCHAR,false,false,50);
			mssql_bind($query,'@Display_Order',$displayorder,SQLINT4,false,false,5);
			mssql_bind($query,'@Action',$action,SQLINT4,false,false,1);
		$result = mssql_execute($query);
		mssql_free_statement($query);
		if($result==1)
		{
			echo "<p class='mesg'>Grade has been Deleted</p>";
			
			}
		else {
			$errmsg1=mssql_get_last_message();
			$errflag=2;
			}
	}

if($errflag==1) 
		echo $errlbl.$errmsg."</p>";
	if($errflag==2) 
		echo "<p class='error'>".$errmsg1."</p>";	
?>
<body>
<form name="myform" id="myform" method="post" action="sms_grade.php">
<input type="hidden" name="editcnt" id="editcnt"/>
<input type="hidden" name ="editval" id ="editval" />
<input type="hidden" name="grade_code" id="grade_code"/>
<input type="hidden" name="mode" id="mode"/>	
<input type="hidden" name="mode1" id="mode1"/>
<table width="100%" height="450" border="0" align="center" cellpadding="3" cellspacing="3">
<tr><td valign="top">
<table width="100%" border="0" align="center" cellpadding="3" cellspacing="1">
<?php titleheader(Grade,0);?>
<tr align="center">
<thead><colgroup>
<col width=5%>
<col width=5%>
<col width=10%>
<col width=10%>
<col width=10%>
<col width=10%>
<col width=10%>
<col width=10%>
<col width=10%>
<col width=10%>
<col width=10%>
<col width=10%>
</colgroup>
<th>&nbsp;</th>
<th>&nbsp;</th>
<th>Grade</th>
<th>TMark From</th>
<th>TMark To</th>
<th>FMark From</th>
<th>FMark To</th>
<th>SMark From</th>
<th>SMark To</th>
<th>Grade English comment</th>
<th>Grade Tamil comment</th>
<th>Display Order</th>
</thead></tr>
<?php   // New Record Insert
		$colorflag+=1; ?>


<tr class=<?php if($colorflag%2==0) { echo "row2"; } else { echo "row1"; } ?> valign="center">
	<td align="center" >
		<img src="../images/new.gif" title="Add New" onclick="adddata();"/>
 	</td>
    <td>
    </td>
    <td align="center">
    	<div id ="grade1" style="display:none;">
    	<input type ="text" id ="grade1" name="grade1" size="20" maxlength="3" 
        value="<?php echo $grade1; ?>" onkeydown="return alphanumeric('grade1')"  />
   		<?php echo $mand; ?>
   		</div>
    </td>
    
    <td align="center"><div id ="tmarkfrom1" style="display:none;">
    	<input type ="text" id ="tmarkfrom1" name="tmarkfrom1" size="8" maxlength="3" 
        value="<?php echo $tmarkfrom1?>"  onkeydown="return numberonly('tmarkfrom1')" />
   		<?php echo $mand; ?>
   		</div></td>
                <td align="center"><div id ="tmarkto1" style="display:none;">
    	<input type ="text" id ="tmarkto1" name="tmarkto1" size="10" maxlength="3" 
        value="<?php echo $tmarkto1?>" onkeydown="return numberonly('tmarkto1')" />
   		<?php echo $mand; ?>
   		</div></td>
          <td align="center"><div id ="fmarkfrom1" style="display:none;">
    	<input type ="text" id ="fmarkfrom1" name="fmarkfrom1" size="8" maxlength="3" 
        value="<?php echo $fmarkfrom1?>"  onkeydown="return numberonly('fmarkfrom1')" />
   		<?php echo $mand; ?>
   		</div></td>
                <td align="center"><div id ="fmarkto1" style="display:none;">
    	<input type ="text" id ="fmarkto1" name="fmarkto1" size="10" maxlength="3" 
        value="<?php echo $fmarkto1?>" onkeydown="return numberonly('fmarkto1')" />
   		<?php echo $mand; ?>
   		</div></td>
          <td align="center"><div id ="smarkfrom1" style="display:none;">
    	<input type ="text" id ="smarkfrom1" name="smarkfrom1" size="8" maxlength="3" 
        value="<?php echo $smarkfrom1?>"  onkeydown="return numberonly('smarkfrom1')" />
   		<?php echo $mand; ?>
   		</div></td>
                <td align="center"><div id ="smarkto1" style="display:none;">
    	<input type ="text" id ="smarkto1" name="smarkto1" size="10" maxlength="3" 
        value="<?php echo $smarkto1?>" onkeydown="return numberonly('smarkto1')" />
   		<?php echo $mand; ?>
   		</div></td>
    <td align="center"><div id ="ecomment1" style="display:none;">
    	<input type ="text" id ="ecomment1" name="ecomment1" size="10" maxlength="50" 
        value="<?php echo $ecomment1?>"  onkeydown="return alphaonly('ecomment1')" />
   		<?php echo $mand; ?>
   		</div></td>
     <td align="center"><div id ="tcomment1" style="display:none;">
    	<input type ="text" id ="tcomment1" name="tcomment1" size="25" maxlength="50" 
        value="<?php echo $tcomment1?>" onkeydown="return alphaonly('tcomment1')" />
   		<?php echo $mand; ?>
   		</div></td>
    <td align="center">
   		 <div id="displayorder1" style="display:none">
   		 <input type="text" id="displayorder1" name="displayorder1" maxlength="5" size="5" 
   		 value="<?php echo $displayorder1?>" onkeydown="return numberonly('displayorder1')" />
    	 <?php echo $mand; ?>
   		 </div>
    </td>
 </tr>
 
<?php 	// UPDATE & SHOW RECORDS
        mssql_free_result($result);
		$query = mssql_init('SP_GetGrade',$mssql);
		$result = mssql_execute($query);
		mssql_free_statement($query);
		$rs_cnt = mssql_num_rows($result);
		$colorflag = 0;
		$i = 0;
		while($field = mssql_fetch_array($result))
		{	$i  +=1;	$colorflag+=1;
			$tot_rec = $i;	
			$grade_code = $field['Grade_Code'];
?>
             
 <tr class=<?php if($colorflag%2==0) { echo "row1"; } else { echo "row2"; } ?> valign="center">
 
 <input type="hidden" name="grade_code_<?php echo $i ?>" id ="grade_code_<?php echo $i ?>" 
        value="<?php echo $field['Grade_Code']?>"	 />	
 
		<td align="center"><img src="../images/delete_d.gif" title="Remove" 
          onclick="deldata('<?php echo $field['Grade_Code'];?>');" />&nbsp;
        </td>
        
		<td align="center"><img src="../images/edit.gif" title="Edit" 
           onclick="editdata('<?php echo $i;?>');" />&nbsp;
        </td>
        
        <td align="center"><?php echo $field['Grade']?>
       
	    	<div id ="grade_<?php echo $i;?>" style="display:none;">
        	 <input type="text" id="grade_<?php echo $i ?>" name="grade_<?php echo $i ?>"  
             size="10" maxlength="3" value="<?php echo $field['Grade']?>" 
             onkeydown="return alphanumeric('grade_<?php echo $i ?>')"/></div>        
        </td>	
        <td align="center"><?php echo $field['Tmark_Frm']?>
       
	    	<div id ="tmarkfrom_<?php echo $i;?>" style="display:none;">
        	 <input type="text" id="tmarkfrom_<?php echo $i ?>" name="tmarkfrom_<?php echo $i ?>"  
             size="10" maxlength="3" value="<?php echo $field['Tmark_Frm']?>" 
             onkeydown="return numberonly('tmarkfrom_<?php echo $i ?>')"/></div>
        </td>	
        <td align="center"><?php echo $field['Tmark_To']?>
       
	    	<div id ="tmarkto_<?php echo $i;?>" style="display:none;">
        	 <input type="text" id="tmarkto_<?php echo $i ?>" name="tmarkto_<?php echo $i ?>"  
             size="10" maxlength="3" value="<?php echo $field['Tmark_To']?>" 
             onkeydown="return numberonly('tmarkto_<?php echo $i ?>')"/></div>        
        </td>	
        
         <td align="center"><?php echo $field['Fmark_Frm']?>
       
	    	<div id ="fmarkfrom_<?php echo $i;?>" style="display:none;">
        	 <input type="text" id="fmarkfrom_<?php echo $i ?>" name="fmarkfrom_<?php echo $i ?>"  
             size="10" maxlength="3" value="<?php echo $field['Fmark_Frm']?>" 
             onkeydown="return numberonly('fmarkfrom_<?php echo $i ?>')"/></div>
        </td>	
        <td align="center"><?php echo $field['Fmark_To']?>
       
	    	<div id ="fmarkto_<?php echo $i;?>" style="display:none;">
        	 <input type="text" id="fmarkto_<?php echo $i ?>" name="fmarkto_<?php echo $i ?>"  
             size="10" maxlength="3" value="<?php echo $field['Fmark_To']?>" 
             onkeydown="return numberonly('fmarkto_<?php echo $i ?>')"/></div>        
        </td>	
        
         <td align="center"><?php echo $field['Smark_Frm']?>
       
	    	<div id ="smarkfrom_<?php echo $i;?>" style="display:none;">
        	 <input type="text" id="smarkfrom_<?php echo $i ?>" name="smarkfrom_<?php echo $i ?>"  
             size="10" maxlength="3" value="<?php echo $field['Smark_Frm']?>" 
             onkeydown="return numberonly('smarkfrom_<?php echo $i ?>')"/></div>
        </td>	
        <td align="center"><?php echo $field['Smark_To']?>
       
	    	<div id ="smarkto_<?php echo $i;?>" style="display:none;">
        	 <input type="text" id="smarkto_<?php echo $i ?>" name="smarkto_<?php echo $i ?>"  
             size="10" maxlength="3" value="<?php echo $field['Smark_To']?>" 
             onkeydown="return numberonly('smarkto_<?php echo $i ?>')"/></div>        
        </td>	
        <td align="center"><?php echo $field['Grade_Ecomment']?>
       
	    	<div id ="ecomment_<?php echo $i;?>" style="display:none;">
        	 <input type="text" id="ecomment_<?php echo $i ?>" name="ecomment_<?php echo $i ?>"  
             size="25" maxlength="50" value="<?php echo $field['Grade_Ecomment']?>" 
             onkeydown="return alphaonly('ecomment_<?php echo $i ?>')"/>
             </div>       
        </td>	
        <td align="center"><?php echo $field['Grade_Tcomment']?>
       
	    	<div id ="tcomment_<?php echo $i;?>" style="display:none;">
        	 <input type="text" id="tcomment_<?php echo $i ?>" name="tcomment_<?php echo $i ?>"  
             size="25" maxlength="50" value="<?php echo $field['Grade_Tcomment']?>" 
             onkeydown="return alphaonly('tcomment_<?php echo $i ?>')"/></div>
         </td>	
        
        <td align="center">
				<?php echo $field['Display_Order'] ?>
				<div id ="displayorder_<?php echo $i;?>" style="display:none;">
                	<input type="text" name="displayorder_<?php echo $i ?>" id="displayorder_<?php echo $i ?>"  
                    size="5" maxlength="3" value="<?php echo $field['Display_Order']?>" 
                    onkeydown="return numberonly('displayorder_<?php echo $i ?>')"/>
                </div>
         </td>
        	
 </tr>
 
 <?php
 }
 if ($errcnt > 0) {
		for($i=0;$i<=$errcnt-1;$i=$i+1){
		$editcnt = split(',',$_POST['editcnt']);?>
			<script>editdata(<?php echo $editcnt[$i];?>);</script>
		<?php } 
		} ?>	
  
 
 
 <tr>
   		 <td colspan="12" align="right">
			<input type="submit" name="save" value="Save" class="winbutton_go" />&nbsp;&nbsp;&nbsp;
			<input type="submit" name="Cancel" value="Cancel" class="winbutton_go"/>
        </td>
   	</tr>
<?php	if($inserr == 1) { ?><script>adddata();</script><?php } ?>
</table></td></tr></table>

</form>



</body>
</html>
<?php include("../includes/copyright.php"); ?>