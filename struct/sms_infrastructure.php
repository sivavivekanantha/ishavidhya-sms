<?php session_start(); ?>
		<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml">
		<head><title>Isha Foundation - A Non-profit Organization</title>
		<?php include("../includes/header.php"); 
		title('Student Management','Infrastructure',2,1,0);
		?>
		<script>


		function adddata()
		{ 
		   $('#infra_code1').show();
		   $('#infra_name1').show();
		   $('#t_infra_name1').show();
		   $('#active1').show();
		   $('#school_id1').show();
		   $('#mode1').val('ADD');
		} 


		function editdata(val)
	   {
		  if($('#editcnt').val() >0 ) return false;
		  $('#editcnt').val(val);

		  $('#infra_name_'+val).show();
		  $('#t_infra_name_'+val).show();
		  $('#active_'+val).show();
		  $('#school_id_'+val).show();	
		  $('#mode').val('EDIT');

	   }
	   function deldata(val)
		{ 
		 var msg="Do you want to delete?";
		 if(!confirm(msg))
		 {
		 return false;
		 }	 
		else {
		  $('#infra_code').val(val);
		  $('#mode').val('DELETE');
		  $('#myform').submit();
			 }
	   }
	   </script>
	   </head>

	   <?php 
			$errmsg="";
			$errflag=0;
			$dummy=0;
			$mode		=	trim($_POST['mode']);
			$mode1		=	trim($_POST['mode1']);
			$editcnt 	=	split(',',$_POST['editcnt']);		
			if($_POST['Cancel']=="Cancel") { $mode=""; $mode1="";	}
			//RECORD EDIT PART
			If($mode == "EDIT")
			{ 
			$action=2;
			//EDIT ROW VALUES ARE GETTING
			for($i=0;$i<count($editcnt);$i=$i+1) 
			{ 
		
			$infra_code		=	trim($_POST['infra_code'.$editcnt[$i]]);	
			$infra_name		=	trim($_POST['infra_name_'.$editcnt[$i]]);
			$t_infra_name	=	trim($_POST['t_infra_name_'.$editcnt[$i]]);
			$active			=	trim($_POST['active_'.$editcnt[$i]]);
			$school_id	    =	trim($_POST['school_id_'.$editcnt[$i]]);
		
			$j=$i+1;
			//VALIDATE THE INPUT
			$dummy = Strcheck($infra_name,$errmsg,$errflag,"Infra Name-".$j);
			$dummy = Strcheck($t_infra_name,$errmsg,$errflag,"T Infra Name-".$j);
			$dummy = Numcheck($school_id,$errmsg,$errflag,"School Name-".$j);
			//ALL INPUTS ARE CORRECTED, THEN GOTO SP
			if($errflag==0)
			{
			    mssql_free_result($result);    
				$query = mssql_init('sp_Infrastructure',$mssql);
				mssql_bind($query,'@Infra_Code',$infra_code,SQLINT4,false,false,5);
				mssql_bind($query,'@Infra_Name',$infra_name,SQLVARCHAR,false,false,100);
				mssql_bind($query,'@T_Infra_Name',$t_infra_name,SQLVARCHAR,false,false,25);
				mssql_bind($query,'@Active',$active,SQLVARCHAR,false,false,1);
				mssql_bind($query,'@School_Id',$school_id,SQLVARCHAR,false,false,5);
				mssql_bind($query,'@Action',$action,SQLINT4,false,false,1);

				$result = @mssql_execute($query);//SP EXECUTE HERE
				mssql_free_statement($query);//$QUERY FREE HERE
				if($result==1)
					echo "<p class='mesg'>Infrstructure has been Updated</p>";
				else {
					$errmsg1=mssql_get_last_message();
					$errflag=2;
					$errcnt=1;
				}
			}
			    else 
					{
				If ($errcnt == 0) { $errcnt = 1;} else { $errcnt = $errcnt + 1; }
				if ($errval == "") { $errval = $editcnt[$i];	 } else { $errval = $errval.",".$editcnt[$i]; } 
			}
		}	
		}
	    //RECORD DELETE PART
		if($mode=="DELETE")
		{	$action=3;
			$infra_code	=	trim($_POST['infra_code']);
		    mssql_free_result($result);   
			$query = mssql_init('sp_Infrastructure',$mssql);		
			mssql_bind($query, '@Infra_Code',$infra_code,SQLINT4,false,false,5);
			mssql_bind($query,'@Infra_Name',$infra_name,SQLVARCHAR,false,false,100);
			mssql_bind($query,'@T_Infra_Name',$t_infra_name,SQLVARCHAR,false,false,25);
			
			mssql_bind($query,'@Active',$active,SQLVARCHAR,false,false,1);
			mssql_bind($query,'@School_Id',$school_id,SQLVARCHAR,false,false,5);
			mssql_bind($query,'@Action',$action,SQLINT4,false,false,1);
			$result = @mssql_execute($query);//SP EXECUTE HERE
			mssql_free_statement($query);//$QUERY FREE HERE
			if($result==1)
				echo "<p class='mesg'>Infrstructure has been Deleted</p>";
			else {
				$errmsg1=mssql_get_last_message();//ERROR MESSAGE GET FROM SP
				//echo "".$errmsg1;
				$errflag=2;
				}
		}
		//RECORD ADD PART
		if($mode1 == "ADD")
		{	$action1=1;
			$infra_name1		=	trim($_POST['infra_name1']);
			$t_infra_name1		=	trim($_POST['t_infra_name1']);
			$active1			=	trim($_POST['active1']);
			$school_id1     	=	trim($_POST['school_id1']);
			//ERROR MESSAGE 
			//INPUT VALIDATE HERE
			$dummy = Strcheck($infra_name1,$errmsg,$errflag,"Infrastructure Name");
			$dummy = Numcheck($school_id1,$errmsg,$errflag,"School Name");
			$dummy = Strcheck($t_infra_name1,$errmsg,$errflag,"T Infrastructure name");
			
			if($errflag==0){
                 //Stored Procedure 
                 mssql_free_result($result);
			$query = mssql_init('sp_Infrastructure',$mssql);
				mssql_bind($query,'@Infra_Code',$infra_code1,SQLINT4,false,false,5);
				mssql_bind($query,'@Infra_Name',$infra_name1,SQLVARCHAR,false,false,100);
				mssql_bind($query,'@T_Infra_Name',$t_infra_name1,SQLVARCHAR,false,false,25);
				mssql_bind($query,'@Active',$active1,SQLVARCHAR,false,false,1);
				mssql_bind($query,'@School_Id',$school_id1,SQLVARCHAR,false,false,5);
				mssql_bind($query,'@Action',$action1,SQLINT4,false,false,1);
				
				$result = @mssql_execute($query);//SP EXECUTE HERE
				mssql_free_statement($query);//$QUERY FREE HERE
			if($result==1) {
				echo "<p class='mesg'>Infrastructure has been Added</p>";
				$infra_name1=$t_infra_name1=$active1=$school_id1="";
				}
			else {
				$errmsg1=mssql_get_last_message();
				$errflag=2;
				$inserr=1;
				}
			} else $inserr=1;
		}
		if($errflag==1) 
			echo $errlbl.$errmsg;
		if($errflag==2) 
			echo "<p class='error'>".$errmsg1;
		
	?>   
	    <!--Form Desining--><!--HIDDEN VALUES--> 
			<body style="margin:0;">
			<form name="myform" id="myform" method="post" action="sms_infrastructure.php">
			<input type="hidden" name="editcnt" id="editcnt"/>
			<input type="hidden" name="infra_code" id="infra_code"/>
			<input type="hidden" name="mode" id="mode"/>
			<input type="hidden" name="mode1" id="mode1"/>
			<table width="70%" height="450" border="0" align="center" cellpadding="3" cellspacing="3">
			<tr><td valign="top">
			<table width="100%" border="0" align="center" cellpadding="3" cellspacing="1">
			<?php titleheader(Infrastructure,0);?>
			<tr align="center">
			<thead><colgroup><col width=3%><col width=3%><col width=10%><col width=10%><col width=10%><col width=5%><col width=5%></colgroup>
			<th>&nbsp;</th>
			<th>&nbsp;</th>
			<th align="center">Infrastructure Name</th>
			<th>T Infrastructure Name</th>
			<th>School Name</th>
			<th>Active</th>
			</thead>
			</tr>

			<?php   // New Record Insert 	 ?>
			<tr class="row1" valign="center">
			<!-- NEW ICON-->
			<td align="center" ><img src="../images/new.gif" title="Add New" onclick="adddata();"/></td><td>&nbsp;</td>
			<td align="center"><div id ="infra_name1" style="display:none;">
			<input type="text" name="infra_name1" id="infra_name1"  size="15" maxlength="100" value="<?php echo $infra_name1 ?>" onkeydown="return alphaonly('infra_name1')" ><?php echo $mand; ?></div></td>
			<td align="center"><div id ="t_infra_name1" style="display:none;">
			<input type="text" name="t_infra_name1" id="t_infra_name1"  size="15" maxlength="100" value="<?php echo $t_infra_name1 ?>"
			onkeydown="return alphaonly('t_infra_name1')"><?php echo $mand; ?></div></td>
			<?php 	//SHOW SCHOOL DROPDOWN
            mssql_free_result($result); 
			$query = mssql_init('sp_GetInfrastructure',$mssql);
			$result = mssql_execute($query);
			mssql_free_statement($query);	?>
		 
			<td align="center" <?php echo $field['School_Name'] ?>>
			<div id="school_id1" style="display:none;">
			<select name="school_id1" id="school_id1">
			<?php echo $Select; ?> 
			<?php	while($field = mssql_fetch_array($result)) {  ?>
			<option value="<?php echo $field['School_Id']?>" <?php if($school_id1==$field['School_Id'])echo "selected"; ?>><?php echo $field['School_Name']?></option>
			<?php } ?>
			</select></div>
			</td>
			<td align="center"><div id ="active1" style="display:none;"><select name="active1" id="active1"><option value="Y">Yes</option><option value="N">No</option></select></div></td>
			<?php 	// UPDATE & SHOW RECORDS
            mssql_free_result($result); 
			$query = mssql_init('sp_SelectInfrastructure',$mssql);
			$result = mssql_execute($query);
			mssql_free_statement($query);
			$rs_cnt = mssql_num_rows($result);
			$colorflag = 0;
			$i = 0;
			while($field = mssql_fetch_array($result))
			{	$i +=1;	$colorflag+=1;
				$tot_rec = $i;	?>
			<tr class=<?php if($colorflag%2==0) { echo "row1"; } else { echo "row2"; } ?> valign="center">
			<td align="center"><input type="hidden" name="infra_code<?php echo $i ?>" id ="infra_code<?php echo $i ?>" value="<?php echo $field['Infra_Code']?>"	 />
			<!-- DELETE ICON-->
			<img src="../images/delete_d.gif" title="Remove" onclick="deldata('<?php echo $field['Infra_Code'];?>');" />&nbsp;</td>	 
			<td align="center">
			<!-- EDIT ICON-->
			<img src="../images/edit.gif" title="Edit" onclick="editdata('<?php echo $i;?>');" />&nbsp;</td>
			<td ><?php echo $field['Infra_Name'] ?>
			<div id ="infra_name_<?php echo $i;?>" style="display:none;"><input type="text" name="infra_name_<?php echo $i ?>" id="infra_name_<?php echo $i ?>"  size="25" maxlength="100" value="<?php echo $field['Infra_Name']?>" onkeydown="return alphaonly('infra_name_<?php echo $i ?>')"></div></td>
			<td align="center"><?php echo $field['T_Infra_Name'] ?>
			<div id ="t_infra_name_<?php echo $i;?>" style="display:none;">
			<input type="text" name="t_infra_name_<?php echo $i ?>" id="t_infra_name_<?php echo $i ?>"  size="25" maxlength="100" value="<?php echo $field['T_Infra_Name']?>" onkeydown="return alphaonly('t_infra_name_<?php echo $i ?>')"></div></td>
			<?php 	//SHOW COUNTRY DROPDOWN
            mssql_free_result($result1); 
			$query = mssql_init('sp_GetInfrastructure',$mssql);
			$result1 = mssql_execute($query);
			mssql_free_statement($query);	?>
			
			<td align="center"><?php echo $field['School_Name'] ?>
			
			<div id="school_id_<?php echo $i ?>" style="display:none;">
			<select name="school_id_<?php echo $i ?>" id="school_id_<?php echo $i ?>">
			<?php	while($field1 = mssql_fetch_array($result1)) {  ?>
			<option value="<?php echo $field1['School_Id']?>" <?php if($field['School_Id']==$field1['School_Id']) echo "Selected";?>><?php echo $field1['School_Name']?></option>
			<?php } ?>
			</select></div>
			</td>
			<td align="center"><?php if(strtoupper($field['Active'])==="Y") echo "Yes"; else echo "No";?>
			<div id ="active_<?php echo $i;?>" style="display:none;">
			<select id="active_<?php echo $i ?>" name="active_<?php echo $i ?>">
			<option value="Y" <?php if(strtoupper($field['Active'])==="Y")  echo "selected" ?>>Yes</option>
			<option value="N" <?php if(strtoupper($field['Active'])==="N")  echo "selected" ?>>No</option>
			</select></div></td>
			</tr>
			<?php }
			//IF ANY ERROR IN EDIT, SHOW THAT ROW
			if ($errcnt > 0) {
			for($i=0;$i<=$errcnt-1;$i=$i+1){
			$editcnt = split(',',$_POST['editcnt']);?>
			<script>editdata(<?php echo $editcnt[$i];?>);</script>
			<?php } 
			} ?>	
			<input type="hidden" name="tot_rec" value="<?php echo $tot_rec ?>" />
			<tr><td colspan="6" align="right">
			<input type="submit" name="save" value="Save" class="winbutton_go" />&nbsp;&nbsp;&nbsp;
			<input type="submit" name="Cancel" value="Cancel" class="winbutton_go"/></td></tr>
			<?php //IF ANY ERROR IN ADD, SHOW THAT ROW ?>
			<?php	if($inserr == 1) { ?><script>adddata();</script><?php } ?>

			</table></td></tr></table>
			</form></body></html>
			<?php include("../includes/copyright.php"); ?>

