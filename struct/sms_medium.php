<?php session_start(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>Isha Foundation - A Non-profit Organization</title>
<?php include("../includes/header.php"); 
 title('Student Management','Medium',2,1,0);
?>
<script>
function adddata()
 { 
 	$('#medium1').show();
	$('#active1').show();
	var $mode1 ='ADD';
	$('#mode1').val($mode1);
  } 


 function editdata(val)
  {
	if($('#editcnt').val() >0 ) return false;
    $('#editcnt').val(val);
	$('#medium_'+val).show();
	$('#active_'+val).show();

	var $mode ='EDIT';
	$('#mode').val($mode);

}
function deldata(val)
{ 
	var msg="Do you want to delete?";
	if(!confirm(msg)){
		return false;
	}	 
	else {
		$('#medium_code').val(val);
		var $mode ='DELETE';
		$('#mode').val($mode);
		$('#myform').submit();
	 }
}
</script>
</head>

<?php 
    //VARIABLE DECLARATION HERE
	$errmsg="";
	$errflag=0;
	$dummy=0;
	// GET MODE VALUE FOR WHICH FUNCTION PERFORMED (ADD / EDIT/ DELETE)
	$mode		=	trim($_POST['mode']);
	$mode1		=	trim($_POST['mode1']);
	$editcnt 	=	split(',',$_POST['editcnt']);		
    //RESET FORM
	if($_POST['Cancel']=="Cancel") { $mode=""; $mode1="";	}
	//RECORD EDIT HERE
	If($mode == "EDIT")
	{ 
		//EDIT ROW VALUES ARE GETTING
		$action=2;
	for($i=0;$i<count($editcnt);$i=$i+1) 
	{ 
		$medium_code	=	trim($_POST['medium_code_'.$editcnt[$i]]);
		$medium     	=	trim($_POST['medium_'.$editcnt[$i]]);
		$active			=	trim($_POST['active_'.$editcnt[$i]]);
		$j=$i+1;
		//VALIDATE INPUT
		$dummy = Strcheck($medium,$errmsg,$errflag,"Medium-".$j);
        //ALL INPUTS ARE CORRECTED, THEN GOTO SP
		if($errflag==0)
		{   
			//SP INITIALIZE AND VALUE BIND HERE
              mssql_free_result($result); 
			$query = mssql_init('sp_Medium',$mssql);
			mssql_bind($query,'@Medium_Code',$medium_code,SQLINT4,false,false,5);
			mssql_bind($query,'@Medium',$medium,SQLVARCHAR,false,false,50);
			mssql_bind($query,'@Active',$active,SQLVARCHAR,false,false,1);
			mssql_bind($query,'@Action',$action,SQLINT4,false,false,1);
			$result = @mssql_execute($query);//SP EXECUTE HERE
			mssql_free_statement($query);//$QUERY FREE HERE
			if($result==1)
				echo "<p class='mesg'>Medium has been Updated</p>";
			else {
				$errmsg1=mssql_get_last_message();//GET ERROR MESSAGE FROM SP
				$errflag=2;
			}
		}
		else {
			If ($errcnt == 0) { $errcnt = 1;} else { $errcnt = $errcnt + 1; }
			if ($errval == "") { $errval = $editcnt[$i];	 } else { $errval = $errval.",".$editcnt[$i]; } 
		}
	}	
	}
    //RECORD DELETE HERE
	if($mode=="DELETE")
	{	$action=3;
		$medium_code	=	trim($_POST['medium_code']);
         mssql_free_result($result); 
		$query = mssql_init('sp_Medium',$mssql);
		mssql_bind($query,'@Medium_Code',$medium_code,SQLINT4,false,false,5);
		mssql_bind($query,'@Medium',$medium,SQLVARCHAR,false,false,50);
		mssql_bind($query,'@Active',$active,SQLVARCHAR,false,false,1);
		mssql_bind($query,'@Action',$action,SQLINT4,false,false,1);
		$result = @mssql_execute($query);//SP EXECUTE HERE
		mssql_free_statement($query);//$QUERY EXECUTE HERE
		if($result==1)
			echo "<p class='mesg'>Medium has been Deleted</p>";
		else {
			$errmsg1=mssql_get_last_message();//ERROR MESSAGE GET FROM SP
			$errflag=2;
			}
	}
	//NEW RECORD INSERT HERE
	if($mode1 == "ADD")
	{	$action1=1;
		$medium1   =	trim($_POST['medium1']);
		$active1   =	trim($_POST['active1']);
        // VALIDATE THE INPUT
		$dummy      = Strcheck($medium1,$errmsg,$errflag,"Medium");
		
		
		if($errflag==0){
              mssql_free_result($result); 
			$query = mssql_init('sp_Medium',$mssql);
			mssql_bind($query,'@Medium_Code',$medium_code1,SQLINT4,false,false,5);
			mssql_bind($query,'@Medium',$medium1,SQLVARCHAR,false,false,50);
			mssql_bind($query,'@Active',$active1,SQLVARCHAR,false,false,1);
			mssql_bind($query,'@Action',$action1,SQLINT4,false,false,1);
			
			$result = @mssql_execute($query);
			mssql_free_statement($query);
		if($result==1)
			echo "<p class='mesg'>Medium has been Added</p>";
		else {
			$errmsg1=mssql_get_last_message();
			$errflag=2;
			$inserr=1;
			}
		} else $inserr=1;
	}
	if($errflag==1) 
		echo $errlbl.$errmsg;
	if($errflag==2) 
		echo "<p class='error'>".$errmsg1;
	
		?>
		<!--FORM DESIGNING AND HIDDEN VARRIABLE DECLARATION-->
		<body style="margin:0;">
		<form name="myform" id="myform" method="post" action="sms_medium.php">
		<input type="hidden" name="editcnt" id="editcnt"/>
		<input type="hidden" name="medium_code" id="medium_code"/>
		<input type="hidden" name="mode" id="mode"/>
		<input type="hidden" name="mode1" id="mode1"/>

		<table width="70%" height="450" border="0" align="center" cellpadding="3" cellspacing="3">
		<tr><td valign="top">
		<table width="100%" border="0" align="center" cellpadding="3" cellspacing="1">
		<?php titleheader(Medium,0);?>
		<tr align="center">
		<thead><colgroup><col width=5%><col width=5%><col width=25%><col width=25%></colgroup>
		<th>&nbsp;</th><th>&nbsp;</th><th align="center">Medium</th>
		<th>Active</th>
		</thead></tr>

		<?php   // New Record Insert
			$colorflag+=1; ?>
		<tr class=<?php if($colorflag%2==0) { echo "row2"; } else { echo "row1"; } ?> valign="center">
		<!--ADD NEW ICON-->
		<td align="center" ><img src="../images/new.gif" title="Add New" onclick="adddata();"/></td><td>&nbsp;</td>
		
		<td align="center"><div id ="medium1" style="display:none;">
		<input type="text" name="medium1" id="medium1"  size="25" maxlength="100" onkeydown="return alphaonly('medium1')" value="<?php echo $medium?>"><?php echo $mand; ?></div></td>
			
		<td align="center"><div id ="active1" style="display:none;"><select name="active1" id="active1"><option value="Y">Yes</option><option value="N">No</option></select></div></td>
		
	
		<?php 	// UPDATE & SHOW RECORDS
          mssql_free_result($result); 
		$query = mssql_init('sp_GetMedium',$mssql);
		$result = mssql_execute($query);
		mssql_free_statement($query);
		$rs_cnt = mssql_num_rows($result);
		$colorflag = 0;
		$i = 0;
		while($field = mssql_fetch_array($result))
		{	$i  +=1;	$colorflag+=1;
			$tot_rec = $i;	?>
		<tr class=<?php if($colorflag%2==0) { echo "row1"; } else { echo "row2"; } ?> valign="center">

		<input type="hidden" name="medium_code_<?php echo $i ?>" id ="medium_code_<?php echo $i ?>" value="<?php echo $field['Medium_Code']?>"	 />
		<!-- DELETE ICON-->
		<td align="center"><img src="../images/delete_d.gif" title="Remove" 
		onclick="deldata('<?php echo $field['Medium_Code'];?>');" />&nbsp;</td>	 
        <!--EDIT ICON-->
		<td align="center"><img src="../images/edit.gif" title="Edit" onclick="editdata('<?php echo $i;?>');" />&nbsp;</td>

		<td ><?php echo $field['Medium'] ?>
		<div id ="medium_<?php echo $i;?>" style="display:none;"><input type="text" name="medium_<?php echo $i ?>" id="medium_   <?php echo $i ?>"  size="25" maxlength="100" value="<?php echo $field['Medium']?>"  onkeydown="return alphaonly('medium_<?php echo $i ?>')"></div></td>
								
		<td align="center"><?php if(strtoupper($field['Active'])=="Y") echo "Yes"; else echo "No";?>
		<div id ="active_<?php echo $i;?>" style="display:none;">
			<select id="active_<?php echo $i ?>" name="active_<?php echo $i ?>">
		<option value="Y" <?php if(strtoupper($field['Active'])=="Y")  echo "selected" ?>>Yes</option>
		<option value="N" <?php if(strtoupper($field['Active'])=="N")  echo "selected" ?>>No</option>
		</select></div></td>

		
		</tr>
	    <?php } 
		//IF ANY ERROR IN EDIT, SHOW THAT ROW 
	    if ($errcnt > 0) {
		for($i=0;$i<=$errcnt-1;$i=$i+1){
		$editcnt = split(',',$_POST['editcnt']);?>
			<script>editdata(<?php echo $editcnt[$i];?>);</script>
		<?php } 
		} ?>	
		<input type="hidden" name="tot_rec" value="<?php echo $tot_rec ?>" />
	    <tr><td colspan="4" align="right">
		<input type="submit" name="save" value="Save" class="winbutton_go" />&nbsp;&nbsp;&nbsp;
		<input type="submit" name="Cancel" value="Cancel" class="winbutton_go"/></td></tr>
		<?php //IF ANY ERROR IN ADD, SHOW THAT ROW ?>
		<?php	if($inserr == 1) { ?><script>adddata();</script><?php } ?>

		</table></td></tr></table>
		</form></body></html>
		<?php include("../includes/copyright.php"); ?>