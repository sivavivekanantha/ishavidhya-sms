<?php session_start();?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Isha Foundation - A Non-profit Organization</title>
<?php include("../includes/header.php"); 
title('Student Management','Pre Comments',2,1,0);
?>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<style type="text/css">
<!--
.style1 {color: #CCFFFF}
-->
</style>
<script>
$(document).ready(function() {
    $('#example').dataTable( {
        "sScrollY": "400px",
        "bPaginate": false
    } );
} );
function deldata(val)
{ 

	var msg="Do you want to delete?";
	if(!confirm(msg)){
		return false;
	}	 
	else { 
		$('#CommentCode').val(val);
		
		var $mode ='DELETE';
		$('#mode').val($mode);
		$('#myform').submit();
	 }
}

function editdata(val)
{	
	if($('#editcnt').val() >0 ) return false;
    $('#editcnt').val(val);

	$('#Assment'+val).show();
	$('#EnglishComment'+val).show();
	$('#TamilComment'+val).show();
		var $mode ='EDIT';
	$('#mode').val($mode);

}
function adddata()
 { 
 	$('#Assment').show();
	$('#EnglishComment').show();
	$('#TamilComment').show();
	
	var $mode1 ='ADD';
	$('#mode1').val($mode1);
}
</script>
</head>

<body>
<?php 
	$errmsg="";
	$errflag=0;
	$dummy=0;
	$mode		=	$_POST['mode'];
	$mode1		=	$_POST['mode1'];
	$editcnt 	=	split(',',$_POST['editcnt']);		

	if($_POST['Cancel']=="Cancel") { $mode=""; $mode1="";	}
	if($mode1 == "ADD")
	{	$action1=1;
		$Assment1	    =	trim($_POST['Assment']);		
		$EnglishComment1=	trim($_POST['EnglishComment']);		
		$TamilComment1  =trim($_POST['TamilComment']);	
		
		
	
		//error messge//
		$dummy = Zerocheck($Assment1,$errmsg,$errflag,"Assment");
		$dummy = Strcheck($EnglishComment1,$errmsg,$errflag,"English Comment");
	//	$dummy = Strcheck($TamilComment1,$errmsg,$errflag,"Tamil Comment");
	
	
	
		if($errflag==0)
		{
	          mssql_free_result($result); 
			$query = mssql_init('sp_SMSPreComments',$mssql);
		    
			mssql_bind($query,'@CommentCode',$CommentCode1,SQLINT4,false,false,5);
			mssql_bind($query,'@AssCode',$Assment1,SQLINT4,false,false,50);
			mssql_bind($query,'@EnglishComment',$EnglishComment1,SQLVARCHAR,false,false,25);
		    mssql_bind($query,'@TamilComment',$TamilComment1,SQLTEXT,false,false,50);
			
			mssql_bind($query,'@Action',$action1,SQLINT4,false,false,1);
				
			$result = @mssql_execute($query);
			
			
			mssql_free_statement($query);
			
		if($result==1)
			{
			echo "<p class='mesg'>Pre Comments has been Added</p>";
			$Assment1			="";		
			$EnglishComment1	="";	
			$TamilComment1		="";
			}
		else {
		
			$errmsg1=mssql_get_last_message();
				
			
			$errflag=2;
			$inserr=1;
			}
		} else $inserr=1;
	}	
	
	If($mode == "EDIT")	
	{ $action=2;
	
	for($i=0;$i<count($editcnt);$i=$i+1) 
	{ 	
		$CommentCode= trim($_POST['CommentCode'.$editcnt[$i]]);
	
		$Assment	=	trim($_POST['Assment'.$editcnt[$i]]);		
	
		$EnglishComment	= trim($_POST['EnglishComment'.$editcnt[$i]]);

		$TamilComment     = trim($_POST['TamilComment'.$editcnt[$i]]);
	
			$j=$i+1;		
		$dummy = Strcheck($EnglishComment,$errmsg,$errflag,"English Comment".$j);	
		$dummy = Strcheck($EnglishComment,$errmsg,$errflag,"English Comment".$j);	
		//$dummy = Strcheck($TamilComment,$errmsg,$errflag,"Tamil Comment".$j);	
			if($errflag==0)
		{
		    mssql_free_result($result); 
			$query = mssql_init('sp_SMSPreComments',$mssql);
					
			mssql_bind($query,'@CommentCode',$CommentCode,SQLINT4,false,false,5);
			mssql_bind($query,'@AssCode',$Assment,SQLINT4,false,false,50);
			mssql_bind($query,'@EnglishComment',$EnglishComment,SQLVARCHAR,false,false,25);
			mssql_bind($query,'@TamilComment',$TamilComment,SQLTEXT,false,false,50);
			mssql_bind($query,'@Action',$action,SQLINT4,false,false,1);
			$result = @mssql_execute($query);
			mssql_free_statement($query);
			if($result==1)
				echo "<p class='mesg'>Pre Comments has been Updated</p>";
			else {
				$errmsg1=mssql_get_last_message();
				$errflag=2;
				If ($errcnt == 0) { $errcnt = 1;} else { $errcnt = $errcnt + 1; }
			if ($errval == "") { $errval = $editcnt[$i]; } else { $errval = $errval.",".$editcnt[$i]; } 
				}
		}
		else {
			If ($errcnt == 0) { $errcnt = 1;} else { $errcnt = $errcnt + 1; }
			if ($errval == "") { $errval = $editcnt[$i];	 } else { $errval = $errval.",".$editcnt[$i]; } 
	
	}	}
	}
	
	
	
	if($mode=="DELETE")		{	
	$action=3;	
		$CommentCode=$_POST['CommentCode'];	
        
        mssql_free_result($result); 		
		$query = mssql_init('sp_SMSPreComments',$mssql);
					
			mssql_bind($query,'@CommentCode',$CommentCode,SQLINT4,false,false,5);
			mssql_bind($query,'@AssCode',$Assment,SQLINT4,false,false,50);
			mssql_bind($query,'@EnglishComment',$EnglishComment,SQLVARCHAR,false,false,25);
			mssql_bind($query,'@TamilComment',$TamilComment,SQLTEXT,false,false,50);
			mssql_bind($query,'@Action',$action,SQLINT4,false,false,1);
			$result = @mssql_execute($query);
			
		mssql_free_statement($query);
		if($result==1)
			echo "<p class='mesg'>Pre Comments has been Deleted</p>";
		else {

			$errmsg1=mssql_get_last_message();		
			$errflag=2;
			}
	}
	
	
	
		if($errflag==1) 
		echo $errlbl.$errmsg;
	if($errflag==2) 
		echo "<p class='error'>".$errmsg1;	
	?>


<form name="myform" id="myform" method="post" action="sms_pre_comments.php">
<input type="hidden" name="CommentCode" id="CommentCode"/>
<input type="hidden" name="mode" id="mode"/>	
<input type="hidden" name="mode1" id="mode1"/>
<input type="hidden" name="editcnt" id="editcnt"/>
<table    width="100%" height="450" border="0" align="right" cellpadding="3" cellspacing="3">

  <tr>
    <td valign="top"><table id="example"  width="90%" border="0" align="right" cellpadding="3" cellspacing="1">
	<?php titleheader('Pre Comments',0);?>
     
      <tr align="center"> </tr>
  <thead>
   <colgroup>
        <col width="5%" />
        <col width="5%" />
        <col width="25%" />
        <col width="25%" />
        <col width="25%" />
       
        </colgroup>
  </thead>
      <tr>
      <thead>
   
        <th>&nbsp;</th>
        <th>&nbsp;</th>
        <th >Assessment</th>
        
        <th >English_Comment
</th>
         <th >Tamil_Comment</th>
       
        <td></thead>
        </td></tr>
        <?php   // New Record Insert
		$colorflag+=1; ?>
	<tr class=<?php if($colorflag%2==0) { echo "row2"; } else { echo "row1"; } ?> valign="center">
	
	<td align="center" ><img src="../images/new.gif" title="Add New" onclick="adddata();"/></td>
    <td>&nbsp;</td>

     <?php 	//Assesment CITY DROPDOWN
         mssql_free_result($result); 
		$query = mssql_init('sp_FetchAssessment',$mssql);
		$result = mssql_execute($query);
		mssql_free_statement($query);	?>
        
    <td align="center"><div id ="Assment" style="display:none;">
   <select id="Assment" name="Assment" >
   <?php echo $Select;
   
   while($field = mssql_fetch_array($result)) 
   {  ?>
   	<option value="<?php echo $field['Ass_Code']?>" <?php if($Assment1==$field['Ass_Code']) echo "selected"; ?>>
			<?php echo $field['E_Assessment']?></option>
	 <?php } ?>
     </select><?php echo $mand; ?></div>
   
      </td> 
    <td align="center"><div id ="EnglishComment" style="display:none;" >
    <input type="text" name="EnglishComment" id="EnglishComment"  size="150"  value="<?php echo $EnglishComment1 ?>" 
	onkeydown="return alphaonly('EnglishComment')" ><?php echo $mand; ?></div></td>  
     
     <td align="center"><div id ="TamilComment" style="display:none;" >
    <input type="text" name="TamilComment" id="TamilComment"   value="<?php echo $TamilComment1 ?>" 
	onkeydown="return alphaonly('TamilComment')" ><?php echo $mand; ?></div>
    </td>  
      
      </tr>
     <?php 	// UPDATE & SHOW RECORDS
        mssql_free_result($result); 
		$query = mssql_init('sp_GetSMSPreComments',$mssql);
		$result = mssql_execute($query);
		mssql_free_statement($query);
		$rs_cnt = mssql_num_rows($result);
		$colorflag = 0;
		$i = 0;
		
		while($field = mssql_fetch_array($result))
		{	$i  +=1;	$colorflag+=1;
			$tot_rec = $i;?>
            
		
        <tr class=<?php if($colorflag%2==0) { echo "row1"; } else { echo "row2"; } ?> valign="center">

		<input type="hidden" name="CommentCode<?php echo $i ?>" id ="CommentCode<?php echo $i ?>" value="<?php echo $field['CommentCode']?>"/>		
		<td align="center"><img src="../images/delete_d.gif" title="Remove" onclick="deldata('<?php echo $field['CommentCode'];?>');" />&nbsp;
        </td>
		<td align="center"><img src="../images/edit.gif" title="Edit" onclick="editdata('<?php echo $i;?>');" />&nbsp;</td>	
        	       
      
           <?php 	//SHOW State DROPDOWN
             mssql_free_result($result1); 
		$query = mssql_init('sp_FetchAssessment',$mssql);
		$result1 = mssql_execute($query);
		mssql_free_statement($query);	?>  
                		
        <td align="center"><?php echo $field['E_Assessment'] ?>    
            
				<div id ="Assment<?php echo $i;?>" style="display:none;">
                <select name="Assment<?php echo $i ?>" id="Assment<?php echo $i ?>">
				<?php	 echo $Select;
				
				while($field1 = mssql_fetch_array($result1)) {  ?>
			
            	<option value="<?php echo $field1['Ass_Code']?>" <?php if($field['AssCode']==$field1['Ass_Code']) echo "Selected";?>><?php echo 			                 $field1['E_Assessment']?></option>
	 			<?php } ?>
     
	 </select></div></td>	
     <td ><?php echo $field['EnglishComment'] ?>	<div id ="EnglishComment<?php echo $i;?>" style="display:none;"><input type="text" name="EnglishComment<?php echo $i ?>" id="EnglishComment<?php echo $i ?>"  size="150" value="<?php echo $field['EnglishComment']?>"  onkeydown="return alphaonly('EnglishComment<?php echo $i ?>')"></div></td> 
     
          <td ><?php echo $field['TamilComment'] ?><div id ="TamilComment<?php echo $i;?>" style="display:none;"><input type="text" name="TamilComment<?php echo $i ?>" id="TamilComment<?php echo $i ?>"  size="25" value="<?php echo $field['TamilComment']?>"  onkeydown="return alphaonly('TamilComment<?php echo $i ?>')"></div></td> 
     							
		
				
		</tr>      
          <?php } 
	if ($errcnt > 0) {
		for($i=0;$i<=$errcnt-1;$i=$i+1){
		$editcnt = split(',',$_POST['editcnt']);?>
			<script>editdata(<?php echo $editcnt[$i];?>);</script>
		<?php } 
		} ?>    
    
       <input type="hidden" name="tot_rec" value="<?php echo $tot_rec ?>" />
            <tr><td colspan="5" align="right">
		<input type="submit" name="save" value="Save" class="winbutton_go" />&nbsp;&nbsp;&nbsp;
		<input type="submit" name="Cancel" value="Cancel" class="winbutton_go"/></table></td></tr>
       
        </table>
     <?php	if($inserr == 1) { ?><script>adddata();</script><?php } ?>
    </td></tr>
</table>

</body>
</form>
</html>
<?php include("../Includes/copyright.php"); ?>