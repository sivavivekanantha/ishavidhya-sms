<?php session_start(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>Isha Foundation - A Non-profit Organization</title>

<?php include("../includes/header.php");
 title('Student Management System','Section',2,1,0);
 ?>
<script>
 

function adddata()
 { 
 	$('#section1').show();
	$('#active1').show();
	var $mode1 ='ADD';
	$('#mode1').val($mode1);
} 


function editdata(val)
{
	if($('#editcnt').val() >0 ) return false;
    $('#editcnt').val(val);

	$('#section_'+val).show();
	$('#active_'+val).show();

	var $mode ='EDIT';
	$('#mode').val($mode);

}
function deldata(val)
{ 
	var msg="Do you want to delete?";
	if(!confirm(msg)){
		return false;
	}	 
	else {
		$('#section_code').val(val);
		var $mode ='DELETE';
		$('#mode').val($mode);
		$('#myform').submit();
	 }
}
</script>
</head>

<?php 
    //VARIABLE DECLARATION
	$errmsg="";
	$errflag=0;
	$dummy=0;
	// GET MODE VALUE FOR WHICH FUNCTION PERFORMED (ADD / EDIT/ DELETE)
	$mode		=	trim($_POST['mode']);
	$mode1		=	trim($_POST['mode1']);
	$editcnt 	=	split(',',$_POST['editcnt']);// ROW ID ( SERIAL NO) IS GETTING	
    //RESET FORM
	if($_POST['Cancel']=="Cancel") { $mode=""; $mode1="";	}
    //RECORD EDIT PART
	If($mode == "EDIT")
	{ $action=2;
	//EDIT ROW VALUES ARE GETTING
	for($i=0;$i<count($editcnt);$i=$i+1) 
	{   //unwanted spaces are trim	  
		$section_code	=	trim($_POST['section_code_'.$editcnt[$i]]);
		$section     	=	trim($_POST['section_'.$editcnt[$i]]);
		$active			=	trim($_POST['active_'.$editcnt[$i]]);
		$j=$i+1;
		//INPUT VALIDATION HERE
		$dummy = Strcheck($section,$errmsg,$errflag,"Section-".$j);
		//ALL INPUTS ARE CORRECTED, THEN GOTO SP
		if($errflag==0)
		{    //STORED PROCEDURE INITIALIZED  AND BACKEND CONNECTION 
            mssql_free_result($result); 
			$query = mssql_init('sp_Section',$mssql);
			mssql_bind($query,'@Section_Code',$section_code,SQLINT4,false,false,5);
			mssql_bind($query,'@Section',$section,SQLVARCHAR,false,false,50);
			mssql_bind($query,'@Active',$active,SQLVARCHAR,false,false,1);
			mssql_bind($query,'@Action',$action,SQLINT4,false,false,1);
			$result = @mssql_execute($query);//STORED PROCEDURE EXECUTE
			mssql_free_statement($query);//$QUERY FREE HERE
			//STORED PROCEDURE SUCCESSFULLY EXECUTE HERE 
			if($result==1)
				echo "<p class='mesg'>Section has been Updated</p>";
			//INPUT VALUES WRONG
			else {
				$errmsg1=mssql_get_last_message();//ERROR MESSEAGE GET FROM SP
				$errflag=2;
			}
		}
		else {
			If ($errcnt == 0) { $errcnt = 1;} else { $errcnt = $errcnt + 1; }
			if ($errval == "") { $errval = $editcnt[$i];	 } else { $errval = $errval.",".$editcnt[$i]; } 
		}
	}	
	}
    //RECORD DELETE PART
	if($mode=="DELETE")
	{	$action=3;
		$section_code	=	trim($_POST['section_code']);
          mssql_free_result($result); 
		$query = mssql_init('sp_Section',$mssql);
		mssql_bind($query,'@Section_Code',$section_code,SQLINT4,false,false,5);
		mssql_bind($query,'@Section',$section,SQLVARCHAR,false,false,50);
		mssql_bind($query,'@Active',$active,SQLVARCHAR,false,false,1);
		mssql_bind($query,'@Action',$action,SQLINT4,false,false,1);
		$result = @mssql_execute($query);
		mssql_free_statement($query);
		if($result==1)
			echo "<p class='mesg'>Section has been Deleted</p>";
		else {
			$errmsg1=mssql_get_last_message();
			$errflag=2;
			}
	}
	//RECORD ADD PART
	if($mode1 == "ADD")
	{	$action1=1;
		$section1    =	trim($_POST['section1']);
		$active1	 =	trim($_POST['active1']);
		//error messge//
		$dummy      = Strcheck($section1,$errmsg,$errflag,"Section");
		
		
		if($errflag==0){
              mssql_free_result($result); 
			$query = mssql_init('sp_Section',$mssql);
			mssql_bind($query,'@Section_Code',$section_code,SQLINT4,false,false,5);
			mssql_bind($query,'@Section',$section1,SQLVARCHAR,false,false,50);
			mssql_bind($query,'@Active',$active1,SQLVARCHAR,false,false,1);
			mssql_bind($query,'@Action',$action1,SQLINT4,false,false,1);
			
			$result = @mssql_execute($query);
			mssql_free_statement($query);
		if($result==1){
			echo "<p class='mesg'>Section has been Added</p>";
            $section1=$active1="";
            }
		else {
			$errmsg1=mssql_get_last_message();
			$errflag=2;
			$inserr=1;
			}
		} else $inserr=1;
	}
	if($errflag==1) 
		echo $errlbl.$errmsg;
	if($errflag==2) 
		echo "<p class='error'>".$errmsg1;
	
		?>
		<body style="margin:0;">
		<!-- FORM DESIGN AND HIDDEN VARRIABLES -->
		<form name="myform" id="myform" method="post" action="sms_section.php">
		<input type="hidden" name="editcnt" id="editcnt"/><input type="hidden" name="section_code" id="section_code"/>
		<input type="hidden" name="mode" id="mode"/>	<input type="hidden" name="mode1" id="mode1"/>
		<table width="70%" border="0" align="center" cellpadding="3" cellspacing="3">
		<tr><td>
		<table width="100%" border="0" align="center" cellpadding="3" cellspacing="1">
		<?php titleheader(Section,0);?>
		<tr align="center">
		<thead><colgroup><col width="3%"><col width="3%"><col width="11%"><col width="10%"></colgroup>
		<th>&nbsp;</th><th>&nbsp;</th><th align="center">Section</th>
		<th>Active</th>
		</thead></tr>

		<?php   // New Record Insert
			$colorflag+=1; ?>
		<tr class=<?php if($colorflag%2==0) { echo "row2"; } else { echo "row1"; } ?> valign="center">
		
		<td align="center" ><img src="../images/new.gif" title="Add New" onclick="adddata();"/></td><td>&nbsp;</td>
		
		<td align="center"><div id ="section1" style="display:none;">
		<input type="text" name="section1" id="section1" maxlength="5" onkeydown="return alphaonly('section1')" value="<?php echo $section1 ?>"><?php echo $mand; ?></div></td>
			
		<td align="center"><div id ="active1" style="display:none;"><select name="active1" id="active1"><option value="Y" <?php if($active1=="Y") echo "Selected"; ?> >Yes</option><option value="N" <?php if($active1=="N") echo "Selected"; ?>>No</option></select></div></td>
	
	
<?php 	// UPDATE & SHOW RECORDS
        mssql_free_result($result); 
		$query = mssql_init('sp_GetSection',$mssql);
		$result = mssql_execute($query);
		mssql_free_statement($query);
		$rs_cnt = mssql_num_rows($result);
		$colorflag = 0;
		$i = 0;
		while($field = mssql_fetch_array($result))
		{	$i  +=1;	$colorflag+=1;
			$tot_rec = $i;	?>
		<tr class=<?php if($colorflag%2==0) { echo "row1"; } else { echo "row2"; } ?> valign="center">
		<input type="hidden" name="section_code_<?php echo $i ?>" id ="section_code_<?php echo $i ?>" value="<?php echo $field['Section_Code']?>"	 />
		<!--DELETE ICON-->
		<td align="center"><img src="../images/delete_d.gif" title="Remove" onclick="deldata('<?php echo $field['Section_Code'];?>');" />&nbsp;</td>	 
        <!--EDIT ICON-->
		<td align="center"><img src="../images/edit.gif" title="Edit" onclick="editdata('<?php echo $i;?>');" />&nbsp;</td>
		<td align="center"><?php echo $field['Section'] ?>
		<div id ="section_<?php echo $i;?>" style="display:none;"><input type="text" name="section_<?php echo $i ?>" id="section_<?php echo $i ?>"  maxlength="5" value="<?php echo $field['Section']?>"  onkeydown="return alphaonly('section_<?php echo $i ?>')"></div></td>
		<td align="center"><?php if(strtoupper($field['Active'])=="Y") echo "Yes"; else echo "No";?>
		<div id ="active_<?php echo $i;?>" style="display:none;">
		<select id="active_<?php echo $i ?>" name="active_<?php echo $i ?>">
		<option value="Y" <?php if(strtoupper($field['Active'])=="Y")  echo "selected" ?>>Yes</option>
		<option value="N" <?php if(strtoupper($field['Active'])=="N")  echo "selected" ?>>No</option>
		</select></div></td>

		
		</tr>
	  <?php } 
		 //IF ANY ERROR IN EDIT, SHOW THAT ROW
	  if ($errcnt > 0) {
		 for($i=0;$i<=$errcnt-1;$i=$i+1){
		 $editcnt = split(',',$_POST['editcnt']);?>
		 <script>editdata(<?php echo $editcnt[$i];?>);</script>
		 <?php } 
		 } ?>	
		 <input type="hidden" name="tot_rec" value="<?php echo $tot_rec ?>" />
		 <tr><td colspan="4" align="right">
		 <input type="submit" name="save" value="Save" class="winbutton_go" />&nbsp;&nbsp;&nbsp;
         <input type="submit" name="Cancel" value="Cancel" class="winbutton_go"/></td></tr>
		 <?php //IF ANY ERROR IN ADD, SHOW THAT ROW ?>
		 <?php	if($inserr == 1) { ?><script>adddata();</script><?php } ?>
		 </table></td></tr></table>
		 </form></body></html>
		 <?php include("../includes/copyright.php"); ?>