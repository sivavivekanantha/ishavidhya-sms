<?php session_start();?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Isha Foundation - A Non-profit Organization</title>
<?php include("../includes/header.php");
 title('Student Management System','Subject',2,1,0);
?>	
 
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<script>



function editdata(val)
{ 
	if($('#editcnt').val() >0 ) return false;
    $('#editcnt').val(val);

	$('#Subject_'+val).show();
	$('#TSubject_'+val).show();
	$('#class_'+val).show();
	$('#active_'+val).show();
	$('#displayorder_'+val).show();	
	var $mode ='EDIT';
	$('#mode').val($mode);

}

function adddata()
{
$('#Subject').show();
$('#TSubject').show();
$('#Class').show();
$('#displayorder').show();
$('#active').show();
var $mode1 ='ADD';
	$('#mode1').val($mode1);
}
function deldata(val)
{ 

	var msg="Do you want to delete?";
	if(!confirm(msg)){
		return false;
	}	 
	else { 
		$('#Sub_Code').val(val);
	
		var $mode ='DELETE';
		$('#mode').val($mode);
		$('#myform').submit();
	 }
}

</script>
</head>


<body style="margin:0;">
<?php
$errmsg="";
	$errflag=0;
	$dummy=0;
	$mode		=	trim($_POST['mode']);
	$mode1		=	trim($_POST['mode1']);
	$editcnt 	=	split(',',$_POST['editcnt']);
	if($_POST['Cancel']=="Cancel") { $mode=""; $mode1="";	
	}
		
	If($mode == "EDIT")
	{ $action=2;

	for($i=0;$i<count($editcnt);$i++) 
	{ 	
		
	$Sub_Code=trim($_POST['Sub_Code_'.$editcnt[$i]]);
	$Subject=trim($_POST['Subject_'.$editcnt[$i]]);	
	$TSubject=trim($_POST['TSubject_'.$editcnt[$i]]);
	$checkBox = $_POST['Class_'.$editcnt[$i]];
	
    for($j=0; $j<sizeof($checkBox); $j++)
	{	
			$Class=$Class.$checkBox[$j].",";
	}
    $Class=substr($Class,0,-1);	
	$Display_Order	=Trim($_POST['displayorder_'.$editcnt[$i]]);	
	$Active=Trim($_POST['active_'.$editcnt[$i]]);	
		
		$j=$i+1;
	    $dummy = Strcheck($Subject,$errmsg,$errflag,"Subject Name in English");
		$dummy = Strcheck($TSubject,$errmsg,$errflag,"Subject Name in English");
		$dummy = Strcheck($Class,$errmsg,$errflag,"Class");
		$dummy = Numcheck($Display_Order,$errmsg,$errflag,"Display Order");
	if($errflag==0)
		{   
		    mssql_free_result($result); 
			$query = mssql_init('sp_SMS_Subject',$mssql);
			mssql_bind($query,'@Sub_Code',$Sub_Code,SQLINT4,false,false,5);
			mssql_bind($query,'@Subject',$Subject,SQLVARCHAR,false,false,50);
			mssql_bind($query,'@T_Subject',$TSubject,SQLVARCHAR,false,false,25);
			mssql_bind($query,'@Class_Code',$Class,SQLVARCHAR,false,false,5);
			mssql_bind($query,'@Display_Order',$Display_Order,SQLINT4,false,false,5);
			mssql_bind($query,'@Active',$Active,SQLVARCHAR,false,false,1);			
			mssql_bind($query,'@Action',$action,SQLINT4,false,false,1);
			$result = @mssql_execute($query);
			mssql_free_statement($query);
			if($result==1)
				echo "<p class='mesg'>Subject has been Updated</p>";
			else {
				$errmsg1=mssql_get_last_message();
				$errflag=2;
			If ($errcnt == 0) { $errcnt = 1;} else { $errcnt = $errcnt + 1; }
			if ($errval == "") { $errval = $editcnt[$i];	 } else { $errval = $errval.",".$editcnt[$i]; }
			}
		}
		else {
			If ($errcnt == 0) { $errcnt = 1;} else { $errcnt = $errcnt + 1; }
			if ($errval == "") { $errval = $editcnt[$i];	 } else { $errval = $errval.",".$editcnt[$i]; } 
		}
	}	
	}
	
		
	if($mode1 == "ADD")
	{
	$action1=1;
	$Subject1=trim($_POST['Subject']);	
	$TSubject1=trim($_POST['TSubject']);	
	
	$checkBox1= $_POST['Class'];
	
    for($i=0; $i<sizeof($checkBox1); $i++)
	{	
			$Class1=$Class1.$checkBox1[$i].",";
	}		
    $Class1=substr($Class1,0,-1);

	$Display_Order1	=trim($_POST['displayorder']);		
	$Active1=trim($_POST['active']);	
	
	$dummy = Strcheck($Subject1,$errmsg,$errflag,"Subject Name in English");
	$dummy = Strcheck($TSubject1,$errmsg,$errflag,"Subject Name in Tamil");		
	$dummy = Strcheck($Class1,$errmsg,$errflag,"Class");		
	$dummy = Numcheck($Display_Order1,$errmsg,$errflag,"Display Order");	
	$dummy = Strcheck($Active1,$errmsg,$errflag,"Active");
	
	if($errflag==0)
	{
	          mssql_free_result($result); 
			$query = mssql_init('[sp_SMS_Subject]',$mssql);		
			mssql_bind($query,'@Sub_Code',$Sub_Code1,SQLINT4,false,false,5);				
			mssql_bind($query,'@Subject',$Subject1,SQLVARCHAR,false,false,50);
		 	mssql_bind($query,'@T_Subject',$TSubject1,SQLTEXT ,false,false,50);
			mssql_bind($query,'@Class_Code',$Class1,SQLVARCHAR,false,false,50);	
			mssql_bind($query,'@Display_Order',$Display_Order1,SQLINT4,false,false,5);
			mssql_bind($query,'@Active',$Active1,SQLVARCHAR,false,false,1);			
			mssql_bind($query,'@Action',$action1,SQLINT4,false,false,1);
				
			$result = @mssql_execute($query);		
			
			mssql_free_statement($query);
			
		if($result==1){
			echo "<p class='mesg'>Subject has been Added</p>";
      
             $Subject1= $TSubject1= $checkBox1=$Class1=$displayorder1=$Active1="";
             }
		else {
		
			$errmsg1=mssql_get_last_message();
			$errflag=2;
			$inserr=1;
			}
		} else $inserr=1;
	}
if($mode=="DELETE")
		
	{	
	$action=3;
	
		$Sub_Code=trim($_POST['Sub_Code']);
	         mssql_free_result($result); 
			$query = mssql_init('[sp_SMS_Subject]',$mssql);		
			mssql_bind($query,'@Sub_Code',$Sub_Code,SQLINT4,false,false,5);				
			mssql_bind($query,'@Subject',$Subject,SQLVARCHAR,false,false,50);
		 	mssql_bind($query,'@T_Subject',$TSubject,SQLTEXT ,false,false,50);
			mssql_bind($query,'@Class_Code',$Class,SQLVARCHAR,false,false,50);	
			mssql_bind($query,'@Display_Order',$Display_Order,SQLINT4,false,false,5);
			mssql_bind($query,'@Active',$Active,SQLVARCHAR,false,false,1);			
			mssql_bind($query,'@Action',$action,SQLINT4,false,false,1);
			
			$result = @mssql_execute($query);
		mssql_free_statement($query);
		if($result==1)
			echo "<p class='mesg'>Subject has been Deleted</p>";
		else {

			$errmsg1=mssql_get_last_message();		
			$errflag=2;
			}
	}
	if($errflag==1)  	 
		echo $errlbl.$errmsg;
	if($errflag==2) 
		echo "<p class='error'>".$errmsg1;
?>	

<form name="myform" id="myform" method="post" action="sms_subject.php"> 

<input type="hidden" name="Sub_Code" id="Sub_Code"/>
<input type="hidden" name="Class_Code" id="Class_Code"/>
<input type="hidden" name="mode" id="mode"/>	
<input type="hidden" name="mode1" id="mode1"/>
<input type="hidden" name="editcnt" id="editcnt"/>
<table width="100%" height="450" cellpadding="3" cellspacing="3" border="0" align="center">
<tr>
    <td valign="top"><table width="80%" border="0" align="center" cellpadding="3" cellspacing="1">
	<?php titleheader(subject,0);?>
           <tr align="center"> </tr>
           <tr>
           <thead>
	   <colgroup>
        <col width="5%" />
        <col width="5%" />
        <col width="20%" />
        <col width="20%" />
        <col width="20%" />
         <col width="10%" />
        <col width="10%" />
        </colgroup>
        <th>&nbsp;</th>
        <th>&nbsp;</th>
        <th >Subject Name in English</th>  
        <th>Subject Name in Tamil</th>  
        <th>Class</th>          
        <th>Display Order</th>
        <th>Active</th>
        <td></thead>
        </td></tr>
        <?php $colorflag+=1; ?>
	<tr class=<?php if($colorflag%2==0) { echo "row2"; } else { echo "row1"; } ?> valign="center">
         	
        <td align="center" ><img src="../images/new.gif" title="Add New" onclick="adddata();"/></td>
        <td>&nbsp;</td>
        <td><div id="Subject" style="display:none;" ><input type="text" name="Subject" id="Subject" size="20" maxlength="100" value="<?php echo $Subject1 ?>" onkeydown="return alphaonly('Subject')" ><?php echo $mand; ?></div></td>
        <td><div id="TSubject" style="display:none;" ><input type="text" name="TSubject" id="TSubject" size="20" maxlength="100" value="<?php echo $TSubject1 ?>" onkeydown="return alphaonly('TSubject')" ><?php echo $mand; ?></div>
        
</td>

        <td><div id="Class" style="display:none;"><div style="overflow:auto;width:120px;height:70px;border:1px solid #336699;padding-left:5px; background: #FFFFFF; ">
      
       	<?php 
             mssql_free_result($result); 
           $query = mssql_init('sp_GetClass',$mssql);
		$result = mssql_execute($query);
		mssql_free_statement($query);
		$rs_cnt = mssql_num_rows($result);
		$colorflag = 0;
		$i = 0; 
		$slcode= split(",",$Class1);
		
		while($field = mssql_fetch_array($result))
		{	
		$i  +=1;	$colorflag+=1;
			$tot_rec = $i;	?> 
        
        <input type="checkbox" id="Class" name="Class[]" class="radio"  value="<?php echo $field['Class_Code']?>"         
		<?php if(in_array($field['Class_Code'],$slcode)) echo "checked"; ?> ><?php echo $field['Class']; ?><br>
		<?php } ?></div> <?php echo $mand; ?></div></td>
       
 
         <td><div id="displayorder" style="display:none;" ><input type="text" id="displayorder" name="displayorder" size="5" maxlength="3" value="<?php echo $Display_Order1 ?>" onkeydown="return numberonly('displayorder')"/><?php echo $mand; ?></div></td>
        <td><div id="active" style="display:none;"><select name="active" id="active"><option value="Y" <?php if($Active1=="Y") echo "Selected"; ?>>Yes</option><option value="N" <?php if($Active1=="N") echo "Selected"; ?>>No</option></select></div></td>
        </tr>
      
            
        <?php
          mssql_free_result($result); 
        $query = mssql_init('sp_GetSubject',$mssql);
		$result = mssql_execute($query);
		

		 mssql_free_statement($query);
		$rs_cnt = mssql_num_rows($result);
		$colorflag = 0;
		$i = 0;
		while($field = mssql_fetch_array($result))
		{	
		$i  +=1;	$colorflag+=1;
			$tot_rec = $i;	?>   
        
        <tr class=<?php if($colorflag%2==0) { echo "row1"; } else { echo "row2"; } ?> valign="center">
        
        <input type="hidden" name="Sub_Code_<?php echo $i ?>" id ="Sub_Code_<?php echo $i ?>" value="<?php echo $field['Sub_Code']?>"	 />	
        <td align="center"><img src="../images/delete_d.gif" title="Remove" onclick="deldata('<?php echo $field['Sub_Code'];?>');" />&nbsp;</td>
        <td align="center"><img src="../images/edit.gif" title="Edit" onclick="editdata('<?php echo $i;?>');" />&nbsp;</td>    
        
        
        <td align="center"><?php echo $field['Subject'] ?>       
        	<div id ="Subject_<?php echo $i;?>" style="display:none;"><input type="text" name="Subject_<?php echo $i ?>" id="Subject_<?php echo $i ?>"  size="25" maxlength="100" value="<?php echo $field['Subject']?>"  onkeydown="return alphaonly('Subject_<?php echo $i ?>')"></div></td> 
            
        <td align="center"><?php echo $field['T_Subject'] ?>       
        	<div id ="TSubject_<?php echo $i;?>" style="display:none;"><input type="text" name="TSubject_<?php echo $i ?>" id="TSubject_<?php echo $i ?>"  size="25" maxlength="100" value="<?php echo $field['T_Subject']?>"  onkeydown="return alphaonly('TSubject_<?php echo $i ?>')"></div></td> 
            <td><?php 
			
			$wordChunks = explode(",", $field['Class_Code']);
			for($j = 0; $j < count($wordChunks); $j++)
			{
				$sqlquery="select Class from SMS_Class where Class_Code='".$wordChunks[$j]."' order by Display_Order";
				$results= mssql_query($sqlquery);
				$Class1="";
				$count_class=0;
				while ($row=mssql_fetch_array($results))
				{
				 	  
					  $Class1=$Class1.$row['Class']." ";  
					  $count_class++;
					  $class_code=$row['Class_Code'];
					
					echo $class_code;

						//	$Class1=substr($Class1,0,-1);
						echo $Class1;
				}
				
		}
     		
	?>		
            
            <div id ="class_<?php echo $i;?>" style="display:none;">
            <div style="overflow:auto;width:120px;height:70px;border:1px solid #336699;padding-left:5px; background: #FFFFFF; ">
            <?php
            
			$sqlquery="select Class,Class_Code from SMS_Class  where Active='Y' order by Display_Order";
			$rowClass= mssql_query($sqlquery);
			while ($dataClass=mssql_fetch_array($rowClass))
				{				 	  
					  
			?> 

   
 <input type="checkbox" id="Class_<?php echo $i;?>" name="Class_<?php echo $i;?>[]" class="radio"  value="<?php echo $dataClass['Class_Code']?>" <?php if(in_array($dataClass['Class_Code'],$wordChunks))  echo "Checked"; ?> ><?php echo $dataClass['Class']; ?><br>
		<?php 
					
		}		
		
		 ?></div> </div> 

            
                
			</td>
            
        <td align="center"><?php echo $field['Display_Order'] ?>
		<div id ="displayorder_<?php echo $i;?>" style="display:none;"><input type="text" name="displayorder_<?php echo $i ?>" id="displayorder_<?php echo $i ?>"  size="5" maxlength="3" value="<?php echo $field['Display_Order']?>" onkeydown="return numberonly('displayorder_<?php echo $i ?>')"></div></td>
        
          <td align="center"><?php if(strtoupper($field['Active'])=="Y") echo "Yes"; else echo  "No";?>
		<div id ="active_<?php echo $i;?>" style="display:none;">
			<select id="active_<?php echo $i ?>" name="active_<?php echo $i ?>">
            
		<option value="Y" <?php if(strtoupper($field['Active'])==="Y")  echo "selected" ?>>Yes</option>
		<option value="N" <?php if(strtoupper($field['Active'])==="N")  echo "selected" ?>>No</option>
		</select></div></td>    
        </tr>
         
          <?php } 
		  
	if ($errcnt > 0) {
		for($i=0;$i<=$errcnt-1;$i=$i+1){
		$editcnt = split(',',$_POST['editcnt']);?>
			<script>editdata(<?php echo $editcnt[$i];?>);</script>
		<?php } 
		} ?>    
        





 <input type="hidden" name="tot_rec" value="<?php echo $tot_rec ?>" />
            <tr><td colspan="7" align="right">
		<input type="submit" name="save" value="Save" class="winbutton_go" />&nbsp;&nbsp;&nbsp;
		<input type="submit" name="Cancel" value="Cancel" class="winbutton_go"/></td></tr>

</table>




 <?php	if($inserr == 1) { ?><script>adddata();</script><?php } ?>


</td></tr></table>
</body>
</form>
</html>
<?php include("../includes/copyright.php"); ?>