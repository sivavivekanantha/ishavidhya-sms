<?php session_start(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Isha Foundation - A Non-profit Organization</title>
<?php include("../includes/header.php");
title('Student Management','Teacher',2,1,0);
?>
<script language="javascript" type="text/javascript">
function make_ajax1(obj2,obj,obj1)	
{
	
		if(obj=='') return false;
		if(obj1=='') obj1 = 0;
		url = "../includes/ajax.php?listcode=4&State="+obj+"&City="+obj1+"&OB="+obj2;
        var st='#Citydiv'+obj2;
    	$(st).load(url);
}
function make_ajax2(obj,obj1,obj2)
	{	 
	if(obj=='') return false;
	if(obj2=='') obj2=0;
	   url = "../includes/ajax.php?listcode=4&State="+obj1+'&City='+obj2+"&OB="+obj;
       	var cdiv = '#Citydiv_'+obj;
		$(cdiv).load(url);
     
	}
function adddata() 
{
	$('#teachername1').show();
	$('#dateofjoin').show();
	$('#mobileno1').show();
	$('#emailid1').show();
	$('#address1').show();
	$('#citydiv').show();
	$('#Citydiv10').show();
	$('#active1').show();
	$('#state_name1').show();
	$('#school_name1').show();
	$('#category_name1').show();
	$('#Username1').show();
	$('#displayorder1').show();
	$('#mode1').val('ADD');
}
function editdata(val,val1,val2)
{ 
	if($('#editcnt').val() >0 ) return false;
    $('#editcnt').val(val);

	$('#teachername_'+val).show();
	$('#dateofjoin'+val).show();
	$('#doj_'+val).show();
	$('#mobileno_'+val).show();
	$('#emailid_'+val).show();
	$('#address_'+val).show();
	$('#state_name_'+val).show();
    $('#Citydiv_'+val).show();
	$('#city_'+val).show();
	$('#school_name_'+val).show();
	$('#category_name_'+val).show();
	$('#Username_'+val).show();
	$('#active_'+val).show();
	$('#displayorder_'+val).show();	
	var $mode ='EDIT';
	$('#mode').val($mode);
	getCity_Edit(val,val1,val2);

}
function deldata(val)
{ 
	var msg="Do you want to delete?";
	if(!confirm(msg)){
		return false;
	}	 
	else {
		$('#teacher_code').val(val);
		$('#mode').val('DELETE');
		$('#myform').submit();
	 }
}

function resetuser(obj)
{
var msg="Are you sure you want to  Reset password?";
	if(!confirm(msg)){
		return false;
	}	 
	else
	{
		url = "../includes/ajax.php?listcode=400&teachercode="+obj;	
		$(user).load(url);
	}	

}
</script>
</head>
<div id="user"></div>
<?php              // variable declaration
	$errmsg="";
	$errflag=0;
	$dummy=0;
	$mode		=	"";
	$mode1		=	"";
	$editcnt 	=	"";
    // GET MODE VALUE FOR WHICH FUNCTION PERFORMED (ADD / EDIT/ DELETE)
	$mode		=	$_POST['mode'];
	$mode1		=	$_POST['mode1'];
	$editcnt 	=	split(',',$_POST['editcnt']);	
	if($_POST['Cancel']=="Cancel") 
	{ 
	$mode=""; 
	$mode1="";	
	}	
    //RECORD INSERT PART HERE    
	if($mode1 == "ADD")       
	{	
		$action=1;
		$teacher_code1=Trim($_POST['teacher_code']);
		$teachername1=Trim($_POST['teachername1']);
		$doj1=Trim($_POST['doj']);
		$mobileno1=Trim($_POST['mobileno1']);
		$emailid1=Trim($_POST['emailid1']);
		$address1=Trim($_POST['address1']);
		$State1=Trim($_POST['state_name1']);
        
  //$city1=	trim($_POST['City_Code']);
        $City_Code	=	trim($_POST['City_Code_10']);
       
		$category1=Trim($_POST['category1']);
		$school1=Trim($_POST['school1']);
		$Username1=Trim($_POST['Username1']);
		$active=Trim($_POST['active1']);
		$displayorder1=Trim($_POST['displayorder1']);
	
		$dummy = Strcheck($teachername1,$errmsg,$errflag,"Teacher Name");
        $dummy = strcheck($doj1,$errmsg,$errflag,"DOJ");
		$dummy = Strcheck($mobileno1,$errmsg,$errflag,"Mobile No.");
		$dummy = Min_lengthcheck($mobileno1,$errmsg,$errflag,"Mobile Number No. must be 10 digits ",10);	
		$dummy = Emailcheck($emailid1,$errmsg,$errflag,"Email id");
		$dummy = Strcheck($address1,$errmsg,$errflag,"Address");
		$dummy = Zerocheck($State1,$errmsg,$errflag,"State");
		$dummy = Zerocheck($City_Code,$errmsg,$errflag,"City");
        
    	$dummy = Zerocheck($category1,$errmsg,$errflag,"Category");
		$dummy = Zerocheck($school1,$errmsg,$errflag,"School");
		$dummy = Strcheck($Username1,$errmsg,$errflag,"Username");
		$dummy=Strcheck($displayorder1,$errmsg,$errflag,"Display Order");
		
		if($errflag==0)
		{
              mssql_free_result($result); 
			$query = mssql_init('SP_TeacherSave',$mssql);
			mssql_bind($query,'@Teacher_Code',$teacher_code1,SQLINT4,false,false,5);
			mssql_bind($query,'@Teacher_Name',$teachername1,SQLVARCHAR,false,false,50);
			mssql_bind($query,'@DOJ',date('Y-m-d',strtotime($doj1)),SQLVARCHAR,false,false,30);
			mssql_bind($query,'@Mobile_no',$mobileno1,SQLVARCHAR,false,false,10);
			mssql_bind($query,'@Email_id',$emailid1,SQLVARCHAR,false,false,50);
			mssql_bind($query,'@Address',$address1,SQLVARCHAR,false,false,50);
			mssql_bind($query,'@State_Code',$State1,SQLINT4,false,false,50);
			mssql_bind($query,'@City_Code',$City_Code,SQLINT4,false,false,50);
			mssql_bind($query,'@Category_Code',$category1,SQLINT4,false,false,50);
			mssql_bind($query,'@School_Id',$school1,SQLINT4,false,false,50);
			mssql_bind($query,'@Username',$Username1,SQLVARCHAR,false,false,50);
			mssql_bind($query,'@Display_Order',$displayorder1,SQLINT4,false,false,5);
			mssql_bind($query,'@Active',$active,SQLVARCHAR,false,false,50);
			mssql_bind($query,'@Action',$action,SQLINT4,false,false,1);
			$result = @mssql_execute($query);
			mssql_free_statement($query);
			if($result==1)
			{
				echo "<p class='mesg'>Teacher has been Added</p>";
				$teacher_code1=" ";
				$teachername1=" ";
				$doj1=" ";
				$mobileno1=" ";
				$emailid1=" ";
				$address1=" ";
				$State1=" ";
				$City_Code=" ";
				$category1=" ";
				$school1=" ";
				$Username1=" ";
				$active1=" ";
				$displayorder1=" ";
			}
			else 
			{
				$errmsg1=mssql_get_last_message();
				$errflag=2;
				$inserr=1;
			}
		} else $inserr=1;
	}
	if($mode == "EDIT")
	{ 
		$action=2;
		
		for($i=0;$i<count($editcnt);$i++) 
		{  
			$j=$i+1;
			$teacher_code=Trim($_POST['teacher_code_'.$editcnt[$i]]);
			
			$teachername=Trim($_POST['teachername_'.$editcnt[$i]]);
			$doj=Trim($_POST['doj_'.$editcnt[$i]]);
			$mobileno=Trim($_POST['mobileno_'.$editcnt[$i]]);
			$emailid=Trim($_POST['emailid_'.$editcnt[$i]]);
			$address=Trim($_POST['address_'.$editcnt[$i]]);
			$State=Trim($_POST['state_name_'.$editcnt[$i]]);                    
            $city	=	trim($_POST['City_Code_'.$editcnt[$i]]);        
            if($city==0 or strlen($city)==0) 
                $city = $_POST['City_Code'];            
          
            if($_POST['City_Code']>0 and $_POST['City_Code'] <> trim($_POST['City_Code_'.$editcnt[$i]]) )
            $city = $_POST['City_Code'];                      
			$category=Trim($_POST['category_name_'.$editcnt[$i]]);
			$school=Trim($_POST['school_name_'.$editcnt[$i]]);
			$Username =Trim($_POST['Username_'.$editcnt[$i]]);
			$active=Trim($_POST['active_'.$editcnt[$i]]);
			$displayorder=Trim($_POST['displayorder_'.$editcnt[$i]]);
			
			//Validate the Inputs	
			$dummy = Strcheck($teachername,$errmsg,$errflag,"Teacher Name");
            $dummy = strcheck($doj,$errmsg,$errflag,"DOJ");
			$dummy = Strcheck($mobileno,$errmsg,$errflag,"Mobile No.");
			$dummy = Min_lengthcheck($mobileno,$errmsg,$errflag,"Mobile No. must be 10 digits",10);			$dummy = Emailcheck($emailid,$errmsg,$errflag,"Email id");
			$dummy = Strcheck($address,$errmsg,$errflag,"Address");
			$dummy = Zerocheck($State,$errmsg,$errflag,"State");
			$dummy = Zerocheck($city,$errmsg,$errflag,"City");
			$dummy = Zerocheck($category,$errmsg,$errflag,"Category");
			$dummy = Zerocheck($school,$errmsg,$errflag,"School");
			$dummy = Strcheck($Username,$errmsg,$errflag,"Username");
			$dummy=Strcheck($displayorder,$errmsg,$errflag,"Display Order");
            
				if($errflag==0)
				{
				      mssql_free_result($result); 
					$query = mssql_init('SP_TeacherSave',$mssql);
					mssql_bind($query,'@Teacher_Code',$teacher_code,SQLINT4,false,false,5);
					mssql_bind($query,'@Teacher_Name',$teachername,SQLVARCHAR,false,false,50);
					mssql_bind($query,'@DOJ',date('Y-m-d',strtotime($doj)),SQLVARCHAR,false,false,20);
					mssql_bind($query,'@Mobile_no',$mobileno,SQLVARCHAR,false,false,10);
					mssql_bind($query,'@Email_id',$emailid,SQLVARCHAR,false,false,50);
					mssql_bind($query,'@Address',$address,SQLVARCHAR,false,false,50);
					mssql_bind($query,'@State_Code',$State,SQLINT4,false,false,50);
					mssql_bind($query,'@City_Code',$city,SQLINT4,false,false,50);
					mssql_bind($query,'@Category_Code',$category,SQLINT4,false,false,50);
					mssql_bind($query,'@School_Id',$school,SQLINT4,false,false,50);
					mssql_bind($query,'@Username',$Username,SQLVARCHAR,false,false,50);
					mssql_bind($query,'@Display_Order',$displayorder,SQLINT4,false,false,5);
					mssql_bind($query,'@Active',$active,SQLVARCHAR,false,false,50);
					mssql_bind($query,'@Action',$action,SQLINT4,false,false,1);
					$result = @mssql_execute($query);
					mssql_free_statement($query);
					if($result==1)
						echo "<p class='mesg'>Teacher has been Updated</p>";
					else {
						$errmsg1=mssql_get_last_message();
						$errflag=2;
						If ($errcnt == 0) { $errcnt = 1;} else { $errcnt = $errcnt + 1; }
					if ($errval == "") { $errval = $editcnt[$i];	 } else { $errval = $errval.",".$editcnt[$i]; } 
					}
				}
				else {
					if ($errcnt == 0) { $errcnt = 1;} else { $errcnt = $errcnt + 1; }
					if ($errval == "") { $errval = $editcnt[$i];	 } else { $errval = $errval.",".$editcnt[$i]; } 
				}
	    }	
	}
	if($mode=="DELETE")
	{	
			$action=3;
			$teacher_code= 	$_POST['teacher_code'];
		       mssql_free_result($result); 
			$query = mssql_init('SP_TeacherSave',$mssql);
			mssql_bind($query,'@Teacher_Code',$teacher_code,SQLINT4,false,false,5);
			mssql_bind($query,'@Teacher_Name',$teachername,SQLVARCHAR,false,false,50);
			mssql_bind($query,'@DOJ',date('y-m-d',strtotime($doj)),SQLVARCHAR,false,false,20);
			mssql_bind($query,'@Mobile_no',$mobileno,SQLVARCHAR,false,false,10);
			mssql_bind($query,'@Email_id',$emailid,SQLVARCHAR,false,false,50);
			mssql_bind($query,'@Address',$address,SQLVARCHAR,false,false,50);
			mssql_bind($query,'@State_Code',$State,SQLINT4,false,false,50);
			mssql_bind($query,'@City_Code',$city,SQLINT4,false,false,50);
			mssql_bind($query,'@Category_Code',$category,SQLINT4,false,false,50);
			mssql_bind($query,'@School_Id',$school,SQLINT4,false,false,50);
			mssql_bind($query,'@Username',$Username,SQLINT4,false,false,50);
			mssql_bind($query,'@Display_Order',$displayorder,SQLINT4,false,false,5);
			mssql_bind($query,'@Active',$active,SQLVARCHAR,false,false,50);
			mssql_bind($query,'@Action',$action,SQLINT4,false,false,1);
			
				$result = @mssql_execute($query);
		mssql_free_statement($query);
			if($result==1)
			{
				echo "<p class='mesg'>Teacher  has been Deleted</p>";
			}
			else 
			{
			$errmsg1=mssql_get_last_message();
			$errflag=2;
			}
	}
	
	if($errflag==1) 
		echo $errlbl.$errmsg."</p>";
	if($errflag==2) 
		echo "<p class='error'>".$errmsg1;	
		//<? php titleheader(Teacher,0);
?>
<body>
<form name="myform" id="myform" method="post" action="sms_teacher.php">
  <input type="hidden" name="editcnt" id="editcnt"/>
  <input type="hidden" name ="editval" id ="editval" />
  <input type="hidden" name="teacher_code" id="teacher_code"/>
  <input type="hidden" name="mode" id="mode"/>
  <input type="hidden" name="mode1" id="mode1"/>
  <?php

   if($_SESSION["UserID"]==1000031 or $_SESSION["UserID"]==1  ){?>
	  <table width="95%" id="reset" align="right" style="padding-right:35px;">
      <tr><td align="right">Please choose a username, you want to reset password &nbsp;
  		<?php    mssql_free_result($userresult); 
            $query = mssql_init('sp_getusername',$mssql);
            $userresult = @mssql_execute($query);
			mssql_free_statement($query);
            ?>
  <select name="user" id="user" onchange="resetuser(this.value);" >
    <?php
	echo $Select;
	 while($user=mssql_fetch_array($userresult))  { ?>
  	<option value="<?php echo $user['Teacher_Code'] ?>">
  		<?php echo $user['USERNAME']?></option>  
		<?php } ?>
  </select></td></tr></table>
  <?php } ?>
  <table width="100%" border="0" align="center" cellpadding="3" cellspacing="3">
  <tr><td valign="top">
      <table width="90%" border="0" align="center" cellpadding="3" cellspacing="1">
		<?php titleheader(Teacher,0);?>
       
          <thead> <colgroup><col width="3%"/><col width="3%"/><col width="10%"/><col width="10%"/><col width="3%"/><col width="4%"/>
          <col width="4%"/><col width="3%"/><col width="3%"/><col width="2%"/><col width="2%"/><col width="2%"/><col width="2%"/>
          </colgroup>
          
          <th>&nbsp;</th>
            <th>&nbsp;</th>
            <th>Teacher Name</th>
            <th>DOJ</th>
            <th>Mobileno</th>
            <th>Email Id</th>
            <th>Address</th>
            <th>State</th>
            <th>City</th>
            <th>Category</th>
            <th>School</th>
            <th>User Name</th>
            <th>Active</th>
            <th>Display Order</th>
            </thead>
			
          <?php   // New Record Insert
		$colorflag+=1; ?>
          <tr class=<?php if($colorflag%2==0) { echo "row2"; } else { echo "row1"; } ?> valign="center">
            <td align="center" ><img src="../images/new.gif" title="Add New" onclick="adddata();"/> </td>
            <td></td>
            <td><div id ="teachername1" style="display:none;">
                <input type ="text" id ="teachername1" name="teachername1" size="20" style="width: 80px;" maxlength="50"
        onkeydown="return alphaonly('teachername1')"  value="<?php echo $teachername1; ?>"  />
              <?php echo $mand; ?> </div></td>
            <td><div id ="dateofjoin" style="display:none; "> <a href="javascript:NewCal('doj','ddmmyyyy')">
                <input type="text" name="doj" id="doj"
         readonly  size="10" style="75px" maxlength="10" value="<?php echo $doj1; ?>"   >
                </a><?php echo $mand; ?> </div></td>
            <td><div id ="mobileno1" style="display:none;">
                <input type ="text" id ="mobileno1" name="mobileno1" size="8" maxlength="15"   
         onkeydown="return numberonly('mobileno1')" value="<?php echo $mobileno1?>"  />
                <?php echo $mand; ?></div></td>
            <td><div id ="emailid1" style="display:none;">
                <input type ="text" id ="emailid1" name="emailid1" size="18" maxlength="50" 
        value="<?php echo $emailid1?>"  />
                <?php echo $mand; ?> </div></td>
            <td><div id ="address1" style="display:none;">
                <textarea id ="address1" name="address1" size="18" rows="1" cols="15"  maxlength="500" 
        ><?php echo $address1 ?></textarea>
               <?php echo $mand; ?> </div></td>
                
        
         <?php 	//SHOW State DROPDOWN
           mssql_free_result($result); 
		$query = mssql_init('sp_FetchState',$mssql);
		$result = mssql_execute($query);
		mssql_free_statement($query);	?> 
        <td align="center">
        <div id ="state_name1" style="display:none;">           
        	   <select name="state_name1" id="state_name1"  onchange="make_ajax1(10,this.value);" >
         <?php	
		 echo $Select;
		 while($field = mssql_fetch_array($result)) {  ?>
         <option value="<?php echo $field['State_Code']?>" <?php if($State1==$field['State_Code']) echo "selected";?>><?php echo $field['State']?></option>
         <?php }?>
       </select>
       </div> </td>        
	 <td><div id="Citydiv10" style="display:none;"><select name="City_Code" id="City_Code"> <?php echo $Select; ?></select></div></td>
	
	<?php   
	if($State1>0) { ?> <script>make_ajax1(10,<?php echo $State1; ?>,<?php echo $City_Code; ?>)</script><?php } ?>
    <?php 	//SHOW CITY DROPDOWN
                        mssql_free_result($result); 
			 	        $query = mssql_init('sp_GetTeacher_Category',$mssql);
						$result = mssql_execute($query);
						mssql_free_statement($query);	?>
            <td align="center"><div id ="category_name1" style="display:none;">
                <select id="category1" name="category1">
                  <?php 
				   echo $Select;
				  while($field = mssql_fetch_array($result)) 
   {  ?>
                  <option value="<?php echo $field['Category_Code']?>"  <?php if($category1==$field['Category_Code']) echo "selected"; ?>> <?php echo $field['Category']?></option>
                  <?php } ?>
                </select> <?php echo $mand; ?>
              </div></td>
            <?php 	//SHOW CITY DROPDOWN
                        mssql_free_result($result); 
						$query = mssql_init('sp_FetchSchool',$mssql);
                        mssql_bind($query, '@School_id', $_SESSION['SchoolId'], SQLINT4, false, false, 5);
                        
						$result = mssql_execute($query);
						mssql_free_statement($query);	?>
            <td align="center"><div id ="school_name1" style="display:none;">
                <select id="school1" name="school1">
                  <?php 
				   echo $Select;
				  while($field = mssql_fetch_array($result)) 
				   {  ?>
                  <option value="<?php echo $field['School_Id']?>" <?php if($school1==$field['School_Id'])echo "selected"; ?>> <?php echo $field['School_Name']?></option>
                  <?php } ?>
                </select>
             <?php echo $mand; ?></div> </td>
            <td><div id ="Username1" style="display:none;">
                <input type ="text" id ="Username1" name="Username1" size="15" maxlength="30"   
         onkeydown="return alphanumeric('Username1')" value="<?php echo $Username1?>"  />
                <?php echo $mand; ?> </div></td>
            <td align="center"><div id ="active1" style="display:none;">
                <select name="active1" id="active1">
                  <option value="Y">Yes</option>
                  <option value="N">No</option>
                </select>
              </div></td>
            <td><div id="displayorder1" style="display:none">
                <input type="text" id="displayorder1" name="displayorder1" maxlength="15" size="5" 
         onkeydown="return numberonly('displayorder1')"
   		 value="<?php echo $displayorder1?>" />
               <?php echo $mand; ?> </div></td>
          </tr>
          

 <?php 	// UPDATE & SHOW RECORDS
          mssql_free_result($result); 
		$query = mssql_init('SP_GetSmsTeacher',$mssql);
		$result = mssql_execute($query);
		mssql_free_statement($query);
		$rs_cnt = mssql_num_rows($result);
		$colorflag = 0;
		$i = 0;
		while($field = mssql_fetch_array($result))
		{	$i  +=1;	$colorflag+=1;
			$tot_rec = $i;	
			$teacher_code = $field['Teacher_Code'];
		      $city = $field['City_Code'];
			
			//echo "==".$teacher_code;
?>
          <tr class=<?php if($colorflag%2==0) { echo "row1"; } else { echo "row2"; } ?> valign="center">
            <input type="hidden" name="teacher_code_<?php echo $i ?>" id ="teacher_code_<?php echo $i ?>" 
        value="<?php echo $field['Teacher_Code']?>"	 />
            <td align="center"><img src="../images/delete_d.gif" title="Remove" 
          onclick="deldata('<?php echo $field['Teacher_Code'];?>');" />&nbsp; </td>
            <td align="center"><img src="../images/edit.gif" title="Edit" onclick="editdata('<?php echo $i;?>',<?php echo $field['State_Code']; ?>, <?php echo $field['City_Code'];?>);"/>&nbsp;</td>
            <td align="center"><?php echo $field['Teacher_Name']?>
              <div id ="teachername_<?php echo $i;?>" style="display:none;">
                <input type="text" id="teachername_<?php echo $i ?>" name="teachername_<?php echo $i ?>"  
             size="25" maxlength="50" value="<?php echo $field['Teacher_Name']?>" 
             onkeydown="return alphaonly('teachername_<?php echo $i ?>')">
              </div></td>
            <td align="center"><?php echo date('d-m-Y',strtotime($field['DOJ'])) ?>
              <div id ="dateofjoin<?php echo $i;?>" style="display:none;"> 
			  <a href="javascript:NewCal('doj_<?php echo $i ?>','ddmmyyyy')">
                <input type="text" id="doj_<?php echo $i ?>" name="doj_<?php echo $i ?>"  
            maxlength="50" value="<?php echo date('d-m-Y',strtotime($field['DOJ'])) ?>" readonly  size="10">
              </a></div></td>
            <td align="center"><?php echo $field['Mobile_No']?>
              <div id ="mobileno_<?php echo $i;?>" style="display:none;">
                <input type="text" id="mobileno_<?php echo $i ?>" name="mobileno_<?php echo $i ?>"  
             size="25" maxlength="15" value="<?php echo $field['Mobile_No']?>" 
             onkeydown="return numberonly('mobileno_<?php echo $i ?>')">
              </div></td>
            <td align="center"><?php echo $field['Email_Id']?>
              <div id ="emailid_<?php echo $i;?>" style="display:none;">
                <input type="text" id="emailid_<?php echo $i ?>" name="emailid_<?php echo $i ?>"  
             size="25" maxlength="50" value="<?php echo $field['Email_Id']?>" 
             >
              </div></td>
            <td align="center"><?php echo $field['Address']?>
              <div id ="address_<?php echo $i;?>" style="display:none;">
                <textarea rows="1" cols="15"  id="address_<?php echo $i ?>" name="address_<?php echo $i ?>"  
             size="25" maxlength="500" 
             onkeydown="return alphaonly('address_<?php echo $i ?>')"><?php echo $field['Address']?></textarea>
              </div></td>
              
              
	<?php 	//SHOW STATE DROPDOWN
         mssql_free_result($result1); 
		$query = mssql_init('sp_FetchState',$mssql);
		$result1 = mssql_execute($query);
		mssql_free_statement($query);	?>
	<td align="center"><?php echo $field['State'] ?>    

	<div id="state_name_<?php echo $i ?>" style="display:none;">
	      <select name="state_name_<?php echo $i ?>" id="state_name_<?php echo $i ?>"  onchange="make_ajax2(<?php echo $i; ?>, this.value)" >
<?php	while($field1 = mssql_fetch_array($result1)) {  ?>
			<option value="<?php echo $field1['State_Code']?>" <?php if( ($field['State_Code']==$field1['State_Code']) or ($errflag=1 and $State==$field1['State_Code']) ) echo "Selected";?>><?php echo $field1['State']?></option>
	 <?php } ?>
	 </select></div></td>
     
    <td align="center"><?php echo $field['City']  ?>  
        <div id="Citydiv_<?php echo $i ?>" style="display:none;">
	<?php 	//SHOW STATE DROPDOWN
          mssql_free_result($result1); 
		$query = mssql_init('sp_Getcityview',$mssql);		
		mssql_bind($query,'@State_Code',$field['State_Code'],SQLINT4,false,false,5);
		$result1 = @mssql_execute($query);
		mssql_free_statement($query); ?>
    <select name="City_Code_<?php echo $i ?>" id="City_Code_<?php echo $i ?>" >
      <?php	while($field1 = mssql_fetch_array($result1)) {  ?>
      <option value="<?php echo $field1['City_Code']?>" 
      <?php if($city==$field1['City_Code']) echo "Selected";?>><?php echo $field1['City']?></option>
      <?php } ?>
    </select> </div></td>             
    
    <?php if($State>0 and $errflag==1) { ?> <script>make_ajax2(<?php echo $i ?>,<?php echo $State ?>, <?php echo $city ?>)
    </script><?php } ?>    
    
    
    <?php 	//SHOW category DROPDOWN
      mssql_free_result($result1); 
		$query = mssql_init('sp_GetTeacher_Category',$mssql);
		$result1 = mssql_execute($query);
		mssql_free_statement($query);?>
            <td align="center"><?php echo $field['Category'] ?>
              <div id ="category_name_<?php echo $i;?>" style="display:none;">
                <select name="category_name_<?php echo $i ?>" id="category_name_<?php echo $i ?>" 
                onchange="getcategory_Edit('<?php echo $i;?>',this.value)" >
                  <?php	while($field1 = mssql_fetch_array($result1)) {  ?>
                  <option value="<?php echo $field1['Category_Code']?>" <?php if($field['Category_Code']==$field1['Category_Code']) echo "Selected";?>><?php echo $field1['Category']?></option>
                  <?php } ?>
                </select>
              </div></td>
            <?php 	//SHOW State DROPDOWN
              mssql_free_result($result1); 
		$query = mssql_init('sp_FetchSchool',$mssql);
        mssql_bind($query, '@School_id', $_SESSION['SchoolId'], SQLINT4, false, false, 5);
		$result1 = mssql_execute($query);
		mssql_free_statement($query);?>
            <td align="center"><?php echo $field['School_Name'] ?>
              <div id ="school_name_<?php echo $i;?>" style="display:none;">
                <select name="school_name_<?php echo $i ?>" id="school_name_<?php echo $i ?>" 
                onchange="getSchool_Edit('<?php echo $i;?>',this.value)" >
                  <?php	while($field1 = mssql_fetch_array($result1)) {  ?>
                  <option value="<?php echo $field1['School_Id']?>" <?php if($field['School_Id']==$field1['School_Id']) echo "Selected";?>><?php echo $field1['School_Name']?></option>
                  <?php } ?>
                </select>
              </div></td>
            <!-- <td align="center">
	     		<div id ="city_name_" style="display:none;">
                <select name="city_name_" id="city_name_">
					<option >     
                    </option>
	 		    </select></div>        </td>	
         -->
            <td><?php echo $field['UserName']?>
              <div id ="Username_<?php echo $i;?>" style="display:none;">
                <input type="text" id="Username_<?php echo $i ?>" name="Username_<?php echo $i ?>"  
             size="15" maxlength="30" value="<?php echo $field['UserName']?>" 
             onkeydown="return alphanumeric('Username_<?php echo $i ?>')">
              </div></td>
            <td align="center"><?php if(strtoupper($field['Active'])=="Y") echo "Yes"; else echo  "No";?>
              <div id ="active_<?php echo $i;?>" style="display:none;">
                <select id="active_<?php echo $i ?>" name="active_<?php echo $i ?>">
                  <option value="Y" <?php if(strtoupper($field['Active'])==="Y")  echo "selected" ?>>Yes</option>
                  <option value="N" <?php if(strtoupper($field['Active'])==="N")  echo "selected" ?>>No</option>
                </select>
              </div></td>
            <td align="center"><?php echo $field['Display_Order'] ?>
              <div id ="displayorder_<?php echo $i;?>" style="display:none;">
                <input type="text" name="displayorder_<?php echo $i ?>" id="displayorder_<?php echo $i ?>"  
                    size="5" maxlength="5" value="<?php echo $field['Display_Order']?>" 
                    onkeydown="return numberonly('displayorder_<?php echo $i ?>')">
              </div></td>
          </tr>
          <?php } 
	if ($errcnt > 0) {
		for($i=0;$i<=$errcnt-1;$i=$i+1){
		$editcnt = split(',',$_POST['editcnt']);?>
          <script>editdata(<?php echo $editcnt[$i];?>);</script>
          <?php } 
		} ?>
          <tr>
            <td colspan="14" align="right"><input type="submit" name="save" value="Save" class="winbutton_go" />
              <input type="submit" name="Cancel" value="Cancel" class="winbutton_go"/>            </td>
          </tr>
      </table></td>
    </tr>
  </table>
</form>
<?php	if($inserr == 1) { ?>
<script>adddata();</script>
<?php } ?>
</body>
</html>
<?php include("../includes/copyright.php"); ?>
