<?php session_start();?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Isha Foundation - A Non-profit Organization</title>
<?php include("../includes/header.php"); 
 title('Student Management','Vehicle',2,1,0);?>

<script>

function adddata()
{
	$('#vehicle1').show();
	$('#Active1').show();
	$('#Displayorder1').show();
	$('#mode1').val('ADD');
}


function editdata(val)
{ 
	if($('#editcnt').val() >0 ) return false;
    $('#editcnt').val(val);
    
	$('#vehicle_'+val).show();
	$('#active_'+val).show();
	$('#displayorder_'+val).show();	
	$('#mode').val('EDIT');

}


function deldata(val)
{ 
	var msg="Do you want to delete?";
	if(!confirm(msg)){
		return false;
	}	 
	else {
		$('#Term_code').val(val);
		$('#mode').val('DELETE');
		$('#myform').submit();
	 }
}



</script>


</head>

<?php
	$errmsg="";
	$errflag=0;
	$dummy=0;
	$mode		=	$_POST['mode'];
	$mode1		=	$_POST['mode1'];
	$editcnt 	=	split(',',$_POST['editcnt']);	
	if($_POST['Cancel']=="Cancel") { $mode=""; $mode1="";	}	
	
	if($mode1 == "ADD")
	{	
		$action1=1;
		$Vehicle_Code1=Trim($_POST['Vehicle_Code']);
		$vehicle1=Trim($_POST['vehicle1']);
		//echo "==".$Term;
		$Active1=$_POST['Active1'];
		$Displayorder1=Trim($_POST['Displayorder1']);
				
		$dummy = Strcheck($vehicle1,$errmsg,$errflag,"Vehicle");
		$dummy=Strcheck($Displayorder1,$errmsg,$errflag,"Display Order");
		
		if($errflag==0)
		{
              mssql_free_result($result); 
			$query = mssql_init('SP_VehicleSave',$mssql);
			mssql_bind($query,'@Vehicle_Code',$Vehicle_Code1,SQLINT4,false,false,5);
			mssql_bind($query,'@Vehicle',$vehicle1,SQLVARCHAR,false,false,50);
			mssql_bind($query,'@Displayorder',$Displayorder1,SQLINT4,false,false,5);
			mssql_bind($query,'@Active',$Active1,SQLVARCHAR,false,false,1);
			mssql_bind($query,'@Action',$action1,SQLINT4,false,false,1);
			
			$result = @mssql_execute($query);
			
			mssql_free_statement($query);
		if($result==1)
			echo "<p class='mesg'>Vehicle has been Added</p>";
		else {
			$errmsg1=mssql_get_last_message();
			$errflag=2;
			$inserr=1;
			}
		} else $inserr=1;	
		
}
//echo "==".$mode."<br>";
If($mode == "EDIT")
	{ $action=2;
	for($i=0;$i<count($editcnt);$i++) 
	{  
		
		$vehicle_code= 	$_POST['vehicle_code_'.$editcnt[$i]];
		//echo "==".$vehicle_code."<br>";
		$vehicle 	=	Trim($_POST['vehicle_'.$editcnt[$i]]);
	//echo "==".$vehicle."<br>";
		$active		=	$_POST['active_'.$editcnt[$i]];
		//echo "==".$active."<br>";
		$disporder	=	Trim($_POST['displayorder_'.$editcnt[$i]]);
		//echo "==".$disporder."<br>";
		$j=$i+1;
		

		//$dummy = Strcheck($term,$errmsg,$errflag,"Term-".$j);
		//$dummy = Strcheck($short_name,$errmsg,$errflag,"Short Name-".$j);
		//$dummy = Strcheck($disporder,$errmsg,$errflag,"Display Order-".$j);
		//echo "==".$errflag."<br>";
		if($errflag==0)
		{   
		     mssql_free_result($result); 
			$query = mssql_init('SP_VehicleSave',$mssql);
			mssql_bind($query,'@Vehicle_Code ',$vehicle_code,SQLINT4,false,false,5);
			mssql_bind($query,'@Vehicle',$vehicle ,SQLVARCHAR,false,false,50);
			mssql_bind($query,'@Displayorder',$disporder,SQLINT4,false,false,5);
			mssql_bind($query,'@Active',$active,SQLVARCHAR,false,false,1);
			mssql_bind($query,'@Action',$action,SQLINT4,false,false,1);
			$result = @mssql_execute($query);
			mssql_free_statement($query);
			if($result==1)
				echo "<p class='mesg'>Vehicle has been Updated</p>";
			else {
				$errmsg1=mssql_get_last_message();
				$errflag=2;
				If ($errcnt == 0) { $errcnt = 1;} else { $errcnt = $errcnt + 1; }
			if ($errval == "") { $errval = $editcnt[$i];	 } else { $errval = $errval.",".$editcnt[$i]; } 
			}
		}
		else {
			If ($errcnt == 0) { $errcnt = 1;} else { $errcnt = $errcnt + 1; }
			if ($errval == "") { $errval = $editcnt[$i];	 } else { $errval = $errval.",".$editcnt[$i]; } 
		}
	}	
	}
	
		



if($errflag==1) 
		echo $errlbl.$errmsg."</p>";
	if($errflag==2) 
		echo "<p class='error'>".$errmsg1."</p>";	
?>

<body>



<form name="myform" id="myform" method="post" action="sms_vehicle.php">

<input type="hidden" name="editcnt" id="editcnt"/>
<input type="hidden" name ="editval" id ="editval" />
<input type="hidden" name="vehicle_code" id="vehicle_code"/>
<input type="hidden" name="mode" id="mode"/>	
<input type="hidden" name="mode1" id="mode1"/>

<table width="100%" height="450" border="0" align="center" cellpadding="3" cellspacing="3">
<tr><td valign="top">
<table width="70%" border="0" align="center" cellpadding="3" cellspacing="1">
<colgroup>
<col width=1%>
<col width=1%>
<col width=5%>
<col width=3%>
<col width=3%>
</colgroup>
<tr align="center">
<thead>
<th>&nbsp;</th>
<th>Vehicle</th>
<th>Active</th>
<th>Display Order</th>
</thead></tr>
<?php   // New Record Insert
		$colorflag+=1; ?>


<tr class=<?php if($colorflag%2==0) { echo "row2"; } else { echo "row1"; } ?> valign="center">
	<td align="center" >
		<img src="../images/new.gif" title="Add New" onclick="adddata();"/>
 	</td>
       <td align="center">
    	<div id ="vehicle1" style="display:none;">
    	<input type ="text" id ="vehicle1" name="vehicle1" size="25" maxlength="50"
         onkeydown="return alphaonly('vehicle1')" value="<?php echo $vehicle1?>"  />
   		<span class="mand"> *</span>
   		</div>
    </td>
    <td align="center">
    	<div id="Active1" style="display:none">
     	<select name="Active1" id="Active1">
  		   <option value="Y">Yes</option>
   		   <option value="N">No</option>
        </select>
        </div>
    </td>
    <td align="center">
   		 <div id="Displayorder1" style="display:none">
   		 <input type="text" id="Displayorder1" name="Displayorder1" onkeydown="return numberonly('Displayorder1')"maxlength="3" size="5" 
   		 value="<?php echo $Displayorder1?>" />
    	 <span class="mand">*</span>
   		 </div>
    </td>
 </tr>
 
   <?php 	// UPDATE & SHOW RECORDS
        mssql_free_result($result); 
		$query = mssql_init('SP_GetVehicle',$mssql);
		$result = mssql_execute($query);
		mssql_free_statement($query);
		$rs_cnt = mssql_num_rows($result);
		$colorflag = 0;
		$i = 0;
		while($field = mssql_fetch_array($result))
		{	$i  +=1;	$colorflag+=1;
			$tot_rec = $i;	?>
            
         
 <tr class=<?php if($colorflag%2==0) { echo "row1"; } else { echo "row2"; } ?> valign="center">
 
 <input type="hidden" name="vehicle_code_<?php echo $i ?>" id ="vehicle_code_<?php echo $i ?>" 
        value="<?php echo $field['Vehicle_Code']?>"	 />	
 
		        
		<td align="center"><img src="../images/edit.gif" title="Edit" 
           onclick="editdata('<?php echo $i;?>');" />&nbsp;
        </td>
        
        <td align="center"><?php echo $field['Vehicle']?>
       
	    	<div id ="vehicle_<?php echo $i;?>" style="display:none;">
        	 <input type="text" id="vehicle_<?php echo $i ?>" 
             name="vehicle_<?php echo $i ?>"  
             size="25"    maxlength="50" 
             value="<?php echo $field['Vehicle']?>" 
             onkeydown="return alphaonly('vehicle_<?php echo $i ?>')"></div>
        
        
        </td>	
                
        <td align="center">
				<?php if(strtoupper($field['Active'])=="Y") echo "Yes"; else echo  "No";?>
				<div id ="active_<?php echo $i;?>" style="display:none;">
			<select id="active_<?php echo $i ?>" name="active_<?php echo $i ?>">
            
				<option value="Y" <?php if(strtoupper($field['Active'])==="Y")  echo "selected" ?>>Yes</option>
				<option value="N" <?php if(strtoupper($field['Active'])==="N")  echo "selected" ?>>No</option>
			</select>
            	</div>
        </td>
        
        <td align="center">
				<?php echo $field['Display_Order'] ?>
				<div id ="displayorder_<?php echo $i;?>" style="display:none;">
                	<input type="text" name="displayorder_<?php echo $i ?>" id="displayorder_<?php echo $i ?>"  
                    size="5" maxlength="5" value="<?php echo $field['Display_Order']?>" 
                    onkeydown="return numberonly('displayorder<?php echo $i ?>')">
                </div>
         </td>
        	
 </tr>
 
 <?php
 }
 ?>
 
 
 
 <tr>
   		 <td colspan="4" align="right">
			<input type="submit" name="save" value="Save" class="winbutton_go" />&nbsp;&nbsp;&nbsp;
			<input type="submit" name="Cancel" value="Cancel" class="winbutton_go"/>
        </td>
   	</tr>
 
</table></td></tr></table>

</form>
<?php	if($inserr == 1) { ?>
<script>adddata();</script>
<?php } ?>
</body>
</html>
<?php include("../includes/copyright.php"); ?>
