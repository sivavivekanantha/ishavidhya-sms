<?php session_start();?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Isha Foundation - A Non-profit Organization</title>
<?php include("../includes/header.php");
 title('Student Management System','Generate Report',2,1,0);	?>
 </head>
<body>
<!--<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr>
    <td ><a href="StudentExport.php">Export Student Details</a></td>
    </tr>
</table>
-->
<form id="myform" name="myform" method="post" action="../transaction/gen_excel_rpt_template.php">

<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
<tr><td><?php   titleheader("Generate Report", 0);  ?></td></tr>
<tr><td height="5">&nbsp;</td></tr>
  <tr>
    <td align="center"><div style="width:980px;" id="scholarshipfr" align="center">
          <table width="100%" border="0" cellspacing="2" cellpadding="0" align="center">
    		<tr>
				<td width="10%" >Report Type </td>
      <td width="21%" ><select id="Rcode" name="Rcode" style="width:150px;">
					<option value="2"  >Evaluvation Report</option>
					<option value="3"  >Tally Report</option>
                   	<!--<option value="4"  >Individual Student Report</option>-->
			  </select></td>
			  <td width="7%" >Status</td>
			 
              <td width="62%" > <select name="Status" id="Status" >
   
    <option value="1" <?php if($Status == 1) echo "selected"; ?>>Existing</option>
    <option value="2" <?php if($Status == 2) echo "selected"; ?>>Passout</option>
    <option value="3" <?php if($Status == 3) echo "selected"; ?>>In-Process</option>
	<option value="4" <?php if($Status == 4) echo "selected"; ?>>Disjoined</option>
    <option value="5" <?php if($Status == 5) echo "selected"; ?>>Discontinue</option>
	<option value="6" <?php if($Status == 6) echo "selected"; ?>>Transfer Certificate</option>
    </select></td>
			</tr>
			 <tr><td colspan="4">
			<table width="100%" border="0" cellspacing="2" cellpadding="0" align="center">           
            <tr>
              <td width="10%" align="left">Location <?php
echo $mand;
?></td>
              <?php
//SHOW Location  DROPDOWN
mssql_free_result($result); 
$query = mssql_init('[sp_selectschoolname]', $mssql);
mssql_bind($query, '@School_id', $_SESSION['SchoolId'], SQLINT4, false, false, 5);
$result = mssql_execute($query);
mssql_free_statement($query);
?>
              <td width="22%" ><select name="Loc_Code" id="Location"  style="width:150px;">
	<?php	while ($field = mssql_fetch_array($result))
			{	?>
                  <option value="<?php echo $field['School_id']
?>" <?php
    if ($Loc_Code == $field['School_id'])
        echo "Selected";
?>>
                  <?php
    echo $field['SchoolName']
?>
                  </option>
                  <?php
}
?>
              </select>              </td>
              <td width="8%" >Class</td>
              <?php
//SHOW Class  DROPDOWN
  mssql_free_result($result); 
$query = mssql_init('[sp_GetClass]', $mssql);
$result = mssql_execute($query);
mssql_free_statement($query);
?>
              <td width="11%"><select name="Class_Code" id="Class">
                  <?php
echo $Select;
?>
                  <?php
while ($field = mssql_fetch_array($result))
{
?>
                  <option value="<?php
    echo $field['Class_Code']
?>" <?php
    if ($Class_Code == $field['Class_Code'])
        echo "Selected";
?>>
                  <?php
    echo $field['Class']
?>
                  </option>
                  <?php
}
?>
              </select></td>
              <td width="8%"  align="right">Section</td>
              <?php
//SHOW CITY DROPDOWN
mssql_free_result($result_section); 
$query_section = mssql_init('sp_GetSection', $mssql);
$result_section = mssql_execute($query_section);
mssql_free_statement($query_section);
?>
              <td width="7%"><select name="Sec_Code" id="Section" >
                  <?php
echo $Select;
?>
                  <?php
while ($field = mssql_fetch_array($result_section))
{
?>
                  <option value="<?php
    echo $field['Section_Code']
?>" <?php
    if ($Sec_Code == $field['Section_Code'])
        echo "Selected";
?>>
                  <?php
    echo $field['Section']
?>
                  </option>
                  <?php
}
?>
              </select></td>
              <!--<td width="10%" align="right">Admission No.</td>
              <td width="31%" ><input type="text" name="Adm_No" id="Admission_No"   onkeydown="return alphanumeric('Admission_No')"  maxlength="15"  
              value="<?php
echo $Adm_No
?>" /></td>-->

<td width="10%" align="right">Adm. No.</td>
           <td width="24%" align="left">
                      <input type="text" name="Admission_No" id="Admission_No" maxlength="50"   onkeydown="return alphanumeric('Admission_No')" /></td>
            </tr>
            <tr><td colspan="8" >
            <table align="right"><tr><td>
            <input type="submit" name="Go"  value="Generate Report" class="winbutton_go" />
            
            </td></tr></table>
            
             </td>
            	
            </tr>
		</table></td>
            </tr>    
        </table>
      </div></td>
  </tr>
</table>
<div id="Rptdiv"></div>	
</form>
</body>
</html>
<?php include("../includes/copyright.php"); ?>