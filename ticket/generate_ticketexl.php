<?php session_start(); 
include("../includes/header.php");
title('Ticketing System','Detailed Ticket Report',5,2,0); 	 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>
<?php 
$FromDate=date("01-m-Y");
$ToDate=date("d-m-Y");

if($_POST['Submit']=='Submit')
{
	$FromDate=trim($_POST['FromDate']);
	$ToDate=trim($_POST['ToDate']);
    	$Status=$_POST['ddlStatus'];
	$dummy = DateCheck($FromDate,$ToDate,$errmsg,$errflag,"To Date must be greater than or equal to From Date");
	if($errflag==1)	echo $errlbl.$errmsg; 
}	?>

<body>
<form name="myform" id="myform" method="post" action="generate_ticketexl.php">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  	<tr>
    	<td>
    		<table width="70%" border="0" cellspacing="2" cellpadding="0" align="center" style="border:dotted #c0ddf6;">
  <tr>
    <td>From Date</td>
            <td><a href="javascript:NewCal('FromDate','ddmmyyyy')">
            <input type="text" name="FromDate" id="FromDate" readonly="readonly"  size="20" maxlength="10" 
            value="<?php echo $FromDate; ?>" />
                    </a></td>
    <td>ToDate</td>
        <td><a href="javascript:NewCal('ToDate','ddmmyyyy')">
            <input type="text" name="ToDate" id="ToDate" readonly="readonly"  size="20" maxlength="10" 
            value="<?php echo $ToDate; ?>" />
                </a></td>
				<td> Status</td><td> <select name="ddlStatus" id="ddlStatus" onchange="status(this.value)" >
                          <option value="2" <?php if($Status==2) echo "Selected" ?> >Un Closed</option>
                          <option value="1" <?php if($Status==1) echo "Selected" ?> >Closed</option>
                         
                          
                        </select>
                          </td>
             <td>
      <input type="submit" name="Submit" id="Submit" value="Submit" class='winbutton_go' />
    </td>
  </tr>
	
</table>
<br/>  
</td>
</tr>
</table>
<?php  if($_POST['Submit']=='Submit')
	{
	mssql_free_result($result);                                                  
	$query=mssql_init('SP_Ticket_Excel',$mssql);
	if(strlen($FromDate)==10) $FromDate = date('Y-m-d', strtotime($FromDate));
	mssql_bind($query,'@Fdate',$FromDate,SQLVARCHAR,false,false,50);
	if(strlen($ToDate)==10)  
	$ToDate = date('Y-m-d', strtotime($ToDate)); 
	mssql_bind($query,'@Tdate',$ToDate,SQLVARCHAR,false,false,50);
	mssql_bind($query,'@Status',$Status,SQLINT4,false,false,5);
	$result1 = @mssql_execute($query);
	if(!$result1) echo mssql_get_last_message();
	$rs_cnt = mssql_num_rows($result1);  
	 if($rs_cnt > 0) { 	 ?>
     <table width="99%" border="0" cellspacing="0" cellpadding="0">
    <tr>
	<td  width="70%" align="right" style="font-size: large;"><a href="ticket_excel.php?frmdate=<?php echo $FromDate;?>&todate=<?php echo $ToDate;?>&status=<?php echo $Status;?>"><img src="../images/excel.jpg" title="Export to Excel"/></a></td>
    </tr> </table>      
	 <table width="99%" cellspacing="1" cellpadding="0" >
	 <thead>
	<tr>
		<th width="7%">S.No</th>
		<th width="7%">Ticket Type</th>
		<th width="10%">School</th>
		<th width="10%">Raised By</th>
		<th width="10%">Raised Date</th>
        <th width="10%">Raised To</th>
        <th width="10%">Assigned To</th>
		<th width="10%">Location</th>
		<th width="10%">Description</th>
		<th width="10%">Priority</th>
		<th width="10%">Ticket Status</th>
		<?php if($Status==1) echo "<th width='10%' >Closed Date</th>"; ?>
	</tr></thead>
		 <?php 
		 $Sno=0;
   while ($field = mssql_fetch_array($result1)) 
    {
	$Type=$field['Ticket_Type'];
	$school=$field['School_Name'];
	$raisedby=$field['Name'];
    $raisedto=$field['Department'];
	$raiseddate=date('d-M-Y',strtotime($field['Created_Date']));
    $assignedto=$field['Teacher_Name'];
	$location=$field['Place'];
   	$description=$field['Description'];
	$Priority=$field['Priority'];
    $Show_Status=$field['Status'];
	$closed_date=$field['Closed_Date'];
	if(strlen($closed_date)==10) $closed_date=date('d-M-Y',strtotime($field['closed_date']));
	$Sno=$Sno+1;
    $colorflag+=1;	 ?>
    <tr class=<?php if($colorflag%2==0) { echo "row1"; } else { echo "row2"; } ?> valign="center">
     	<td align="center"><?php echo $Sno ?></td>
	    <td align="center"><?php if($Type == 1) echo "Task"?>
        <?php if($Type == 2) echo "Issue"?></td>
		<td align="center"><?php echo $school; ?></td>
		<td align="center"><?php echo $raisedby; ?></td>
		<td align="center"><?php echo $raiseddate; ?></td>
		<td align="center"><?php echo $raisedto; ?></td>
		<td align="center"><?php echo $assignedto; ?></td>
		<td align="center"><?php echo $location; ?></td>
        <td align="center"><?php echo $description; ?></td>
	    <td align="center"><?php if($Priority == 1) echo "Low"?>
        <?php if($Priority == 2) echo "Medium"?>
        <?php if($Priority == 3) echo "High"?></td>            
		<td align="center"><?php if($Show_Status == 1) echo "Open"?>
        <?php if($Show_Status == 7) echo "Closed"?>
        <?php if($Show_Status == 4) echo "Ignored"?>
        <?php if($Show_Status == 2) echo "Inprogress"?>
        <?php if($Show_Status == 5) echo "Fixed"?></td>
	<?php if($Status==1) { ?> <td><?php echo $closed_date; ?></td> <?php }?>
		
	</tr>
	<?php } 	$Sno++;
	?>
	
</table>
	<?php }
	else
	echo "NO RECORDS";
	}
	?>
 </form>
</body>
</html>


