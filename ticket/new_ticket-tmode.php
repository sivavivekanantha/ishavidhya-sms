<?php session_start(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php if($_GET['frm']==1 or $_POST['frm']==1) {
		include("../Includes/header1.php"); 
		$frm = 1;
	  if($_GET['Ticket_Mode']>0 )  $Ticket_Mode = $_GET['Ticket_Mode'];
		}
	else {
		include("../Includes/header.php"); 
		title('Ticketing System','New Ticket',5,2,0);     }    ?>
<script language="javascript" type="text/javascript">
function make_ajax(obj,obj1)
{ 
	if(obj==0 || obj1==0) return false;
    url = "../includes/ajax.php?listcode=2&Tmode="+obj1+"&catcode="+obj;
	$('#divAssignto').load(url);
}
 function showtask(obj)
{    
	if(obj==1) {
		$("#Task_Grp").show();
		$("#Issue_grp").hide();
		}
	else {
		$("#Task_Grp").hide();
		$("#Issue_grp").show();
		}
}
function Reopen()
{ 
	if ($('#chk_reopen').attr('checked')) 
		 {
		$('#rerem').show();
		$('#Save').show();
		}
	else if($('#chk_closed').attr('checked')) 
		 {
		$('#Save').show();
		$('#rerem').hide();
		}
	else
		{
		$('#rerem').hide();
		$('#Save').hide();
		}
}

function Clear()
{    
	$('#rname').val("");
	$('#rmobile').val("");
	$('#remail').val("");
	$('#ddlTicket_type').val("");
	$('#ddlPriority').val("");
	$('#Category_Code').val("");
	$('#ddlAssignto').val("Select");
	$('#Raised_to').val("");
	$('#txtPlace').val("");
	$('#txtDescription').val("");
	$('#divmsg').hide();
	url = "../includes/ajax.php?listcode=2&School_Id="+'';
	$('#divAssignto').load(url);
}
</script>
<?php 
	$errmsg="";
	$errflag=0;
	$dummy=0;
	$Ticket_Id=0;

	if($_GET['Ticket_Id']>0) 
		$Ticket_Id=$_GET['Ticket_Id'];

	if($_POST['Save'] =='Save')    
	{        
        $rname          =    Trim($_POST['rname']);
        $rmobile        =    Trim($_POST['rmobile']);
        $remail         =    Trim($_POST['remail']);
        
        $Ticket_Mode    =    $_POST['ddlTicket_Mode'];
        $Ticket_Type    =    $_POST['ddlTicket_type'];            
        $Priority       =    $_POST['ddlPriority'];                
        $Category_Code  =    $_POST['Category_Code'];
        $Assign_To      =    $_POST['ddlAssignto'];        
        $Raised_To      =    $_POST['Raised_to'];
        $Place          =    Trim($_POST['txtPlace']);                
        $Description    =    Trim($_POST['txtDescription']);    
       
        $Ticket_Id      =    $_POST['Ticket_Id'];
        $Remarks        =    Trim($_POST['Remarks']);
        $PRemarks       =    $_POST['hdnRemarks'];
        
        $Status         =    $_POST['ddlStatus'];    // Current Status
        $Status1        =    $_POST['Status'];         // Click ReOpen 
        $Reopen_Descr   =    Trim($_POST['txtReopen_Descr']);    //ReOPen Ticket Description
        $Terms          =   $_POST['Terms']; 
		
		if($Ticket_Id==0)
		{
			 $dummy = Strcheck($rname,$errmsg,$errflag,"Name");
			 $dummy = Strcheck($rmobile,$errmsg,$errflag,"Mobile");
			 if(strlen($remail)>0)
				 $dummy = Emailcheck($remail,$errmsg,$errflag,"Email Id");
			
            $dummy = Zerocheck($Ticket_Mode,$errmsg,$errflag,"Ticket Category"); 
			$dummy = Zerocheck($Ticket_Type,$errmsg,$errflag,"Ticket Type");
			$dummy = Zerocheck($Priority,$errmsg,$errflag,"Priority");
			if($Ticket_Type==1) {
				$dummy = Zerocheck($Category_Code,$errmsg,$errflag,"Assign To");
				$dummy = Zerocheck($Assign_To,$errmsg,$errflag,"Person");
			} elseif($Ticket_Type==2) {
				$dummy = Zerocheck($Raised_To,$errmsg,$errflag,"Raised To");
			}
			$dummy = Strcheck($Description,$errmsg,$errflag,"Description");
		 }    
		else 
		{
			 if($Status == 3 or $Status ==4 ) $dummy = Strcheck($Remarks,$errmsg,$errflag,"Remarks");
			if($Status1 == 6 )
			{ 
				$dummy = Strcheck($Reopen_Descr,$errmsg,$errflag,"Remarks");
			}
			
			if($Status == 1 or $Status == 6 )
			{ 
			$dummy = Strcheck($Terms,$errmsg,$errflag,"Accept ticket");
			}
		}
		
		 if($errflag==0)
		{
			if(strlen($PRemarks) > 0 )
				$Remarks  = $Remarks."<br>".$PRemarks;
			
			if($Status1 > 0 ) {
				$Remarks  =     $Reopen_Descr."<br>".$Remarks;
				$Status = $Status1;
			}
			
			 if($Ticket_Id==0)         {
				$query=mssql_init('Sp_New_Ticket_Save',$mssql);
				mssql_bind($query,'@Rname',$rname,SQLVARCHAR,false,false,50);
				mssql_bind($query,'@Rmobile',$rmobile,SQLVARCHAR,false,false,25);
				mssql_bind($query,'@Remail',$remail,SQLVARCHAR,false,false,100);
                
                mssql_bind($query,'@Ticket_Mode',$Ticket_Mode,SQLINT4,false,false,5);
				mssql_bind($query,'@Ticket_Type',$Ticket_Type,SQLINT4,false,false,5);
				mssql_bind($query,'@Priority',$Priority,SQLINT4,false,false,5);
                //echo $Raised_To."==".$Category_Code."==".$Assign_To."<br>"; 
                If($Category_Code=="0" and $Raised_To<>"999" and $Assign_To=="0") $Category_Code=7;
				mssql_bind($query,'@Category_Code',$Category_Code,SQLINT4,false,false,5);
                If($Raised_To=="" or $Raised_To=="0") $Raised_To = $Assign_To;
                //echo $Raised_To."==".$Category_Code."==".$Assign_To."<br>"; exit();
				mssql_bind($query,'@Raised_to',$Raised_To,SQLINT4,false,false,5);
				mssql_bind($query,'@Place',$Place,SQLVARCHAR,false,false,50);
				mssql_bind($query,'@Description',$Description,SQLTEXT,false,false,strlen($Description));
				mssql_bind($query,'@Userid',$_SESSION["UserID"],SQLINT4,false,false,5);
			}
			elseif($Ticket_Id>0)
			{
				$query=mssql_init('Sp_New_Ticket_Update',$mssql);
				mssql_bind($query,'@Ticket_Id',$Ticket_Id,SQLINT4,false,false,5);
				mssql_bind($query,'@Status',$Status,SQLINT4,false,false,5);
				mssql_bind($query,'@Remarks',$Remarks,SQLVARCHAR,false,false,20);
			}
			$result = @mssql_execute($query);
			mssql_free_statement($query);
			if($result)
			{  
				if($Ticket_Id==0)
				{
					 echo "<p class='mesg'>Your Ticket has been Registered Successfully.</p>";
					   While($Tresult = mssql_fetch_array($result))
                       {
                            if(strlen($email)>0)    $email.= ",".$Tresult['Emailid'];    else $email = $Tresult['Emailid']; 
                       }
						//        MAIL SENT CODES
						$submail = "New Ticket Raised In Ishavidhya";
						if($Ticket_Type==1) $Ttype = "Task"; Else $Ttype = "Issue";
						$mbody = "Namaskaram,<br><br><br>\tThank you for contacting us.<br><br>\t<br>\t".$Ttype." Description :" .$Description. "<br>\t Raised by: " . $rname . "<br><br>\t ".$rname." Mobile No: " . $rmobile . " <br><br><br>\t A ticket request has been created and a respective department staff will look at the problem and take necessary action. <br><br>Pranam<br>ISHAVIDHYA TEAM<br><br><br>This is a system generated mail. Please do not reply to this email ID. If you have a query or need any clarification please contact Administrator" ;
					echo $email;
                    	$email = "sivavivekanantha@gmail.com";
						mail($email, $submail ,$mbody,  
						"Reply-To: sivavivekanantha@yahoo.com\n" .  
						"From:sivavivekanantha@yahoo.com\n" .  
						"MIME-Version: 1.0\n" .  
						"Content-type: text/html; charset=iso-8859-1"); 
						
						$mbody=""; 
	
					echo "<p align='center'><a href='New_Ticket.php'>Goto NewTicket</a> &nbsp;&nbsp;&nbsp; <a href='Ticket_Status.php'>Goto Ticket Status</a></p>";
					$skpform = 1;
				}
				else if($Ticket_Id>0)
				{
				echo "<p class='mesg'>Ticket Status has been Updated Successfully.</p><br>";
				echo "<p align='center'><a href='' onclick='window.close(); window.opener.location.reload();' >Colse Window</a></p>";
				$skpform = 1;
				}                
			
			}
			else
			{
				$errmsg1=mssql_get_last_message();            
				$errflag=2;
			}
		}        
		if($errflag==1) 
			echo "<p class='error'>Incomplete / Invalid entries for<br>".$errmsg;
		elseif($errflag==2)
			echo "<p class='error'>".$errmsg1;
				
	}    ?>
<body>
<?php 

	if($Ticket_Id > 0 )
	{
		$Query=mssql_init('Sp_GetTicket_Status',$mssql);
		mssql_bind($Query,'@Ticket_Id',$Ticket_Id,SQLINT4,false,false,5);
		mssql_bind($Query,'@Ticket_Type',$Ticket_Type,SQLINT4,false,false,20);
		mssql_bind($Query,'@Ticket_Mode',$Ticket_Mode,SQLINT4,false,false,20);
		mssql_bind($Query,'@UserId',$_SESSION['UserID'],SQLINT4,false,false,20);
		mssql_bind($Query,'@DeptCode',$_SESSION['DeptCode'],SQLINT4,false,false,20);
		mssql_bind($Query,'@CatCode',$_SESSION['CatCode'],SQLINT4,false,false,20);
		mssql_bind($Query,'@SchoolID',$_SESSION['SchoolId'],SQLINT4,false,false,20);
		$Tresult= mssql_execute($Query);
		mssql_free_statement($Query);
		while($field=mssql_fetch_array($Tresult)) 
		{
            $rname              =    $field['Name'];
            $rmobile            =    $field['Mobile'];
            $remail             =    $field['Email'];
            $Ticket_Id          =    $field['Ticket_Id'] ;
            $Ticket_Type        =    $field['Ticket_Type'] ;
            $Ticket_Mode        =    $field['Ticket_Mode'] ;
            $Category_Code      =    $field['Category_Code'] ;
            $Priority           =    $field['Priority'] ;    
            $School_Name        =    $field['School_Name'] ;
            $School_Id          =    $field['School_Id'] ;
            $Assign_To          =    $field['Teacher_Name'];    
            $Raised_To          =    $field['Raised_To'];                
            $Place              =    $field['Place'];                
            $Description        =    $field['Description'];    
            $Status             =    $field['Status'];    
            $Username           =    $field['Teacher_Name'];    
            $Reopen_Remarks     =    $field['Reopen_Remarks'];    
            $PrevRemarks        =    $field['Remarks'];
            $Created_By         =   $field['Created_By'];
            
            If($Created_By == $_SESSION['UserID'])    $Viewmode=1; 
            elseIf($Raised_To == $_SESSION['UserID']) $Viewmode=2;
            //echo $Raised_To."==".$Viewmode;
			if($Status==1) $Show_Status="OPEN";
			else if($Status==2) $Show_Status="IN-PROGRESS";
			else if($Status==3) $Show_Status="HOLD";
			else if($Status==4) $Show_Status="IGNORE";
			else if($Status==5) $Show_Status="FIXED";
			else if($Status==6) $Show_Status="CLOSED";
			else if($Status==7) $Show_Status="RE-OPEN";
		} 
	}    
if($skpform <> 1) { ?>
    <form name="myform" action="New_Ticket.php" id="myform" method="post">
		<input type="hidden" name="frm"  value=<?php echo $frm?>/>
		<input type="hidden" name="Ticket_Id"  value=<?php echo $Ticket_Id?>/>
		<input type="hidden" name="Ticket_Mode"  value=<?php echo $Ticket_Mode?>/>
		<input type="hidden" name="hdnRemarks"  value="<?php echo $PrevRemarks?>" />
    	  <table width="65%" border="0" align="center" cellpadding="5" cellspacing="1" >
			<tr>
			  <?php if($Ticket_Id == 0) { ?>
              <td align='Left' colspan='3' ><img src='../images/arrow_skip.png' width='16' height='16' />&nbsp;&nbsp;<span class='view_tit_text1'>New Ticket</span>
			  <td align='right'><span class='mand'> *</span> mandatory fields
				<?php } else { ?>
			  <td align='Left' colspan='4' ><img src='../images/arrow_skip.png' width='16' height='16' /><span class='view_tit_text1'>Ticket Status</span>
				<?php }                ?>
			  </td>
			</tr>
			<tr>
			  <td class="label">Name<?php if($Ticket_Id == 0) echo "<span class='mand'> *</span>"?></td>
			  <td class="value"><?php if($Ticket_Id > 0) echo $rname; else { ?>
				<input type="text" name="rname" id="rname"  maxlength="30" onkeydown="return alphaonly('rname')" value="<?php echo $rname ?>" />
				<?php } ?></td>
			  <td class="label">Mobile
				<?php if($Ticket_Id == 0) echo "<span class='mand'> *</span>"?></td>
			  <td class="value" ><?php if($Ticket_Id > 0) echo $rmobile; else { ?>
				<input type="textbox" name="rmobile" id="rmobile" maxlength="13" onkeydown="return numberonly('rmobile')" value="<?php echo $rmobile ?>" />
				<?php } ?>
			  </td>
			</tr>
			<tr>
			  <td class="label">Email Id&nbsp;&nbsp;</td>
			  <td class="value"><?php if($Ticket_Id > 0) echo $remail; else { ?>
				<input type="textbox" name="remail" id="remail" value="<?php echo $remail ?>" />
				<?php } ?></td>
                
                <td class="label">Ticket Mode
				<?php   if($Ticket_Mode == 0) echo "<span class='mand'> *</span>"?></td>
			     <td class="value"><?php if($Ticket_Id > 0) { if($Ticket_Mode==1) echo "Acadamic"; else  echo "Non-Acadamic";  } else { ?>
				<select name="ddlTicket_Mode" id="ddlTicket_Mode" onchange="make_ajax($('#Category_Code').val(),this.value)">
				  <option selected="selected" value="0">Select</option>
				  <option value="1" <?php if($Ticket_Mode==1) echo "Selected"; ?>>Acadamic</option>
				  <option value="2" <?php if($Ticket_Mode==2) echo "Selected"; ?>>Non-Acadamic</option>
				</select><?php } ?>
			  </td>	
 
			</tr>
			<tr>
               <td class="label">Ticket Type
				<?php   if($Ticket_Id == 0) echo "<span class='mand'> *</span>"?></td>
			  <td class="value"><?php if($Ticket_Id > 0) { if($Ticket_Type==1) echo "Task"; else  echo "Issue";  } else { ?>
				<select name="ddlTicket_type" id="ddlTicket_type"  onchange="showtask(this.value)">
				  <?php if($_SESSION['CatCode'] == 1 ) {?>
				  <option selected="selected" value="0">Select</option>
				  <option value="1" <?php if($Ticket_Type==1) echo "Selected"; ?>>Task</option>
				  <?php } ?>
				  <option value="2" <?php if($Ticket_Type==2) echo "Selected"; ?>>Issue</option>
				</select>
				<?php } ?>
			  </td>               		  
			  <td class="label">Priority
				<?php if($Ticket_Id == 0) echo "<span class='mand'> *</span>"?></td>
			  <td class="value"><?php if($Ticket_Id > 0) { if($Priority==1) echo "Low" ; if($Priority==2) echo "Medium" ; if($Priority==3) echo "High"; }  else { ?>
				<select name="ddlPriority" id="ddlPriority">
				  <option selected="selected"  value="0">Select</option>
				  <option value="1"  <?php if($Priority==1) echo "Selected"; ?>>Low</option>
				  <option value="2" <?php if($Priority==2) echo "Selected"; ?>>Medium</option>
				  <option value="3" <?php if($Priority==3) echo "Selected"; ?>>High</option>
				</select>
				<?php } ?>
			  </td>
			</tr>
			<tr id="Task_Grp" <?php if($Ticket_Type==2 or $Ticket_Type==0) echo "style=display:none"; ?> >
			  <td class="label">Assign To
				<?php if($Ticket_Id == 0) echo "<span class='mand'> *</span>"?></td>
			  <td class="value"><?php if($Ticket_Id > 0) echo $School_Name;  else { 
				//SHOW SCHOOL DROPDOWN
				$query = mssql_init('sp_Category_Select',$mssql);
				$result = mssql_execute($query);
				mssql_free_statement($query);    ?>
				<select id="Category_Code" name="Category_Code" onchange="make_ajax(this.value,$('#ddlTicket_Mode').val())">
				  <option  value="0">Select</option>
				  <?php while($field = mssql_fetch_array($result))    {  ?>
				  <option value="<?php echo $field['Category_Code']?>" <?php if($School_Id == $field['Category_Code']) echo "Selected"; ?>> <?php echo $field['Category']?></option>
				  <?php } ?>
				</select>
				<?php } ?>
			  </td>
			  <td class="label">Person
				<?php if($Ticket_Id == 0) echo "<span class='mand'> *</span>"?></td>
			  <td class="value"><?php if($Ticket_Id > 0) echo $Assign_To;  else {
			if($errflag== 0 ) { ?>
				<div id="divAssignto">
				  <select name="ddlAssignto" id="ddlAssignto" >
					<option  value="0">Select</option>
				  </select>
				</div>
				<?php       }  else { 
					$query = mssql_init('Get_Teachername',$mssql);
					mssql_bind($query,'@Category_Code',$Category_Code,SQLINT4,false,false,5);
                     mssql_bind($query,'@Ticket_Mode',$Ticket_Mode,SQLINT4,false,false,5);
					$result = @mssql_execute($query);
					mssql_free_statement($query);   ?>
				<select name="ddlAssignto" id="ddlAssignto">
				  <option  value="0">Select</option>
				  <?php while($row=mssql_fetch_array($result)) { ?>
				  <option value="<?=$row['Teacher_Code']?>"  <?php if($Assign_To == $row['Teacher_Code']) echo "Selected"; ?>>
				  <?=$row['Teacher_Name']?>
				  </option>
				  <?php } ?>
				</select>
				<?php               }     
			} ?>
			  </td>
			</tr>
			<tr id="Issue_grp" <?php if($Ticket_Type==1) echo "style=display:none"; ?> >
			  <td class="label">Raised To
				<?php if($Ticket_Id == 0) echo "<span class='mand'> *</span>"?></td>
			  <td class="value" valign="top"><?php if($Ticket_Id > 0) echo $Raised_To;  else {
				$query = mssql_init('sp_GetDepartment',$mssql);
				$result = mssql_execute($query);
				mssql_free_statement($query);    ?>
				<select id="Raised_to" name="Raised_to">
				  <option  value="0">Select</option>
                  <option  value="999">Super Admin</option>
				  <?php while($field=mssql_fetch_array($result))    {    
					  if($field['Department_Code'] <> $_SESSION['DeptCode'] ) {?>
				  <option value="<?php echo $field['Department_Code'] ?>" <?php if($Raised_To == $field['Department_Code']) echo "Selected"; ?>> <?php echo $field['Department'] ?> </option>
				  <?php } }?>
				</select>
				<?php  } ?>
			  </td>
			  <td class="label">Location</td>
			  <td valign="top" class="value"><?php if($Ticket_Id > 0) echo $Place;  
							else { ?>
				<div align="left">
				  <input name="txtPlace" type="text" id="txtPlace" maxlength="50" onkeydown="return alphanumeric('txtPlace')" value="<?php echo $Place ?>" />
				</div>
				<?php } ?></td>
			</tr>
			<tr>
			  <td valign="top" class="label">Description
				<?php if($Ticket_Id == 0) echo "<span class='mand'> *</span>"?></td>
			  <td colspan="3" class="value"><?php if($Ticket_Id > 0) echo $Description;  
							else { ?>
				<textarea name="txtDescription" cols="58" rows="4" maxlength="500" id="txtDescription"  onkeyup="lengthcheck(500,'#divmsg','#txtDescription');"><?php echo $Description ?></textarea>
				<div id="divmsg" style="color:#FF0000"></div></td>
			  <?php } ?>
			</tr>
			<?php   
				if($Ticket_Id > 0 and ( $Status!=5 and $Status!=4 and $Status!=7 ))    
					{ ?><tr><td class="label">Status </td>
				<?php if($Viewmode==1 ) { ?>
			<td class="value" colspan="3"><?php echo $Show_Status; ?></td>
			  <?php }
						else if($Viewmode==2 and $Status!=5 && $Status!=4 )  { ?>
			  <td class="value" colspan="3"><select id='ddlStatus' name='ddlStatus' onchange='getCity(this.value)'>
				  <?php  if( $Status==1 || $Status==6) { ?>
				  <option value='1' <?php if($Status==1) echo 'Selected'; ?> >Open</option>
				  <option value='3' <?php  if($Status==3) echo 'Selected'; ?> >Hold</option>
				  <option value='4' <?php  if($Status==4)  echo 'Selected'; ?>  >Ignore</option>
				  <?php   }    
						if($Viewmode==2 and $Status==2) { ?>
				  <option value='2' <?php if($Status==2) echo 'Selected';?> >In Progress</option>
				  <option value='3' <?php  if($Status==3) echo 'Selected'; ?> >Hold</option>
				  <option value='4' <?php  if($Status==4) echo 'Selected'; ?>  >Ignore</option>
				  <?php       }    
						if($Viewmode==2 and $Status==3) { ?>
				  <option value='3' <?php  if($Status==3) echo 'Selected'; ?> >Hold</option>
				  <option value='4' <?php  if($Status==4) echo 'Selected'; ?> >Ignore</option>
				  <?php }    ?>
				  <option value='5'<?php  if($Status==5) echo 'Selected'; ?>  >Fixed</option>
				</select></td>
			</tr>
			<tr>
			  <td class="label" valign="top" >Remarks</td>
			  <td colspan="3" class="value"><textarea name='Remarks' cols='40' rows='3' maxlength='500' id='txtRemarks' onkeyup=lengthcheck(500,'#divRemarks','#txtRemarks')><?php echo $Remarks?></textarea>
				<div id='divRemarks' style='color:#FF0000'></div></td>
			</tr>
			<?php  }  }
			if($Status==5 and $Viewmode ==1) { ?>
			<tr>
			  <td class="label">Status</td>
			  <td class="value" colspan="3"><table width="100%" border="0" cellpadding="5" cellspacing="1">
				  <tr>
					<td width="4%"><input name='Status' id='chk_closed' type='radio' value='7'; onclick=Reopen(); /></td>
					<td class="td_r_text" width="9%">Closed</td>
					<td width="4%"><input name='Status' id='chk_reopen' type='radio' value='6'; onclick=Reopen(); /></td>
					<td class="td_r_text" width="69%">Reopen</td>
				  </tr>
				</table></td>
			</tr>
			<?php } ?>
			<tr id='rerem' style='display:none'>
			  <td class="label" valign='top' id='trReopen'>Reopen Remarks</td>
			  <td colspan='3' class="value"><textarea name='txtReopen_Descr' cols='40' rows='3' maxlength='500' id='txtReopen_Descr'  value='<?php echo $txtReopen_Descr ?>'   onkeyup=lengthcheck(500,'#divReopen','#txtReopen_Descr');></textarea></td>
			</tr>
			<?php if($Status > 1 ) { ?>
			<tr>
			  <td class="label" valign="top">Prev.Remarks</td>
			  <td class="value" colspan="3"><?php echo $PrevRemarks; ?></td>
			</tr>
			<?php } if($Status==1 || $Status==6) {    ?>
			<tr>
			  <td colspan="4" class="value"><table width="100%" border="0" cellpadding="5" cellspacing="1">
				  <tbody>
					<tr>
					  <?php  if( $Viewmode==2){?>
					  <td width="3%"><input style="width:15px; height:15px;" name="Terms" id="Terms" type="checkbox" value="1" /></td>
					  <td width="97%" class="td_r_text">I agreed this Ticket to In-Progress </td>
					  <?php }?>
					</tr>
				  </tbody>
				</table></td>
			</tr>
			<?php }?>
			<tr align="right">
            <td colspan="4" >
            <table border="0" cellpadding="1" cellspacing="1" align="right">
            <tr><td>
            <?php  if($Viewmode==2 and $Status<>4 and $Status<>5 and $Status<>7){ ?>
				<input type="submit" name="Save" id="Save" value="Save" class='winbutton_go' />
			</td><td>
				<?php } if(($Viewmode == 1 and $Status==5) or $Ticket_Id==0){?>
				<input type="submit" name="Save" id="Save" value="Save" class='winbutton_go' />
				<?php   } ?>
            </td><td>
				<?php   if($Ticket_Id > 0) { ?>
				<input type="submit" name="Close" value="Close" class="winbutton_go" onclick="window.close(); window.opener.location.reload();" />
			</td><td>
                <?php } if($Ticket_Id == 0){ ?>
				<input type='button' name='Reset' value='Reset' class='winbutton_go' onclick="Clear();" />
				<?php }?>
            </td>
            </tr></table>			  
            </td>
			</tr>
		  </table>

	  </form>
<?php } if($errflag==1 ) echo "<script>showtask(". $Ticket_Type.")</script>"    ?>
</body>
</html>
<?php if($Ticket_Id == 0) include("../includes/copyright.php") ?>
