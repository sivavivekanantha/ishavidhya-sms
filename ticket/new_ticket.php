<?php session_start();  ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php if($_GET['frm']==1 or $_POST['frm']==1) {
		include("../includes/header1.php"); 
		$frm = 1;
	   if($_GET['Ticket_Mode']>0 )  $Ticket_Mode = $_GET['Ticket_Mode'];			//TICKET MODE CHECKED FOR ASSINGNED OR RAISED TICKET
       if($_GET['Ticket_View']>0 )  $Ticket_View = $_GET['Ticket_View'];			//TICKET VIEW CHECKED FOR SUPER ADMIN LOGIN
		}
	else {
		include("../includes/header.php"); 
		title('Ticketing System','New Ticket',5,2,0);     
		}    ?>
<script language="javascript" type="text/javascript">
function make_ajax(obj,obj1)
{   
	if(obj==0) return false;
    url = "../includes/ajax.php?listcode=2&catcode="+obj+"&Assign_To="+obj1;
	$('#divAssignto').load(url);
}

function make_ajax11(obj,obj1)
{   

	if(obj==0) return false;
    url = "../includes/ajax.php?listcode=11&deptcode="+obj+"&Raised_To="+obj1;
	$('#ddlRaisedto').load(url);
}

function showtask(obj)
{    
	if(obj==1) {
		$("#Task_Grp").show();
		$("#Issue_grp").hide();
		}
	else {
		$("#Task_Grp").hide();
		$("#Issue_grp").show();
		}
}
function Reopen()
{ 
	if ($('#chk_reopen').attr('checked')) 
		 {
		$('#rerem').show();
		$('#Save').show();
		}
	else if($('#chk_closed').attr('checked')) 
		 {
		$('#Save').show();
		$('#rerem').hide();
		}
	else
		{
		$('#rerem').hide();
		$('#Save').hide();
		}
}

function Clear()
{    
	$('#rname').val("");
	$('#rmobile').val("");
	$('#remail').val("");
	$('#ddlTicket_type').val("");
	$('#ddlPriority').val("");
	$('#Category_Code').val("");
	$('#ddlAssignto').val("Select");
	$('#Raised_to').val("");
	$('#txtPlace').val("");
	$('#txtDescription').val("");
	$('#divmsg').hide();
	url = "../includes/ajax.php?listcode=2&School_Id="+'';
	$('#divAssignto').load(url);
}
</script>
<?php 
	$errmsg="";
	$errflag=0;
	$dummy=0;
	$Ticket_Id=0;
	if($_GET['Ticket_Id']>0) 
		$Ticket_Id=$_GET['Ticket_Id']; 
	if($_POST['Save'] =='Save')    
	{        
	
	if(is_uploaded_file($_FILES['ticketphotos']['tmp_name']))
        //Photo upload in new ticket screen
        {
		$exts = array('gif', 'png', 'jpg', 'jpeg','pjpeg'); 
		$extension=end(explode("/",$_FILES['ticketphotos']['type']));
       	if(!in_array($extension, $exts)){
            $file_error = 'YES';
            echo "<script type='text/javascript'>alert('Not a valid image type!! Please Retry!');</script>";
        }
    } else {
        $file = $_FILES['ticketphotos']['tmp_name']; //image size checking
        $size = $_FILES['ticketphotos']['size'];
    }
	
	   // $Ticket_Id1     =    "";
        $rname          =    Trim($_POST['rname']);
        $rmobile        =    Trim($_POST['rmobile']);
        $remail         =    Trim($_POST['remail']);
        $Ticket_Type    =    $_POST['ddlTicket_type'];            
        $Priority       =    $_POST['ddlPriority'];                
        $Category_Code  =    $_POST['Category_Code'];
        $Assign_To      =    $_POST['ddlAssignto'];        
        $Raised_To      =    $_POST['Raised_to'];
		$ddlRaised_To   =    $_POST['ddlRaisedto'];
        $Place          =    Trim($_POST['txtPlace']);                
        $Description    =    Trim($_POST['txtDescription']);    
        $Ticket_Id      =    $_POST['Ticket_Id'];
        $Remarks        =    Trim($_POST['Remarks']);
        $PRemarks       =    $_POST['hdnRemarks'];
        $Status         =    $_POST['ddlStatus'];					  // Current Status
        $Cur_status     =      $Status;
        $Status1        =    $_POST['Status'];         				 // Click ReOpen 
        $Reopen_Descr   =    Trim($_POST['txtReopen_Descr']);    	//ReOPen Ticket Description
        $Terms          =   $_POST['Terms']; 
		$Day_Count   =    Trim($_POST['txtDay_Count']); 
		$Days_Months   =    $_POST['ddlDays_Months']; 
        
		if($Ticket_Id==0)								  		  //FOR INSERT NEW TICKET PROCESS
		{
			 $dummy = Strcheck($rname,$errmsg,$errflag,"From");
			 $dummy = Strcheck($rmobile,$errmsg,$errflag,"Mobile");
             if($dummy==0){
                $dummy = Min_Lengthcheck($rmobile,$errmsg,$errflag,"Mobile No. must be 10 digit",10);
             }
			 if(strlen($remail)>0)$dummy = Emailcheck($remail,$errmsg,$errflag,"Email Id");
			$dummy = Zerocheck($Ticket_Type,$errmsg,$errflag,"Ticket Type");
			$dummy = Zerocheck($Priority,$errmsg,$errflag,"Priority");
			if($Ticket_Type==1) 
			{
				$dummy = Zerocheck($Category_Code,$errmsg,$errflag,"Assign To");
				//$dummy = Zerocheck($Assign_To,$errmsg,$errflag,"Person");
			}else if($Ticket_Type==2) 
			{
				$dummy = Zerocheck($Raised_To,$errmsg,$errflag,"Raised To");
			}
			$dummy = Strcheck($Description,$errmsg,$errflag,"Description");
		 }    
		else 	//FOR UPDATE EXISTING TICKET STATUS PROCESS
		{
			if($Status == 3 or $Status ==4 ) $dummy = Strcheck($Remarks,$errmsg,$errflag,"Remarks"); //TO CEHCK THE REMARKS WHEN CHANGE STATUS HOLD  OR INGNORE
			if($Status1 == 6 )
			{ 
				$dummy = Strcheck($Reopen_Descr,$errmsg,$errflag,"Remarks"); 						//TO CEHCK THE REOPEN REMARKS WHEN CHANGE STATUS REOPEN
			}
			if($Status == 1 or $Status == 6 )
			{ 
			$dummy = Strcheck($Day_Count,$errmsg,$errflag,"No of days");
			//$dummy = Zerocheck($Days_Months,$errmsg,$errflag,"Days/Months");
			$dummy = Strcheck($Terms,$errmsg,$errflag,"Accept ticket");                            //TO CEHCK THE ACCEPT TICKET WHEN CHANGE STATUS FROM OPEN OR REOPEN
			}
		}
		if($errflag==0)                                                                           //TO ADD REMARKS TO PREVIOUS REMARKS
		{
			if(strlen($PRemarks) > 0 )
				$Remarks  = $Remarks."<br>".$PRemarks; 											
			if($Status1 > 0 ) 
			{
				$Remarks  =     $Reopen_Descr."<br>".$Remarks;
				$Status = $Status1;
			}
		
			if($Ticket_Id==0)          													       //FOR INSERT NEW TICKET PROCESS
			{   
			//exit();	
				mssql_free_result($result);                                                  
				$query=mssql_init('Sp_New_Ticket_Save',$mssql);
				mssql_bind($query,'@Rname',$rname,SQLVARCHAR,false,false,50);
				mssql_bind($query,'@Rmobile',$rmobile,SQLVARCHAR,false,false,25);
				mssql_bind($query,'@Remail',$remail,SQLVARCHAR,false,false,100);
				mssql_bind($query,'@Ticket_Type',$Ticket_Type,SQLINT4,false,false,5);
				mssql_bind($query,'@Priority',$Priority,SQLINT4,false,false,5);
                If($Category_Code=="0" and $Assign_To=="0") $Category_Code=6;
				mssql_bind($query,'@Category_Code',$Category_Code,SQLINT4,false,false,5);
                If($ddlRaised_To=="" or $ddlRaised_To=="0") $ddlRaised_To = $Assign_To;
				mssql_bind($query,'@Raised_to',$ddlRaised_To,SQLINT4,false,false,5);
				mssql_bind($query,'@Place',$Place,SQLVARCHAR,false,false,50);
				mssql_bind($query,'@Description',$Description,SQLTEXT,false,false,strlen($Description));
				mssql_bind($query,'@Userid',$_SESSION["UserID"],SQLINT4,false,false,5);
                mssql_bind($query,'@SchoolID',$_SESSION["SchoolId"],SQLINT4,false,false,5);
                mssql_bind($query,'@CatCode',$_SESSION["CatCode"],SQLINT4,false,false,5);
				//mssql_bind($query,'@Ticket_Id1',$Ticket_Id1,SQLINT4,true);	
				mssql_bind($query,'@Ticket_Id1',$Ticket_Id1,SQLINT4,false,false,5);
			
				//echo "photo".$Ticket_Id1 ."<br>";
					
			}
			elseif($Ticket_Id>0)         													//FOR UPDATE EXISTING TICKET STATUS PROCESS
			{  	
			 
				mssql_free_result($result);
				$query=mssql_init('Sp_New_Ticket_Update',$mssql);
				mssql_bind($query,'@Ticket_Id',$Ticket_Id,SQLINT4,false,false,5);
				mssql_bind($query,'@Status',$Status,SQLINT4,false,false,5);
				mssql_bind($query,'@Remarks',$Remarks,SQLVARCHAR,false,false,strlen($Description));
				mssql_bind($query,'@Day_Count',$Day_Count,SQLINT4,false,false,5);
				mssql_bind($query,'@Days_Months',$Days_Months,SQLINT4,false,false,5);
			}
			$result = mssql_execute($query);
			if(!$result)  echo mssql_get_last_message();
			mssql_free_statement($query);
			if($result)
			{  
				if($Ticket_Id==0)
				{
					 echo "<p class='mesg'>Your Ticket has been Registered Successfully.</p>";
					   While($Tresult = mssql_fetch_array($result))
                       {   
					     if($Ticket_Id==0)  $Ticket_Id1 = $Tresult['Ticket_Id1'];
			                if(strlen($email)>0)   $email.= ",".$Tresult['Emailid'];    else $email = $Tresult['Emailid']; 
							//if(strlen($mno)>0)  $mno.= ",91".$Tresult['Mno'];    else $mno = "91".$Tresult['Mno']; 
                       }   
					   
					   //Create foloder for creating pic
                //mkdir("../ticketphotos/" . $Ticket_Id1, 0700); //photo folder create
	                $path = "../ticketphotos/". $Ticket_Id1 . ".jpg"; //photo name creation
    	            move_uploaded_file($_FILES["ticketphotos"]["tmp_name"], $path); //file upload to folder
				
					$result = move_uploaded_file($tmpName, $path);
					$orig_image = imagecreatefromjpeg($path);
					
					$image_info = getimagesize($path); 
					$width_orig  = $image_info[0]; // current width as found in image file
					$height_orig = $image_info[1]; // current height as found in image file
					$width = 300; // new image width
					$height = 250; // new image height
					$destination_image = imagecreatetruecolor($width, $height);
					imagecopyresampled($destination_image, $orig_image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
					// This will just copy the new image over the original at the same filePath.
					imagejpeg($destination_image, $path, 100);
					   
					   // if(strlen($mno)>0) $mno="91".$rmobile.",".$mno;
					 	if(strlen($mno)>0) $mno=$mno;
						// MAIL SENT CODES
						$submail = "New Ticket Raised In Ishavidhya";
						if($Ticket_Type==1) $Ttype = "Task"; Else $Ttype = "Issue";
                        if($Ticket_Type==2) $IssueCmd = "A ticket request has been created and a respective department staff
						 will look at the problem and take necessary action."; else $IssueCmd="";
						$link="To know your ticket status please"."<a href='www.ishadb.com/ishavidhya'>".  "  click here</a>";
						
					$mbody="<table width='21%' border='0' cellspacing='0' cellpadding='0' align='center'>
	<tr>
		
		<td  bgcolor='#f1ede5'><img src='www.ishadb.com/ishavidhya/images/mail_logo.jpg' width='120' height='100' /></td>
		<td><img src='www.ishadb.com/ishavidhya/images/rds_success1.gif' width='497' /></td>
	</tr>
	<tr style='background-color:#c9813c;'>
		<td colspan='2' height='7'></td>
		
	</tr>
	<tr>
		<td colspan='2' height='250' style='background:#fbfaf7' >
		<table width='100%' border='0' cellspacing='1' cellpadding='3' style='margin-left:25px;'>
	<tr>
		<td style='color:#6a6c60; font-family:Tahoma; font-size:14px;'>Namaskaram, </td>
	</tr>
	<tr>
		<td height='3'></td>
	</tr>
	<tr><td style='color:#6a6c60; font-family:Tahoma; font-size:14px; padding-left:30px'>Thank you for contacting us. </td>
	
	</tr>
	<tr>
		<td><table width='98%' border='0' cellspacing='5' cellpadding='0'>
		
		<tr>
				<td width='37%' style='color:#6a6c60; font-family:Tahoma; font-size:14px; padding-left:60px' valign='top'>Issue Description</td>
				<td style='color:#6a6c60; font-family:Tahoma; font-size:14px;' valign='top'>:</td>
				<td  >&nbsp;</td>
				<td width='63%' style='color:#6a6c60; font-family:Tahoma; font-size:14px;' valign='top' >".$Description."</td>
			</tr><tr>
				<td style='color:#6a6c60; font-family:Tahoma; font-size:14px; padding-left:60px'>Raised by</td>
				<td style='color:#6a6c60; font-family:Tahoma; font-size:14px;'>:</td>
				<td>&nbsp;</td>
				<td style='color:#6a6c60; font-family:Tahoma; font-size:14px; align:left'>".$rname."</td>
			</tr><tr>
				<td style='color:#6a6c60; font-family:Tahoma; font-size:14px; padding-left:60px'>Mobile No</td>
				<td style='color:#6a6c60; font-family:Tahoma; font-size:14px;'>:</td>
				<td>&nbsp;</td>
				<td style='color:#6a6c60; font-family:Tahoma; font-size:14px;'>".$rmobile."</td>
			</tr>
			
			
		</table></td>
	</tr>
	<tr><td   height='20'></td></tr>
	<tr>
				<td   style='color:#6a6c60; font-family:Tahoma; font-size:14px; padding-left:10px'>". $link. "</td>
				
			</tr>
</table>
		
		</td>
	</tr>
	
	<tr>
	<td colspan='2' style='color:#6a6c60; font-family:Tahoma; font-size:14px; padding-left:500px ; background:#fbfaf7' >Pranam<br>
</td>

	</tr>
	<tr><td colspan='2'  nowrap style='color:#6a6c60; font-family:Tahoma; font-size:14px; padding-left:500px ; background:#fbfaf7' >ISHAVIDHYA TEAM <br /> <br /></td></tr>
	<tr style='background-color:#c9813c;'>
		<td colspan='2' height='7' ></td>
	</tr>
	<tr >
		
		<td colspan='2' bgcolor='#fbfaf7' style='background:url(www.ishadb.com/ishavidhya/images/copy_right.gif)  no-repeat ;color:#6a6c60; font-family:Tahoma; font-size:14px;' height='120' align='center'>This is a system generated mail. Please do not reply to this email ID. If you have a query or need any clarification please contact Administrator</td>
	</tr>
</table>";
						
						if(strlen($Description)>70)
							$sms_body="Namaskaram,NewTicket Raised by:".$rname." [".$rmobile."] ".$Ttype." Desc.: ".substr($Description,0,69)."... Pranam";
						else
							$sms_body="Namaskaram,NewTicket Raised by:".$rname."[".$rmobile."]".$Ttype." Desc.: ".$Description." Pranam";			
                        if(strlen($email)>0) 
						{ 	
							if($Raised_To==500001 or $Raised_To==500002){
								mail($email, $submail ,$mbody,  
								"Reply-To: Tickets@ishavidhya.org\n" .  
								"From:Tickets@ishavidhya.org\n" .  
								"CC:gopi.p@ishavidhya.org \n".
								"BCC:santhoshm.isha@gmail.com \n".
								"MIME-Version: 1.0\n" .  
								"Content-type: text/html; charset=iso-8859-1"); 
							 }else{
								mail($email, $submail ,$mbody,  
								"Reply-To: Tickets@ishavidhya.org\n" .  
								"From:Tickets@ishavidhya.org\n" . 
								"BCC:santhoshm.isha@gmail.com \n". 
								"MIME-Version: 1.0\n" .  
								"Content-type: text/html; charset=iso-8859-1");  
								echo "Mail Sent";
								}
						} 
						$mbody=""; 
					echo "<p align='center'><a href='new_ticket.php'>Goto NewTicket</a> &nbsp;&nbsp;&nbsp; <a href='ticket_status.php'>Goto Ticket Status</a></p>";
					$skpform = 1;
					
					/*=----------------------- SEND A MESSAGE TO MOBILE -----------------------------------------*/
						
					/*	 $user="IT_Lakshmi"; //your username
						 $password="Ishait"; //your password
						 $mobilenumbers=$mno; //enter Mobile numbers comma seperated
						 $message = $sms_body; //enter Your Message 
						 $senderid="SMSCountry"; //Your senderid
						 $messagetype="N"; //Type Of Your Message
						 $DReports="Y"; //Delivery Reports
						 $url="http://www.smscountry.com/SMSCwebservice.asp";
						 
						 $message = urlencode($message);
						 $ch = curl_init(); 
						 if (!$ch){die("Couldn't initialize a cURL handle");}
						 $ret = curl_setopt($ch, CURLOPT_URL,$url);
						 curl_setopt ($ch, CURLOPT_POST, 1);
						 curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);          
						curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
						 curl_setopt ($ch, CURLOPT_POSTFIELDS,"User=$user&passwd=$password&mobilenumber=$mobilenumbers&message=$message&sid=$senderid&mtype=$messagetype&DR=$DReports");
						 $ret = curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
						
						//If you are behind proxy then please uncomment below line and provide your proxy ip with port.
						// $ret = curl_setopt($ch, CURLOPT_PROXY, "PROXY IP ADDRESS:PORT");
						 $ret = curl_setopt($ch, CURLOPT_PROXY, "192.168.99.253:8080");
						 curl_setopt($ch, CURLOPT_PROXYUSERPWD, 'level4'.':'.'test#');
						 $curlresponse = curl_exec($ch); // execute

						if(curl_errno($ch))
							echo 'curl error : '. curl_error($ch);
						
						 if (empty($ret)) {
							// some kind of an error happened
							die(curl_error($ch));
							curl_close($ch); // close cURL handler
						 } else {
							$info = curl_getinfo($ch);
							curl_close($ch); // close cURL handler
							//echo "<br>";
							echo $curlresponse;    //echo "Message Sent Succesfully" ;
						 }
					*/

					/* --------------------------------------------------------------------------------------------*/
					
				}
				else if($Ticket_Id>0)
				{
				echo "<p class='mesg'>Ticket Status has been Updated Successfully.</p><br>";
				echo "<p align='center'><a href='' onclick='window.close(); window.opener.location.reload();' >Close Window</a></p>";
				$skpform = 1;
				}                
			}
			else
			{
				$errmsg1=mssql_get_last_message();            
				$errflag=2;
			}
		}        
		if($errflag==1 ) 
			echo $errlbl.$errmsg."</p>";
		elseif($errflag==2)
			echo "<p class='error'>".$errmsg1;		
	} ?>
<body>
<?php 
	if($Ticket_Id > 0 )
	{	mssql_free_result($Tresult);
		$Query=mssql_init('Sp_GetTicket_Status',$mssql);  //TO GET EXISTING TICKET STATUS 
		mssql_bind($Query,'@Ticket_Id',$Ticket_Id,SQLINT4,false,false,5);
		mssql_bind($Query,'@Ticket_Type',$Ticket_Type,SQLINT4,false,false,20);
        mssql_bind($Query,'@Ticket_Mode',$Ticket_Mode,SQLINT4,false,false,20);
        mssql_bind($Query,'@Ticket_View',$Ticket_View,SQLINT4,false,false,20); 
		mssql_bind($Query,'@UserId',$_SESSION['UserID'],SQLINT4,false,false,20);
		mssql_bind($Query,'@CatCode',$_SESSION['CatCode'],SQLINT4,false,false,20);
        mssql_bind($Query,'@DeptCode',$_SESSION['DeptCode'],SQLINT4,false,false,20);
		$Tresult= mssql_execute($Query);
		mssql_free_statement($Query);
		while($field=mssql_fetch_array($Tresult)) 
		{
            $rname              =    $field['Name'];
            $rmobile            =    $field['Mobile'];
            $remail             =    $field['Email'];
            $Ticket_Id          =    $field['Ticket_Id'] ;
            $Ticket_Type        =    $field['Ticket_Type'] ;
            $Category_Code      =    $field['Category_Code'] ;
            $Category           =    $field['Category'] ;
            $Priority           =    $field['Priority'] ;    
            $School_Name        =    $field['School_Name'] ;
            $School_Id          =    $field['School_Id'] ;
            $Assign_To          =    $field['Raised_To'];
            $Department         =    $field['Department'];    
            $Raised_To          =    $field['Raised_To'];                
            $Place              =    $field['Place'];                
            $Description        =    $field['Description'];    
            $Status             =    $field['Status'];
  
            $Username           =    $field['Teacher_Name'];    
            $Reopen_Remarks     =    $field['Reopen_Remarks'];    
            $PrevRemarks        =    $field['Remarks'];
            $Created_By         =    $field['Created_By'];
            $Created_Date        =    $field['Created_Date'];
            $Response_Time      =    $field['Response_Time'];
            $Closed_Time        =    $field['Closed_Time'];
			$Day_Count			=    $field['Day_Count'];
			$Days_Months			=    $field['Days_Months'];
          
            if($Created_By == $_SESSION['UserID']) $Viewmode=1; 
            else if($Raised_To == $_SESSION['UserID'] or $Raised_To == $_SESSION['DeptCode']) $Viewmode=2;
           	if($Status==1) $Show_Status="OPEN";
			else if($Status==2) $Show_Status="IN-PROGRESS";
			else if($Status==3) $Show_Status="HOLD";
			else if($Status==4) $Show_Status="IGNORE";
			else if($Status==5) $Show_Status="FIXED";
			else if($Status==6) $Show_Status="RE-OPEN";
			else if($Status==7) $Show_Status="CLOSED";
            
		} 
	}   

		  		//mssql_free_result($result);
                $Query1=mssql_init('Get_NewticketUsers',$mssql);  //TO GET EXISTING TICKET STATUS 
                mssql_bind($Query1,'@UserName',$_SESSION["UserName"],SQLVARCHAR,false,false,20);
				$result= mssql_execute($Query1);
				mssql_free_statement($Query1);
			
				while($field5=mssql_fetch_array($result)) 
					{
					$rname = $field5['Teacher_Name'];
					$rmobile = $field5['Mobile_No'];
					$remail = $field5['Email_Id'];
					}
               	 
    if($skpform <> 1) { ?>
    <form name="myform" action="new_ticket.php" id="myform" method="post">
        <input type="hidden" name="frm"  value=<?php echo $frm?> />
        <input type="hidden" name="Ticket_Id"  value=<?php echo $Ticket_Id?> />
        <input type="hidden" name="Ticket_Mode"  value=<?php echo $Ticket_Mode?> />
        <input type="hidden" name="hdnRemarks"  value="<?php echo $PrevRemarks?>" />
        <input type="hidden" name="Ticket_View"  value=<?php echo $Ticket_View?>/>
     	<table width=<?php if($Ticket_Id >0) echo "90%"; else echo "65%"; ?> border="0" align="center" cellpadding="5" cellspacing="1" >
            
                <tr>
                  <?php if($Ticket_Id == 0) { ?>
                  <td align='Left' colspan='3' ><img src='../images/arrow_skip.png' width='16' height='16' />&nbsp;&nbsp;<span class='view_tit_text1'>New Ticket</span>
                  <td align='right'><span class='mand'> *</span> mandatory fields                  </td>
                    <?php } else { ?>
                  <td align='Left' colspan='4' ><img src='../images/arrow_skip.png' width='16' height='16' /><span class='view_tit_text1'>Ticket Status</span>
                    <?php }?>                  </td>
                </tr>       
                
                <tr>
                  <td class="label">From<?php if($Ticket_Id == 0) echo $mand?></td>
                  <td class="value"><?php if($Ticket_Id > 0) echo $rname; else { ?>
                    <input type="text" name="rname" id="rname"  maxlength="30" onkeydown="return alphaonly('rname')" value="<?php echo $rname ?>" />  <?php } ?></td>
                  
                  <td class="label">Mobile
                    <?php if($Ticket_Id == 0) echo $mand?></td>
                  <td class="value" ><?php if($Ticket_Id > 0) echo $rmobile; else { ?>
                     <input type="textbox" name="rmobile" id="rmobile" maxlength="13" onkeydown="return numberonly('rmobile')" value="<?php echo $rmobile ?>" /> 
					 <?php } ?>                  </td>   
                </tr>
                
                
                <tr>
                  <td class="label">Email Id&nbsp;&nbsp;</td>
                  <td class="value" <?php if($Ticket_Id==0)  { ?> colspan="3" <?php } ?>> <?php if($Ticket_Id > 0) echo $remail; else { ?>
                   <input type="textbox" name="remail" id="remail" maxlength="50" value="<?php echo $remail ?>" /> <?php } ?></td>
                  <?php if($Ticket_Id>0){ ?>
                  
                  <td class="label">Ticket Raised Date</td>
                  <td class="value"><?php echo $Created_Date; ?></td>
                    <?php } ?>
                </tr>
                
                <tr>
                   <td class="label">Ticket Type <?php if($Ticket_Id == 0) echo $mand?></td>
                  <td class="value"><?php if($Ticket_Id > 0) { if($Ticket_Type==1) echo "Task"; else  echo "Issue";  } else { ?>
                    <select name="ddlTicket_type" id="ddlTicket_type" onchange="showtask(this.value)">
                      <?php echo $Select; 
					  if($_SESSION['CatCode']==1 or $_SESSION['CatCode']==6 or $_SESSION['CatCode']==7 or $_SESSION['CatCode']==8 ) { ?>
                      <option value="1" <?php if($Ticket_Type==1) echo "Selected"; ?>>Task</option><?php } ?>
                      <option value="2" <?php if($Ticket_Type==2) echo "Selected"; ?>>Issue</option></select><?php } ?>					  </td> 
                                          
                  <td class="label">Priority <?php if($Ticket_Id == 0) echo $mand?></td>
                  <td class="value"><?php if($Ticket_Id > 0) { if($Priority==1) echo "Low" ; if($Priority==2) echo "Medium" ; if($Priority==3) echo "High"; }  else { ?>
                        <select name="ddlPriority" id="ddlPriority">
                         <?php echo $Select; ?>
                          <option value="1"  <?php if($Priority==1) echo "Selected"; ?>>Low</option>
                          <option value="2" <?php if($Priority==2) echo "Selected"; ?>>Medium</option>
                          <option value="3" <?php if($Priority==3) echo "Selected"; ?>>High</option>
                        </select>
                         <?php } ?>                  
                    </td>
                </tr>
                
                <tr id="Task_Grp" <?php if($Ticket_Type==2 or $Ticket_Type==0) echo "style=display:none"; ?> >
                  <td class="label"> Assigned To
                  <?php if($Ticket_Id == 0) echo $mand?></td>
                  <td class="value"><?php if($Ticket_Id > 0) echo $Category;  else { 
						//SHOW SCHOOL DROPDOWN
						mssql_free_result($result);
						$query = mssql_init('sp_Category_Select',$mssql);
						$result = mssql_execute($query);
						mssql_free_statement($query);    ?>
						<select id="Category_Code" name="Category_Code" onchange="make_ajax(this.value,0)">
							  <?php  echo $Select; 
							   while($field = mssql_fetch_array($result))    {  ?>
							  <option value="<?php echo $field['Category_Code']?>" <?php if($Category_Code == $field['Category_Code']) echo "Selected"; ?>> 
							  <?php echo $field['Category']?></option>
							  <?php } ?>
						</select>
					<?php }?>                  
                    </td>
                  
                  <td class="label">Person</td>
                  <td class="value"><?php if($Ticket_Id > 0) { if(strlen($School_Name)>0) echo $Username." - ".$School_Name; else echo $Username; }  
                        else { ?>
                                <div id="divAssignto">
                                  <select name="ddlAssignto" id="ddlAssignto"  multiple="multiple" >
                                    <option  value="0">Select</option>
                                  </select>
                                </div>
                                <?php if($errflag==1 ) echo "<script>make_ajax(". $Category_Code.",".$Assign_To.")</script>";
                            } ?><br>Choose more than one using CTRL key
				</td>
                </tr>
                
                <tr id="Issue_grp" <?php if($Ticket_Type==1) echo "style=display:none"; ?>>
                  <td class="label">Raised To <?php if($Ticket_Id == 0) echo $mand?></td>
                  <td class="value" ><?php if($Ticket_Id > 0) echo $Department; else {
				  mssql_free_result($result);
                        $query = mssql_init('sp_FetchDepartment',$mssql);
                        $result = mssql_execute($query);
                        mssql_free_statement($query);    ?>
                        <select id="Raised_to" name="Raised_to" onchange="make_ajax11(this.value,0)">
                              <?php echo $Select; 
							   while($field=mssql_fetch_array($result))    {    
                                  if($field['Department_Code'] <> $_SESSION['DeptCode'] ) {?>
                              <option value="<?php echo $field['Department_Code'] ?>" 
							  <?php if($Raised_To == $field['Department_Code']) echo "Selected"; ?>> 
                              <?php echo $field['Department'] ?> 
                              </option>
                              <?php } }?>
                        </select>
                        <?php  } ?>                  
                   </td>
                  
                  <td class="label">Person</td>
                  <td class="value"><?php if($Ticket_Id > 0) { if(strlen($School_Name)>0) echo $Username." - ".$School_Name; else echo $Username; }  
                        else { ?>
                      <div id="ddlRaisedto">
                        <select name="ddlRaisedto" id="ddlRaisedto">
                          <option  value="0">Select</option>
                        </select>
                      </div>
                    <?php if($errflag==1 ) echo "<script>make_ajax11(". $Raised_To.",".$Raised_To.")</script>";
                            } ?><br>Choose more than one using CTRL key
                  </td>
          </tr>
                
                <tr>
                  <td valign="top" class="label">Description <?php if($Ticket_Id == 0) echo $mand?></td>
                  <td colspan="3" class="value"><?php if($Ticket_Id > 0) echo $Description;  
                    else { ?>
                            <textarea name="txtDescription" cols="58" rows="4" maxlength="200" id="txtDescription" onkeyup="lengthcheck(200,'#divmsg','#txtDescription');"><?php echo $Description ?></textarea>
                            <div id="divmsg" style="color:#FF0000"></div>                            </td>
                    <?php } ?>
                </tr>
				<?php if($Ticket_Id==0) {?>
				  <tr>
				 <td class="label">Upload Photo</td>
                       <td colspan="3" class="value"> <input type="file" name="ticketphotos" size="25" /></td></tr>	
					   <?php }?>
			<?php if($Ticket_Id>0) {?>
			<tr>
				 <td class="label">Image</td>
                       <td colspan="3" class="value"> 
					     <?php if($Ticket_Id >0) {?>
						 
						 <img  id="imgBlueScoop_1" src="../ticketphotos/<?php echo $Ticket_Id ?>.jpg" alt="No Image"  class="PopBoxImageSmall" onClick="Pop(this,200,'PopBoxImageLarge');" onMouseOut="Revert(this,35,'PopBoxImageSmall')" width="128" height="128" style="height: 150px; border: dashed 1px #000000; " /> 
						 
					   
                         <?php }                           
						  ?>                         
						
					   </td></tr>	
					     <?php }?>				
                <?php  if($Ticket_Id > 0 ){ ?><tr>
                    <td class="label">Status </td>
                    <?php if($Viewmode==1 or $Ticket_View==1 or ($Viewmode==2 and $Status <>1 and $Status<>2 and $Status<>6 and $Status<>3 )) { ?>
                    <td class="value" colspan="3"><?php echo $Show_Status; ?></td>
                    <?php }
                     else if($Viewmode==2 and $Status!=5 && $Status!=4 )  { ?>
                     <td class="value" colspan="3"><select id='ddlStatus' name='ddlStatus' onchange='getCity(this.value)'>
                          <?php  if( $Status==1 || $Status==6) { ?>
                          <option value='1' <?php if(($errflag==0 and $Status==1) or ($errflag==1 and $Cur_status==1)) echo 'Selected'; ?> >Open</option>
                          <option value='3' <?php if(($errflag==0 and $Status==3) or ($errflag==1 and $Cur_status==3)) echo 'Selected'; ?> >Hold</option>
                          <option value='4' <?php if(($errflag==0 and $Status==4) or ($errflag==1 and $Cur_status==4))  echo 'Selected'; ?>  >Ignore</option>
                          <?php }    
                          if($Viewmode==2 and $Status==2) { ?>
                          <option value='2' <?php if(($errflag==0 and $Status==2) or ($errflag==1 and $Cur_status==2)) echo 'Selected';?> >In Progress</option>
                          <option value='3' <?php if(($errflag==0 and $Status==3) or ($errflag==1 and $Cur_status==3)) echo 'Selected'; ?> >Hold</option>
                          <option value='4' <?php if(($errflag==0 and $Status==4) or ($errflag==1 and $Cur_status==4)) echo 'Selected'; ?>  >Ignore</option>
                          <?php }    
                          if($Viewmode==2 and $Status==3) { ?>
                          <option value='3' <?php  if(($errflag==0 and $Status==3) or ($errflag==1 and $Cur_status==3)) echo 'Selected'; ?> >Hold</option>
                          <option value='4' <?php  if(($errflag==0 and $Status==4) or ($errflag==1 and $Cur_status==4)) echo 'Selected'; ?> >Ignore</option>
                          <?php } ?>
                          <option value='5'<?php  if(($errflag==0 and $Status==5) or ($errflag==1 and $Cur_status==5)) echo 'Selected'; ?>  >Fixed</option>
                        </select>                    
                     </td>
                </tr>
				
                <tr><td class="label" valign="top">No of days</td>
				<td colspan="3" class="value" ><?php if($Status==1 or $Status==6) {?> <input type="text"  maxlength="5" name="txtDay_Count" id="txtDay_Count" value="<?php echo $Day_Count; ?>" onkeydown="return numberonly('txtDay_Count')" />
				 <select name="ddlDays_Months" id="ddlDays_Months">
						  <option  value="1" <?php if($Days_Months==1) echo 'Selected'; ?>>Days</option>
						  <option  value="2" <?php if($Days_Months==2) echo 'Selected'; ?>>Months</option>
                        </select><?php }
						 else { echo $Day_Count;  
							if($Day_Count==1){if($Days_Months==1) echo " Day"; elseif($Days_Months==2) echo " Month";} 
							else { if($Days_Months==1) echo " Days"; elseif($Days_Months==2) echo " Months";  } 
						}?>   </td>
				</tr>
				
                <tr>
                  <td class="label" valign="top" >Remarks</td>
                  <td colspan="3" class="value"><textarea name='Remarks' cols='40' rows='3' maxlength='200' id='txtRemarks' 
                  	onkeyup=lengthcheck(200,'#divRemarks','#txtRemarks')><?php echo $Remarks?></textarea>
                    <div id='divRemarks' style='color:#FF0000'></div>                  
                  </td>
                </tr>
                <?php  }  }
                if($Status==5 and $Viewmode ==1) { ?>
                <tr>
                      <td class="label">Status</td> 
                      <td class="value" colspan="3">
                          <table width="100%" border="0" cellpadding="5" cellspacing="1">
                              <tr>
                                    <td width="4%"><input name='Status' id='chk_closed' type='radio' value='7'; onclick=Reopen(); /></td>
                                    <td class="td_r_text" width="9%">Closed</td>
                                    
                                    <td width="4%"><input name='Status' id='chk_reopen' type='radio' value='6'; onclick=Reopen(); /></td>
                                    <td class="td_r_text" width="69%">Reopen</td>
                              </tr>
                          </table>                  
                      </td>
                </tr>
                <?php } ?>
                
                <tr id='rerem' style='display:none'>
                      <td class="label" valign='top' id='trReopen'>Reopen Remarks</td>
                      <td colspan='3' class="value"><textarea name='txtReopen_Descr' cols='40' rows='3' maxlength='200' id='txtReopen_Descr' 
                          onkeyup=lengthcheck(200,'#divReopen','#txtReopen_Descr');><?php echo $txtReopen_Descr ?></textarea> 
                          <div id='divReopen' style='color:#FF0000'>     
                      </td>
                </tr>
                
                <?php if($Status > 1 ) { ?>
                <tr>
                    <td class="label">Response Time</td>
                    <td class="value"><?php echo $Response_Time; ?></td>
                   
                    <td class="label">Closed Time</td>
                    <td class="value"><?php echo $Closed_Time; ?></td>
                </tr>
                
                <tr>
                  <td class="label" valign="top">Prev.Remarks</td>
                  <td class="value" colspan="3"><?php echo $PrevRemarks; ?></td>
                </tr>
                
                <?php } if($Status==1 || $Status==6) { ?>
                <tr>
                      <td colspan="4" class="value">
                          <table width="100%" border="0" cellpadding="5" cellspacing="1">
                              <tbody>
                                <tr>
                                  <?php  if( $Viewmode==2){?>
                                  <td width="3%"><input style="width:15px; height:15px;" name="Terms" id="Terms" type="checkbox" value="1" /></td>
                                  <td width="97%" class="td_r_text">I agree this Ticket</td>
                                  <?php }?>
                                </tr>
                              </tbody>
                           </table>
                      </td>
                </tr>
                
                <?php }?>
                <tr align="right">
                    <td colspan="4">
                        <table border="0" cellpadding="1" cellspacing="1" align="right">
                            <tr>
                                <td>
                                `	<?php if($Viewmode==2 and $Status<>4 and $Status<>5 and $Status<>7){ ?>
                                    <input type="submit" name="Save" id="Save" value="Save" class='winbutton_go'/>                                
                                </td>
                                <td>
                                    <?php } if(($Viewmode == 1 and $Status==5) or $Ticket_Id==0){?>
                                    <input type="submit" name="Save" id="Save" value="Save" class='winbutton_go' />
                                    <?php   } ?>                                
                                </td>
                                <td>
                                    <?php   if($Ticket_Id > 0) { ?>
                                    <input type="submit" name="Close" value="Close" class="winbutton_go" onclick="window.close(); window.opener.location.reload();" />                                </td>
                                <td>
                                    <?php } if($Ticket_Id == 0){ ?>
                                    <input type='button' name='Reset' value='Reset' class='winbutton_go' onclick="Clear();" />
                                    <?php }?>                                
                                </td>
                            </tr>
                        </table>                    
                   </td>
                </tr>
          </table>
		 
</form>
<?php } if($errflag==1 ){echo "<script>showtask(". $Ticket_Type.")</script>" ;} ?>
</body>
</html>
<?php if($Ticket_Id == 0) include("../includes/copyright.php") ?>
