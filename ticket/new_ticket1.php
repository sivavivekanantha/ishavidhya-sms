<?php session_start(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php if($_GET['frm']==1 or $_POST['frm']==1) {
		include("../Includes/header1.php"); 
		$frm = 1;
      if($_GET['Ticket_Mode']>0 )  $Ticket_Mode = $_GET['Ticket_Mode'];
		}
	else {
		include("../Includes/header.php"); 
		title('Ticketing System','New Ticket',5,2,0); 	}	?>
<script language="javascript" type="text/javascript">
function getCity(obj)
{	
	if(obj=='') return false;
    url = "../includes/ajax.php?listcode=2&School_Id="+obj;
	$('#divAssignto').load(url);
}
 function showtask(obj)
{	
	if(obj==1) {
		$("#Task_Grp").show();
		$("#Issue_grp").hide();
		}
	else {
		$("#Task_Grp").hide();
		$("#Issue_grp").show();
		}
}
function Reopen()
{ 
	if ($('#chk_reopen').attr('checked')) 
	 	{
		$('#rerem').show();
		$('#Save').show();
		}
	else if($('#chk_closed').attr('checked')) 
	 	{
		$('#Save').show();
		$('#rerem').hide();
		}
	else
		{
		$('#rerem').hide();
		$('#Save').hide();
		}
}

function Clear()
{	
//alert('dfdfdfd');
	$('#rname').val("");
	$('#rmobile').val("");
	$('#remail').val("");
	$('#ddlTicket_type').val("");
	$('#ddlPriority').val("");
	$('#ddlSchool').val("");
	$('#ddlAssignto').val("Select");
	$('#ddldepartmnet').val("");
	$('#txtPlace').val("");
	$('#txtDescription').val("");
	$('#divmsg').hide();
	url = "../includes/ajax.php?listcode=2&School_Id="+'';
	$('#divAssignto').load(url);
}
</script>
<?php 
	$errmsg="";
	$errflag=0;
	$dummy=0;
	$Ticket_Id=0;

	if($_GET['Ticket_Id']>0) 
		$Ticket_Id=$_GET['Ticket_Id'];

	if($_POST['Save'] =='Save')	
	{		
	 	$rname				=	Trim($_POST['rname']);
	 	$rmobile			=	Trim($_POST['rmobile']);
	 	$remail				=	Trim($_POST['remail']);
		$Ticket_Type		=	$_POST['ddlTicket_type'];			
		$Priority			=	$_POST['ddlPriority'];				
		$School_Id			=	$_POST['ddlSchool'];
		$Assign_To			=	$_POST['ddlAssignto'];		
		$Raised_To			=	$_POST['ddldepartmnet'];				
		$Place				=	Trim($_POST['txtPlace']);				
		$Description 		=	Trim($_POST['txtDescription']);	
		//echo"Description=".Trim($_POST['txtDescription']);
		$Ticket_Id 			=	$_POST['Ticket_Id'];
        $Ticket_Mode          =    $_POST['Ticket_Mode'];
		$Remarks			=	Trim($_POST['Remarks']);
		$PRemarks			=	$_POST['hdnRemarks'];

		$Status				=	$_POST['ddlStatus'];    // Current Status
		$Status1			=	$_POST['Status']; 		// Click ReOpen 
		$Reopen_Descr		=	Trim($_POST['txtReopen_Descr']);	//ReOPen Ticket Description
        $Terms				=   $_POST['Terms']; 
		
		if($Ticket_Id==0)
		{
		 	$dummy = Strcheck($rname,$errmsg,$errflag,"Name");
		 	$dummy = Strcheck($rmobile,$errmsg,$errflag,"Mobile");
		 	if(strlen($remail)>0)
			 	$dummy = Emailcheck($remail,$errmsg,$errflag,"Email Id");
		 	
			$dummy = Zerocheck($Ticket_Type,$errmsg,$errflag,"Ticket Type");
			$dummy = Zerocheck($Priority,$errmsg,$errflag,"Priority");
			if($Ticket_Type==1) {
				$dummy = Zerocheck($School_Id,$errmsg,$errflag,"School Id");
				$dummy = Zerocheck($Assign_To,$errmsg,$errflag,"Assign To");
			} elseif($Ticket_Type==2) {
				$dummy = Zerocheck($Raised_To,$errmsg,$errflag,"Raised To");
			}
			//$dummy = Strcheck($Place,$errmsg,$errflag,"Place");
			$dummy = Strcheck($Description,$errmsg,$errflag,"Description");
	 	}	
        else 
        {
		 	if($Status == 3 or $Status ==4 ) $dummy = Strcheck($Remarks,$errmsg,$errflag,"Remarks");
			if($Status1 == 6 )
			{ 
				$dummy = Strcheck($Reopen_Descr,$errmsg,$errflag,"Remarks");
			}
			
			if($Status == 1 or $Status == 6 )
			{ 
			$dummy = Strcheck($Terms,$errmsg,$errflag,"Accept ticket");
			}
		}
		
	 	if($errflag==0)
		{
			if(strlen($PRemarks) > 0 )
				$Remarks  = $Remarks."<br>".$PRemarks;
			
			if($Status1 > 0 ) {
				$Remarks  = 	$Reopen_Descr."<br>".$Remarks;
				$Status = $Status1;
			}
			
	 		if($Ticket_Id==0) 		{
				$query=mssql_init('Sp_New_Ticket_Save',$mssql);
				mssql_bind($query,'@Rname',$rname,SQLVARCHAR,false,false,50);
				mssql_bind($query,'@Rmobile',$rmobile,SQLVARCHAR,false,false,25);
				mssql_bind($query,'@Remail',$remail,SQLVARCHAR,false,false,100);
				mssql_bind($query,'@Ticket_Type',$Ticket_Type,SQLINT4,false,false,5);
				mssql_bind($query,'@Priority',$Priority,SQLINT4,false,false,5);
				mssql_bind($query,'@School_Id',$School_Id,SQLINT4,false,false,5);
				mssql_bind($query,'@Assign_To',$Assign_To,SQLINT4,false,false,5);
				mssql_bind($query,'@Raised_to',$Raised_To,SQLINT4,false,false,5);
				mssql_bind($query,'@Place',$Place,SQLVARCHAR,false,false,50);
				mssql_bind($query,'@Description',$Description,SQLTEXT,false,false,strlen($Description));
				mssql_bind($query,'@Userid',$_SESSION["UserID"],SQLINT4,false,false,5);
				mssql_bind($query,'@Email',$email,SQLVARCHAR,true,false,strlen($email));
			}
			elseif($Ticket_Id>0)
			{
				$query=mssql_init('Sp_New_Ticket_Update',$mssql);
				mssql_bind($query,'@Ticket_Id',$Ticket_Id,SQLINT4,false,false,5);
				mssql_bind($query,'@Status',$Status,SQLINT4,false,false,5);
				mssql_bind($query,'@Remarks',$Remarks,SQLVARCHAR,false,false,20);
			}
			$result = @mssql_execute($query);
			mssql_free_statement($query);
						  
			if($result==1)
			{  
				if($Ticket_Id==0)
				{
					 echo "<p class='mesg'>Your Ticket has been Registered Successfully.</p>";
					
						//		MAIL SENT CODES
						$submail = "New Ticket Raised In Ishavidhya";
						if($Ticket_Type==1) $Ttype = "Task"; Else $Ttype = "Issue";
						$mbody = "Namaskaram,<br><br><br>\tThank you for contacting us.<br><br>\t<br>\t".$Ttype." Description :" .$Description. "<br>\t Raised by: " . $rname . "<br><br>\t ".$rname." Mobile No: " . $rmobile . " <br><br><br>\t A ticket request has been created and a respective department staff will look at the problem and take necessary action. <br><br>Pranam<br>ISHAVIDHYA TEAM<br><br><br>This is a system generated mail. Please do not reply to this email ID. If you have a query or need any clarification please contact Administrator" ;
						$email = "sivavivekanantha@gmail.com";
						mail($email, $submail ,$mbody,  
						"Reply-To: sivavivekanantha@yahoo.com\n" .  
						"From:sivavivekanantha@yahoo.com\n" .  
						"MIME-Version: 1.0\n" .  
						"Content-type: text/html; charset=iso-8859-1"); 
						
						$mbody=""; 
	
					echo "<p align='center'><a href='New_Ticket.php'>Goto NewTicket</a> &nbsp;&nbsp;&nbsp; <a href='Ticket_Status.php'>Goto Ticket Status</a></p>";
					$skpform = 1;
				}
				else if($Ticket_Id>0)
				{
				echo "<p class='mesg'>Ticket Status has been Updated Successfully.</p><br>";
				echo "<p align='center'><a href='' onclick='window.close(); window.opener.location.reload();' >Colse Window</a></p>";
				$skpform = 1;
				}				
			
			}
			else
			{
				$errmsg1=mssql_get_last_message();			
				$errflag=2;
			}
		}		
		if($errflag==1) 
			echo "<p class='error'>Incomplete / Invalid entries for<br>".$errmsg;
		elseif($errflag==2)
			echo "<p class='error'>".$errmsg1;
				
	}	?>
<body>   
<?php 

	if($Ticket_Id > 0 )
	{
		$Query=mssql_init('Sp_GetTicket_Status',$mssql);
		mssql_bind($Query,'@Ticket_Id',$Ticket_Id,SQLINT4,false,false,5);
		mssql_bind($Query,'@Ticket_Type',$Ticket_Type,SQLINT4,false,false,20);
        mssql_bind($Query,'@Ticket_Mode',$Ticket_Mode,SQLINT4,false,false,20);
        mssql_bind($Query,'@UserId',$_SESSION['UserID'],SQLINT4,false,false,20);
        mssql_bind($Query,'@DeptCode',$_SESSION['DeptCode'],SQLINT4,false,false,20);
        mssql_bind($Query,'@CatCode',$_SESSION['CatCode'],SQLINT4,false,false,20);
        mssql_bind($Query,'@SchoolID',$_SESSION['SchoolId'],SQLINT4,false,false,20);
		$Tresult= mssql_execute($Query);
		mssql_free_statement($Query);
		while($field=mssql_fetch_array($Tresult)) 
		{
		 	$rname				=	$field['Name'];
		 	$rmobile			=	$field['Mobile'];
		 	$remail				=	$field['Email'];
			$Ticket_Type		=	$field['Ticket_Type'] ;
			$Priority			=	$field['Priority'] ;	
			$School_Name		=	$field['School_Name'] ;
            $School_Id          =    $field['School_Id'] ;
			$Assign_To			=	$field['Teacher_Name']	;	
			$Raised_To			=	$field['Department'];				
			$Place				=	$field['Place'];				
			$Description 		=	$field['Description'];	
			$Status 			=	$field['Status'];	
			$Mobile_No 			=	$field['Mobile_No'];
			$Username 			=	$field['Teacher_Name'];	
			$Reopen_Remarks     = 	$field['Reopen_Remarks'];	
			$PrevRemarks     	= 	$field['Remarks'];
            $Created_By         =   $field['Created_By'];
               
            if($Status==1) $Show_Status="OPEN";
            else if($Status==2) $Show_Status="IN-PROGRESS";
            else if($Status==3) $Show_Status="HOLD";
            else if($Status==4) $Show_Status="IGNORE";
            else if($Status==5) $Show_Status="FIXED";
            else if($Status==6) $Show_Status="CLOSED";
            else if($Status==7) $Show_Status="RE-OPEN";
		} 
	}	
if($skpform <> 1) { ?>
<table width="100%" border="0" align="center" cellpadding="5" cellspacing="2">
  <tr>
  
  <td align="center" scope="col">
  
  
  <form name="myform" action="New_Ticket.php" id="myform" method="post">
    <input type="hidden" name="frm"  value=<?php echo $frm?>/>
    <input type="hidden" name="Ticket_Id"  value=<?php echo $Ticket_Id?>/>
    <input type="hidden" name="Ticket_Mode"  value=<?php echo $Ticket_Mode?>/>
    <input type="hidden" name="hdnRemarks"  value="<?php echo $PrevRemarks?>" />
    <div id="scholarshipfr" style="width:700px;"  align="center">
    <table width="70%" border="0" align="center" cellpadding="5" cellspacing="3" >
      <tr>
        <?php if($Ticket_Id == 0) echo "<td align='right' colspan='4' ><span class='mand'> *</span> mandatory fields";
                else echo "<td align='left' colspan='4' ><img src='../images/arrow_skip.png' width='16' height='16' /><span class='view_tit_text'>Ticket Status</span>"?>
      </td>
      
      </tr>
      <tr>
        <td valign="top" ><table width="627" border="0"  align="center" cellpadding="8" cellspacing="2">
            <tr>
              <td width="20%" class="td_lable">Name
                <?php if($Ticket_Id == 0) echo "<span class='mand'> *</span>"?></td>
              <td width="30%" colspan="5" class="td_r_text"><?php if($Ticket_Id > 0) echo $rname; else {?>
                <label>
                <input type="text" name="rname" id="rname" maxlength="30" onkeydown="return alphaonly('rname')" value="<?php echo $rname ?>" />
                </label>
                <?php }?></td>
              <td width="115" class="td_lable">Mobile
                <?php if($Ticket_Id == 0) echo "<span class='mand'> *</span>"?></td>
              <td width="201" class="td_r_text"><?php if($Ticket_Id > 0) echo $rmobile; else { ?>
                <input type="textbox" name="rmobile" id="rmobile" maxlength="13" onkeydown="return numberonly('rmobile')" value="<?php echo $rmobile ?>" />
                <?php } ?>
              </td>
            </tr>
            <tr>
              <td class="td_lable">Email Id</td>
              <td colspan="7" class="td_r_text"><?php if($Ticket_Id > 0) echo $remail; else {?>
                <input type="textbox" name="remail" id="remail" value="<?php echo $remail ?>" />
                <?php } ?></td>
            </tr>
            <tr>
              <td class="td_lable">Ticket Type
                <?php if($Ticket_Id == 0) echo "<span class='mand'> *</span>"?></td>
              <td colspan="5" class="td_r_text"><?php  if($Ticket_Id > 0) { if($Ticket_Type==1) echo "Task"; else  echo "Issue";  }
										else { ?>
                <select name="ddlTicket_type" id="ddlTicket_type"  onchange="showtask(this.value)">
                <?php if($_SESSION['CatCode'] < 3 ) {?>
                    <option selected="selected" value="0">Select</option>                      
                  <option value="1" <?php if($Ticket_Type==1) echo "Selected"; ?>>Task</option>
                <?php } ?>
                  <option value="2" <?php if($Ticket_Type==2) echo "Selected"; ?>>Issue</option>
                </select>
                <?php } ?>
              </td>
              <td class="td_lable">Priority
                <?php if($Ticket_Id == 0) echo "<span class='mand'> *</span>"?></td>
              <td class="td_r_text"><?php if($Ticket_Id > 0) { if($Priority==1) echo "Low" ; if($Priority==2) echo "Medium" ; if($Priority==3) echo "High"; }  else { ?>
                <select name="ddlPriority" id="ddlPriority">
                  <option selected="selected"  value="0">Select</option>
                  <option value="1"  <?php if($Priority==1) echo "Selected"; ?>>Low</option>
                  <option value="2" <?php if($Priority==2) echo "Selected"; ?>>Medium</option>
                  <option value="3" <?php if($Priority==3) echo "Selected"; ?>>High</option>
                </select>
                <?php } ?>
              </td>
            </tr>
            <tr id="Task_Grp" <?php if($Ticket_Type==2 or $Ticket_Type==0) echo "style=display:none"; ?> >
              <td class="td_lable">School
                <?php if($Ticket_Id == 0) echo "<span class='mand'> *</span>"?></td>
              <td colspan="5" class="td_r_text"><?php if($Ticket_Id > 0) echo $School_Name;  else { 
							 		//SHOW SCHOOL DROPDOWN
										$query = mssql_init('sp_GetSchool',$mssql);
										$result = mssql_execute($query);
										mssql_free_statement($query);	?>
                <select id="ddlSchool" name="ddlSchool" onchange="getCity(this.value)">
                  <option  value="0">Select</option>
                  <?php while($field = mssql_fetch_array($result))    {  ?>
                  <option value="<?php echo $field['School_Id']?>" <?php if($School_Id == $field['School_Id']) echo "Selected"; ?>> <?php echo $field['School_Name']?>
                  </option>
                  <?php } ?>
                </select>
                <?php } ?>
              </td>
              <td class="td_lable">Assign To
                <?php if($Ticket_Id == 0) echo "<span class='mand'> *</span>"?></td>
              <td class="td_r_text"><?php if($Ticket_Id > 0) echo $Assign_To;  else {
			    if($errflag== 0 ) { ?>
                <div id="divAssignto">
                  <select name="ddlAssignto" id="ddlAssignto" ><option  value="0">Select</option></select>
                </div>
                <?php } 
                else { 
					$query = mssql_init('Get_Teachername',$mssql);
					mssql_bind($query,'@School_Id',$School_Id,SQLINT4,false,false,5);
					$result = @mssql_execute($query);
					mssql_free_statement($query);   ?>
                    <select name="ddlAssignto" id="ddlAssignto">
                    <option  value="0">Select</option>
                        <?php while($row=mssql_fetch_array($result)) { ?>
                            <option value="<?=$row['Teacher_Code']?>"  <?php if($Assign_To == $row['Teacher_Code']) echo "Selected"; ?>><?=$row['Teacher_Name']?>
                            </option>
                        <?php } ?>
                    </select>
                <?php 	}     
							} ?>
              </td>
            </tr>
            <tr id="Issue_grp" <?php if($Ticket_Type==1) echo "style=display:none"; ?> >
              <td class="td_lable">Raised To
                <?php if($Ticket_Id == 0) echo "<span class='mand'> *</span>"?></td>
              <td colspan="5" valign="top" class="td_r_text"><?php if($Ticket_Id > 0) echo $Raised_To;  
							else {
							 	$query = mssql_init('sp_GetDepartment',$mssql);
								$result = mssql_execute($query);
								mssql_free_statement($query);	?>
                <select id="ddldepartmnet" name="ddldepartmnet">
                  <option  value="0">Select</option>
                  <?php while($field=mssql_fetch_array($result))	{	
                      if($field['Department_Code'] <> $_SESSION['DeptCode'] ) {?>
                  <option value="<?php echo $field['Department_Code'] ?>" <?php if($Raised_To == $field['Department_Code']) echo "Selected"; ?>> <?php echo $field['Department'] ?> </option>
                  <?php } }?>
                </select>
                <?php  } ?>
              </td>
              <td class="td_lable">Place</td>
              <td valign="top" class="td_r_text"><?php if($Ticket_Id > 0) echo $Place;  
							else { ?>
                <div align="left">
                  <input name="txtPlace" type="text" id="txtPlace" maxlength="50" onkeydown="return alphanumeric('txtPlace')" value="<?php echo $Place ?>" />
                </div>
                <?php } ?></td>
            </tr>
            <tr>
              <td class="td_lable">Description
                <?php if($Ticket_Id == 0) echo "<span class='mand'> *</span>"?></td>
              <td colspan="7" class="td_r_text"><?php if($Ticket_Id > 0) echo $Description;  
							else { ?>
                <textarea name="txtDescription" cols="58" rows="4" maxlength="500" id="txtDescription"  onkeyup="lengthcheck(500,'#divmsg','#txtDescription');"><?php echo $Description ?></textarea>
                <div id="divmsg" style="color:#FF0000"></div></td>
              <?php } ?>
            </tr>
            <?php   
                if($Ticket_Id > 0 and ( $Status!=5 and $Status!=4 and $Status!=7 ))	
			        { 
                        echo " <tr><td class='td_lable' >Status </td> ";
                        if($Ticket_Mode==1 ) { ?>
                        <td class="td_r_text"><?php echo $Show_Status; ?></td>
                        <?php }
						else if($Ticket_Mode==2 and $Status!=5 && $Status!=4 )  { ?>
                         <td class="td_r_text"><select id='ddlStatus' name='ddlStatus' onchange='getCity(this.value)'>
                  <?php  if( $Status==1 || $Status==6) { ?>
                  <option value='1' <?php if($Status==1) echo 'Selected'; ?> >Open</option>
                  <option value='3' <?php  if($Status==3) echo 'Selected'; ?> >Hold</option>
                  <option value='4' <?php  if($Status==4)  echo 'Selected'; ?>  >Ignore</option>
                  <?php   }	
						if($Ticket_Mode==2 and $Status==2) { ?>
                  <option value='2' <?php if($Status==2) echo 'Selected';?> >In Progress</option>
                  <option value='3' <?php  if($Status==3) echo 'Selected'; ?> >Hold</option>
                  <option value='4' <?php  if($Status==4) echo 'Selected'; ?>  >Ignore</option>
                  <?php   	}	
				        if($Ticket_Mode==2 and $Status==3) { ?>
                  <option value='3' <?php  if($Status==3) echo 'Selected'; ?> >Hold</option>
                  <option value='4' <?php  if($Status==4) echo 'Selected'; ?> >Ignore</option>
                  <?php }	?>
                  <option value='5'<?php  if($Status==5) echo 'Selected'; ?>  >Fixed</option>
                </select></td>

              <td colspan="7" class="td_r_text"></td>
            </tr>
            <tr>
              <td valign="top" class="td_lable">Remarks</td>
              <td colspan="7" class="td_r_text"><textarea name='Remarks' cols='40' rows='3' maxlength='500' id='txtRemarks' onkeyup=lengthcheck(500,'#divRemarks','#txtRemarks')><?php echo $Remarks?></textarea>
              <div id='divRemarks' style='color:#FF0000'></div></td>
            </tr>
            <?php  }  }
            
            //if($Status==5 and $Ticket_Mode=1 and ($Ticket_Type=1 and $_SESSION['SchoolId']<>$School_Id and $_SESSION['UserID']<>$Assign_To and  $_SESSION['UserID']<>$Raised_To) 
              //              or ($Ticket_Type=2 and $_SESSION['UserID']<>$Assign_To and  $_SESSION['UserID']<>$Raised_To)) { 
            if($Status==5 and $Ticket_Mode ==1) { ?>
            <tr >
              <td class="td_lable">&nbsp;</td>
              <td class="td_r_text" colspan="7"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr >
                    <td width="4%"><input name='Status' id='chk_closed' type='radio' value='7'; onclick=Reopen(); />
                    </td>
                    <td class="td_r_text" width="9%">Closed</td>
                    <td width="4%"><input name='Status' id='chk_reopen' type='radio' value='6'; onclick=Reopen(); />
                    </td>
                    <td class="td_r_text" width="69%">Reopen</td>
                    <td width="7%">&nbsp;</td>
                    <td width="7%">&nbsp;</td>
                  </tr>
                </table></td>
            </tr>
            <?php } ?>
            <tr id='rerem' style='display:none'>
              <td valign='top' id='trReopen' class="td_lable">Reopen Remarks</td>
              <td colspan='7' class="td_r_text"><textarea name='txtReopen_Descr' cols='40' rows='3' maxlength='500' id='txtReopen_Descr'  value='<?php echo $txtReopen_Descr ?>'   onkeyup=lengthcheck(500,'#divReopen','#txtReopen_Descr');></textarea></td>
            </tr>
            <?php 
						if($Status > 1 ) { ?>
            <tr>
              <td valign="top" class="td_lable">Prev.Remarks</td>
              <td colspan="7" class="td_r_text"><?php echo $PrevRemarks; ?></td>
            </tr>
            <?php } if($Status==1 || $Status==6) {	?>
            <tr class="td_r_text" >
              <td colspan="8" class="td_r_text"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tbody>
                    <tr>
                    <?php  if( $Ticket_Mode==2){?>
                      <td width="3%"><input style="width:15px; height:15px;" name="Terms" id="Terms" type="checkbox" value="1" /></td>
                      <td width="97%" class="td_r_text">I accept
                        <?php if($Status==1) echo "this Ticket"; 
	else if($Status==6) echo " to Reopen the Ticket";?>
                      </td><?php }?>
                    </tr> 
                  </tbody>
                </table></td>
            </tr>
            <?php }?>
            <tr class="td_r_text" align="center" >
              <td class="td_r_text" colspan="8" align="center" ><table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="43%">&nbsp;</td>
                    <td width="29%"><table width="%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                         <?php  if($Ticket_Mode<>1 and $Status<>4 and $Status<>5 and $Status<>7){ ?>
                            <td width="5%" align="right"><input type="submit" name="Save" id="Save" value="Save" class='winbutton_go' />
                            </td> <?php   }   
                          if($Ticket_Mode== 1 and $Status==5) {?>
                             <td width="5%" align="right"><input type="submit" name="Save" id="Save" value="Save" class='winbutton_go' />
                            </td><?php } ?>
                          <td><?php if($Ticket_Id > 0) { ?>
                            &nbsp;
                            <input type="submit" name="Close" value="Close" class="winbutton_go" onclick="window.close(); window.opener.location.reload();" />
                            <?php }?>
                            <?php if($Ticket_Id == 0){?>
                            &nbsp;
                            <input type='button' name='Reset' value='Reset' class='winbutton_go' onclick="Clear();" />
                            <?php }?>
                          </td>
                        </tr>
                      </table></td>
                    <td width="28%">&nbsp;</td>
                  </tr>
                </table></td>
            </tr>
          </table></td>
      </tr>
    </table>
  </form>
  </div>
  </td>
  
  <td></td>
  </tr>
</table>
<?php } 
if($errflag==1 ) echo "<script>showtask(". $Ticket_Type.")</script>"?>
</body>
</html>
<?php if($Ticket_Id == 0)include("../includes/copyright.php") ?>
