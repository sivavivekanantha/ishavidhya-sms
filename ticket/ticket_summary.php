<?php session_start();?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php 
  	include("../includes/header.php");	
		title('Ticketing System','Ticket Summary',5,2,0); 
        $FromDate=date("01-m-Y");
        $ToDate=date("d-m-Y");
             
        if($_POST['GO']=="Go")
        {
            $FromDate=trim($_POST['FromDate']);
            $ToDate=trim($_POST['ToDate']);
            $BasedOn=trim($_POST['BasedOn']);
            $dummy = DateCheck($FromDate,$ToDate,$errmsg,$errflag,"To Date must be greater than or equal to From Date");
            if($errflag==1)
                echo $errlbl.$errmsg;
        }  ?>
<body>
	<form name="summeryreport" id="summeryreport" action="ticket_summary.php" method="post">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
  	<tr>
    	<td>
    		<table width="70%" border="0" cellspacing="2" cellpadding="0" align="center" style="border:dotted #CCCCCC;">
  <tr>
    <td>From Date</td>
            <td><a href="javascript:NewCal('FromDate','ddmmyyyy')">
            <input type="text" name="FromDate" id="FromDate" readonly="readonly"  size="20" maxlength="10" 
            value="<?php echo $FromDate; ?>" />
                    </a></td>
    <td>ToDate</td>
        <td><a href="javascript:NewCal('ToDate','ddmmyyyy')">
            <input type="text" name="ToDate" id="ToDate" readonly="readonly"  size="20" maxlength="10" 
            value="<?php echo $ToDate; ?>" />
                </a></td>
    <td>Based On</td>
        <td><label>
          <select name="BasedOn" id="BasedOn">       
            <option value="2"<?php if($BasedOn==2)  echo "selected" ?>>Task</option>
            <option value="1"<?php if($BasedOn==1)  echo "selected" ?>>Issue</option>
          </select>
        </label></td>
          <td>
      <input type="submit" name="GO" value="Go" class='winbutton_go' />
    </td>
  </tr>
	
</table>
<br />
<?php if($errflag==0) { ?>
<table width="70%" border="0" cellspacing="1" cellpadding="3" align="center" style="border:dashed thin #CCCCCC;">
<?php if($BasedOn==1){   ?>
<tr>
  
    <th width="5%">S.No</th>
    <th width="17%">Deparment</th>
    <th width="12%">Total Ticket</th>
    <th width="12%">Closed</th><th width="12%">Ignored</th>
    <th width="12%">Pending</th>
    
 <?php } if($BasedOn==2){ ?>
    
    <th width="5%">S.No</th>
    <th width="12%">Name</th>
    <th width="12%">Type</th>
    <th width="12%">Total Ticket</th>
    <th width="12%">Closed</th><th width="12%">Ignored</th>
    <th width="12%">Pending</th>
        
 <?php }
        ?> 
  </tr>
	<?php
  //SHOW Ritual DROPDOWN
      
        if($_POST['GO']=="Go")   
        {
		 mssql_free_result($result);
        $query = mssql_init('sp_Ticket_summeryreport', $mssql);
       	if(strlen($FromDate)==10)  
   		$FromDate = date('Y-m-d', strtotime($FromDate));
        mssql_bind($query,'@Fdate',$FromDate,SQLVARCHAR,false,false,50);
        if(strlen($ToDate)==10)  
  		$ToDate = date('Y-m-d', strtotime($ToDate)); 
        mssql_bind($query,'@Tdate',$ToDate,SQLVARCHAR,false,false,50);
        mssql_bind($query,'@BasedOn',$BasedOn,SQLINT4,false,false,5);
		$result =mssql_execute($query);
        $rs_cnt=mssql_num_rows($result);
		mssql_free_statement($query); 
        $colorflag = 0;
        $i = 0;    
        if($rs_cnt<>0) {
        while($field = mssql_fetch_array($result))
		 {
	       $i += 1;
            $colorflag += 1;?>
         <tr class=<?php if($colorflag%2==0) 
            { echo "row1"; 
                        } else { echo "row2"; } ?> valign="center">
         <?php if($BasedOn==1){ ?>
			<td align="center"><?php echo $i; ?></td>
			<td align="center"><?php echo $field['Department']; ?></td>
			<td align="center"><?php echo $field['Raisedto']; ?></td>
			<td align="center"><?php echo $field['Closed']; ?></td>
			<td align="center"><?php echo $field['Ignored']; ?></td>
			<?php $pending= ($field['Raisedto']-($field['Closed']+$field['Ignored']))?>
			<td align="center"><?php echo $pending; ?></td>
		<?php } else { ?>            
			<td align="center"><?php echo $i; ?></td>
			<td align="center"><?php echo $field['Teacher_Name']; ?></td>
			<td align="center"><?php echo $field['Category']; ?></td>
			<td align="center"><?php echo $field['Raisedto']; ?></td>
			<td align="center"><?php echo $field['Closed']; ?></td>
			<td align="center"><?php echo $field['Ignored']; ?></td>
			<?php $pending= ($field['Raisedto']-($field['Closed']+$field['Ignored']));?>
			<td align="center"><?php echo $pending; ?></td>
            <?php } ?>      
                </tr>
                <?php }  } 
                
                  else
                        {?>
               <tr>
               <td align="center" colspan="5">
                        <p class='mesg'>No Records Found</p>                  
                <?php }?>
				</td>
				</tr> 
                
                        <?php }  ?>          
                </table> <?php }  ?>          
    	</td>
            </tr>
                </table>
	</form>
</body>
</html>
<?php if($Ticket_Id == 0) include("../includes/copyright.php") ?>
