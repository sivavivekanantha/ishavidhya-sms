<?php session_start(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php include("../Includes/header.php");
title('Student Management','Report Card',2,1,2); ?>
<head>
<script>
function showtitle(obj,obj1) {
    var obj2 = "#"+obj;
    showtitle1(obj2,obj1);
}
function showtitle1(obj2,obj1)
{ 
    $(obj2).poshytip({
    content: obj1,
	className: 'tip-twitter',
    showOn: 'focus',
	alignTo: 'target',
	alignX: 'center',
	offsetY: 5,
	allowTipHover: false,
	fade: true,
	slide: false
    })
}

function editval(val)
{	

	if($('#editcnt').val() >0 ) return false;
    $('#editcnt').val(val);

	if($('#editcnt').val()!="") {
	 var el = $('#editcnt').val()+","+val;
	 $('#editcnt').val(el);

	}
	else { 
		$('#editcnt').val(val)
		}
}
//Mark Validation Mark should be less than or equal to 100
//Mark Above 100 Means Error Message Will Be Displayed
function markcheck(obj)
{
 	if($(obj).val() >100 )
	{
		alert('Mark should be less than or equal to 100');
		//$('#Save').attr("disabled", true);
		$(obj).val('');
	}
	
}
</script>
</head>
<?php
			  $errmsg="";
			  $errflag=0;
			  $dummy=0;
					
			  if($_POST['Save'] =='Save')	
              { 		 
				   $Location        =   Trim($_POST['Location']);
                   $Class           =   Trim($_POST['Class']);
				
				   $Section         =   Trim($_POST['Section']);
				   $Admission_No	=	Trim($_POST['Admission_No']);
				   $Term_Code	    =	Trim($_POST['Term2']);
                   $Term_Date       =   Trim($_POST['Term_Date']);
				   $WorkingDays  	=   Trim($_POST['Working_Days']);
                   $TeacherName  	=   Trim($_POST['TeacherName']);
                   $Period_Code     =   Trim($_POST['Period_Code']);
                   $School_Id	    =   1;
				  $scount= $_POST['scount'];
			  //More Than One Record Saved
			  for($cnt=1;$cnt<=$scount;$cnt=$cnt+1) 
			  {
				  $Ass_Header_Code    = Trim($_POST['Ass_Header_Code'.$cnt]);
                  $Student_Id         = Trim($_POST['Student_Id'.$cnt]);
                  
				  $Spoken_English     = Trim($_POST['Spoken_English'.$cnt]);
				  $Writing_Neatness	  = Trim($_POST['Writing_Neatness'.$cnt]);
				  $Enthusiam_Energy	  = Trim($_POST['Enthusiam_Energy'.$cnt]);	
				  $Responsibilty      = Trim($_POST['Responsibilty'.$cnt]);
				  $Dicipline      	  = Trim($_POST['Dicipline'.$cnt]);
				  $Attention_Span  	  = Trim($_POST['Attention_Span'.$cnt]);
				  $Punctuality        = Trim($_POST['Punctuality'.$cnt]);
				  $Social_Interaction = Trim($_POST['Social_Interaction'.$cnt]);
				  $Hygiene	          = Trim($_POST['Hygiene'.$cnt]);
				  $Group_Work         = Trim($_POST['Group_Work'.$cnt]);
				  $Sports_Games   	  = Trim($_POST['Sports_Games'.$cnt]);
				  $Art_Craft      	  = Trim($_POST['Art_Craft'.$cnt]);
				  $Computer_Skills    = Trim($_POST['Computer_Skills'.$cnt]);
				  $Projects	          = Trim($_POST['Projects'.$cnt]);
				  $Participation	  = Trim($_POST['Participation'.$cnt]);
				  $Fine_Motor_Skills  = Trim($_POST['Fine_Motor_Skills'.$cnt]);
			      $Gross_Motor_Skills = Trim($_POST['Gross_Motor_Skills'.$cnt]);
		     	  $Noday_Attendance	  = Trim($_POST['Noday_Attendance'.$cnt]);
				  $CommentCode	      = Trim($_POST['EnglishComment_1'.$cnt]);
              	  $English_Classwrk	  = Trim($_POST['English_Classwrk'.$cnt]);
				  $English_Examgrade  = Trim($_POST['English_Examgrade'.$cnt]);
				  $English_Totalgrade = Trim($_POST['English_Totalgrade'.$cnt]);
				  $Tamil_Classwrk 	  = Trim($_POST['Tamil_Classwrk'.$cnt]);
				  $Tamil_Examgrade    = Trim($_POST['Tamil_Examgrade'.$cnt]);
				  $Tamil_Totalgrade	  = Trim($_POST['Tamil_Totalgrade'.$cnt]);
				  $Maths_Classwrk	  = Trim($_POST['Maths_Classwrk'.$cnt]);
				  $Maths_Examgrade    = Trim($_POST['Maths_Examgrade'.$cnt]);
				  $Maths_Totalgrade	  = Trim($_POST['Maths_Totalgrade'.$cnt]);
				  $Science_Classwrk	  = Trim($_POST['Science_Classwrk'.$cnt]);
				  $Science_Examgrade  = Trim($_POST['Science_Examgrade'.$cnt]);
				  $Science_Totalgrade = Trim($_POST['Science_Totalgrade'.$cnt]);
				  $Sscience_Classwrk  = Trim($_POST['Sscience_Classwrk'.$cnt]);
				  $Sscience_Examgrade = Trim($_POST['Sscience_Examgrade'.$cnt]);
				  $Sscience_Totalgrade= Trim($_POST['Sscience_Totalgrade'.$cnt]);
				  $Computer_Classwrk  = Trim($_POST['Computer_Classwrk'.$cnt]);
				  $Computer_Examgrade = Trim($_POST['Computer_Examgrade'.$cnt]);
				  $Computer_Totalgrade= Trim($_POST['Computer_Totalgrade'.$cnt]);
				  $Project_Classwrk	  = Trim($_POST['Project_Classwrk'.$cnt]);
			 	  $Project_Examgrade  = Trim($_POST['Project_Examgrade'.$cnt]);
                  $Project_Totalgrade = Trim($_POST['Project_Totalgrade'.$cnt]);
				  $Eng_Comprehension  = Trim($_POST['Eng_Comprehension'.$cnt]);
				  $Eng_Speaking       = Trim($_POST['Eng_Speaking'.$cnt]);
				  $Eng_Reading        = Trim($_POST['Eng_Reading'.$cnt]);
				  $Eng_Writing        = Trim($_POST['Eng_Writing'.$cnt]);
				  $Tam_Comprehension  = Trim($_POST['Tam_Comprehension'.$cnt]);
				  $Tam_Speaking       = Trim($_POST['Tam_Speaking'.$cnt]);
				  $Tam_Reading        = Trim($_POST['Tam_Reading'.$cnt]);
				  $Tam_Writing        = Trim($_POST['Tam_Writing'.$cnt]);
				  $Mat_Comprehension  = Trim($_POST['Mat_Comprehension'.$cnt]);
				  $Scn_Comprehension  = Trim($_POST['Scn_Comprehension'.$cnt]);
                  $Dcode              = Trim($_POST['Dcode'.$cnt]);
							 
			      $j=$cnt+1;

				  //$dummy = NumCheck($Term_Code,$errmsg,$errflag,"Term"); 
				  //$dummy = NumCheck($Term_Date,$errmsg,$errflag,"Term Date"); 
				  //$dummy = NumCheck($Teacher_Code,$errmsg,$errflag,"Teacher Code"); 
				  //$dummy = NumCheck($Working_Days,$errmsg,$errflag,"Working Days");
		          //Empty Field Check The Field Value Empty Means The error Messages
                  //Will Be Displayed

			if($Class > 2 ) {
				 $dummy = NumCheck($Spoken_English,$errmsg,$errflag,"Spoken English".$cnt);
                 $dummy = NumCheck($Writing_Neatness,$errmsg,$errflag,"Writing Neatness".$cnt);
                 $dummy = NumCheck($Responsibilty,$errmsg,$errflag,"Responsibilty".$cnt);
                 $dummy = NumCheck($Sports_Games,$errmsg,$errflag,"Sports Games".$cnt);
                 $dummy = NumCheck($Art_Craft,$errmsg,$errflag,"Art Craft".$cnt);
                 $dummy = NumCheck($Computer_Skills,$errmsg,$errflag,"Computer Skills".$cnt);
                 $dummy = NumCheck($Projects,$errmsg,$errflag,"Projects".$cnt);
                 $dummy = NumCheck($English_Classwrk,$errmsg,$errflag,"English Classwrk".$cnt);
                 $dummy = NumCheck($English_Examgrade,$errmsg,$errflag,"English Examgrade".$cnt);
                 $dummy = NumCheck($English_Totalgrade,$errmsg,$errflag,"English Totalgrade".$cnt);
                 $dummy = NumCheck($Tamil_Classwrk,$errmsg,$errflag,"Tamil Classwrk".$cnt);
                 $dummy = NumCheck($Tamil_Examgrade,$errmsg,$errflag,"Tamil Examgrade".$cnt);
				 $dummy = NumCheck($Tamil_Totalgrade,$errmsg,$errflag,"Tamil Totalgrade".$cnt);
				 $dummy = NumCheck($Maths_Classwrk,$errmsg,$errflag,"Maths Classwrk".$cnt);
				 $dummy = NumCheck($Maths_Examgrade,$errmsg,$errflag,"Maths Examgrade".$cnt);
				 $dummy = NumCheck($Maths_Totalgrade,$errmsg,$errflag,"Maths Totalgrade".$cnt);
		    	 $dummy = NumCheck($Science_Classwrk,$errmsg,$errflag,"Science Classwrk".$cnt);
		    	 $dummy = NumCheck($Science_Examgrade,$errmsg,$errflag,"Science Examgrade".$cnt);
		    	 $dummy = NumCheck($Science_Totalgrade,$errmsg,$errflag,"Science Totalgrade".$cnt);
                 $dummy = NumCheck($Computer_Classwrk,$errmsg,$errflag,"Computer Classwrk".$cnt);
				 $dummy = NumCheck($Computer_Examgrade,$errmsg,$errflag,"Computer Examgrade".$cnt);
				 $dummy = NumCheck($Computer_Totalgrade,$errmsg,$errflag,"Computer Totalgrade".$cnt);
				 $dummy = NumCheck($Project_Classwrk,$errmsg,$errflag,"Project Classwrk".$cnt);
				 $dummy = NumCheck($Project_Examgrade,$errmsg,$errflag,"Project Examgrade".$cnt);
				 $dummy = NumCheck($Project_Totalgrade,$errmsg,$errflag,"Project Totalgrade".$cnt);
				    }
			       
                 $dummy = NumCheck($Enthusiam_Energy,$errmsg,$errflag,"Enthusiam Energy".$cnt);
			     $dummy = NumCheck($Dicipline,$errmsg,$errflag,"Dicipline".$cnt);
				 $dummy = NumCheck($Attention_Span,$errmsg,$errflag,"Attention Span".$cnt);
		    	 $dummy = NumCheck($Punctuality,$errmsg,$errflag,"Punctuality".$cnt);
			     $dummy = NumCheck($Social_Interaction,$errmsg,$errflag,"Social Interaction".$cnt);
			     $dummy = NumCheck($Hygiene,$errmsg,$errflag,"Hygiene".$cnt);
				 $dummy = NumCheck($Noday_Attendance,$errmsg,$errflag,"Noday Attendance".$cnt);
                 
				 
				
				if($Class < 2 ) {
     	         $dummy = NumCheck($Participation,$errmsg,$errflag,"Participation".$cnt);
                 $dummy = NumCheck($Fine_Motor_Skills,$errmsg,$errflag,"Fine Motor Skills".$cnt);
                 $dummy = NumCheck($Gross_Motor_Skills,$errmsg,$errflag,"Gross Motor Skills".$cnt);
                 $dummy = NumCheck($Eng_Comprehension,$errmsg,$errflag,"English Comprehension".$cnt);
                 $dummy = NumCheck($Eng_Speaking,$errmsg,$errflag,"English Speaking".$cnt);
                 $dummy = NumCheck($Eng_Reading,$errmsg,$errflag,"English Reading".$cnt);
                 $dummy = NumCheck($Eng_Writing,$errmsg,$errflag,"English Writing".$cnt);
                 $dummy = NumCheck($Tam_Comprehension,$errmsg,$errflag,"Tamil Comprehension".$cnt);
                 $dummy = NumCheck($Tam_Speaking,$errmsg,$errflag,"Tamil Speaking".$cnt);
                 $dummy = NumCheck($Tam_Reading,$errmsg,$errflag,"Tamil Reading".$cnt);
                 $dummy = NumCheck($Tam_Writing,$errmsg,$errflag,"Tamil Writing".$cnt);
                 $dummy = NumCheck($Mat_Comprehension,$errmsg,$errflag,"Maths Comprehension".$cnt);
                 $dummy = NumCheck($Scn_Comprehension,$errmsg,$errflag,"Science Comprehension".$cnt);
                   }
                if($Class > 4 ) {
                 $dummy = NumCheck($Sscience_Classwrk,$errmsg,$errflag,"Sscience Classwrk".$cnt); 
			     $dummy = NumCheck($Sscience_Examgrade,$errmsg,$errflag,"Sscience Examgrade".$cnt); 
				 $dummy = NumCheck($Sscience_Totalgrade,$errmsg,$errflag,"Sscience Totalgrade".$cnt);
                  }
                 //$dummy = NumCheck($Dcode,$errmsg,$errflag,"Dcode".$cnt);
			 }
        	   if($errflag==0)
		      {
	            $query = mssql_init('sp_Assesment_Header',$mssql);
				mssql_bind($query,'@Ass_Header_Code',$Ass_Header_Code,SQLINT4,false,false,5);
				mssql_bind($query,'@Term_Code ',$Term_Code,SQLINT4,false,false,5);		
				mssql_bind($query,'@Class_Code ',$Class,SQLINT4,false,false,5);
				mssql_bind($query,'@Section_code ',$Section,SQLINT4,false,false,5);	
				mssql_bind($query,'@Term_Date',date('Y-m-d',strtotime($Term_Date)),
                SQLVARCHAR,false,false,20);
				mssql_bind($query,'@Teacher_Code ',$TeacherName,SQLINT4,false,false,5);	
				mssql_bind($query,'@Working_Days ',$WorkingDays,SQLINT4,false,false,5);
				mssql_bind($query,'@Period_Code ',$Period_Code,SQLINT4,false,false,5);
                mssql_bind($query,'@School_Id ',$Location,SQLINT4,false,false,5);
				mssql_bind($query,'@Ass_Header_Code1',$Ass_Header_Code1,SQLINT4,true);	
				$result = @mssql_execute($query);					
				mssql_free_statement($query);
		      for($cnt=1;$cnt<=$scount;$cnt++) 
			  {
                  $Ass_Header_Code    = Trim($_POST['Ass_Header_Code'.$cnt]);                  
				  $Student_Id         = Trim($_POST['Student_Id'.$cnt]);
				  $Spoken_English     = Trim($_POST['Spoken_English'.$cnt]);
				  $Writing_Neatness	  = Trim($_POST['Writing_Neatness'.$cnt]);
				  $Enthusiam_Energy	  = Trim($_POST['Enthusiam_Energy'.$cnt]);	
				  $Responsibilty      = Trim($_POST['Responsibilty'.$cnt]);
				  $Dicipline      	  = Trim($_POST['Dicipline'.$cnt]);
				  $Attention_Span  	  = Trim($_POST['Attention_Span'.$cnt]);
				  $Punctuality        = Trim($_POST['Punctuality'.$cnt]);
				  $Social_Interaction = Trim($_POST['Social_Interaction'.$cnt]);
				  $Hygiene	          = Trim($_POST['Hygiene'.$cnt]);
				  $Group_Work         = Trim($_POST['Group_Work'.$cnt]);
				  $Sports_Games   	  = Trim($_POST['Sports_Games'.$cnt]);
				  $Art_Craft      	  = Trim($_POST['Art_Craft'.$cnt]);
				  $Computer_Skills    = Trim($_POST['Computer_Skills'.$cnt]);
				  $Projects	          = Trim($_POST['Projects'.$cnt]);
				  $Participation	  = Trim($_POST['Participation'.$cnt]);
				  $Fine_Motor_Skills  = Trim($_POST['Fine_Motor_Skills'.$cnt]);
			      $Gross_Motor_Skills = Trim($_POST['Gross_Motor_Skills'.$cnt]);
		     	  $Noday_Attendance	  = Trim($_POST['Noday_Attendance'.$cnt]);
				  $CommentCode	      = Trim($_POST['EnglishComment_1'.$cnt]);
                  $English_Classwrk	  = Trim($_POST['English_Classwrk'.$cnt]);
				  $English_Examgrade  = Trim($_POST['English_Examgrade'.$cnt]);
				  $English_Totalgrade = Trim($_POST['English_Totalgrade'.$cnt]);
				  $Tamil_Classwrk 	  = Trim($_POST['Tamil_Classwrk'.$cnt]);
				  $Tamil_Examgrade    = Trim($_POST['Tamil_Examgrade'.$cnt]);
				  $Tamil_Totalgrade	  = Trim($_POST['Tamil_Totalgrade'.$cnt]);
				  $Maths_Classwrk	  = Trim($_POST['Maths_Classwrk'.$cnt]);
				  $Maths_Examgrade    = Trim($_POST['Maths_Examgrade'.$cnt]);
				  $Maths_Totalgrade	  = Trim($_POST['Maths_Totalgrade'.$cnt]);
				  $Science_Classwrk	  = Trim($_POST['Science_Classwrk'.$cnt]);
				  $Science_Examgrade  = Trim($_POST['Science_Examgrade'.$cnt]);
				  $Science_Totalgrade = Trim($_POST['Science_Totalgrade'.$cnt]);
				  $Sscience_Classwrk  = Trim($_POST['Sscience_Classwrk'.$cnt]);
				  $Sscience_Examgrade = Trim($_POST['Sscience_Examgrade'.$cnt]);
				  $Sscience_Totalgrade= Trim($_POST['Sscience_Totalgrade'.$cnt]);
				  $Computer_Classwrk  = Trim($_POST['Computer_Classwrk'.$cnt]);
				  $Computer_Examgrade = Trim($_POST['Computer_Examgrade'.$cnt]);
				  $Computer_Totalgrade= Trim($_POST['Computer_Totalgrade'.$cnt]);
				  $Project_Classwrk	  = Trim($_POST['Project_Classwrk'.$cnt]);
			 	  $Project_Examgrade  = Trim($_POST['Project_Examgrade'.$cnt]);
                  $Project_Totalgrade = Trim($_POST['Project_Totalgrade'.$cnt]);
				  $Eng_Comprehension  = Trim($_POST['Eng_Comprehension'.$cnt]);
				  $Eng_Speaking       = Trim($_POST['Eng_Speaking'.$cnt]);
				  $Eng_Reading        = Trim($_POST['Eng_Reading'.$cnt]);
				  $Eng_Writing        = Trim($_POST['Eng_Writing'.$cnt]);
				  $Tam_Comprehension  = Trim($_POST['Tam_Comprehension'.$cnt]);
				  $Tam_Speaking       = Trim($_POST['Tam_Speaking'.$cnt]);
				  $Tam_Reading        = Trim($_POST['Tam_Reading'.$cnt]);
				  $Tam_Writing        = Trim($_POST['Tam_Writing'.$cnt]);
				  $Mat_Comprehension  = Trim($_POST['Mat_Comprehension'.$cnt]);
				  $Scn_Comprehension  = Trim($_POST['Scn_Comprehension'.$cnt]);
                  $Dcode              = trim($_POST['Dcode'.$cnt]);
                  
			      $query = mssql_init('sp_Assesment_Detail',$mssql);
                  If($Ass_Header_Code>0) $Ass_Header_Code1 =$Ass_Header_Code; 
                  	
				  mssql_bind($query,'@Ass_Header_Code',$Ass_Header_Code1,SQLINT4,false,false,5);
				  mssql_bind($query,'@Student_Id',$Student_Id,SQLINT4,false,false,5);		
			                       
				  mssql_bind($query,'@Spoken_English',$Spoken_English,SQLINT4,false,false,5);	
				  mssql_bind($query,'@Writing_Neatness',$Writing_Neatness,SQLINT4,false,false,5);
				  mssql_bind($query,'@Enthusiam_Energy',$Enthusiam_Energy,SQLINT4,false,false,5);	
				  mssql_bind($query,'@Responsibilty',$Responsibilty,SQLINT4,false,false,5);
				  mssql_bind($query,'@Dicipline',$Dicipline,SQLINT4,false,false,5);		
				  mssql_bind($query,'@Attention_Span',$Attention_Span,SQLINT4,false,false,5);
				  mssql_bind($query,'@Punctuality',$Punctuality,SQLINT4,false,false,5);		
				  mssql_bind($query,'@Social_Interaction',$Social_Interaction,SQLINT4,false,false,5);
				  mssql_bind($query,'@Hygiene',$Hygiene,SQLINT4,false,false,5);		
			      mssql_bind($query,'@Group_Work',$Group_Work,SQLINT4,false,false,5);
				  mssql_bind($query,'@Sports_Games',$Sports_Games,SQLINT4,false,false,5);		
				  mssql_bind($query,'@Art_Craft',$Art_Craft,SQLINT4,false,false,5);
				  mssql_bind($query,'@Computer_Skills',$Computer_Skills,SQLINT4,false,false,5);	
     			  mssql_bind($query,'@Projects',$Projects,SQLINT4,false,false,5);
				  mssql_bind($query,'@Participation',$Participation,SQLINT4,false,false,5);
                  mssql_bind($query,'@Fine_Motor_Skills',$Fine_Motor_Skills,SQLINT4,false,false,5);
				  mssql_bind($query,'@Gross_Motor_Skills',$Gross_Motor_Skills,SQLINT4,false,false,5);	
				  mssql_bind($query,'@Noday_Attendance',$Noday_Attendance,SQLINT4,false,false,5);
				  mssql_bind($query,'@English_Classwrk',$English_Classwrk,SQLINT4,false,false,5);	
     			  mssql_bind($query,'@English_Examgrade',$English_Examgrade,SQLINT4,false,false,5);
                  mssql_bind($query,'@CommentCode',$CommentCode,SQLINT4,false,false,5);	
     	     	  mssql_bind($query,'@English_Totalgrade',$English_Totalgrade,SQLINT4,false,false,5);
                  mssql_bind($query,'@Tamil_Classwrk',$Tamil_Classwrk,SQLINT4,false,false,5);
				  mssql_bind($query,'@Tamil_Examgrade',$Tamil_Examgrade,SQLINT4,false,false,5);	
				  mssql_bind($query,'@Tamil_Totalgrade',$Tamil_Totalgrade,SQLINT4,false,false,5);	
     			  mssql_bind($query,'@Maths_Classwrk',$Maths_Classwrk,SQLINT4,false,false,5);
				  mssql_bind($query,'@Maths_Examgrade',$Maths_Examgrade,SQLINT4,false,false,5);
                  mssql_bind($query,'@Maths_Totalgrade',$Maths_Totalgrade,SQLINT4,false,false,5);
				  mssql_bind($query,'@Science_Classwrk',$Science_Classwrk,SQLINT4,false,false,5);	
				  mssql_bind($query,'@Science_Examgrade',$Science_Examgrade,SQLINT4,false,false,5);
				  mssql_bind($query,'@Science_Totalgrade',$Science_Totalgrade,SQLINT4,false,false,5);
				  mssql_bind($query,'@Sscience_Classwrk',$Sscience_Classwrk,SQLINT4,false,false,5);
				  mssql_bind($query,'@Sscience_Examgrade',$Sscience_Examgrade,SQLINT4,false,false,5);	
				  mssql_bind($query,'@Sscience_Totalgrade',$Sscience_Totalgrade,SQLINT4,false,false,5);
				  mssql_bind($query,'@Computer_Classwrk',$Computer_Classwrk,SQLINT4,false,false,5);
				  mssql_bind($query,'@Computer_Examgrade',$Computer_Examgrade,SQLINT4,false,false,5);
				  mssql_bind($query,'@Computer_Totalgrade',$Computer_Totalgrade,SQLINT4,false,false,5);	
				  mssql_bind($query,'@Project_Classwrk',$Project_Classwrk,SQLINT4,false,false,5);
				  mssql_bind($query,'@Project_Examgrade',$Project_Examgrade,SQLINT4,false,false,5);
				  mssql_bind($query,'@Project_Totalgrade',$Project_Totalgrade,SQLINT4,false,false,5);
				  mssql_bind($query,'@Eng_Comprehension',$Eng_Comprehension,SQLINT4,false,false,5);	
				  mssql_bind($query,'@Eng_Speaking',$Eng_Speaking,SQLINT4,false,false,5);
				  mssql_bind($query,'@Eng_Reading',$Eng_Reading,SQLINT4,false,false,5);
				  mssql_bind($query,'@Eng_Writing',$Eng_Writing,SQLINT4,false,false,5);	
				  mssql_bind($query,'@Tam_Comprehension',$Tam_Comprehension,SQLINT4,false,false,5);
				  mssql_bind($query,'@Tam_Speaking',$Tam_Speaking,SQLINT4,false,false,5);
				  mssql_bind($query,'@Tam_Reading',$Tam_Reading,SQLINT4,false,false,5);	
				  mssql_bind($query,'@Tam_Writing',$Tam_Writing,SQLINT4,false,false,5);
                  mssql_bind($query,'@Mat_Comprehension',$Mat_Comprehension,SQLINT4,false,false,5);
				  mssql_bind($query,'@Scn_Comprehension',$Scn_Comprehension,SQLINT4,false,false,5);
                  mssql_bind($query,'@Dcode',$Dcode,SQLINT4,false,false,5);
                
                  $result = @mssql_execute($query);
				  mssql_free_statement($query);
            
				  if(!$result)					
				  {
                    $errmsg1.=mssql_get_last_message();
                    If ($errcnt == 0) { $errcnt = 1;} else { $errcnt = $errcnt + 1; }
                    if ($errval == "") { $errval = $scount[$cnt];	 } else { $errval = $errval.",".$scount[$cnt]; } 
                  }
                }
			}
			else {
				If ($errcnt == 0) { $errcnt = 1;} else { $errcnt = $errcnt + 1; }
				if ($errval == "") { $errval = $scount[$cnt];	 } else { $errval = $errval.",".$scount[$cnt]; } 
				}
			//Error Message String Length Must 150 Characters
			if($errflag==1) {
				if(strlen($errmsg)<150)
					echo $errlbl.$errmsg;
			    else
					echo "<P CLASS='error'>Please fill all the fields</p>";
			} else if($errcnt>0) 
                    echo $errmsg1;
            else echo "<p class='mesg'>Record has been saved</p>";
}            
    if($_POST['Go'] =='Go')	
	{   
	   $Location        =   Trim($_POST['Location']);
	   $Class           =   Trim($_POST['Class']);	
	   $Section         =   Trim($_POST['Section']);
	   $Admission_No	=	Trim($_POST['Admission_No']);
       $Term	        =	Trim($_POST['Term']);
       //The Field Value Should be Empty Means The Error Message Displayed
	   $dummy = ZeroCheck($Location,$errmsg,$errflag,"Location"); 
	   $dummy = ZeroCheck($Class,$errmsg,$errflag,"Class"); 
	   $dummy = ZeroCheck($Section,$errmsg,$errflag,"Section"); 

    if($errflag==1) {
	   echo $errlbl.$errmsg;
       $errflag=2;
	}
}	?>
	<body>
    <form id="myform" name="myform" method="post" action="NewReport23.php" >
    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr><td align="center">
        <div id="scholarshipfr" align="center" style="width: 100%;">
        <table border="0"  cellpadding="1" cellspacing="1" width="100%" align="center">
        <tr>
                    <?php 	//SHOW Location  DROPDOWN
						$query = mssql_init('[sp_selectschoolname]',$mssql);
						$result = mssql_execute($query);
						mssql_free_statement($query);	?>
                   <td width="100" align="right" style="width:100px;" >School<?php echo $mand; ?> </td>
                   <td width="130" align="left" ><div id ="Location">
                   <select id="Location" name="Location" style="width:125px">
                        <?php echo $Select;?>
                        <?php while($field = mssql_fetch_array($result))   { 	   ?>
	                        <option value="<?php echo $field['School_id']?>" <?php if($Location==$field['School_id']) echo "Selected"; ?>>
                                                    <?php echo $field['SchoolName']?>                           </option>
                        <?php } ?>
                    </select></div>
                    </td>
                    <?php 	//SHOW CLASS DROPDOWN
		$query = mssql_init('sp_GetClass',$mssql);
		$result = mssql_execute($query);
		mssql_free_statement($query);	?>
                    <td width="100" align="right" style="width:100px;">Class<?php echo $mand; ?> </td>
                    <td width="54" align="right"><div id ="Class">
                        <select id="Class" name="Class">
                          <?php echo $Select;?>
                          <?php while($field = mssql_fetch_array($result)) 	{  ?>
                          <option value="<?php echo $field['Class_Code']?>" <?php if( $Class==$field['Class_Code']) echo "Selected"; ?>> <?php echo $field['Class']?></option>
                          <?php } ?>
                        </select>
                      </div> 
				    </td>
                    <?php 	//SHOW SECTION DROPDOWN
		$query = mssql_init('sp_GetSection',$mssql);
		$result = mssql_execute($query);
		mssql_free_statement($query);	?>
                    <td width="100" align="right" style="width:100px;">Section<?php echo $mand; ?> </td>
<td width="143" align="left"><div id ="Section">
                        <select id="Section" name="Section">
                          <?php echo $Select;?>
                          <?php while($field = mssql_fetch_array($result)) 
   {  ?>
                          <option value="<?php echo $field['Section_Code']?>" <?php if($Section==$field['Section_Code']) echo "selected"; ?>> <?php echo $field['Section']?></option>
                          <?php } ?>
                        </select>
                    </div></td>
                    <td width="93" align="right">Adm. No.</td>
                    <td width="152" align="left"><div id ="Admission_No"></div>
                      <input type="text" name="Admission_No" id="Admission_No" maxlength="50" value="<?php echo $Admission_No ?>" onkeydown="return numberonly('Admission_No')" /></td>
          
        <?php 	//SHOW TERM DROPDOWN
		$query = mssql_init('sp_SelectTerm',$mssql);
		$result = mssql_execute($query);
		mssql_free_statement($query);	?>
                    <td width="32" align="right">Term</td>
<td width="150" align="left"><div id ="Term">
                        <select id="Term" name="Term">
                          <?php echo $Select;?>
                          <?php while($field = mssql_fetch_array($result))    {  ?>
                          <option value="<?php echo $field['Term_Code']?>" <?php if($Term==$field['Term_Code']) echo "selected"; ?>> <?php echo $field['Term']?></option>
                          <?php } ?>
                        </select>
                    </div></td>
                    <td width="162" align="right"><input name="Go"  type="submit" value="Go" class="winbutton_go" /></td>
                  </tr>
                </table></div></td></tr>
   <?php   
   if(($_POST['Go'] =='Go' and $errflag==0) or ($_POST['Save'] =='Save' or $errflag==1) )
        {    
            
        //Particular Fields Only Search  sp_NewReport sp_NewStudentReport
        If($errflag==0) {
    	   	$query = mssql_init('sp_Reportcardview',$mssql);
            mssql_bind($query,'@School_id',$Location,SQLINT4,false,false,5);
            mssql_bind($query,'@Section_Code',$Section,SQLINT4,false,false,5);
            mssql_bind($query,'@Class_Code',$Class,SQLINT4,false,false,5);
    		if(strlen($Admission_No)==0) $Admission_No=0;
            mssql_bind($query,'@Admission_No',$Admission_No,SQLVARCHAR,false,false,25);
            mssql_bind($query,'@Term_Code',$Term,SQLINT4,false,false,5);
          
            $Rptresult = @mssql_execute($query);
    		mssql_free_statement($query);                
    	   $Count = mssql_num_rows($Rptresult);
           if($Count > 0)
    	    {
    		$colorflag = 0;
    		$i = 0;	           
            while($field = mssql_fetch_array($Rptresult))
			{ 
				$i  +=1;	$colorflag+=1;
				$tot_rec = $i;	
				//VALUE GET FROM DB
                
                  $Term	          =	 Trim($field['Term_Code']);                    
                  $Term_Date      =  Trim($field['Term_date']);
                  $TeacherName    =  Trim($field['Teacher_code']);
                  $WorkingDays	  =	 Trim($field['Working_Days']);
                  
                  if(strlen($Term_Date)>0) {
                    $Term_Date = str_replace("/","-",$Term_Date);
                    $Term_Date = date('d-m-Y', strtotime($Term_Date));
                  }

                  $Ass_Header_Code =$field['Ass_Header_Code'];
                  $Spoken_English     = $field['Spoken_English'];
				  $Writing_Neatness	  = Trim($field['Writing_Neatness']);
				  $Enthusiam_Energy	  = Trim($field['Enthusiam_Energy']);	
				  $Responsibilty      = Trim($field['Responsibilty']);
				  $Dicipline      	  = Trim($field['Dicipline']);
				  $Attention_Span  	  = Trim($field['Attention_Span']);
				  $Punctuality        = Trim($field['Punctuality']);
				  $Social_Interaction = Trim($field['Social_Interaction']);
				  $Hygiene	          = Trim($field['Hygiene']);
				  $Group_Work         = Trim($field['Group_Work']);
				  $Sports_Games   	  = Trim($field['Sports_Games']);
				  $Art_Craft      	  = Trim($field['Art_Craft']);
				  $Computer_Skills    = Trim($field['Computer_Skills']);
				  $Projects	          = Trim($field['Projects']);
				  $Participation	  = Trim($field['Participation']);
				  $Fine_Motor_Skills  = Trim($field['Fine_Motor_Skills']);
			      $Gross_Motor_Skills = Trim($field['Gross_Motor_Skills']);
		     	  $Noday_Attendance	  = Trim($field['Noday_Attendance']);
				  $CommentCode	      = Trim($field['CommentCode']);
				  $English_Classwrk	  = Trim($field['English_Classwrk']);
				  $English_Examgrade  = Trim($field['English_Examgrade']);
				  $English_Totalgrade = Trim($field['English_Totalgrade']);
				  $Tamil_Classwrk 	  = Trim($field['Tamil_Classwrk']);
				  $Tamil_Examgrade    = Trim($field['Tamil_Examgrade']);
				  $Tamil_Totalgrade	  = Trim($field['Tamil_Totalgrade']);
				  $Maths_Classwrk	  = Trim($field['Maths_Classwrk']);
				  $Maths_Examgrade    = Trim($field['Maths_Examgrade']);
				  $Maths_Totalgrade	  = Trim($field['Maths_Totalgrade']);
				  $Science_Classwrk	  = Trim($field['Science_Classwrk']);
				  $Science_Examgrade  = Trim($field['Science_Examgrade']);
				  $Science_Totalgrade = Trim($field['Science_Totalgrade']);
				  $Sscience_Classwrk  = Trim($field['Sscience_Classwrk']);
				  $Sscience_Examgrade = Trim($field['Sscience_Examgrade']);
				  $Sscience_Totalgrade= Trim($field['Sscience_Totalgrade']);
				  $Computer_Classwrk  = Trim($field['Computer_Classwrk']);
				  $Computer_Examgrade = Trim($field['Computer_Examgrade']);
				  $Computer_Totalgrade= Trim($field['Computer_Totalgrade']);
				  $Project_Classwrk	  = Trim($field['Project_Classwrk']);
			 	  $Project_Examgrade  = Trim($field['Project_Examgrade']);
                  $Project_Totalgrade  = Trim($field['Project_Totalgrade']);
				  $Eng_Comprehension  = Trim($field['Eng_Comprehension']);
				  $Eng_Speaking       = Trim($field['Eng_Speaking']);
				  $Eng_Reading        = Trim($field['Eng_Reading']);
				  $Eng_Writing        = Trim($field['Eng_Writing']);
				  $Tam_Comprehension  = Trim($field['Tam_Comprehension']);
				  $Tam_Speaking       = Trim($field['Tam_Speaking']);
				  $Tam_Reading        = Trim($field['Tam_Reading']);
				  $Tam_Writing        = Trim($field['Tam_Writing']);
				  $Mat_Comprehension  = Trim($field['Mat_Comprehension']);
				  $Scn_Comprehension  = Trim($field['Scn_Comprehension']);
	            
                If($insheader==0) { 
                    $insheader=1;   ?>
    <tr><td height="10">&nbsp;</td></tr>
    <tr><td>
    <div style= "display:block;width:100%; overflow:auto" id="scholarshipfr" align="center">
    <table align="center" cellpadding="3" cellspacing="1" width="100%">
    <tr>
    <?php 	//SHOW TEACHER DROPDOWN
		$query = mssql_init('sp_SelectTerm',$mssql);
		$result = mssql_execute($query);
		mssql_free_statement($query);	?>
  <td width="100" align="right" style="width:100px;">Term</td>
  <td width="127" align="left"><div id ="Term">
  <select id="Term2" name="Term2">
    <?php echo $Select;?>
    <?php while($field1 = mssql_fetch_array($result)) 
   {  ?>
    <option value="<?php echo $field1['Term_Code']?>" <?php if($Term==$field1['Term_Code']) echo "selected"; ?>> <?php echo $field1['Term']?></option>
    <?php } ?>
  </select>
</div></td>
          <td width="61" align="right"  >TermDate</td>
          <td width="125" align="right"><a href="javascript:NewCal('Term_Date','ddmmyyyy')">
<input type="text" name="Term_Date" id="Term_Date"readonly="readonly" style="width:125px" size="10" maxlength="50" value="<?php echo $Term_Date ?>" >
                </a></td>
<?php 	//SHOW TEACHER DROPDOWN
		$query  = mssql_init('sp_SelectTeacher',$mssql);
		$result = mssql_execute($query);
		mssql_free_statement($query);	?>
                    <td width="68"  align="right">Teacher</td>
<td width="125" align="left"><div id ="TeacherName">
                        <select id="TeacherName" name="TeacherName" style="width:125px">
                          <?php echo $Select;?>
                          <?php while($field1 = mssql_fetch_array($result)) 
   {  ?>
                          <option value="<?php echo $field1['Teacher_Code']?>" <?php if($TeacherName==$field1['Teacher_Code']) echo "selected"; ?>> <?php echo $field1['Teacher_Name']?></option>
                          <?php } ?>
                        </select>
                </div></td>
                <td width="90"  align="right">WorkingDays</td>
                        <td width="315" align="left"><div id ="Working_Days">
                        <input type="text" name="Working_Days" id="Working_Days"  maxlength="50" value="<?php echo $WorkingDays ?>" 
	                      onkeydown="return numberonly('Working_Days')" />
                </div></td>
            <td width="173" align="left">&nbsp;</td>
            </tr>
          </table>
          <table align="center" cellpadding="3" cellspacing="1" width="100%">
            <tr align="center"> </tr>
            <thead>
            </thead>
            <th align="left">S.No.</th>
                <th>ADM. No.</th>
              <th>NAME</th>
              <?php  for($hcnt=1;$hcnt<=18;$hcnt++)
                    echo "<th>&nbsp;</th>";  ?>
                <th colspan="3">English Comments</th>
            </tr>
            <?php } ?>
            <input type="hidden" name="Student_Id<?php echo $i?>" id="Student_Id<?php echo $i?>" value="<?php echo $field['Student_Id'] ?>"/>
            <input type="hidden" name="Ass_Header_Code<?php echo $i?>" id="Ass_Header_Code<?php echo $i?>" value="<?php echo $field['Ass_Header_Code'] ?>"/>
            <input type="hidden" name="scount" id="scount" value=<?php echo $Count ?>/>
            <tr class=<?php if($colorflag%2==0) { echo "row1"; } else { echo "row2"; } ?> valign="center">
              <td align="center" rowspan="3"><?php echo $i ?> </td>
              <td rowspan="3"><input type="text" name="Admission_No<?php echo $i ?>" id="Admission_No2"  size="10" maxlength="10" style="width:80px;" value="<?php echo $field['Admission_No'] ?>" readonly="readonly"/>              </td>
              <td rowspan="3"><input type="text" name="Name<?php echo $i ?>" id="Name<?php echo $i;?>"  size="10" maxlength="25" style="width:100px;" value="<?php echo $field['Name'] ?> " readonly="readonly"	/>              </td>

             <td align="left"><div id ="Spoken_English<?php echo $i;?>" style="display:<?php if($Class > 2 ){?>block<? }else {?>none;<? }?>">
                <input type="text"   name="Spoken_English<?php echo $i ?>" id="Spoken_English_<?php echo $i ?>" size="3" maxlength="3" 
                value="<?php echo $Spoken_English ?>"  onkeyup="markcheck('<?php echo "#Spoken_English_".$i ?>')"
                onkeydown="return numberonly('Spoken_English<?php echo $i ?>')"  style="width:25px;" onfocus="showtitle('Spoken_English_<?php echo $i ?>','SPOKEN ENGLISH')" />
              </div></td>
                       
                <td align="left"><div id ="Enthusiam_Energy<?php echo $i;?>">
                <input type="text" name="Enthusiam_Energy<?php echo $i ?>" 
                               id="Enthusiam_Energy_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"
                               value="<?php echo $Enthusiam_Energy ?>"onkeyup="markcheck('<?php echo "#Enthusiam_Energy_".$i ?>')" 
                               onkeydown="return numberonly('Enthusiam_Energy<?php echo $i ?>')" onfocus="showtitle('Enthusiam_Energy_<?php echo $i ?>','ENTHUSIASM & ENERGY')" />
              </div></td>
              <td align="left"><div id ="Writing_Neatness<?php echo $i;?>" style="display:<? if($Class > 2){?>block<? }else {?>none;<? }?>">
                <input type="text" name="Writing_Neatness<?php echo $i ?>" id="Writing_Neatness_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"
                               value="<?php echo $Writing_Neatness ?>"onkeyup="markcheck('<?php echo "#Writing_Neatness_".$i ?>')" 
                               onkeydown="return numberonly('Writing_Neatness<?php echo $i ?>')" onfocus="showtitle('Writing_Neatness_<?php echo $i ?>','WRITING NEATNESS')" />
              </div></td>
              <td align="left"><div id ="Responsibilty<?php echo $i;?>" style="display:<? if($Class > 2){?>block<? }else {?>none;<? }?>">
                  <input type="text" name="Responsibilty<?php echo $i ?>" id="Responsibilty_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"
                               value="<?php echo $Responsibilty ?>" onkeyup="markcheck('<?php echo "#Responsibilty_".$i ?>')"  
                               onkeydown="return numberonly('Responsibilty<?php echo $i ?>')"
							   onfocus="showtitle('Responsibilty_<?php echo $i ?>','RESPONSIBILITY')" />
              </div></td>
              <td align="left"><div id ="Dicipline<?php echo $i;?>">
                  <input type="text" name="Dicipline<?php echo $i ?>" id="Dicipline_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo $Dicipline ?>" onkeyup="markcheck('<?php echo "#Dicipline_".$i ?>')" onkeydown="return numberonly('Dicipline<?php echo $i ?>')" onfocus="showtitle('Dicipline_<?php echo $i ?>','DICIPLINE')" />
              </div></td>
              <td align="left"><div id ="Attention_Span<?php echo $i;?>">
                  <input type="text" name="Attention_Span<?php echo $i ?>" id="Attention_Span_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo $Attention_Span ?>" onkeyup="markcheck('<?php echo "#Attention_Span_".$i ?>')" onkeydown="return numberonly('Attention_Span<?php echo $i ?>')" onfocus="showtitle('Attention_Span_<?php echo $i ?>','ATTENTION SPAN')" />
              </div></td>
              <td align="left"><div id ="Punctuality<?php echo $i;?>">
                  <input type="text" name="Punctuality<?php echo $i ?>" id="Punctuality_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo $Punctuality ?>" onkeyup="markcheck('<?php echo "#Punctuality_".$i ?>')" onkeydown="return numberonly('Punctuality<?php echo $i ?>')" onfocus="showtitle('Punctuality_<?php echo $i ?>','PUNCTUALITY')" />
              </div></td>
              <td align="left"><div id ="Social_Interaction<?php echo $i;?>">
                  <input type="text" name="Social_Interaction<?php echo $i ?>" id="Social_Interaction_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo $Social_Interaction ?>" onkeyup="markcheck('<?php echo "#Social_Interaction_".$i ?>')" onkeydown="return numberonly('Social_Interaction<?php echo $i ?>')" 
							   onfocus="showtitle('Social_Interaction_<?php echo $i ?>','SOCIAL INTERACTION')"/>
              </div></td>
              <td align="left"><div id ="Hygiene<?php echo $i;?>">
                  <input type="text" name="Hygiene<?php echo $i ?>" id="Hygiene_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo $Hygiene ?>"  onkeyup="markcheck('<?php echo "#Hygiene_".$i ?>')" onkeydown="return numberonly('Hygiene<?php echo $i ?>')"
							   onfocus="showtitle('Hygiene_<?php echo $i ?>','HYGIENE')"/>
              </div></td>
              <td align="left"><div id ="Group_Work<?php echo $i;?>" style="display:<? if($Class > 2){?>block<? }else {?>none;<? }?>">
                  <input type="text" name="Group_Work<?php echo $i ?>" id="Group_Work_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo $Group_Work ?>"  onkeyup="markcheck('<?php echo "#Group_Work_".$i ?>')" onkeydown="return numberonly('Group_Work<?php echo $i ?>')"  onfocus="showtitle('Group_Work_<?php echo $i ?>','GROUP WORK')"/>
              </div></td>
              <td align="left"><div id ="Sports_Games<?php echo $i;?>" style="display:<? if($Class > 2){?>block<? }else {?>none;<? }?>">
                  <input type="text" name="Sports_Games<?php echo $i ?>" id="Sports_Games_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo $Sports_Games ?>" onkeyup="markcheck('<?php echo "#Sports_Games_".$i ?>')" onkeydown="return numberonly('Sports_Games<?php echo $i ?>')"
							   onfocus="showtitle('Sports_Games_<?php echo $i ?>','SPORTS AND GAMES')"/>
              </div></td>
              <td align="left"><div id ="Art_Craft<?php echo $i;?>" style="display:<? if($Class > 2){?>block<? }else {?>none;<? }?>">
                  <input type="text" name="Art_Craft<?php echo $i ?>" id="Art_Craft_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo $Art_Craft ?>" onkeyup="markcheck('<?php echo "#Art_Craft_".$i ?>')" onkeydown="return numberonly('Art_Craft<?php echo $i ?>')" onfocus="showtitle('Art_Craft_<?php echo $i ?>','ART AND CRAFT')"/>
              </div></td>
              <td align="left"><div id ="Computer_Skills<?php echo $i;?>" style="display:<? if($Class > 2){?>block<? }else {?>none;<? }?>">
                  <input type="text" name="Computer_Skills<?php echo $i ?>" id="Computer_Skills_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo $Computer_Skills ?>" onkeyup="markcheck('<?php echo "#Computer_Skills_".$i ?>')" onkeydown="return numberonly('Computer_Skills<?php echo $i ?>')" onfocus="showtitle('Computer_Skills_<?php echo $i ?>','COMPUTER SKILLS')"/>
              </div></td>
              <td align="left"><div id ="Projects<?php echo $i;?>" style="display:<? if($Class > 2){?>block<? }else {?>none;<? }?>">
                  <input type="text" name="Projects<?php echo $i ?>" id="Projects_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo $Projects ?>" onkeyup="markcheck('<?php echo "#Projects_".$i ?>')" onkeydown="return numberonly('Projects<?php echo $i ?>')" 
							   onfocus="showtitle('Projects_<?php echo $i ?>','PROJECTS')"/>
              </div></td>
              <td align="left"><div id ="Participation<?php echo $i;?>" style="display:<? if($Class > 2){?>none<? }else {?>block;<? }?>">
                  <input type="text" name="Participation<?php echo $i ?>" id="Participation_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo $Participation ?>"  onkeyup="markcheck('<?php echo "#Participation_".$i ?>')" onkeydown="return numberonly('Participation<?php echo $i ?>')"
							   onfocus="showtitle('Participation_<?php echo $i ?>','PARTICIPATION')"/>
              </div></td>
             
              <td align="left"><div id ="Gross_Motor_Skills<?php echo $i;?>"style="display:<? if($Class > 2){?>none<? }else {?>block;<? }?>">
                  <input type="text" name="Gross_Motor_Skills<?php echo $i ?>" id="Gross_Motor_Skills_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo $Gross_Motor_Skills ?>"  onkeyup="markcheck('<?php echo "#Gross_Motor_Skills_".$i ?>')" onkeydown="return numberonly('Gross_Motor_Skills<?php echo $i ?>')"
							   onfocus="showtitle('Gross_Motor_Skills_<?php echo $i ?>','GROSS MOTOR SKILLS')"/>
              </div></td>
              <td align="left"><div id ="Fine_Motor_Skills<?php echo $i;?>"style="display:<? if($Class > 2){?>none<? }else {?>block;<? }?>">
                  <input type="text" name="Fine_Motor_Skills<?php echo $i ?>" id="Fine_Motor_Skills_<?php echo $i ?>" size="3" 
                  maxlength="3" style="width:25px"value="<?php echo $Fine_Motor_Skills ?>"  onkeyup="markcheck('<?php echo "#Fine_Motor_Skills_".$i ?>')" 
                  onkeydown="return numberonly('Fine_Motor_Skills<?php echo $i ?>')"
		          onfocus="showtitle('Fine_Motor_Skills_<?php echo $i ?>','FINE MOTOR SKILLS')"/>
              </div></td>
                  <td align="left"><div id ="Noday_Attendance<?php echo $i;?>">
                  <input type="text" name="Noday_Attendance<?php echo $i ?>" id="Noday_Attendance_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo $Noday_Attendance ?>" onkeyup="markcheck('<?php echo "#Noday_Attendance_".$i ?>')" onkeydown="return numberonly('Noday_Attendance<?php echo $i ?>')" 
							   onfocus="showtitle('Noday_Attendance_<?php echo $i ?>','NO OF DAYS ATTENDANCE')"/>
              </div></td>
              
               
              
              <td></td>
              <?php 	//SHOW COMMENT DROPDOWN
		                  $query = mssql_init('sp_FetchComment',$mssql);
	                      $result = mssql_execute($query);
		                  mssql_free_statement($query);	?>
              <td width="128" align="right" colspan="3"><div id ="EnglishComment">
                  <select id="EnglishComment_1<?php echo $i ?>" 
                         name="EnglishComment_1<?php echo $i?>" style="width:125px">
                    <?php echo $Select;?>
                    <?php while($field = mssql_fetch_array($result))                                                                        	{   ?>
                    <option value="<?php echo $field['CommentCode']?>" 
                          <?php if($CommentCode==$field['CommentCode']) echo "Selected"; ?>> <?php echo $field['EnglishComment']?></option>
                    <?php } ?>
                  </select>
              </div></td>
            </tr>
            <tr class=<?php if($colorflag%2==0) { echo "row1"; } else { echo "row2"; } ?> valign="center">
              <td align="left"><?php echo $field['English_Classwrk'] ?>
                  <div id ="English_Classwrk<?php echo $i;?>" style="display:<? if($Class > 2){?>block<? }else {?>none;<? }?>">
                    <input type="text" name="English_Classwrk<?php echo $i ?>" id="English_Classwrk_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo $English_Classwrk ?>" onkeyup="markcheck('<?php echo "#English_Classwrk_".$i ?>')" onkeydown="return numberonly('English_Classwrk<?php echo $i ?>')"
							   onfocus="showtitle('English_Classwrk_<?php echo $i ?>','ENGLISH CLASSWORK')"/>
                  </div></td>
              <td align="left"><?php echo $field['English_Examgrade'] ?>
                  <div id ="English_Examgrade<?php echo $i;?>" style="display:<? if($Class > 2){?>block<? }else {?>none;<? }?>">
                    <input type="text" name="English_Examgrade<?php echo $i ?>" id="English_Examgrade_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo $English_Examgrade ?>"  onkeyup="markcheck('<?php echo "#English_Examgrade_".$i ?>')" onkeydown="return numberonly('English_Examgrade<?php echo $i ?>')"
							  onfocus="showtitle('English_Examgrade_<?php echo $i ?>','ENGLISH EXAMGRADE')"/>
                  </div></td>
              <td align="left"><?php echo $field['English_Totalgrade'] ?>
                  <div id ="English_Totalgrade<?php echo $i;?>" style="display:<? if($Class > 2){?>block<? }else {?>none;<? }?>">
                    <input type="text" name="English_Totalgrade<?php echo $i ?>" id="English_Totalgrade_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo $English_Totalgrade ?>" onkeyup="markcheck('<?php echo "#English_Totalgrade_".$i ?>')" onkeydown="return numberonly('English_Totalgrade<?php echo $i ?>')"
							 onfocus="showtitle('English_Totalgrade_<?php echo $i ?>','ENGLISH TOTALGRADE')"/>
                  </div></td>
              <td align="left"><?php echo $field['Tamil_Classwrk'] ?>
                  <div id ="Tamil_Classwrk<?php echo $i;?>" style="display:<? if($Class > 2){?>block<? }else {?>none;<? }?>">
                    <input type="text" name="Tamil_Classwrk<?php echo $i ?>" id="Tamil_Classwrk_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo $Tamil_Classwrk ?>" onkeyup="markcheck('<?php echo "#Tamil_Classwrk_".$i ?>')" onkeydown="return numberonly('Tamil_Classwrk<?php echo $i ?>')" onfocus="showtitle('Tamil_Classwrk_<?php echo $i ?>','TAMIL CLASSWORK')"/>
                  </div></td>
              <td align="left"><?php echo $field['Tamil_Examgrade'] ?>
                  <div id ="Tamil_Examgrade<?php echo $i;?>" style="display:<? if($Class > 2){?>block<? }else {?>none;<? }?>">
                    <input type="text" name="Tamil_Examgrade<?php echo $i ?>" id="Tamil_Examgrade_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo $Tamil_Examgrade ?>"  onkeyup="markcheck('<?php echo "#Tamil_Examgrade_".$i ?>')" onkeydown="return numberonly('Tamil_Examgrade<?php echo $i ?>')" onfocus="showtitle('Tamil_Examgrade_<?php echo $i ?>','TAMIL EXAMGRADE')"/>
                  </div></td>
              <td align="left"><?php echo $field['Tamil_Totalgrade'] ?>
                  <div id ="Tamil_Totalgrade<?php echo $i;?>" style="display:<? if($Class > 2){?>block<? }else {?>none;<? }?>">
                    <input type="text" name="Tamil_Totalgrade<?php echo $i ?>" id="Tamil_Totalgrade_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo $Tamil_Totalgrade ?>" onkeyup="markcheck('<?php echo "#Tamil_Totalgrade_".$i ?>')" onkeydown="return numberonly('Tamil_Totalgrade<?php echo $i ?>')" onfocus="showtitle('Tamil_Totalgrade_<?php echo $i ?>','TAMIL TOTALGRADE')"/>
                  </div></td>
              <td align="left"><?php echo $field['Maths_Classwrk'] ?>
                  <div id ="Maths_Classwrk<?php echo $i;?>" style="display:<? if($Class > 2){?>block<? }else {?>none;<? }?>">
                    <input type="text" name="Maths_Classwrk<?php echo $i ?>" id="Maths_Classwrk_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo $Maths_Classwrk ?>" onkeyup="markcheck('<?php echo "#Maths_Classwrk_".$i ?>')" onkeydown="return numberonly('Maths_Classwrk<?php echo $i ?>')" onfocus="showtitle('Maths_Classwrk_<?php echo $i ?>','MATHS CLASSWORK')"/>
                  </div></td>
              <td align="left"><div id ="Maths_Examgrade<?php echo $i;?>" style="display:<? if($Class > 2){?>block<? }else {?>none;<? }?>">
                    <input type="text" name="Maths_Examgrade<?php echo $i ?>" id="Maths_Examgrade_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo $Maths_Examgrade ?>" onkeyup="markcheck('<?php echo "#Maths_Examgrade_".$i ?>')" onkeydown="return numberonly('Maths_Examgrade<?php echo $i ?>')" onfocus="showtitle('Maths_Examgrade_<?php echo $i ?>','MATHS EXAMGRADE')"/>
                  </div></td>
              <td align="left"><?php echo $field['Maths_Totalgrade'] ?>
                  <div id ="Maths_Totalgrade<?php echo $i;?>" style="display:<? if($Class > 2){?>block<? }else {?>none;<? }?>">
                    <input type="text" name="Maths_Totalgrade<?php echo $i ?>" id="Maths_Totalgrade_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo $Maths_Totalgrade ?>" onkeyup="markcheck('<?php echo "#Maths_Totalgrade_".$i ?>')" onkeydown="return numberonly('Maths_Totalgrade<?php echo $i ?>')" 
							   onfocus="showtitle('Maths_Totalgrade_<?php echo $i ?>','MATHS TOTALGRADE')"/>
                  </div></td>
              <td align="left"><?php echo $field['Science_Classwrk'] ?>
                  <div id ="Science_Classwrk<?php echo $i;?>" style="display:<? if($Class > 2){?>block<? }else {?>none;<? }?>">
                    <input type="text" name="Science_Classwrk<?php echo $i ?>" id="Science_Classwrk_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo $Science_Classwrk ?>" onkeyup="markcheck('<?php echo "#Science_Classwrk_".$i ?>')" onkeydown="return numberonly('Science_Classwrk<?php echo $i ?>')" onfocus="showtitle('Science_Classwrk_<?php echo $i ?>','SCIENCE CLASSWORK')"/>
                  <?php echo $field['Maths_Examgrade'] ?>                  </div></td>
              <td align="left"><?php echo $field['Science_Examgrade'] ?>
                  <div id ="Science_Examgrade<?php echo $i;?>" style="display:<? if($Class > 2){?>block<? }else {?>none;<? }?>">
                    <input type="text" name="Science_Examgrade<?php echo $i ?>" id="Science_Examgrade_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo $Science_Examgrade ?>" onkeyup="markcheck('<?php echo "#Science_Examgrade_".$i ?>')" onkeydown="return numberonly('Science_Examgrade<?php echo $i ?>')"
							 onfocus="showtitle('Science_Examgrade_<?php echo $i ?>','SCIENCE EXAMGRADE')"/>
                  </div></td>
              <td align="left"><?php echo $field['Science_Totalgrade'] ?>
                  <div id ="Science_Totalgrade<?php echo $i;?>" style="display:<? if($Class > 2){?>block<? }else {?>none;<? }?>">
                    <input type="text" name="Science_Totalgrade<?php echo $i ?>" id="Science_Totalgrade_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo $Science_Totalgrade ?>"onkeyup="markcheck('<?php echo "#Science_Totalgrade_".$i ?>')"  onkeydown="return numberonly('Science_Totalgrade<?php echo $i ?>')" onfocus="showtitle('Science_Totalgrade_<?php echo $i ?>','SCIENCE TOTALGRADE')"/>
                  </div></td>
              <td align="left"><?php echo $field['Sscience_Classwrk'] ?>
                  <div id ="Sscience_Classwrk<?php echo $i;?>" style="display:<? if($Class > 4){?>block<? }else {?>none;<? }?>">
                    <input type="text" name="Sscience_Classwrk<?php echo $i ?>" id="Sscience_Classwrk_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo $Sscience_Classwrk ?>" onkeyup="markcheck('<?php echo "#Sscience_Classwrk_".$i ?>')" onkeydown="return numberonly('Sscience_Classwrk<?php echo $i ?>')"
							  onfocus="showtitle('Sscience_Classwrk_<?php echo $i ?>','SSCIENCE CLASSWORK')"/>
                  </div></td>
              <td align="left"><?php echo $field['Sscience_Examgrade'] ?>
                  <div id ="Sscience_Examgrade<?php echo $i;?>" style="display:<? if($Class > 4){?>block<? }else {?>none;<? }?>">
                    <input type="text" name="Sscience_Examgrade<?php echo $i ?>" id="Sscience_Examgrade_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo $Sscience_Examgrade ?>"  onkeyup="markcheck('<?php echo "#Sscience_Examgrade_".$i ?>')" onkeydown="return numberonly('Sscience_Examgrade<?php echo $i ?>')"
							  onfocus="showtitle('Sscience_Examgrade_<?php echo $i ?>','SSCIENCE EXAMGRADE')"/>
                  </div></td>
              <td align="left"><?php echo $field['Sscience_Totalgrade'] ?>
                  <div id ="Sscience_Totalgrade<?php echo $i;?>" style="display:<? if($Class > 4){?>block<? }else {?>none;<? }?>">
                    <input type="text" name="Sscience_Totalgrade<?php echo $i ?>" id="Sscience_Totalgrade_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo $Sscience_Totalgrade ?>" onkeyup="markcheck('<?php echo "#Sscience_Totalgrade_".$i ?>')" onkeydown="return numberonly('Sscience_Totalgrade<?php echo $i ?>')" 
							  onfocus="showtitle('Sscience_Totalgrade_<?php echo $i ?>','SSCIENCE TOTALGRADE')"/>
                  </div></td>
              <td align="left"><?php echo $field['Computer_Classwrk'] ?>
                  <div id ="Computer_Classwrk<?php echo $i;?>" style="display:<? if($Class > 2){?>block<? }else {?>none;<? }?>">
                    <input type="text" name="Computer_Classwrk<?php echo $i ?>" id="Computer_Classwrk_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo $Computer_Classwrk ?>" onkeyup="markcheck('<?php echo "#Computer_Classwrk_".$i ?>')" onkeydown="return numberonly('Computer_Classwrk<?php echo $i ?>')"
							  onfocus="showtitle('Computer_Classwrk_<?php echo $i ?>','COMPUTER CLASSWORK')"/>
                  </div></td>
              <td align="left"><?php echo $field['Computer_Examgrade'] ?>
                  <div id ="Computer_Examgrade<?php echo $i;?>" style="display:<? if($Class > 2){?>block<? }else {?>none;<? }?>">
                    <input type="text" name="Computer_Examgrade<?php echo $i ?>" id="Computer_Examgrade_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo $Computer_Examgrade ?>" onkeyup="markcheck('<?php echo "#Computer_Examgrade_".$i ?>')" onkeydown="return numberonly('Computer_Examgrade<?php echo $i ?>')" 
							  onfocus="showtitle('Computer_Examgrade_<?php echo $i ?>','COMPUTER EXAMGARDE')"/>
                  </div></td>
              <td align="left"><?php echo $field['Computer_Totalgrade'] ?>
                  <div id ="Computer_Totalgrade<?php echo $i;?>" style="display:<? if($Class > 2){?>block<? }else {?>none;<? }?>">
                    <input type="text" name="Computer_Totalgrade<?php echo $i ?>" id="Computer_Totalgrade_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo $Computer_Totalgrade ?>" onkeyup="markcheck('<?php echo "#Computer_Totalgrade_".$i ?>')" onkeydown="return numberonly('Computer_Totalgrade<?php echo $i ?>')" 
							   onfocus="showtitle('Computer_Totalgrade_<?php echo $i ?>','COMPUTER TOTALGRADE')"/>
                  </div></td>
              <td align="left"><?php echo $field['Project_Classwrk'] ?>
                  <div id ="Project_Classwrk<?php echo $i;?>" style="display:<? if($Class > 2){?>block<? }else {?>none;<? }?>">
                    <input type="text" name="Project_Classwrk<?php echo $i ?>" id="Project_Classwrk_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo $Project_Classwrk ?>" onkeyup="markcheck('<?php echo "#Project_Classwrk_".$i ?>')" onkeydown="return numberonly('Project_Classwrk<?php echo $i ?>')"  onfocus="showtitle('Project_Classwrk_<?php echo $i ?>','PROJECT CLASSWORK')"/>
                  </div></td>
              <td align="left"><?php echo $field['Project_Examgrade'] ?>
                  <div id ="Project_Examgrade<?php echo $i;?>" style="display:<? if($Class > 2){?>block<? }else {?>none;<? }?>">
                    <input type="text" name="Project_Examgrade<?php echo $i ?>" id="Project_Examgrade_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo $Project_Examgrade ?>" onkeyup="markcheck('<?php echo "#Project_Examgrade_".$i ?>')" onkeydown="return numberonly('Project_Examgrade<?php echo $i ?>')" 
							  onfocus="showtitle('Project_Examgrade_<?php echo $i ?>','PROJECT EXAMGRADE')"/>
                  </div></td>
              <td align="left"><?php echo $field['Project_Totalgrade'] ?>
                  <div id ="Project_Totalgrade<?php echo $i;?>" style="display:<? if($Class > 2){?>block<? }else {?>none;<? }?>">
                    <input type="text" name="Project_Totalgrade<?php echo $i ?>" id="Project_Totalgrade_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo $Project_Totalgrade ?>" onkeyup="markcheck('<?php echo "#Project_Totalgrade_".$i ?>')" onkeydown="return numberonly('Project_Totalgrade<?php echo $i ?>')" 
							  onfocus="showtitle('Project_Totalgrade_<?php echo $i ?>','PROJECT TOTALGRADE')"/>
                  </div></td>
            </tr>
  <td align="left"><?php echo $field['Eng_Comprehension'] ?>
          <div id ="Eng_Comprehension<?php echo $i;?>" style="display:<? if($Class > 2){?>none<? }else {?>block;<? }?>">
            <input type="text" name="Eng_Comprehension<?php echo $i ?>" id="Eng_Comprehension_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo $Eng_Comprehension ?>"  onkeyup="markcheck('<?php echo "#Eng_Comprehension_".$i ?>')" onkeydown="return numberonly('Eng_Comprehension<?php echo $i ?>')"
							   onfocus="showtitle('Eng_Comprehension_<?php echo $i ?>','ENGLISH COMPREHENSION')"/>
          </div></td>
      <td align="left"><?php echo $field['Eng_Speaking'] ?>
          <div id ="Eng_Speaking<?php echo $i;?>" style="display:<? if($Class > 2){?>none<? }else {?>block;<? }?>">
            <input type="text" name="Eng_Speaking<?php echo $i ?>" id="Eng_Speaking_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo $Eng_Speaking ?>"  onkeyup="markcheck('<?php echo "#Eng_Speaking_".$i ?>')" onkeydown="return numberonly('Eng_Speaking<?php echo $i ?>')"
							   onfocus="showtitle('Eng_Speaking_<?php echo $i ?>','ENGLISH SPEAKING')"/>
          </div></td>
    <td align="left"><?php echo $field['Eng_Reading'] ?>
        <div id ="Eng_Reading<?php echo $i;?>" style="display:<? if($Class > 2){?>none<? }else {?>block;<? }?>">
            <input type="text" name="Eng_Reading<?php echo $i ?>" id="Eng_Reading_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo $Eng_Reading ?>"  onkeyup="markcheck('<?php echo "#Eng_Reading_".$i ?>')" onkeydown="return numberonly('Eng_Reading<?php echo $i ?>')"
							   onfocus="showtitle('Eng_Reading_<?php echo $i ?>','ENGLISH READING')"/>
        </div></td>
    <td align="left"><?php echo $field['Eng_Writing'] ?>
        <div id ="Eng_Writing<?php echo $i;?>" style="display:<? if($Class > 2){?>none<? }else {?>block;<? }?>">
            <input type="text" name="Eng_Writing<?php echo $i ?>" id="Eng_Writing_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo $Eng_Writing ?>"  onkeyup="markcheck('<?php echo "#Eng_Writing_".$i ?>')" onkeydown="return numberonly('Eng_Writing<?php echo $i ?>')"
							   onfocus="showtitle('Eng_Writing_<?php echo $i ?>','ENGLISH WRITING')"/>
        </div></td>
    <td align="left"><?php echo $field['Tam_Comprehension'] ?>
        <div id ="Tam_Comprehension<?php echo $i;?>" style="display:<? if($Class > 2){?>none<? }else {?>block;<? }?>">
            <input type="text" name="Tam_Comprehension<?php echo $i ?>" id="Tam_Comprehension_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo $Tam_Comprehension ?>"  onkeyup="markcheck('<?php echo "#Tam_Comprehension_".$i ?>')" onkeydown="return numberonly('Tam_Comprehension<?php echo $i ?>')"
							   onfocus="showtitle('Tam_Comprehension_<?php echo $i ?>','TAMIL COMPREHENSION')"/>
        </div></td>
    <td align="left"><?php echo $field['Tam_Speaking'] ?>
        <div id ="Tam_Speaking<?php echo $i;?>" style="display:<? if($Class > 2){?>none<? }else {?>block;<? }?>">
            <input type="text" name="Tam_Speaking<?php echo $i ?>" id="Tam_Speaking_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo $Tam_Speaking ?>"  onkeyup="markcheck('<?php echo "#Tam_Speaking_".$i ?>')" onkeydown="return numberonly('Tam_Speaking<?php echo $i ?>')"
							   onfocus="showtitle('Tam_Speaking_<?php echo $i ?>','TAMIL SPEAKING')"/>
        </div></td>
    <td align="left"><?php echo $field['Tam_Reading'] ?>
        <div id ="Tam_Reading<?php echo $i;?>" style="display:<? if($Class > 2){?>none<? }else {?>block;<? }?>">
            <input type="text" name="Tam_Reading<?php echo $i ?>" id="Tam_Reading_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo $Tam_Reading ?>"  onkeyup="markcheck('<?php echo "#Tam_Reading_".$i ?>')" onkeydown="return numberonly('Tam_Reading<?php echo $i ?>')"
							   onfocus="showtitle('Tam_Reading_<?php echo $i ?>','TAMIL READING')"/>
        </div></td>
    <td align="left"><?php echo $field['Tam_Writing'] ?>
        <div id ="Tam_Writing<?php echo $i;?>" style="display:<? if($Class > 2){?>none<? }else {?>block;<? }?>">
            <input type="text" name="Tam_Writing<?php echo $i ?>" id="Tam_Writing_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo $Tam_Writing ?>"  onkeyup="markcheck('<?php echo "#Tam_Writing_".$i ?>')" onkeydown="return numberonly('Tam_Writing<?php echo $i ?>')"
							   onfocus="showtitle('Tam_Writing_<?php echo $i ?>','TAMIL WRITING')"/>
        </div></td>
    <td align="left"><?php echo $field['(Mat_Comprehension,'] ?>
        <div id ="Mat_Comprehension<?php echo $i;?>" style="display:<? if($Class > 2){?>none<? }else {?>block;<? }?>">
            <input type="text" name="Mat_Comprehension<?php echo $i ?>" id="Mat_Comprehension_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo $Mat_Comprehension ?>"  onkeyup="markcheck('<?php echo "#Mat_Comprehension_".$i ?>')" onkeydown="return numberonly('Mat_Comprehension<?php echo $i ?>')"
							   onfocus="showtitle('Mat_Comprehension_<?php echo $i ?>','MATHS COMPREHENSION')"/>
        </div></td>
    <td align="left"><?php echo $field['Scn_Comprehension'] ?>
        <div id ="Scn_Comprehension<?php echo $i;?>" style="display:<? if($Class > 2){?>none<? }else {?>block;<? }?>">
            <input type="text" name="Scn_Comprehension<?php echo $i ?>" id="Scn_Comprehension_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo $Scn_Comprehension ?>"  onkeyup="markcheck('<?php echo "#Scn_Comprehension_".$i ?>')" onkeydown="return numberonly('Scn_Comprehension<?php echo $i ?>')"
							   onfocus="showtitle('Scn_Comprehension_<?php echo $i ?>','SCIENCE COMPREHENSION')"/>
        </div></td>
  </tr>
  <?php	         }   ?>
  <tr>
    <td colspan="5" align="left"><table width="50%">
      <tr>
        <td><input type="Submit" name="Save" value="Save" id="Save" class="winbutton_go" /></td>
        <td><input type="Submit" name="Cancel" value="Cancel" class="winbutton_go"/>        </td>
      </tr>
    </table></td>
  </tr>
  <?php    }
		}	
		else 
		{ 	?>
  <tr>
    <td colspan="9" align="center" class="error">No Records Found...</td>
  </tr>
  <?php  }   ?>
  <input type="hidden" name="tot_rec" value="<?php echo $tot_rec ?>" />
          </table>
          <br /><br />
    </div></td></tr>
<?php }?>    
    </table>
    </form>
    </body>
</html>
<?php include("../Includes/copyright.php"); ?>
