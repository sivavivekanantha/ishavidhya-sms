<?php session_start(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>Isha Foundation - A Non-profit Organization</title>
<?php include("../includes/header.php"); title('Student Management','Renewal Mail Tracking',2,1,2);
?>

<script>
$(document).ready(function(){

    $('#example').dataTable( {
        "sScrollY": '400px',
        "bPaginate": false,
	"bSort": false,
    } );	
});	
</script>
</head>
<body style="margin:0;">
        <table width="90%" height="450" border="0" align="center" cellpadding="1" cellspacing="3">
		<?php titleheader("Renewal Mail Tracking",0); ?>
        <tr><td valign="top">
			<table width="100%" border="0" align="center" cellpadding="5" cellspacing="1" id="example">
            	<colgroup><col width=3%><col width=10%><col width=15%><col width=8%><col width=5%><col width=15%><col width=10%><col width=10%>
                <col width=15%><col width=10%></colgroup>
                <thead><th>S.No</th><th align="center">Admission Number</th>
                <th>Student Name</th><th>Donor Code</th><th>Title</th><th align="center">Donor First Name</th><th align="center">Donor Middle Name</th>
                <th align="center">Donor Last Name</th><th align="center">Email Id</th><!--<th align="center">Allocation Mail</th>
                <th align="center">Report Card</th><th align="center">Renewal Mail</th><th align="center">Renew Date</th><th align="center">Donation Type</th>-->
				<th align="center">Mail Tracking Status</th>
                </thead><tbody>
        	                	        
<?php 			$query = mssql_init('sp_GetDonorMappingView',$mssql);
				$result = mssql_execute($query);
                mssql_free_statement($query);
	            $rs_cnt = mssql_num_rows($result);
                $colorflag = 0;
                $i = 0;
                while($field = mssql_fetch_array($result))
                {	$i  +=1;	$colorflag+=1;
                    $tot_rec = $i;	?>
                    <tr class=<?php if($colorflag%2==0) { echo "row1"; } else { echo "row2"; } ?> valign="center">
					<td><?php echo $i; ?></td>
                    <td><?php echo $field['Admission_No'] ?></td>
                    <td><?php echo $field['Name'] ?></td>
                    <td><?php echo $field['Donor_Code'] ?>
                    <div id ="donor_code_<?php echo $i;?>" style="display:none;"><input type="text" name="donor_code_<?php echo $i ?>" id="donor_code_<?php echo $i ?>"  size="6" maxlength="25" value="<?php echo $field['Donor_Code']?>"></div></td>
                    
                    <td ><?php echo $field['Title'] ?>
                    <div id ="title_<?php echo $i;?>" style="display:none;"><input type="text" name="title_<?php echo $i ?>" id="title_<?php echo $i ?>"  size="5" maxlength="5" value="<?php echo $field['Title']?>"
                    onkeydown="return alphaonly('title_<?php echo $i ?>')"></div></td>
                    <td ><?php echo $field['First_Name'] ?>
                    <div id ="first_name_<?php echo $i;?>" style="display:none;"><input type="text" name="first_name_<?php echo $i ?>" id="first_name_<?php echo $i ?>"  size="13" maxlength="25" value="<?php echo $field['First_Name']?>"
                    onkeydown="return alphaonly('first_name_<?php echo $i ?>')"></div></td>
                    <td ><?php echo $field['Middle_Name'] ?>
                    <div id ="middle_name_<?php echo $i;?>" style="display:none;"><input type="text" name="middle_name_<?php echo $i ?>" id="middle_name_<?php echo $i ?>"  size="7" maxlength="15" value="<?php echo $field['Middle_Name']?>"
                    onkeydown="return alphaonly('middle_name_<?php echo $i ?>')"></div></td>
                    <td ><?php echo $field['Last_Name'] ?>
                    <div id ="last_name_<?php echo $i;?>" style="display:none;"><input type="text" name="last_name_<?php echo $i ?>" id="last_name_<?php echo $i ?>"  size="7" maxlength="15" value="<?php echo $field['Last_Name']?>"
                    onkeydown="return alphaonly('last_name_<?php echo $i ?>')"></div></td>
                    <td ><?php echo $field['Email_id'] ?>
                    <div id ="email_id_<?php echo $i;?>" style="display:none;"><input type="text" name="email_id_<?php echo $i ?>" id="email_id_<?php echo $i ?>"  size="25" maxlength="50" value="<?php echo $field['Email_id']?>"></div></td>
					  <td> <?php  if(strlen($field['Read_Time'])==26) echo date('d-M-Y h:i A', strtotime( $field['Read_Time'])); else echo "<span class='mand'>Still not open</span>";?></td>
                </tr>
          <?php } ?></tbody>
				</table>
                </td>
                </tr></table>
<?php include("../includes/copyright.php"); ?>
 </body></html>