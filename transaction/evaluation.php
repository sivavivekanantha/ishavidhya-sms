<?php session_start(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<?php include ("../includes/header.php"); ?>
<?php title('Student Management', 'Scholarship Evaluation', 2, 1, 2); ?>
<script>

$(document).ready(function() {
    $('#example').dataTable( {
        "sScrollY": "400px",
        "bPaginate": false
    } );
} );	

 //open new window for scholarship eligible form 
function displayPopup(windowUrl,windowName)
{
	var newWindow=window.open(windowUrl,windowName,"top=175,left=175,width=900,height=450,menubar=no,toolbar=no,scrollbars=yes,resizable=no,titlebar=0,status=no");
}

function displayAlert(obj1,obj2)
{
	var parent=obj1;
	var volunteer=obj2;
	if(parent==0 && volunteer==0)
	{
		alert("Please fill Parent and Volunteer Scholarship Form");
	}
	else if(parent==0 && volunteer==1)
	{
		alert("Please fill Parent Scholarship Form");
	}
	else if(parent==1 && volunteer==0)
	{
		alert("Please fill Volunteer Scholarship Form");
	}


}
</script>
</head>
<body>
 <?php $school_id = $class = $action = 0;
$school_id = $_GET['school_id'];
$class = $_GET['class'];
$action = $_GET['action']; ?>
       
 <table width="80%" border="0" align="center" cellpadding="1" cellspacing="1"  id="example" >
<tr><td><?php titleheader("Scholarship Evaluation", 0); ?></td></tr>
<tr><td align="right">
    <a href="evaluation.php"  class="link">First</a>&nbsp;&nbsp;
    <a href="#" onclick="window.history.back()" class="link">Back</a></td></tr>
<tr>
    <td align="center">
        <div style="width:950px;  border:thin;" id="evaluation" align="center">
        <form id="myform" name="myform" method="post" action="evaluation.php" >
            <table width="100%" border="0" cellspacing="1" cellpadding="3" >
            <tr>
            <td>
              <table width="100%" border="0" cellspacing="1" cellpadding="3" align="center" >
              <tr>
              <?php if($action == 1 or $action == 4) { ?>
                <th colspan="5" align="center"></th>
                <th colspan="2" align="center">System View</th>
              <?php } else
    if($action == 2) { ?>
                <th colspan="3" align="center"></th>
                <th colspan="3" align="center">System View</th>
              <?php     } else { ?>
              <tr><th colspan="2" align="center"></th>
                <th colspan="3" align="center">System View</th>
              <?php     } ?>
              <th colspan="3" align="center">Final View</th></tr>
              <tr>
              
                <?php  if($action != 1 and $action != 4) { ?><th>School Name</th> <?php     } else { ?> <th>S.No.</th><?php     } ?>
                <?php     if($action == 1 or $action == 4) { ?>
                    <th>Name</th>
                    <th>Admission Number</th>
                <?php     }
if($action > 0) { ?>  <th>Class</th> <?php }
if($action == 1 or $action == 4) { ?>
                    <th>Form Status</th>
                    <th>System View</th>
                    <th>Volunteer View</th>
                                   
            <?php } else { ?>
                <th>Applied</th>
                <th>Approved</th>
                <th>Rejected</th>
                <th>Reconsidered</th>
                
            <?php } ?>                
      	        <th>Final Eligible</th>
                <th>Recommendation</th>
                <th>Reconsideration</th>
               </tr>
        <?php
		mssql_free_result($result);
		 $query = mssql_init('[sp_Evaluation]', $mssql);
mssql_bind($query, '@school_id', $school_id, SQLINT4, false, false, 5);
mssql_bind($query, '@class_code', $class, SQLINT4, false, false, 5);
mssql_bind($query, '@Action', $action, SQLINT4, false, false, 5);
$result = mssql_execute($query);
mssql_free_statement($query);
$colorflag = 0;
$i = 0;
while ($field = mssql_fetch_array($result)) {
    $colorflag += 1;
    $i = $i + 1; ?>
                    <tr class=<?php     if($colorflag % 2 == 0) {
        echo "row1";
    } else {
        echo "row2";
    } ?>>
                    
                    <?php     if($action != 1 and $action != 4) { ?>
                    <?php         if($action == 1 or $action == 3) { ?>
                        <td width="13%" align="center" class="link" onmousemove="this.className='link_over'"  onmouseout="this.className='link'">
                      <?php             echo "<a class='link' href='#' onClick=\"javascript:displayPopup('scholarship_eligible.php?name=" .
                $field['Name'] . "&admn=" . $field['Admission_No'] . "&student_id=" . $field['Student_Id'] .
                "&class=" . $field['Class'] . "','Scholarship Eligible');\">" . $field['Name'] .
                "</a>";
        } else {
            if($action == 0) { ?>
                            <td width="13%" align="center" class="link" onmousemove="this.className='link_over'" onmouseout="this.className='link'">
                             <?php                 echo
                    "<a  class='link' href='evaluation.php?action=2&school_id=" . $field['School_id'] .
                    "'>" . $field['School_name'] . "</a>";
            } else { ?>
                                    <td width="13%" align="center">
                             <?php                 echo $field['School_name'];
            }
        } ?>
                        </td>
                    <?php     } else { ?>
                        <td width="12%" align="center"> <?php         echo $i; ?></td> 
                    <?php     }
    if($action == 1 or $action == 4) { ?>
                        <td width="13%" align="left" onmousemove="this.className='link_over'"  onmouseout="this.className='link'"><?php         if($field['Parents'] ==
            1 && $field['Volunteer'] == 1) {
            echo "<a class='link' href='#' onClick=\"javascript:displayPopup('scholarship_eligible.php?name=" .
                $field['Name'] . "&admn=" . $field['Admission_No'] . "&student_id=" . $field['Student_Id'] .
                "&class=" . $field['Class'] . "','Scholarship Eligible');\">" . $field['Name'] .
                "</a>";
        } else {
            echo "<a class='link' href='#' onClick=\"javascript:displayAlert('" . $field['Parents'] .
                "','" . $field['Volunteer'] . "');\">" . $field['Name'] . "</a>";
            //echo '<script language="javascript">alert("Do you want this?")</script>';
        } ?></td>
                        <td width="13%" align="center"><?php         echo $field['Admission_No']; ?></td>
                        <td width="10%" align="center"><?php         echo $field['Class']; ?></td>         
                    <?php     } elseif($action == 2) { ?>
                        <td width="10%" align="center" onmousemove="this.className='link_over'"  onmouseout="this.className='link'">
                        <?php         echo
            "<a  class='link' href='evaluation.php?action=1&school_id=" . $field['School_id'] .
            "&class=" . $field['Class_Code'] . "'>" . $field['Class'] . "</a>"; ?>
                        </td> 
                        
                    <?php     }
    if($action == 1 or $action == 4) { ?>                        
                        <td width="10%" align="center">
         
                        <a href="#" onclick='javascript:window.open("scholarship.php?frm=1&Form_Code=1&Stu_Id=<?php         echo
            $field['Student_Id'] ?>&","","top=175,left=175,width=950,height=700,menubar=no,toolbar=no,scrollbars=yes,resizable=no,titlebar=0,status=no")'> 
    <span class=<?php if($field['Parents'] == 1) echo "fill";
        else  echo "notfill" ?> >P</span></a>    &nbsp;&nbsp;  <a href="#" onclick='javascript:window.open("scholarship.php?frm=1&Form_Code=3&Stu_Id=<?php                 echo
                $field['Student_Id'] ?>&","","top=175,left=175,width=950,height=700,menubar=no,toolbar=no,scrollbars=yes,resizable=no,titlebar=0,status=no")'>
                        <span class=<?php                 if($field['Volunteer'] == 1) echo
                    "fill";
            else  echo "notfill" ?> >V</span></a>
                        </td>
                            
                        <td width="10%" align="center" nowrap> 
				<?php if($field['System_View'] == 'Y') echo 'Yes'; else  echo 'No'; echo "<br><span class='silver'>".$field['System_Reject']."</span>";?></td>
                        <td width="8%" align="center">
                <?php         if($field['Volunteer_View'] == 'Y') echo 'Yes';
        elseif($field['Volunteer_View'] == 'N') echo "No"; else "-"; ?></td>
                        <td width="9%" align="center">
                <?php         if($field['Child_Eligible'] == 'Y') echo 'Yes';
        elseif($field['Child_Eligible'] == 'N')  echo "No"; else echo "-"; ?></td>
                        <td width="8%" align="center">
                <?php         if($field['Recommendation'] == 'Y') echo "Yes";
        elseif($field['Recommendation'] == 'N')  echo "No"; else echo "-"; ?> </td>
                        <td align="center">
                <?php         if($field['Reconsideration'] == 'Y') echo "Yes";
        elseif($field['Reconsideration'] == 'N')  echo "No"; else echo "-"; ?></td>
                <?php     } else {
        if($action == 0) { ?>
                        <td width="13%" align="center" class="link" onmousemove="this.className='link_over'"  onmouseout="this.className='link'"> <?php             echo
                "<a  class='link' href='evaluation.php?school_id=" . $field['School_id'] .
                "&action=4'>" . $field['Applied'] . "</a>"; ?></td>
                <?php         } else { ?>
                        <td width="13%" align="center" > 
<?php             echo $field['Applied']; ?></td>
                <?php         } ?>
                        <td width="10%" align="center"> <?php         echo $field['approved']; ?></td>
                        <td width="10%" align="center">	<?php         echo $field['rejected']; ?></td>
        	            <td width="8%" align="center">	<?php         echo $field['reconsidered']; ?></td>
                        <td width="9%" align="center">	<?php         echo $field['eligible']; ?></td>
                        <td width="8%" align="center">	<?php         echo $field['recommendation']; ?></td>
                        <td width="8%" align="center">	<?php         echo $field['reconsideration']; ?></td>
                <?php     } ?>
                    </tr>
            <?php } ?>
            </table>
       </td></tr>
    </table>
</form>
</div>
</td></tr>
</table>
</body>
</html>
<?php include ("../includes/copyright.php"); ?>