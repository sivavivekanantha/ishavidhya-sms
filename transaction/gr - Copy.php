﻿<!DOCTYPE html PUBLIC “-//W3C//DTD XHTML 1.0 Transitional//EN” “http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd”>
<html xmlns=”http://www.w3.org/1999/xhtml”>
<head>
<meta http-equiv=”Content-Type” content=”text/html; charset=utf-8″ / >
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<title>Isha Vidhya</title>
<style type="text/css">
<!--
body
{
	margin-left: 00px;
	margin-top: 0px;
	margin-right: 00px;
	margin-bottom: 0px;
}
.bg
{

background:#CCCCCC;
color:#BD3D03;
font-size:14px;

}
.val
{
font-size:15px;
}
.bg1
{
font-size:14px;
}
.nor
{
color:#bd3d03;
font-size:14px;

}
.nor1
{
font-size:11px;
}
.style2 {color: #BD3D03}
.style3 {color: #FFFFFF}
-->
</style></head>

<body>
<?php /* <p align='center' style='color:blue;'><a href='Progress_report.php'>Back</a></p> */ ?>

  <?php include ("../Includes/header1.php"); 
  
function report($sloc,$sname,$speriod,$sclass,$ssection,$steacher,$sdob,$sadmno,$ecomment1,$tcomment1,$ecomment2,$tcomment2,$ecomment3,$tcomment3,$term1,$wday1,$term2,$wday2,$term3,$wday3,$se1,$se2,$se3,$wn1,$wn2,$wn3,$em1,$em2,$em3,$rp1,$rp2,$rp3,$db1,$db2,$db3,$as1,$as2,$as3,$pt1,$pt2,$pt3,$si1,$si2,$si3,$hy1,$hy2,$hy3,$gw1,$gw2,$gw3,$sp1,$sp2,$sp3,$cr1,$cr2,$cr3,$cs1,$cs2,$cs3,$pgt1,$pgt2,$pgt3,$att1,$att2,$att3,$ecw1,$eeg1,$etg1,$ecw2,$eeg2,$etg2,$ecw3,$eeg3,$etg3,$tcw1,$teg1,$ttg1,$tcw2,$teg2,$ttg2,$tcw3,$teg3,$ttg3,$mcw1,$meg1,$mtg1,$mcw2,$meg2,$mtg2,$mcw3,$meg3,$mtg3,$scw1,$seg1,$stg1,$scw2,$seg2,$stg2,$scw3,$seg3,$stg3,$sscw1,$sseg1,$sstg1,$sscw2,$sseg2,$sstg2,$sscw3,$sseg3,$sstg3,$ccw1,$ceg1,$ctg1,$ccw2,$ceg2,$ctg2,$ccw3,$ceg3,$ctg3,$pcw1,$peg1,$ptg1,$pcw2,$peg2,$ptg2,$pcw3,$peg3,$ptg3,$part1,$fms1,$gms1,$ec1,$es1,$er1,$ew1,$tc1,$ts1,$tr1,$tw1,$mc1,$sc1,$pd1,$part2,$fms2,$gms2,$ec2,$es2,$er2,$ew2,$tc2,$ts2,$tr2,$tw2,$mc2,$sc2,$pd2,$part3,$fms3,$gms3,$ec3,$es3,$er3,$ew3,$tc3,$ts3,$tr3,$tw3,$mc3,$sc3,$pd3)
{ 
 $rpt = "<table width='50%' border='0' cellspacing='0' style='background:url(bg.jpg) repeat-x; border:solid 1px #990000;' align='center' cellpadding='0'>
  <tr>
    <td><table class='nor' width='100%' border='0' cellspacing='0' cellpadding='0' >
  <tr>
    <td><table width='98%' border='0' align='center' cellpadding='0' cellspacing='0'>
        <tr>
        <td width='49%' valign='top'><table width='98%' border='0' align='center' cellpadding='0' cellspacing='0'>
          <tr><td colspan='3' align='center'> </td></tr>
          <tr>
            <td width='18%'><img src='../imgs/image001.png' alt='' width='114' height='93' /></td>
            <td width='82%'><div align='center' style='padding-right:10px;'><strong style='font-size:14px'>ISHA VIDHYA MATRICULATION SCHOOL<br />
                  COMPREHENSIVE EVALUATION REPORT<br />
              ஈஷா வித்யா மெட்ரிகுலேஷன் பள்ளி<br />
              விரிவான மதிப்பீட்டு அறிக்கை</strong></div></td>
              <td>      
              <img src='../Photos/".$Student_Id."/".$Student_Id.".jpg' width='114' height='93'/>
              </td>
          </tr>
          <tr>
            <td colspan='2' align='center'>&nbsp;</td>
          </tr>
          <tr>
            <td colspan='3' align='center'><b>".$sloc."</b></td>
          </tr>
          <tr>
            <td colspan='2'><div align='center'><br />
            </div></td>
          </tr>
          <tr>
            <td colspan='3'><table width='76%' style='border: dashed 1px #990000;' border='0' cellspacing='0' cellpadding='9' align='center'>
              <tr>
                <td width='50%' class='bg1'>Academic Year / <span class='nor1'>கல்வி ஆண்டு</span></td><td class='val'><b>".$speriod."</b></span></td>
              </tr>
              <tr>
                <td class='bg1'>Name / <span class='nor1'>பெயர்</span></td><td class='val'><b>".  $sname ."</b></span></td>
              </tr>
              <tr>
				<td class='bg1'>Class / <span class='nor1'>வகுப்பு</span></td><td class='val'><b>".  $sclass  ."</b></td>
              </tr>
			  <tr>
                <td class='bg1'>Section / <span class='nor1'>பிரிவு</span></td><td class='val'> <b>".$ssection."</b></td>
              </tr>
              <tr>
                <td class='bg1'>Date of Birth / <span class='nor1'>பிறந்த தேதி</span> </td><td class='val'>  <b>".$sdob."</b></td>
              </tr>
              <tr>
                <td class='bg1'>Admission No / <span class='nor1'>அனுமதி எண்.</span></td><td class='val'> <b>".$sadmno."</b></td>
              </tr>
              <tr>
                <td class='bg1'>Class Teacher / <span class='nor1'>வகுப்பாசிரியர்</span> </td><td class='val'><b>".$steacher."</b></td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td colspan='3' class='bg1' ><p>&nbsp </p>
              <p><br />
                Parents are requested to review this card each term, sign on page 3 and return to the school within 7 days. Clarifications can be discussed during &quotOpen Day&quot  or by making an appointment through the Administrative office to meet the Principal.<br /></p>
                 <p><span class='nor1'>
                பெற்றோர்கள் இந்த மதிப்பீட்டு அறிக்கையை பார்த்தபின், மூன்றாம் பக்கத்தில் கையொப்பம் இட்டு ஏழு நாட்களுக்குள் திருப்பிக் கொடுக்குமாறு கேட்டுக் கொள்ளப்படுகிறார்கள். ஐயங்கள் ஏதேனும் இருந்தால் அதை பெற்றோர் ஆசிரியர் சந்திப்பில் ஆசிரியரிடம் கலந்தாலோசிக்கவும் அல்லது பள்ளி நிர்வாகியின் மூலமாக  பள்ளி முதல்வரை சந்திக்கவும்.</span></p></td>
          </tr>
          <tr>
            <td colspan='3'>&nbsp </td>
          </tr>
        </table></td>
      </tr>
      <tr><td width='5%' valign='top'>&nbsp </td></tr>
      <tr>
        <td width='50%' valign='top' align='center'><table width='100%' border='0' cellspacing='0' cellpadding='0'>
          <tr>
            <td ><p><strong>Note to parents</strong>:  &quot General Development&quot  reflects the child's overall development and is not related to any one subject. &quot Scholastic Achievement&quot  shows the child's progress in each subject throughout the term as the &quot Term Grade&quot .  The &quot Term Grade&quot  is calculated as 50% from the &quot Exam Grade&quot  - the score achieved at the end of term exam, and 50% from &quot Class Work&quot  - assessed throughout the term, reflecting performance in homework, classwork, class participation and quizzes. <span class='nor1'>பெற்றோர்களுக்கு குறிப்பு :           ஒரு பருவத்தில் மாணவர்களின் தேர்ச்சி மற்றும் வகுப்பறை பாடத்தை முடிக்கும் திறன் ஆகியவற்றை ஆசிரியர்  கண்காணித்து வருகிறார். கிண்டர்கார்டன் குழந்தைகளுக்கு பருவ தேர்வு முறை  நடத்தப்படுவதில்லை. குழந்தைகளின் பொதுவான வளர்ச்சி அவர்களின் ஒட்டுமொத்த வளர்ச்சியை குறிக்கின்றது. இந்த வளர்ச்சி குறிப்பிட்ட  எந்த ஒரு பாடத்தோடும் தொடர்புடையதல்ல. ஒவ்வொரு பாடத்திலும் குழந்தை பெறும் தேர்ச்சி அக்குழந்தையின் ஒட்டு மொத்த கல்வி வளர்ச்சியை காட்டுகிறது. இந்த ஒட்டு மொத்த கல்வி வளர்ச்சி  குறிப்பிட்ட வகுப்பிற்கும் பருவத்திற்கும் அக்குழந்தையிடம் இருக்க வேண்டிய தேர்ச்சியையும் அறிவுத்திறனையும் வைத்து நிர்ணயிக்கப்படுகின்றது.</span></p>              </td>
          </tr>
          <tr>
            <td><table width='100%' border='0' cellspacing='0' cellpadding='0'>
               
                <tr>
                  <td align='center'>&nbsp </td>
                </tr>
				<tr>
                  <td align='center'>&nbsp </td>
                </tr>
                <tr>
                  <td align='center'><strong>GENERAL DEVELOPMENT / பொதுவான வளர்ச்சி</strong></td>
                </tr>
               
                <tr>
                  <td align='center' height='3'></td>
                </tr>
                <tr>
                  <td align='center' class='bg'>A+: Excellent  <span class='nor1'>மிக நன்று</span>  A: Above Average  <span class='nor1'>நன்று</span>  B: Average  <span class='nor1'>சராசரி</span> <br />
                    C: Below Average <span class='nor1'>சராசரிக்கும் கீழே</span>  D: Needs Improvement  <span class='nor1'>முயற்சி தேவை</span>.</td>
                </tr>
                <tr>
                  <td><table style='border-collapse:collapse; border: solid 1px #666666; ' width='100%' border='1' cellspacing='0' cellpadding='1'>
                      <tr>
                        <td width='63%' bgcolor='#f8f8f8' class='bg'>SKILL ASSESSMENT / <span style='font-size:10px'>திறன்களை மதிப்பிடுதல்</span></td>
                        <td align='center' bgcolor='#f8f8f8' class='bg'>TERM-1<br/><span class='nor1'>பருவம்-1</span></td>
                        <td align='center' bgcolor='#f8f8f8' class='bg'>TERM-2<br/><span class='nor1'>பருவம்-2</span></td>
                        <td align='center' bgcolor='#f8f8f8' class='bg'>TERM-3<br/><span class='nor1'>பருவம்-3</span></td>
                      </tr>";
                      
					  if($sclass=='LKG' or $sclass=='UKG') {
						$rpt.= "<tr><td>Fine Motor Skills / <span class='nor1'>நுண் இயங்கு திறன்</span></td>
                        <td width='12%' bgcolor='#f8f8f8' align='center'>".  $fms1  ."</td>
                        <td width='12%' bgcolor='#f8f8f8' align='center'>".  $fms2  ."</td>
                        <td width='13%' bgcolor='#f8f8f8' align='center'>".  $fms3  ."</td>
                      </tr>
					  <tr>
                        <td>Gross Motor Skills / <span class='nor1'>ஒட்டு மொத்த இயங்கு திறன்</span></td>
                        <td width='12%' align='center'>".  $gms1  ."</td>
                        <td width='12%' align='center'>".  $gms2  ."</td>
                        <td width='13%' align='center'>".  $gms3  ."</td>
                      </tr>
					   <tr>
                        <td>Participation / <span class='nor1'>பங்கேற்றல்</span></td>
                        <td width='12%' align='center'>".  $part1  ."</td>
                        <td width='12%' align='center'>".  $part2  ."</td>
                        <td width='13%' align='center'>".  $part3  ."</td>
                      </tr>";
					  }
					  if($sclass<>'LKG' and $sclass<>'UKG') { 
                      $rpt.="<tr>
                        <td>Spoken English / <span class='nor1'>ஆங்கிலத்தில் பேசும் திறன்</span></td>
                        <td width='12%' align='center'>".  $se1  ."</td>
                        <td width='12%' align='center'>".  $se2  ."</td>
                        <td width='13%' align='center'>".  $se3  ."</td>
                      </tr>
                      <tr>
                        <td>Writing Neatness / <span class='nor1'>தெளிவாக எழுதுதல்</span></td>
                        <td align='center'>".  $wn1  ."</td>
                        <td align='center'>".  $wn2  ."</td>
                        <td align='center'>".  $wn3  ."</td>
                      </tr>"; } 

                      $rpt.= "<tr>
                        <td>Enthusiasm &amp  Energy / <span class='nor1'>ஆர்வம் மற்றும் ஆற்றல்</span></td>
                        <td align='center'>".  $em1  ."</td>
                        <td align='center'>".  $em2  ."</td>
                        <td align='center'>".  $em3  ."</td>
                      </tr>";
					  if($sclass<>'LKG' and $sclass<>'UKG') { 
                      $rpt.="<tr>
                        <td>Responsibility / <span class='nor1'>பொறுப்பேற்றல்</span></td>
                        <td align='center'>".  $rp1  ."</td>
                        <td align='center'>".  $rp2  ."</td>
                        <td align='center'>".  $rp3  ."</td>
                      </tr>";	} 

                      $rpt.="<tr>
                        <td>Discipline / <span class='nor1'>ஒழுக்கம்</span></td>
                        <td align='center'>".  $db1  ."</td>
                        <td align='center'>".  $db2  ."</td>
                        <td align='center'>".  $db3  ."</td>
                      </tr>
                      <tr>
                        <td>Attention Span / <span class='nor1'>கவனம்</span></td>
                        <td align='center'>".  $as1  ."</td>
                        <td align='center'>".  $as2  ."</td>
                        <td align='center'>".  $as3  ."</td>
                      </tr>
                      <tr>
                        <td>Punctuality / <span class='nor1'>நேரம் தவறாமை</span></td>
                        <td align='center'>".  $pt1  ."</td>
                        <td align='center'>".  $pt2  ."</td>
                        <td align='center'>".  $pt3  ."</td>
                      </tr>
                      <tr>
                        <td>Social Interaction / <span class='nor1'>பழகுதல்</span></td>
                        <td align='center'>".  $si1  ."</td>
                        <td align='center'>".  $si2  ."</td>
                        <td align='center'>".  $si3  ."</td>
                      </tr>
                      <tr>
                        <td>Hygiene / <span class='nor1'>சுகாதாரம்</span></td>
                        <td align='center'>".  $hy1  ."</td>
                        <td align='center'>".  $hy2  ."</td>
                        <td align='center'>".  $hy3  ."</td>
                      </tr>";

					  if($sclass<>'LKG' and $sclass<>'UKG') { 
                      $rpt.="<tr>
                        <td class='bg'>CO-CURRICULAR ACTIVITIES / <br />
                          <span class='nor1'>இணை பாட  செயல்பாடுகள்</span></td>
                        <td align='center' class='bg'>TERM-1<br /><span class='nor1'>பருவம்-1</span></td>
                        <td align='center' class='bg'>TERM-2<br /><span class='nor1'>பருவம்-2</span></td>
                        <td align='center' class='bg'>TERM-3<br /><span class='nor1'>பருவம்-3</span></td>
                      </tr>
                      <tr>
                        <td>Group Work / <span class='nor1'>குழுவுடன் இணைந்து இருத்தல்</span></td>
                        <td align='center'>".  $gw1  ."</td>
                        <td align='center'>".  $gw2  ."</td>
                        <td align='center'>".  $gw3  ."</td>
                      </tr>
                      <tr>
                        <td>Sports &amp  Games /<span class='nor1'>விளையாட்டு</span></td>
                        <td align='center'>".  $sp1  ."</td>
                        <td align='center'>".  $sp2  ."</td>
                        <td align='center'>".  $sp3  ."</td>
                      </tr>
                      <tr>
                        <td>Art and Craft /  <span class='nor1'>கலை மற்றும் கைவினை</span></td>
                        <td align='center'>".  $cr1  ."</td>
                        <td align='center'>".  $cr2  ."</td>
                        <td align='center'>".  $cr3  ."</td>
                      </tr>
                      <tr>
                        <td>Computer Skills/ <span class='nor1'>கணினி செயல் திறன்</span></td>
                        <td align='center'>".  $cs1  ."</td>
                        <td align='center'>".  $cs2  ."</td>
                        <td align='center'>".  $cs3  ."</td>
                      </tr>
                      <tr>
                        <td>Project Work/ <span class='nor1'>திட்டபணி செயல்பாடுகள்</span>	</td>
                        <td align='center'>".  $pgt1  ."</td>
                        <td align='center'>".  $pgt2  ."</td>
                        <td align='center'>".  $pgt3  ."</td>
                      </tr>";
					} 
                  $rpt.="</table></td>
                </tr>
            </table></td>
          </tr>
        </table></td></tr>
    </table></td>
  </tr>
</table>
<table class='nor' width='98%' border='0' cellspacing='0' cellpadding='0'>
  <tr>
    <td><table width='100%' border='0' cellspacing='0' cellpadding='0'>
        <tr><td width='49%' valign='top'><table width='100%' border='0' cellspacing='0' cellpadding='0'>
          <tr>
            <td align='center'>&nbsp </td>
          </tr>
          <tr>
            <td align='center'><strong> SCHOLASTIC ACHIEVEMENT</strong></td>
          </tr>
          <tr>
            <td height='4' align='center'>&nbsp </td>
          </tr>
          <tr>
            <td height='4' align='center'>A+: 100   A: 99-90   B: 89-80   C: 79-70  D: 69-55   E: 54 and below</td>
          </tr>
          <tr>
            <td height='4' align='center'>&nbsp </td>
          </tr>";
		  if($sclass<>'LKG' and $sclass<>'UKG') {
          $rpt.="<tr>
            <td height='4' align='center'>CW : Class Work / <span class='nor1'>வகுப்பறை பயிற்சி</span> <br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EG : Exam Grade / <span class='nor1'>தேர்வில் தேர்ச்சி நிலை</span>
<br/>TG : Term Grade /<span class='nor1'>பருவ தேர்ச்சி நிலை</span></td>
          </tr>"; 
		  } else {
		  $rpt.="<tr>
            <td height='4' align='center'>A+: Excellent  <span class='nor1'>மிக நன்று</span> A: Above Average <span class='nor1'>நன்று</span>
B: Average <span class='nor1'>சராசரி</span> C: Below Average <span class='nor1'>சராசரிக்கும் கீழே</span> D: Needs Improvement <span class='nor1'>முயற்சி தேவை</span></td>
          </tr>"; 
		  }

          $rpt.="<tr>
            <td><table style='border-collapse:collapse ; border: solid 1px ##bd3d03' width='100%' border='1' cellspacing='0' cellpadding='5'>";
			if($sclass<>'LKG' and $sclass<>'UKG') {
              $rpt.="<tr>
                <td width='37%'>&nbsp </td>
                <td width='8%' align='center'>CW</td>
                <td width='7%' align='center'>EG</td>
                <td width='7%' align='center'>TG</td>
                <td width='7%' align='center'>CW</td>
                <td width='7%' align='center'>EG</td>
                <td width='7%' align='center'>TG</td>
                <td width='7%' align='center'>CW</td>
                <td width='6%' align='center'>EG</td>
                <td width='7%' align='center'>TG</td>
              </tr>
              <tr>
                <td height='17'>SUBJECTS</td>
                <td colspan='3' align='center' class='bg'>TERM 1</td>
                <td colspan='3' align='center' class='bg'>TERM 2</td>
                <td colspan='3' align='center' class='bg'>TERM 3</td>
              </tr>";
			  }
			  if($sclass=='LKG' or $sclass=='UKG') {
			  $rpt.="<tr>
                <td align='center' height='17' colspan='2'>SUBJECTS</td>
                <td  align='center' class='bg'>TERM 1</td>
                <td  align='center' class='bg'>TERM 2</td>
                <td  align='center' class='bg'>TERM 3</td>
              </tr>";
			  }
			  if($sclass<>'LKG' and $sclass<>'UKG') {
              $rpt.="<tr>
                <td class='bg'> English / <span class='nor1'>ஆங்கிலம்</span></td>
                <td align='center'>".  $ecw1  ."</td>
                <td align='center'>".  $eeg1  ."</td>
                <td align='center'>".  $etg1  ."</td>
                <td align='center'>".  $ecw2  ."</td>
                <td align='center'>".  $eeg2  ."</td>
                <td align='center'>".  $etg2  ."</td>
				<td align='center'>".  $ecw3  ."</td>
                <td align='center'>".  $eeg3  ."</td>
                <td align='center'>".  $etg3  ."</td>
              </tr>
              <tr>
                <td class='bg'> Tamil / <span class='nor1'>தமிழ்</span></td>
                <td align='center'>".  $tcw1  ."</td>
                <td align='center'>".  $teg1  ."</td>
                <td align='center'>".  $ttg1  ."</td>
                <td align='center'>".  $tcw2  ."</td>
                <td align='center'>".  $teg2  ."</td>
                <td align='center'>".  $ttg2  ."</td>
				<td align='center'>".  $tcw3  ."</td>
                <td align='center'>".  $teg3  ."</td>
                <td align='center'>".  $ttg3  ."</td>
              </tr>
              <tr>
                <td class='bg'>Mathematics / <span class='nor1'> கணிதம்</span></td>
                <td align='center'>".  $mcw1  ."</td>
                <td align='center'>".  $meg1  ."</td>
                <td align='center'>".  $mtg1  ."</td>
                <td align='center'>".  $mcw2  ."</td>
                <td align='center'>".  $meg2  ."</td>
                <td align='center'>".  $mtg2  ."</td>
				<td align='center'>".  $mcw3  ."</td>
                <td align='center'>".  $meg3  ."</td>
                <td align='center'>".  $mtg3  ."</td>
              </tr>
              <tr>
                <td class='bg'>   Science / <span class='nor1'>அறிவியல்<span></td>
                <td align='center'>".  $scw1  ."</td>
                <td align='center'>".  $seg1  ."</td>
                <td align='center'>".  $stg1  ."</td>
                <td align='center'>".  $scw2  ."</td>
                <td align='center'>".  $seg2  ."</td>
                <td align='center'>".  $stg2  ."</td>
				<td align='center'>".  $scw3  ."</td>
                <td align='center'>".  $seg3  ."</td>
                <td align='center'>".  $stg3  ."</td>
              </tr>"; }

			  if($sclass>2 and $sclass<>'LKG' and $sclass<>'UKG') { 
              $rpt.="<tr>
                <td class='bg'> Social Science / <br />
                 <span class='nor1'> சமூக அறிவியல்</span></td>
                <td align='center'>".  $sscw1  ."</td>
                <td align='center'>".  $sseg1  ."</td>
                <td align='center'>".  $sstg1  ."</td>
                <td align='center'>".  $sscw2  ."</td>
                <td align='center'>".  $sseg2  ."</td>
                <td align='center'>".  $sstg2  ."</td>
				<td align='center'>".  $sscw3  ."</td>
                <td align='center'>".  $sseg3  ."</td>
                <td align='center'>".  $sstg3  ."</td>
              </tr>"; } 

			/*if($sclass<>'LKG' and $sclass<>'UKG') {
              $rpt.="<tr>
                <td class='bg'> Computer Skills /<br />
                 <span class='nor1'> கணினி அறிவு</span></td>
                <td align='center'>".  $ccw1  ."</td>
                <td align='center'>".  $ceg1  ."</td>
                <td align='center'>".  $ctg1  ."</td>
                <td align='center'>".  $ccw2  ."</td>
                <td align='center'>".  $ceg2  ."</td>
                <td align='center'>".  $ctg2  ."</td>
				<td align='center'>".  $ccw3  ."</td>
                <td align='center'>".  $ceg3  ."</td>
                <td align='center'>".  $ctg3  ."</td>
              </tr>
              <tr>
                <td class='bg'> Projects /<br />
                 <span class='nor1'> செயல்முறை திட்டம்</span></td>
                <td align='center'>".  $pcw1  ."</td>
                <td align='center'>".  $peg1  ."</td>
                <td align='center'>".  $ptg1  ."</td>
                <td align='center'>".  $pcw2  ."</td>
                <td align='center'>".  $peg2  ."</td>
                <td align='center'>".  $ptg2  ."</td>
				<td align='center'>".  $pcw3  ."</td>
                <td align='center'>".  $peg3  ."</td>
                <td align='center'>".  $ptg3  ."</td>
              </tr>
              <tr>
                <td class='bg'>Attendance / <br />
                  <span class='nor1'>வருகைப் பதிவு</span></td>
                <td colspan='3' align='center'>".  $att1  ."/".  $wday1  ."</td>
                <td colspan='3' align='center'>".  $att2  ."/".  $wday2  ."</td>
                <td colspan='3' align='center'>".  $att3  ."/".  $wday3  ."</td>
             </tr>'; 
			  }
			  */
			   if($sclass<>'LKG' and $sclass<>'UKG') {
              $rpt.="<tr>
                <td class='bg'>Attendance / <br />
                  <span class='nor1'>வருகைப் பதிவு</span></td>
                <td colspan='3' align='center'>".  $att1  ."/".  $wday1  ."</td>
                <td colspan='3' align='center'>".  $att2  ."/".  $wday2  ."</td>
                <td colspan='3' align='center'>".  $att3  ."/".  $wday3  ."</td>
             </tr>"; 
			  }
			 if($sclass=='LKG' or $sclass=='UKG') { 
			 $rpt.="<tr><td class='bg' rowspan='5'> English / <span class='nor1'>ஆங்கிலம்</span></td></tr>
			<tr>
				<td class='bg'> Comprehension / <span class='nor1'>புரிந்து கொள்ளுதல்</span></td>
                <td align='center'>".  $ec1  ."</td><td align='center'>".  $ec2  ."</td><td align='center'>".  $ec3  ."</td>
			</tr>
            <tr>
				<td class='bg'> Speaking / <span class='nor1'>பேசுதல்</span></td>
                <td align='center'>".  $es1  ."</td><td align='center'>".  $es2  ."</td><td align='center'>".  $es3  ."</td>
			</tr>
			<tr>
				<td class='bg'> Reading / <span class='nor1'>படித்தல்</span></td>
                <td align='center'>".  $er1  ."</td><td align='center'>".  $er2  ."</td><td align='center'>".  $er3  ."</td>
			</tr>
			<tr>
				<td class='bg'> Writing / <span class='nor1'>எழுதுதல்</span></td>
                <td align='center'>".  $ew1  ."</td><td align='center'>".  $ew2  ."</td><td align='center'>".  $ew3  ."</td>
			</tr>
			<tr><td class='bg' rowspan='5'> Tamil / <span class='nor1'>தமிழ்</span></td></tr>
			<tr>
				<td class='bg'> Comprehension / <span class='nor1'>புரிந்து கொள்ளுதல்</span></td>
                <td align='center'>".  $tc1  ."</td><td align='center'>".  $tc2  ."</td><td align='center'>".  $tc3  ."</td>
			</tr>
            <tr>
				<td class='bg'> Speaking / <span class='nor1'>பேசுதல்</span></td>
                <td align='center'>".  $ts1  ."</td><td align='center'>".  $ts2  ."</td><td align='center'>".  $ts3  ."</td>
			</tr>
			<tr>
				<td class='bg'> Reading / <span class='nor1'>படித்தல்</span></td>
                <td align='center'>".  $tr1  ."</td><td align='center'>".  $tr2  ."</td><td align='center'>".  $tr3  ."</td>
			</tr>
			<tr>
				<td class='bg'> Writing / <span class='nor1'>எழுதுதல்</span></td>
                <td align='center'>".  $tw1  ."</td><td align='center'>".  $tw2  ."</td><td align='center'>".  $tw3  ."</td>
			</tr>
			<tr><td class='bg'> Maths / <span class='nor1'>கணிதம்</span></td>
				<td class='bg'> Comprehension / <span class='nor1'>புரிந்து கொள்ளுதல்</span></td>
                <td align='center'>".  $mc1  ."</td><td align='center'>".  $mc2  ."</td><td align='center'>".  $mc3  ."</td>
			</tr>
			<tr><td class='bg'> Science / <span class='nor1'>அறிவியல்</span></td>
				<td class='bg'> Comprehension / <span class='nor1'>புரிந்து கொள்ளுதல்</span></td>
                <td align='center'>".  $sc1  ."</td><td align='center'>".  $sc2  ."</td><td align='center'>".  $sc3  ."</td>
			</tr>
			 <tr><td class='bg' colspan='2'>Attendance / <span class='nor1'>வருகைப் பதிவு</span></td>
                <td align='center'>".  $att1  ." / ".  $wday1  ."</td>
                <td align='center'>".  $att2  ." / ".  $wday2  ."</td>
                <td align='center'>".  $att3  ." / ".  $wday3  ."</td>
             </tr>";
			  } 
            $rpt.="</table></td>
          </tr>
          <tr>
            <td>&nbsp </td>
          </tr>
          
          <tr>
            <td>&nbsp </td>
          </tr>
          <tr>
            <td height='20' align='center'>SIGNATURES / கையொப்பம்</td>
          </tr>
          <tr>
            <td height='20' align='center'>&nbsp </td>
          </tr>
          <tr>
            <td><table style='border-collapse:collapse ; border: solid 1px #000000 ' width='100%' border='1' cellspacing='0' cellpadding='8'>
              <tr>
                <td width='47%' class='bg'>Class Teacher  / <span class='nor1'>வகுப்பாசிரியர்</span></td>
                <td width='17%'>&nbsp </td>
                <td width='18%'>&nbsp </td>
                <td width='18%'>&nbsp </td>
              </tr>
              <tr>
                <td class='bg'>Principal / <span class='nor1'>முதல்வர்</span></td>
                <td>&nbsp </td>
                <td>&nbsp </td>
                <td>&nbsp </td>
              </tr>
              <tr>
                <td class='bg'>Parent / <span class='nor1'>பெற்றோர்</span></td>
                <td>&nbsp </td>
                <td>&nbsp </td>
                <td>&nbsp </td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr><br/><br/>
      
      <tr><td width='5%' valign='top'>&nbsp </td></tr>
      
      <tr>
        <td width='50%' valign='top'>
        <table  width='100%' border='0' cellspacing='0' cellpadding='0'>
          <tr><td align='center'>&nbsp </td></tr>
          <tr>
            <td align='center'><strong>CLASS TEACHER'S COMMENT<br />
வகுப்பாசிரியரின் கருத்து</strong></td>
          </tr>
          <tr>
            <td><table style='border-collapse:collapse ; border: solid 1px #666666;'  width='100%' border='1' cellspacing='0' cellpadding='0'>
              <tr>
                <td width='69%' class='bg'> Term 1 / <span class='nor1'>பருவம் 1</span></td>
                <td width='31%' class='bg'> Date / <span class='nor1'>தேதி ".  $pd1  ."</span></td>
              </tr>
              <tr>
                <td colspan='2' rowspan='4' style='height:100px;' valign='top'>".$ecomment1 ."<br><font face='AV-Font-Tam1'>".  $tcomment1  ."</font></td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td><table style='border-collapse:collapse;  border: solid 1px #666666; border-bottom:solid 1px #666666;'  width='100%' border='1' cellspacing='0' cellpadding='0'>
              <tr>
                <td width='69%' class='bg'>Term 2 / <span class='nor1'>பருவம் 2</span></td>
                <td width='31%' class='bg'>Date / <span class='nor1'> தேதி ".  $pd2  ."</span></td>
              </tr>
             <tr>
                <td colspan='2' rowspan='4' style='height:100px ' valign='top'>".$ecomment2 ."<br><font face='AV-Font-Tam1'>".  $tcomment2  ."</font></td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td><table style='border-collapse:collapse ; border: solid 1px #666666;'  width='100%' border='1' cellspacing='0' cellpadding='0'>
              <tr>
                <td width='69%' class='bg'>Term 3 / <span class='nor1'>பருவம் 3</span></td>
                <td width='31%' class='bg'>Date / <span class='nor1'>தேதி ".  $pd3  ."</span></td>
              </tr>
              <tr>
                <td colspan='2' rowspan='4' style='height:100px ' valign='top'>".$ecomment3 ."<br><font face='AV-Font-Tam1'>".  $tcomment3  ."</font></td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td align='center' class='bg'>YEAR-END RESULT / <span class='nor1'>வருடத்தின் இறுதி மதிப்பீடு</span></td>
          </tr>
          <tr>
            <td><table style='border-collapse:collapse ; border: solid 1px #666666; border-bottom:solid 1px #666666;' width='100%' border='1' cellspacing='0' cellpadding='5'>
              <tr>
                <td class='bg'>Passed and Promoted to Class  /<br />
                  <span class='nor1'>தேர்ச்சி விவரம் </span></td>
                <td> __________________________</td>
              </tr>
              <tr>
                <td class='bg'>Promoted to Class / <br />
                  <span class='nor1'>தேர்ச்சி பெற்று சென்ற அடுத்த வகுப்பு </span></td>
                <td>____________________ </td>
              </tr>
              <tr>
                <td colspan='2' class='bg'>Remarks (if any) / <span class='nor1'>கருத்து (ஏதேனுமிருந்தால்)   </span></td>
              </tr>
              <tr>
                <td colspan='2' style='height:65px;'>&nbsp </td>
              </tr>
              
              <tr>
                <td colspan='2'><table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td align='center'><table width='100%' border='0' cellspacing='0' cellpadding='0'>
                  <tr>
                    <td align='center' class='bg'><p>_______________ <br/>
                        Class Teacher <br/>
                        <span class='nor1'>வகுப்பாசிரியர்</spa></p></td>
                    <td align='center' class='bg'><p>_______________ <br/>
                        Seal <br/>
                        <span class='nor1'>முத்திரை</span></p></td>
                    <td align='center' class='bg'><p>_______________ <br/>
                        Principal<br/>
                        <span class='nor1'>முதல்வர்</span></p></td>
                  </tr>
                </table></td>
                    </tr>
                </table></td>
              </tr>
            </table></td>
          </tr>
        </table></td>
        </tr>
    </table></td>
  </tr>
</table></td>
  </tr>
  <tr>
    <td align='center'>&nbsp;</td>
  </tr>
  <tr>
  <td align='center'><img src='bot.jpg' /></td>
  </tr>
</table><br><br>";
echo $rpt;
}


 $Location = $_GET['Location'];
 $Class = $_GET['Class'];
 $Section=$_GET['section'];
 $Adm_No=$_GET['Adm_No'];
 
    $query=mssql_init('sp_GenerateReportcard',$mssql);
    mssql_bind($query,'@Location',$Location,SQLINT4,false,false,5);
    mssql_bind($query,'@Class',$Class,SQLINT4,false,false,5);
    mssql_bind($query,'@Section',$Section,SQLINT4,false,false,5); 
    mssql_bind($query,'@Admission_No',$Adm_No,SQLVARCHAR,false,false,50);      
    $result=mssql_execute($query);

    mssql_free_statement($query);
    $rs_cnt = mssql_num_rows($result);
    if($rs_cnt==0)
       echo "<p align='center' style='color:red;'>No Records Found...</p>"; 
    else
       {
      $sname=$speriod=$sclass=$ssection=$steacher=$sdob=$sadmno=$ecomment1=$tcomment1=$ecomment2=$tcomment2=$ecomment3=$tcomment3=$term1=$wday1=$term3=$wday3=$term3=$wday3=$se1=$se2=$se3=$wn1=$wn2=$wn3=$em1=$em2=$em3=$rp1=$rp2=$rp3=$db1=$db2=$db3=$as1=$as2=$as3=$pt1=$pt2=$pt3=$si1=$si2=$si3=$hy1=$hy2=$hy3=$gw1=$gw2=$gw3=$sp1=$sp2=$sp3=$cr1=$cr2=$cr3=$cs1=$cs2=$cs3=$pgt1=$pgt2=$pgt3=$att1=$att2=$att3=$ecw1=$eeg1=$etg1=$ecw2=$eeg2=$etg2=$ecw3=$eeg3=$etg3=$tcw1=$teg1=$ttg1=$tcw2=$teg2=$ttg2=$tcw3=$teg3=$ttg3=$mcw1=$meg1=$mtg1=$mcw2=$meg2=$mtg2=$mcw3=$meg3=$mtg3=$scw1=$seg1=$stg1=$scw2=$seg2=$stg2=$scw3=$seg3=$stg3=$sscw1=$sseg1=$sstg1=$sscw2=$sseg2=$sstg2=$sscw3=$sseg3=$sstg3=$ccw1=$ceg1=$ctg1=$ccw2=$ceg2=$ctg2=$ccw3=$ceg3=$ctg3=$pcw1=$peg1=$ptg1=$pcw2=$peg2=$ptg2=$pcw3=$peg3=$ptg3=$part1=$fms1=$gms1=$ec1=$es1=$er1=$ew1=$tc1=$ts1=$tr1=$tw1=$mc1=$sc1=$pd1=$part2=$fms2=$gms2=$ec2=$es2=$er2=$ew2=$tc2=$ts2=$tr2=$tw2=$mc2=$sc2=$pd2=$part3=$fms3=$gms3=$ec3=$es3=$er3=$ew3=$tc3=$ts3=$tr3=$tw3=$mc3=$sc3=$pd3="&nbsp;";
      while($rfld = mssql_fetch_array($result))
	   {	$cnt = $cnt+1;
	   
	   if(strlen($sno)>0 and  $sno <>$rfld['Student_Id'] )
       { 
	     report($sloc,$sname,$speriod,$sclass,$ssection,$steacher,$sdob,$sadmno,$ecomment1,$tcomment1,$ecomment2,$tcomment2,$ecomment3,$tcomment3,$term1,$wday1,$term2,$wday2,$term3,$wday3,$se1,$se2,$se3,$wn1,$wn2,$wn3,$em1,$em2,$em3,$rp1,$rp2,$rp3,$db1,$db2,$db3,$as1,$as2,$as3,$pt1,$pt2,$pt3,$si1,$si2,$si3,$hy1,$hy2,$hy3,$gw1,$gw2,$gw3,$sp1,$sp2,$sp3,$cr1,$cr2,$cr3,$cs1,$cs2,$cs3,$pgt1,$pgt2,$pgt3,$att1,$att2,$att3,$ecw1,$eeg1,$etg1,$ecw2,$eeg2,$etg2,$ecw3,$eeg3,$etg3,$tcw1,$teg1,$ttg1,$tcw2,$teg2,$ttg2,$tcw3,$teg3,$ttg3,$mcw1,$meg1,$mtg1,$mcw2,$meg2,$mtg2,$mcw3,$meg3,$mtg3,$scw1,$seg1,$stg1,$scw2,$seg2,$stg2,$scw3,$seg3,$stg3,$sscw1,$sseg1,$sstg1,$sscw2,$sseg2,$sstg2,$sscw3,$sseg3,$sstg3,$ccw1,$ceg1,$ctg1,$ccw2,$ceg2,$ctg2,$ccw3,$ceg3,$ctg3,$pcw1,$peg1,$ptg1,$pcw2,$peg2,$ptg2,$pcw3,$peg3,$ptg3,$part1,$fms1,$gms1,$ec1,$es1,$er1,$ew1,$tc1,$ts1,$tr1,$tw1,$mc1,$sc1,$pd1,$part2,$fms2,$gms2,$ec2,$es2,$er2,$ew2,$tc2,$ts2,$tr2,$tw2,$mc2,$sc2,$pd2,$part3,$fms3,$gms3,$ec3,$es3,$er3,$ew3,$tc3,$ts3,$tr3,$tw3,$mc3,$sc3,$pd3);
		  $sname=$speriod=$sclass=$ssection=$steacher=$sdob=$sadmno=$ecomment1=$tcomment1=$ecomment2=$tcomment2=$ecomment3=$tcomment3=$term1=$wday1=$term2=$wday2=$term3=$wday3=$se1=$se2=$se3=$wn1=$wn2=$wn3=$em1=$em2=$em3=$rp1=$rp2=$rp3=$db1=$db2=$db3=$as1=$as2=$as3=$pt1=$pt2=$pt3=$si1=$si2=$si3=$hy1=$hy2=$hy3=$gw1=$gw2=$gw3=$sp1=$sp2=$sp3=$cr1=$cr2=$cr3=$cs1=$cs2=$cs3=$pgt1=$pgt2=$pgt3=$att1=$att2=$att3=$ecw1=$eeg1=$etg1=$ecw2=$eeg2=$etg2=$ecw3=$eeg3=$etg3=$tcw1=$teg1=$ttg1=$tcw2=$teg2=$ttg2=$tcw3=$teg3=$ttg3=$mcw1=$meg1=$mtg1=$mcw2=$meg2=$mtg2=$mcw3=$meg3=$mtg3=$scw1=$seg1=$stg1=$scw2=$seg2=$stg2=$scw3=$seg3=$stg3=$sscw1=$sseg1=$sstg1=$sscw2=$sseg2=$sstg2=$sscw3=$sseg3=$sstg3=$ccw1=$ceg1=$ctg1=$ccw2=$ceg2=$ctg2=$ccw3=$ceg3=$ctg3=$pcw1=$peg1=$ptg1=$pcw2=$peg2=$ptg2=$pcw3=$peg3=$ptg3=$part1=$fms1=$gms1=$ec1=$es1=$er1=$ew1=$tc1=$ts1=$tr1=$tw1=$mc1=$sc1=$pd1=$part2=$fms2=$gms2=$ec2=$es2=$er2=$ew2=$tc2=$ts2=$tr2=$tw2=$mc2=$sc2=$pd2=$part3=$fms3=$gms3=$ec3=$es3=$er3=$ew3=$tc3=$ts3=$tr3=$tw3=$mc3=$sc3=$pd3="&nbsp;";
        }        
				$sname = $rfld['Name'];
				$speriod = $rfld['Period'];
				$sclass = $rfld['Class'];
				$ssection = $rfld['Section'];
				$steacher = $rfld['Teacher_Name'];
             
			$sdob = $rfld['DOB'];
            $sdob=date('d-m-Y',strtotime($sdob));
  	             //$sdob = $rfld[(strtotime("m/d/y", 'DOB')]);
                 
                //strtotime("YYYY-MM-DD", $sdob);
                //echo $sdob;
              // $sdob= strtotime("d-m", $sdob);
                //echo $sdob;
				$sadmno = $rfld['Admission_No'];
				$sloc = $rfld['Location'];
				if($rfld['Term_Code']==1)
				{
					$ecomment1 = $rfld['EnglishComment'];
					$tcomment1 = $rfld['TamilComment'];
					$term1 = 1;
					$wday1 = $rfld['Working_Days'];
					$se1 = $rfld['Spoken_English'];
					$wn1 = $rfld['Writing_Neatness'];
					$em1 = $rfld['Enthusiam_Energy'];
					$rp1 = $rfld['Responsibilty'];
					$db1 = $rfld['Dicipline'];
					$as1 = $rfld['Attention_Span'];
					$pt1 = $rfld['Punctuality'];
					$si1 = $rfld['Social_Interaction'];
					$hy1 = $rfld['Hygiene'];
					$gw1 = $rfld['Group_Work'];
					$sp1 = $rfld['Sports_Games'];
					$cr1 = $rfld['Art_Craft'];
					$cs1 = $rfld['Computer_Skills'];
					$pgt1 = $rfld['Projects'];
					$att1 = $rfld['Noday_Attendance'];
					$ecw1 = $rfld['English_Classwrk'];
					$eeg1 = $rfld['English_Examgrade'];
					$etg1 = $rfld['English_Totalgrade'];

					$tcw1 = $rfld['Tamil_Classwrk'];
					$teg1 = $rfld['Tamil_Examgrade'];
					$ttg1 = $rfld['Tamil_Totalgrade'];

					$mcw1 = $rfld['Maths_Classwrk'];
					$meg1 = $rfld['Maths_Examgrade'];
					$mtg1 = $rfld['Maths_Totalgrade'];

					$scw1 = $rfld['Science_Classwrk'];
					$seg1 = $rfld['Science_Examgrade'];
					$stg1 = $rfld['Science_Totalgrade'];

					$sscw1 = $rfld['Sscience_Classwrk'];
					$sseg1 = $rfld['Sscience_Examgrade'];
					$sstg1 = $rfld['Sscience_Totalgrade'];

					$ccw1 = $rfld['Computer_Classwrk'];
					$ceg1 = $rfld['Computer_Examgrade'];
					$ctg1 = $rfld['Computer_Totalgrade'];

					$pcw1 = $rfld['Project_Classwrk'];
					$peg1 = $rfld['Project_Examgrade'];
					$ptg1 = $rfld['Project_Totalgrade'];

					$part1 = $rfld['Participation'];
					$fms1  = $rfld['Fine_Motor_Skills'];
					$gms1  = $rfld['Gross_Motor_Skills'];
					$ec1 = $rfld['Eng_Comprehension'];
					$es1 = $rfld['Eng_Speaking'];
					$er1 = $rfld['Eng_Reading'];
					$ew1 = $rfld['Eng_Writing'];

					$tc1 = $rfld['Tam_Comprehension'];
					$ts1 = $rfld['Tam_Speaking'];
					$tr1 = $rfld['Tam_Reading'];
					$tw1 = $rfld['Tam_Writing'];

					$mc1 = $rfld['Mat_Comprehension'];
					$sc1 = $rfld['Scn_Comprehension'];
					$pd1  = $rfld['Progress_Date'];
				}
				if($rfld['Term_Code']==2)
				{$ecomment2 = $rfld['EnglishComment'];
					$tcomment2 = $rfld['TamilComment'];
					$term2 = 2;
					$wday2 = $rfld['Working_Days'];
					$se2 = $rfld['Spoken_English'];
					$wn2 = $rfld['Writing_Neatness'];
					$em2 = $rfld['Enthusiam_Energy'];
					$rp2 = $rfld['Responsibilty'];
					$db2 = $rfld['Dicipline'];
					$as2 = $rfld['Attention_Span'];
					$pt2 = $rfld['Punctuality'];
					$si2 = $rfld['Social_Interaction'];
					$hy2 = $rfld['Hygiene'];
					$gw2 = $rfld['Group_Work'];
					$sp2 = $rfld['Sports_Games'];
					$cr2 = $rfld['Art_Craft'];
					$cs2 = $rfld['Computer_Skills'];
					$pgt2 = $rfld['Projects'];
					$att2 = $rfld['Noday_Attendance'];
					$ecw2 = $rfld['English_Classwrk'];
					$eeg2 = $rfld['English_Examgrade'];
					$etg2 = $rfld['English_Totalgrade'];

					$tcw2 = $rfld['Tamil_Classwrk'];
					$teg2 = $rfld['Tamil_Examgrade'];
					$ttg2 = $rfld['Tamil_Totalgrade'];

					$mcw2 = $rfld['Maths_Classwrk'];
					$meg2 = $rfld['Maths_Examgrade'];
					$mtg2 = $rfld['Maths_Totalgrade'];

					$scw2 = $rfld['Science_Classwrk'];
					$seg2 = $rfld['Science_Examgrade'];
					$stg2 = $rfld['Science_Totalgrade'];

					$sscw2 = $rfld['Sscience_Classwrk'];
					$sseg2 = $rfld['Sscience_Examgrade'];
					$sstg2 = $rfld['Sscience_Totalgrade'];

					$ccw2 = $rfld['Computer_Classwrk'];
					$ceg2 = $rfld['Computer_Examgrade'];
					$ctg2 = $rfld['Computer_Totalgrade'];

					$pcw2 = $rfld['Project_Classwrk'];
					$peg2 = $rfld['Project_Examgrade'];
					$ptg2 = $rfld['Project_Totalgrade'];

					$part2 = $rfld['Participation'];
					$fms2  = $rfld['Fine_Motor_Skills'];
					$gms2  = $rfld['Gross_Motor_Skills'];
					$ec2 = $rfld['Eng_Comprehension'];
					$es2 = $rfld['Eng_Speaking'];
					$er2 = $rfld['Eng_Reading'];
					$ew2 = $rfld['Eng_Writing'];

					$tc2 = $rfld['Tam_Comprehension'];
					$ts2 = $rfld['Tam_Speaking'];
					$tr2 = $rfld['Tam_Reading'];
					$tw2 = $rfld['Tam_Writing'];

					$mc2 = $rfld['Mat_Comprehension'];
					$sc2 = $rfld['Scn_Comprehension'];
					$pd2  = $rfld['Progress_Date'];
				}
				if($rfld['Term_Code']==4)
				{
					$ecomment3 = $rfld['EnglishComment'];
					$tcomment3 = $rfld['TamilComment'];
					$term3 = 3;
					$wday3 = $rfld['Working_Days'];
					$se3 = $rfld['Spoken_English'];
					$wn3 = $rfld['Writing_Neatness'];
					$em3 = $rfld['Enthusiam_Energy'];
					$rp3 = $rfld['Responsibilty'];
					$db3 = $rfld['Dicipline'];
					$as3 = $rfld['Attention_Span'];
					$pt3 = $rfld['Punctuality'];
					$si3 = $rfld['Social_Interaction'];
					$hy3 = $rfld['Hygiene'];
					$gw3 = $rfld['Group_Work'];
					$sp3 = $rfld['Sports_Games'];
					$cr3 = $rfld['Art_Craft'];
					$cs3 = $rfld['Computer_Skills'];
					$pgt3 = $rfld['Projects'];
					$att3 = $rfld['Noday_Attendance'];
					$ecw3 = $rfld['English_Classwrk'];
					$eeg3 = $rfld['English_Examgrade'];
					$etg3 = $rfld['English_Totalgrade'];

					$tcw3 = $rfld['Tamil_Classwrk'];
					$teg3 = $rfld['Tamil_Examgrade'];
					$ttg3 = $rfld['Tamil_Totalgrade'];

					$mcw3 = $rfld['Maths_Classwrk'];
					$meg3 = $rfld['Maths_Examgrade'];
					$mtg3 = $rfld['Maths_Totalgrade'];

					$scw3 = $rfld['Science_Classwrk'];
					$seg3 = $rfld['Science_Examgrade'];
					$stg3 = $rfld['Science_Totalgrade'];

					$sscw3 = $rfld['Sscience_Classwrk'];
					$sseg3 = $rfld['Sscience_Examgrade'];
					$sstg3 = $rfld['Sscience_Totalgrade'];

					$ccw3 = $rfld['Computer_Classwrk'];
					$ceg3 = $rfld['Computer_Examgrade'];
					$ctg3 = $rfld['Computer_Totalgrade'];

					$pcw3 = $rfld['Project_Classwrk'];
					$peg3 = $rfld['Project_Examgrade'];
					$ptg3 = $rfld['Project_Totalgrade'];

					$part3 = $rfld['Participation'];
					$fms3  = $rfld['Fine_Motor_Skills'];
					$gms3  = $rfld['Gross_Motor_Skills'];
					$ec3 = $rfld['Eng_Comprehension'];
					$es3 = $rfld['Eng_Speaking'];
					$er3 = $rfld['Eng_Reading'];
					$ew3 = $rfld['Eng_Writing'];

					$tc3 = $rfld['Tam_Comprehension'];
					$ts3 = $rfld['Tam_Speaking'];
					$tr3 = $rfld['Tam_Reading'];
					$tw3 = $rfld['Tam_Writing'];

					$mc3 = $rfld['Mat_Comprehension'];
					$sc3 = $rfld['Scn_Comprehension'];
					$pd3  = $rfld['Progress_Date'];
				}
			 $sno = $rfld['Student_Id'];
		} 
        	
		//if($cnt==mssql_num_rows($result))
			report($sloc,$sname,$speriod,$sclass,$ssection,$steacher,$sdob,$sadmno,$ecomment1,$tcomment1,$ecomment2,$tcomment2,$ecomment3,$tcomment3,$term1,$wday1,$term2,$wday2,$term3,$wday3,$se1,$se2,$se3,$wn1,$wn2,$wn3,$em1,$em2,$em3,$rp1,$rp2,$rp3,$db1,$db2,$db3,$as1,$as2,$as3,$pt1,$pt2,$pt3,$si1,$si2,$si3,$hy1,$hy2,$hy3,$gw1,$gw2,$gw3,$sp1,$sp2,$sp3,$cr1,$cr2,$cr3,$cs1,$cs2,$cs3,$pgt1,$pgt2,$pgt3,$att1,$att2,$att3,$ecw1,$eeg1,$etg1,$ecw2,$eeg2,$etg2,$ecw3,$eeg3,$etg3,$tcw1,$teg1,$ttg1,$tcw2,$teg2,$ttg2,$tcw3,$teg3,$ttg3,$mcw1,$meg1,$mtg1,$mcw2,$meg2,$mtg2,$mcw3,$meg3,$mtg3,$scw1,$seg1,$stg1,$scw2,$seg2,$stg2,$scw3,$seg3,$stg3,$sscw1,$sseg1,$sstg1,$sscw2,$sseg2,$sstg2,$sscw3,$sseg3,$sstg3,$ccw1,$ceg1,$ctg1,$ccw2,$ceg2,$ctg2,$ccw3,$ceg3,$ctg3,$pcw1,$peg1,$ptg1,$pcw2,$peg2,$ptg2,$pcw3,$peg3,$ptg3,$part1,$fms1,$gms1,$ec1,$es1,$er1,$ew1,$tc1,$ts1,$tr1,$tw1,$mc1,$sc1,$pd1,$part2,$fms2,$gms2,$ec2,$es2,$er2,$ew2,$tc2,$ts2,$tr2,$tw2,$mc2,$sc2,$pd2,$part3,$fms3,$gms3,$ec3,$es3,$er3,$ew3,$tc3,$ts3,$tr3,$tw3,$mc3,$sc3,$pd3);
	 } ?> 
</body>
</html>