<?php session_start(); 
ini_set ('display_errors', 1);
    error_reporting (E_ALL | E_STRICT);
	?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php
include ("../includes/header.php");
title('Student Management', 'GenerateReport', 2, 1, 2);
?>
<title>Isha Vidhya</title>
<style type="text/css">
</style></head>
<?php
$errmsg="";
$errflag=0;
$dummy=0;
 if($_POST["Go"]=="Generate Report")
  { 
    $Location = $_POST['Loc_Code'];
    $Adm_No = $_POST['Admission_No'];
    $Class  = $_POST['Class_Code'];
    $section = $_POST['Sec_Code'];
    $dummy = ZeroCheck($Location,$errmsg,$errflag,"Location"); 	   ?>
 <?php if($errflag==0)  {  ?>
	<script>window.open("gr.php?Location=<?php echo $Location;?>&Adm_No=<?php echo $Adm_No;?>&Class=<?php echo $Class;?>&section=<?php echo $section;?>","","top=100,left=75,width=1000,height=1000,menubar=yes,toolbar=no,scrollbars=yes,resizable=no,status=no"); </script>
<?php }
	if($errflag==1) 
	echo $errlbl.$errmsg;
	if($errflag==2) 
	echo "<p class='error'>".$errmsg1;

 }  ?>

<body>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
<tr><td><?php   titleheader("Generate ReportCard", 0);  ?></td></tr>
<tr><td height="5">&nbsp;</td></tr>
  <tr>
    <td align="center"><div style="width:980px; border:thin;" id="scholarshipfr" align="center">
        <form id="myform" name="myform" method="post" action="greport.php">
          <table width="100%" border="0" cellspacing="2" cellpadding="0" align="center">
            <tr>
              <td width="9%" align="left">Location <?php
echo $mand;
?></td>
 <?php
//SHOW Location  DROPDOWN
$query = mssql_init('[sp_selectschoolname]', $mssql);
mssql_bind($query, '@School_id', $_SESSION['SchoolId'], SQLINT4, false, false, 5);
$result = mssql_execute($query);
mssql_free_statement($query);
?>
              <td width="20%"><select name="Loc_Code" id="Location" >
                  <?php
echo $Select;
?>
                  <?php
while ($field = mssql_fetch_array($result))
{
?>
                  <option value="<?php echo $field['School_id']
?>" <?php
    if ($Location == $field['School_id'])
        echo "Selected";
?>>
                  <?php
    echo $field['SchoolName']
?>
                  </option>
                  <?php
}
?>
              </select>              </td>
              <td width="8%"  align="right">Class</td>
              <?php
//SHOW Class  DROPDOWN
 mssql_free_result($result);
$query = mssql_init('[sp_GetClass]', $mssql);
$result = mssql_execute($query);
mssql_free_statement($query);
?>
              <td width="6%"><select name="Class_Code" id="Class">
                  <?php
echo $Select;
?>
                  <?php
while ($field = mssql_fetch_array($result))
{
?>
                  <option value="<?php
    echo $field['Class_Code']
?>" <?php
    if ($Class_Code == $field['Class_Code'])
        echo "Selected";
?>>
                  <?php
    echo $field['Class']
?>
                  </option>
                  <?php
}
?>
              </select></td>
              <td width="6%" align="right">Section</td>
              <?php
//SHOW CITY DROPDOWN
 mssql_free_result($result);
$query_section = mssql_init('sp_GetSection', $mssql);
$result_section = mssql_execute($query_section);
mssql_free_statement($query_section);
?>
              <td width="10%"><select name="Sec_Code" id="Section" >
                  <?php
echo $Select;
?>
                  <?php
while ($field = mssql_fetch_array($result_section))
{
?>
                  <option value="<?php
    echo $field['Section_Code']
?>" <?php
    if ($Sec_Code == $field['Section_Code'])
        echo "Selected";
?>>
                  <?php
    echo $field['Section']
?>
                  </option>
                  <?php
}
?>
              </select></td>
              <!--<td width="10%" align="right">Admission No.</td>
              <td width="31%" ><input type="text" name="Adm_No" id="Admission_No"   onkeydown="return alphanumeric('Admission_No')"  maxlength="15"  
              value="<?php
echo $Adm_No
?>" /></td>-->

<td width="93" align="right">Adm. No.</td>
                    <td width="152" align="left"><div id ="Admission_No"></div>
                      <input type="text" name="Admission_No" id="Admission_No" maxlength="50" value="<?php echo $Admission_No ?>" onkeydown="return alphanumeric('Admission_No')" /></td>
            </tr>
            <tr>
              <td colspan="11"><table align="Center">
                <tr>
                  <td ><input type="submit" name="Go" value="Generate Report" class="winbutton_go"/></td>
                </tr>
              </table></td>
            </tr>    
        </table>
        </form>
      </div></td>
  </tr>
</table>
</body>
</html>