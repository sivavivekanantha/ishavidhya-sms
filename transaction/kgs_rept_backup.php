
	 $rpt = "<table width='50%' border='0' cellspacing='0' style='background:url(bg.jpg) repeat-x; border:solid 1px #990000;' align='center' cellpadding='0'>
  <tr>
    <td><table class='nor' width='100%' border='0' cellspacing='0' cellpadding='0' >
  <tr>
    <td><table width='98%' border='0' align='center' cellpadding='0' cellspacing='0'>
        <tr>
        <td width='49%' valign='top'><table width='98%' border='0' align='center' cellpadding='0' cellspacing='0'>
          <tr><td colspan='3' align='center'> </td></tr>
          <tr>
            <td width='18%'><img src='../imgs/image001.png' alt='' width='114' height='93' /></td>
            <td width='82%'><div align='center' style='padding-right:10px;'><strong style='font-size:14px'>ISHA VIDHYA MATRICULATION SCHOOL<br />
                  COMPREHENSIVE EVALUATION REPORT<br />
              ஈஷா வித்யா மெட்ரிகுலேஷன் பள்ளி<br />
              விரிவான மதிப்பீட்டு அறிக்கை</strong></div></td>
              <td>      
              <img src='../photos/".$sno."/".$sno.".jpg' width='114' height='93'/>
              </td>
          </tr>
          <tr>
            <td colspan='2' align='center'>&nbsp;</td>
          </tr>
          <tr>
            <td colspan='3' align='center'><b>".$sloc."</b></td>
          </tr>
          <tr>
            <td colspan='2'><div align='center'><br />
            </div></td>
          </tr>
          <tr>
            <td colspan='3'><table width='76%' style='border: dashed 1px #990000;' border='0' cellspacing='0' cellpadding='9' align='center'>
              <tr>
                <td width='50%' class='bg1'>Academic Year / <span class='nor1'>கல்வி ஆண்டு</span></td><td class='val'><b>".$speriod."</b></span></td>
              </tr>
              <tr>
                <td class='bg1'>Name / <span class='nor1'>பெயர்</span></td><td class='val'><b>".  $sname ."</b></span></td>
              </tr>
              <tr>
				<td class='bg1'>Class / <span class='nor1'>வகுப்பு</span></td><td class='val'><b>".  $sclass  ."</b></td>
              </tr>
			  <tr>
                <td class='bg1'>Section / <span class='nor1'>பிரிவு</span></td><td class='val'> <b>".$ssection."</b></td>
              </tr>
              <tr>
                <td class='bg1'>Date of Birth / <span class='nor1'>பிறந்த தேதி</span> </td><td class='val'>  <b>".$sdob."</b></td>
              </tr>
              <tr>
                <td class='bg1'>Admission No / <span class='nor1'>அனுமதி எண்.</span></td><td class='val'> <b>".$sadmno."</b></td>
              </tr>
              <tr>
                <td class='bg1'>Class Teacher / <span class='nor1'>வகுப்பாசிரியர்</span> </td><td class='val'><b>".$steacher."</b></td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td colspan='3' class='bg1' ><p>&nbsp </p>
              <p><br />
                Parents are requested to review this card each term, sign on page 3 and return to the school within 7 days. Clarifications can be discussed during &quotOpen Day&quot  or by making an appointment through the Administrative office to meet the Principal.<br /></p>
                 <p><span class='nor1'>
                பெற்றோர்கள் இந்த மதிப்பீட்டு அறிக்கையை பார்த்தபின், மூன்றாம் பக்கத்தில் கையொப்பம் இட்டு ஏழு நாட்களுக்குள் திருப்பிக் கொடுக்குமாறு கேட்டுக் கொள்ளப்படுகிறார்கள். ஐயங்கள் ஏதேனும் இருந்தால் அதை பெற்றோர் ஆசிரியர் சந்திப்பில் ஆசிரியரிடம் கலந்தாலோசிக்கவும் அல்லது பள்ளி நிர்வாகியின் மூலமாக  பள்ளி முதல்வரை சந்திக்கவும்.</span></p></td>
          </tr>
          <tr>
            <td colspan='3'>&nbsp </td>
          </tr>
        </table></td>
      </tr>
      <tr><td width='5%' valign='top'>&nbsp </td></tr>
      <tr>
        <td width='50%' valign='top' align='center'><table width='100%' border='0' cellspacing='0' cellpadding='0'>
          <tr>
            <td ><p><strong>Note to parents</strong>:  &quot General Development&quot  reflects the child's overall development and is not related to any one subject. &quot Scholastic Achievement&quot  shows the child's progress in each subject throughout the term as the &quot Term Grade&quot .  The &quot Term Grade&quot  is calculated as 50% from the &quot Exam Grade&quot  - the score achieved at the end of term exam, and 50% from &quot Class Work&quot  - assessed throughout the term, reflecting performance in homework, classwork, class participation and quizzes. <span class='nor1'>பெற்றோர்களுக்கு குறிப்பு :           ஒரு பருவத்தில் மாணவர்களின் தேர்ச்சி மற்றும் வகுப்பறை பாடத்தை முடிக்கும் திறன் ஆகியவற்றை ஆசிரியர்  கண்காணித்து வருகிறார். கிண்டர்கார்டன் குழந்தைகளுக்கு பருவ தேர்வு முறை  நடத்தப்படுவதில்லை. குழந்தைகளின் பொதுவான வளர்ச்சி அவர்களின் ஒட்டுமொத்த வளர்ச்சியை குறிக்கின்றது. இந்த வளர்ச்சி குறிப்பிட்ட  எந்த ஒரு பாடத்தோடும் தொடர்புடையதல்ல. ஒவ்வொரு பாடத்திலும் குழந்தை பெறும் தேர்ச்சி அக்குழந்தையின் ஒட்டு மொத்த கல்வி வளர்ச்சியை காட்டுகிறது. இந்த ஒட்டு மொத்த கல்வி வளர்ச்சி  குறிப்பிட்ட வகுப்பிற்கும் பருவத்திற்கும் அக்குழந்தையிடம் இருக்க வேண்டிய தேர்ச்சியையும் அறிவுத்திறனையும் வைத்து நிர்ணயிக்கப்படுகின்றது.</span></p>              </td>
          </tr>
          <tr>
            <td><table width='100%' border='0' cellspacing='0' cellpadding='0'>
               
                <tr>
                  <td align='center'>&nbsp </td>
                </tr>
				<tr>
                  <td align='center'>&nbsp </td>
                </tr>
                <tr>
                  <td align='center'><strong>GENERAL DEVELOPMENT / பொதுவான வளர்ச்சி</strong></td>
                </tr>
               
                <tr>
                  <td align='center' height='3'></td>
                </tr>
                <tr>
                  <td align='center' class='bg'>
				  A+: Outstanding   A: Excellent B+: Very Good 
				  B: Good  C: Fair  D: Needs more attention  E: Needs Improvement 
				  </td>
					
                </tr>
                <tr>
                  <td><table style='border-collapse:collapse; border: solid 1px #666666; ' width='100%' border='1' cellspacing='0' cellpadding='1'>
                      <tr>
                        <td width='63%' bgcolor='#f8f8f8' class='bg'>SKILL ASSESSMENT / <span style='font-size:10px'>திறன்களை மதிப்பிடுதல்</span></td>
                        <td align='center' bgcolor='#f8f8f8' class='bg'>TERM-1<br/><span class='nor1'>பருவம்-1</span></td>
                        <td align='center' bgcolor='#f8f8f8' class='bg'>TERM-2<br/><span class='nor1'>பருவம்-2</span></td>
                        <td align='center' bgcolor='#f8f8f8' class='bg'>TERM-3<br/><span class='nor1'>பருவம்-3</span></td>
                      </tr>";
                      
						$rpt.= "<tr><td>Fine Motor Skills / <span class='nor1'>நுண் இயங்கு திறன்</span></td>
                        <td width='12%' bgcolor='#f8f8f8' align='center'>".  $fms1  ."</td>
                        <td width='12%' bgcolor='#f8f8f8' align='center'>".  $fms2  ."</td>
                        <td width='13%' bgcolor='#f8f8f8' align='center'>".  $fms3  ."</td>
                      </tr>
					  <tr>
                        <td>Gross Motor Skills / <span class='nor1'>ஒட்டு மொத்த இயங்கு திறன்</span></td>
                        <td width='12%' align='center'>".  $gms1  ."</td>
                        <td width='12%' align='center'>".  $gms2  ."</td>
                        <td width='13%' align='center'>".  $gms3  ."</td>
                      </tr>
					   <tr>
                        <td>Participation / <span class='nor1'>பங்கேற்றல்</span></td>
                        <td width='12%' align='center'>".  $part1  ."</td>
                        <td width='12%' align='center'>".  $part2  ."</td>
                        <td width='13%' align='center'>".  $part3  ."</td>
                      </tr>";

                      $rpt.= "<tr>
                        <td>Enthusiasm &amp  Energy / <span class='nor1'>ஆர்வம் மற்றும் ஆற்றல்</span></td>
                        <td align='center'>".  $em1  ."</td>
                        <td align='center'>".  $em2  ."</td>
                        <td align='center'>".  $em3  ."</td>
                      </tr>";

                      $rpt.="<tr>
                        <td>Discipline / <span class='nor1'>ஒழுக்கம்</span></td>
                        <td align='center'>".  $db1  ."</td>
                        <td align='center'>".  $db2  ."</td>
                        <td align='center'>".  $db3  ."</td>
                      </tr>
                      <tr>
                        <td>Attention Span / <span class='nor1'>கவனம்</span></td>
                        <td align='center'>".  $as1  ."</td>
                        <td align='center'>".  $as2  ."</td>
                        <td align='center'>".  $as3  ."</td>
                      </tr>
                      <tr>
                        <td>Punctuality / <span class='nor1'>நேரம் தவறாமை</span></td>
                        <td align='center'>".  $pt1  ."</td>
                        <td align='center'>".  $pt2  ."</td>
                        <td align='center'>".  $pt3  ."</td>
                      </tr>
                      <tr>
                        <td>Social Interaction / <span class='nor1'>பழகுதல்</span></td>
                        <td align='center'>".  $si1  ."</td>
                        <td align='center'>".  $si2  ."</td>
                        <td align='center'>".  $si3  ."</td>
                      </tr>
                      <tr>
                        <td>Hygiene / <span class='nor1'>சுகாதாரம்</span></td>
                        <td align='center'>".  $hy1  ."</td>
                        <td align='center'>".  $hy2  ."</td>
                        <td align='center'>".  $hy3  ."</td>
                      </tr>";

                  $rpt.="</table></td>
                </tr>
            </table></td>
          </tr>
        </table></td></tr>
    </table></td>
  </tr>
</table>
<table class='nor' width='98%' border='0' cellspacing='0' cellpadding='0'>
  <tr>
    <td><table width='100%' border='0' cellspacing='0' cellpadding='0'>
        <tr><td width='49%' valign='top'><table width='100%' border='0' cellspacing='0' cellpadding='0'>
          <tr>
            <td align='center'>&nbsp </td>
          </tr>
          <tr>
            <td align='center'><strong> SCHOLASTIC ACHIEVEMENT</strong></td>
          </tr>
          <tr>
            <td height='4' align='center'>&nbsp </td>
          </tr>
          <tr align='center'>
            <td height='4' align='center'>A+: 100   A: 99-90  B+: 89-80   B: 79-70 C: 69-60  D: 59-50 E:49 and below</td>
          </tr>
          <tr>
            <td height='4' align='center'>&nbsp </td>
          </tr>";
		  $rpt.="<tr>
            <td height='4' align='center' >A+: Outstanding   A: Excellent B+: Very Good   B: Good  C: Fair  D: Needs more attention  E: Needs Improvement 
					</td>
          </tr> "; 

          $rpt.="<tr>
            <td colspan='3'><table style='border-collapse:collapse ; border: solid 1px ##bd3d03' width='100%' border='1' cellspacing='0' cellpadding='5'>";
			  $rpt.="<tr>
                <td align='center' height='17' colspan='2'>SUBJECTS</td>
                <td  align='center' class='bg'>TERM 1</td>
                <td  align='center' class='bg'>TERM 2</td>
                <td  align='center' class='bg'>TERM 3</td>
              </tr>";

				 
			 $rpt.="<tr><td class='bg' rowspan='5'> English / <span class='nor1'>ஆங்கிலம்</span></td></tr>
			<tr>
				<td class='bg'> Comprehension / <span class='nor1'>புரிந்து கொள்ளுதல்</span></td>
                <td align='center'>".  $ec1  ."</td><td align='center'>".  $ec2  ."</td><td align='center'>".  $ec3  ."</td>
			</tr>
            <tr>
				<td class='bg'> Speaking / <span class='nor1'>பேசுதல்</span></td>
                <td align='center'>".  $es1  ."</td><td align='center'>".  $es2  ."</td><td align='center'>".  $es3  ."</td>
			</tr>
			<tr>
				<td class='bg'> Reading / <span class='nor1'>படித்தல்</span></td>
                <td align='center'>".  $er1  ."</td><td align='center'>".  $er2  ."</td><td align='center'>".  $er3  ."</td>
			</tr>
			<tr>
				<td class='bg'> Writing / <span class='nor1'>எழுதுதல்</span></td>
                <td align='center'>".  $ew1  ."</td><td align='center'>".  $ew2  ."</td><td align='center'>".  $ew3  ."</td>
			</tr>
			<tr><td class='bg' rowspan='5'> Tamil / <span class='nor1'>தமிழ்</span></td></tr>
			<tr>
				<td class='bg'> Comprehension / <span class='nor1'>புரிந்து கொள்ளுதல்</span></td>
                <td align='center'>".  $tc1  ."</td><td align='center'>".  $tc2  ."</td><td align='center'>".  $tc3  ."</td>
			</tr>
            <tr>
				<td class='bg'> Speaking / <span class='nor1'>பேசுதல்</span></td>
                <td align='center'>".  $ts1  ."</td><td align='center'>".  $ts2  ."</td><td align='center'>".  $ts3  ."</td>
			</tr>
			<tr>
				<td class='bg'> Reading / <span class='nor1'>படித்தல்</span></td>
                <td align='center'>".  $tr1  ."</td><td align='center'>".  $tr2  ."</td><td align='center'>".  $tr3  ."</td>
			</tr>
			<tr>
				<td class='bg'> Writing / <span class='nor1'>எழுதுதல்</span></td>
                <td align='center'>".  $tw1  ."</td><td align='center'>".  $tw2  ."</td><td align='center'>".  $tw3  ."</td>
			</tr>
			<tr><td class='bg'> Maths / <span class='nor1'>கணிதம்</span></td>
				<td class='bg'> Comprehension / <span class='nor1'>புரிந்து கொள்ளுதல்</span></td>
                <td align='center'>".  $mc1  ."</td><td align='center'>".  $mc2  ."</td><td align='center'>".  $mc3  ."</td>
			</tr>
			<tr><td class='bg'> Science / <span class='nor1'>அறிவியல்</span></td>
				<td class='bg'> Comprehension / <span class='nor1'>புரிந்து கொள்ளுதல்</span></td>
                <td align='center'>".  $sc1  ."</td><td align='center'>".  $sc2  ."</td><td align='center'>".  $sc3  ."</td>
			</tr>
			 <tr><td class='bg' colspan='2'>Attendance / <span class='nor1'>வருகைப் பதிவு</span></td>
                <td align='center'>".  $att1  ." / ".  $wday1  ."</td>
                <td align='center'>".  $att2  ." / ".  $wday2  ."</td>
                <td align='center'>".  $att3  ." / ".  $wday3  ."</td>
             </tr>";
			  
            $rpt.="</table></td>
          </tr>
          <tr>
            <td>&nbsp </td>
          </tr>
          
          <tr>
            <td>&nbsp </td>
          </tr>
          <tr>
            <td height='20' align='center'>SIGNATURES / கையொப்பம்</td>
          </tr>
          <tr>
            <td height='20' align='center'>&nbsp </td>
          </tr>
          <tr>
            <td><table style='border-collapse:collapse ; border: solid 1px #000000 ' width='100%' border='1' cellspacing='0' cellpadding='8'>
              <tr>
                <td width='47%' class='bg'>Class Teacher  / <span class='nor1'>வகுப்பாசிரியர்</span></td>
                <td width='17%'>&nbsp </td>
                <td width='18%'>&nbsp </td>
                <td width='18%'>&nbsp </td>
              </tr>
              <tr>
                <td class='bg'>Principal / <span class='nor1'>முதல்வர்</span></td>
                <td>&nbsp </td>
                <td>&nbsp </td>
                <td>&nbsp </td>
              </tr>
              <tr>
                <td class='bg'>Parent / <span class='nor1'>பெற்றோர்</span></td>
                <td>&nbsp </td>
                <td>&nbsp </td>
                <td>&nbsp </td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr><br/><br/>
      
      <tr><td width='5%' valign='top'>&nbsp </td></tr>
      
      <tr>
        <td width='50%' valign='top'>
        <table  width='100%' border='0' cellspacing='0' cellpadding='0'>
          <tr><td align='center'>&nbsp </td></tr>
          <tr>
            <td align='center'><strong>CLASS TEACHER'S COMMENT<br />
வகுப்பாசிரியரின் கருத்து</strong></td>
          </tr>
          <tr>
            <td><table style='border-collapse:collapse ; border: solid 1px #666666;'  width='100%' border='1' cellspacing='0' cellpadding='0'>
              <tr>
                <td width='69%' class='bg'> Term 1 / <span class='nor1'>பருவம் 1</span></td>
                <td width='31%' class='bg'> Date / <span class='nor1'>தேதி ".  $pd1  ."</span></td>
              </tr>
              <tr>
                <td colspan='2' rowspan='4' style='height:100px;' valign='top'>".$ecomment1 ."<br><font face='AV-Font-Tam1'>".  $tcomment1  ."</font></td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td><table style='border-collapse:collapse;  border: solid 1px #666666; border-bottom:solid 1px #666666;'  width='100%' border='1' cellspacing='0' cellpadding='0'>
              <tr>
                <td width='69%' class='bg'>Term 2 / <span class='nor1'>பருவம் 2</span></td>
                <td width='31%' class='bg'>Date / <span class='nor1'> தேதி ".  $pd2  ."</span></td>
              </tr>
             <tr>
                <td colspan='2' rowspan='4' style='height:100px ' valign='top'>".$ecomment2 ."<br><font face='AV-Font-Tam1'>".  $tcomment2  ."</font></td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td><table style='border-collapse:collapse ; border: solid 1px #666666;'  width='100%' border='1' cellspacing='0' cellpadding='0'>
              <tr>
                <td width='69%' class='bg'>Term 3 / <span class='nor1'>பருவம் 3</span></td>
                <td width='31%' class='bg'>Date / <span class='nor1'>தேதி ".  $pd3  ."</span></td>
              </tr>
              <tr>
                <td colspan='2' rowspan='4' style='height:100px ' valign='top'>".$ecomment3 ."<br><font face='AV-Font-Tam1'>".  $tcomment3  ."</font></td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td align='center' class='bg'>YEAR-END RESULT / <span class='nor1'>வருடத்தின் இறுதி மதிப்பீடு</span></td>
          </tr>
          <tr>
            <td><table style='border-collapse:collapse ; border: solid 1px #666666; border-bottom:solid 1px #666666;' width='100%' border='1' cellspacing='0' cellpadding='5'>
              <tr>
                <td class='bg'>Passed and Promoted to Class  /<br />
                  <span class='nor1'>தேர்ச்சி விவரம் </span></td>
                <td> __________________________</td>
              </tr>
              <tr>
                <td class='bg'>Promoted to Class / <br />
                  <span class='nor1'>தேர்ச்சி பெற்று சென்ற அடுத்த வகுப்பு </span></td>
                <td>____________________ </td>
              </tr>
              <tr>
                <td colspan='2' class='bg'>Remarks (if any) / <span class='nor1'>கருத்து (ஏதேனுமிருந்தால்)   </span></td>
              </tr>
              <tr>
                <td colspan='2' style='height:65px;'>&nbsp </td>
              </tr>
              
              <tr>
                <td colspan='2'><table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td align='center'><table width='100%' border='0' cellspacing='0' cellpadding='0'>
                  <tr>
                    <td align='center' class='bg'><p>_______________ <br/>
                        Class Teacher <br/>
                        <span class='nor1'>வகுப்பாசிரியர்</spa></p></td>
                    <td align='center' class='bg'><p>_______________ <br/>
                        Seal <br/>
                        <span class='nor1'>முத்திரை</span></p></td>
                    <td align='center' class='bg'><p>_______________ <br/>
                        Principal<br/>
                        <span class='nor1'>முதல்வர்</span></p></td>
                  </tr>
                </table></td>
                    </tr>
                </table></td>
              </tr>
            </table></td>
          </tr>
        </table></td>
        </tr>
    </table></td>
  </tr>
</table></td>
  </tr>
  <tr>
    <td align='center'>&nbsp;</td>
  </tr>
  <tr>
  <td align='center'><img src='../images/bot.jpg' /></td>
  </tr>
</table><br><br>";
echo $rpt;