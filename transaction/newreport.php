<?php session_start();
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php include("../includes/header.php");
title('Student Management','Manual Report Card Entry',2,1,2); ?>
<head>
<script>
function chkempty(obj,obj1) {
    var obj2 = "#"+obj;
    if($(obj2).val()=='')
        alert('Please fill ' + obj1 + ' field' );
}
function showtitle(obj,obj1) {
    var obj2 = "#"+obj;
    showtitle1(obj1,obj2);
}
function showtitle1(obj1,obj2)
{ 
    $(obj2).poshytip({
    content: obj1,
    className: 'tip-twitter',
    showOn: 'focus',
	alignTo: 'target',
	alignX: 'right',
	alignY: 'center',
	offsetX: 5
    })
}

function editval(val)
{	
	if($('#editcnt').val() >0 ) return false;
    $('#editcnt').val(val);

	if($('#editcnt').val()!="") {
	 var el = $('#editcnt').val()+","+val;
	 $('#editcnt').val(el);
    }
	else { 
		$('#editcnt').val(val)
		}
}
//Mark Validation Mark should be less than or equal to 100
//Mark Above 100 Means Error Message Will Be Displayed
function emptychk(obj)
{ 
    if(($(obj).val()==0) )
	{
	   	$(obj).focus();
		alert('Mark should not be blank');
	}
}
function markcheck(obj)
{   
    if(($(obj).val()>100) ) 
	{
		alert('Mark should be less than or equal to 100');
		$(obj).val('');
        $(obj).focus();
	}
}
</script>
</head>
<?php
			  $errmsg="";
			  $errflag=0;
			  $dummy=0;
					
			  if($_POST['Save'] =='Save')	
              { 		 
                
				   $Location        =   Trim($_POST['Location']);
                   $Class           =   Trim($_POST['Class']);
                   $Section         =   Trim($_POST['Section']);
                   $Admission_No	=	Trim($_POST['Admission_No']);
                   $Term_Code	    =	Trim($_POST['Term2']);
                   $Term_Date       =   Trim($_POST['Term_Date']);
                   $WorkingDays  	=   Trim($_POST['Working_Days']);
                   $TeacherName  	=   Trim($_POST['TeacherName']);
                   $PeriodName      =   Trim($_POST['PeriodName']);
                                   
                   $scount= $_POST['scount'];
                   
                   $dummy = ZeroCheck($Term_Code,$errmsg,$errflag,"Term"); 
			       $dummy = ZeroCheck($Term_Date,$errmsg,$errflag,"Term Date"); 
				   $dummy = ZeroCheck($TeacherName,$errmsg,$errflag,"Teacher Code"); 
				   $dummy = ZeroCheck($WorkingDays,$errmsg,$errflag,"Working Days");
                   $dummy = ZeroCheck($PeriodName,$errmsg,$errflag,"Period Name");
                  
			  //More Than One Record Saved
			  for($cnt=1;$cnt<=$scount;$cnt=$cnt+1) 
			  {
				  $Ass_Header_Code    = Trim($_POST['Ass_Header_Code']);

                  $Student_Id     = Trim($_POST['Student_Id'.$cnt]);
                  $varA55 = "Student_Id".$cnt;
                  $$varA55 = $Student_Id;
                  
                  $Spoken_English     = Trim($_POST['Spoken_English'.$cnt]);
                  $varA1 = "Spoken_English_".$cnt;
                  $$varA1 = $Spoken_English;
                  $Writing_Neatness	  = Trim($_POST['Writing_Neatness'.$cnt]);
                  $varA2 = "Writing_Neatness_".$cnt;
                  $$varA2 = $Writing_Neatness;
				  $Enthusiam_Energy	  = Trim($_POST['Enthusiam_Energy'.$cnt]);	
                  $varA3 = "Enthusiam_Energy_".$cnt;
                  $$varA3 = $Enthusiam_Energy;                  
				  $Responsibilty      = Trim($_POST['Responsibilty'.$cnt]);
                  $varA4 = "Responsibilty_".$cnt;
                  $$varA4 = $Responsibilty;
				  $Dicipline      	  = Trim($_POST['Dicipline'.$cnt]);
                  $varA5 = "Dicipline_".$cnt;
                  $$varA5 = $Dicipline;
				  $Attention_Span  	  = Trim($_POST['Attention_Span'.$cnt]);
                  $varA6 = "Attention_Span_".$cnt;
                  $$varA6 = $Attention_Span;
				  $Punctuality        = Trim($_POST['Punctuality'.$cnt]);
                  $varA7 = "Punctuality_".$cnt;
                  $$varA7 = $Punctuality;
				  $Social_Interaction = Trim($_POST['Social_Interaction'.$cnt]);
                  $varA8 = "Social_Interaction_".$cnt;
                  $$varA8 = $Social_Interaction;
				  $Hygiene = Trim($_POST['Hygiene'.$cnt]);
                  $varA9 = "Hygiene_".$cnt;
                  $$varA9 = $Hygiene;
				  $Group_Work         = Trim($_POST['Group_Work'.$cnt]);
                  $varA10 = "Group_Work_".$cnt;
                  $$varA10 = $Group_Work;
				  $Sports_Games   	  = Trim($_POST['Sports_Games'.$cnt]);
                  $varA11 = "Sports_Games_".$cnt;
                  $$varA11 = $Sports_Games;
				  $Art_Craft      	  = Trim($_POST['Art_Craft'.$cnt]);
                  $varA12 = "Art_Craft_".$cnt;
                  $$varA12 = $Art_Craft;
				  $Computer_Skills    = Trim($_POST['Computer_Skills'.$cnt]);
                  $varA13 = "Computer_Skills_".$cnt;
                  $$varA13 = $Computer_Skills;
				  $Projects	          = Trim($_POST['Projects'.$cnt]);
                  $varA14 = "Projects_".$cnt;
                  $$varA14 = $Projects;
				  $Participation	  = Trim($_POST['Participation'.$cnt]);
                  $varA15 = "Participation_".$cnt;    
                  $$varA15 = $Participation;
				  $Fine_Motor_Skills  = Trim($_POST['Fine_Motor_Skills'.$cnt]);
                  $varA16 = "Fine_Motor_Skills_".$cnt;
                  $$varA16 = $Fine_Motor_Skills;
			      $Gross_Motor_Skills = Trim($_POST['Gross_Motor_Skills'.$cnt]);
                  $varA17 = "Gross_Motor_Skills_".$cnt;
                  $$varA17 = $Gross_Motor_Skills;
		     	  $Noday_Attendance	  = Trim($_POST['Noday_Attendance'.$cnt]);
                  $varA18 = "Noday_Attendance_".$cnt;
                  $$varA18 = $Noday_Attendance;
				  $CommentCode	      = Trim($_POST['EnglishComment_1'.$cnt]);
                  $varA19 = "CommentCode_".$cnt;
                  $$varA19 = $CommentCode;
                                  
              	  $English_Classwrk	  = Trim($_POST['English_Classwrk'.$cnt]);
                  $varA20 = "English_Classwrk_".$cnt;
                  $$varA20 = $English_Classwrk;
				  $English_Examgrade  = Trim($_POST['English_Examgrade'.$cnt]);
                  $varA21 = "English_Examgrade_".$cnt;
                  $$varA21 = $English_Examgrade;
				  $English_Totalgrade = Trim($_POST['English_Totalgrade'.$cnt]);
                  $varA22 = "English_Totalgrade_".$cnt;
                  $$varA22 = $English_Totalgrade;
				  $Tamil_Classwrk 	  = Trim($_POST['Tamil_Classwrk'.$cnt]);
                  $varA23 = "Tamil_Classwrk_".$cnt;
                  $$varA23 = $Tamil_Classwrk;
				  $Tamil_Examgrade    = Trim($_POST['Tamil_Examgrade'.$cnt]);
                  $varA24 = "Tamil_Examgrade_".$cnt;
                  $$varA24 = $Tamil_Examgrade;
				  $Tamil_Totalgrade	  = Trim($_POST['Tamil_Totalgrade'.$cnt]);
                  $varA25 = "Tamil_Totalgrade_".$cnt;
                  $$varA25 = $Tamil_Totalgrade;
				  $Maths_Classwrk	  = Trim($_POST['Maths_Classwrk'.$cnt]);
                  $varA26 = "Maths_Classwrk_".$cnt;
                  $$varA26 = $Maths_Classwrk;
				  $Maths_Examgrade    = Trim($_POST['Maths_Examgrade'.$cnt]);
                  $varA27 = "Maths_Examgrade_".$cnt;
                  $$varA27 = $Maths_Examgrade;
				  $Maths_Totalgrade	  = Trim($_POST['Maths_Totalgrade'.$cnt]);
                  $varA28 = "Maths_Totalgrade_".$cnt;
                  $$varA28 = $Maths_Totalgrade;
				  $Science_Classwrk	  = Trim($_POST['Science_Classwrk'.$cnt]);
                  $varA29 = "Science_Classwrk_".$cnt;
                  $$varA29 = $Science_Classwrk;
				  $Science_Examgrade  = Trim($_POST['Science_Examgrade'.$cnt]);
                  $varA30 = "Science_Examgrade_".$cnt;
                  $$varA30 = $Science_Examgrade;
				  $Science_Totalgrade = Trim($_POST['Science_Totalgrade'.$cnt]);
                  $varA31 = "Science_Totalgrade_".$cnt;
                  $$varA31 = $Science_Totalgrade;
				  $Sscience_Classwrk  = Trim($_POST['Sscience_Classwrk'.$cnt]);
                  $varA32 = "Sscience_Classwrk_".$cnt;
                  $$varA32 = $Sscience_Classwrk;
				  $Sscience_Examgrade = Trim($_POST['Sscience_Examgrade'.$cnt]);
                  $varA33 = "Sscience_Examgrade_".$cnt;
                  $$varA33 = $Sscience_Examgrade;
				  $Sscience_Totalgrade= Trim($_POST['Sscience_Totalgrade'.$cnt]);
                  $varA34 = "Sscience_Totalgrade_".$cnt;
                  $$varA34 = $Sscience_Totalgrade;
				  $Computer_Classwrk  = Trim($_POST['Computer_Classwrk'.$cnt]);
                  $varA35 = "Computer_Classwrk_".$cnt;
                  $$varA35 = $Computer_Classwrk;
				  $Computer_Examgrade = Trim($_POST['Computer_Examgrade'.$cnt]);
                  $varA36 = "Computer_Examgrade_".$cnt;
                  $$varA36 = $Computer_Examgrade;
				  $Computer_Totalgrade= Trim($_POST['Computer_Totalgrade'.$cnt]);
                  $varA37 = "Computer_Totalgrade_".$cnt;
                  $$varA37 = $Computer_Totalgrade;
				  $Project_Classwrk	  = Trim($_POST['Project_Classwrk'.$cnt]);
                  $varA38 = "Project_Classwrk_".$cnt;
                  $$varA38 = $Project_Classwrk;
			 	  $Project_Examgrade  = Trim($_POST['Project_Examgrade'.$cnt]);
                  $varA39 = "Project_Examgrade_".$cnt;
                  $$varA39 = $Project_Examgrade;
                  $Project_Totalgrade = Trim($_POST['Project_Totalgrade'.$cnt]);
                  $varA40 = "Project_Totalgrade_".$cnt;
                  $$varA40 = $Project_Totalgrade;
				  $Eng_Comprehension  = Trim($_POST['Eng_Comprehension'.$cnt]);
                  $varA41 = "Eng_Comprehension_".$cnt;
                  $$varA41 = $Eng_Comprehension;
				  $Eng_Speaking       = Trim($_POST['Eng_Speaking'.$cnt]);
                  $varA42 = "Eng_Speaking_".$cnt;
                  $$varA42 = $Eng_Speaking;
				  $Eng_Reading        = Trim($_POST['Eng_Reading'.$cnt]);
                  $varA43 = "Eng_Reading_".$cnt;
                  $$varA43 = $Eng_Reading;
				  $Eng_Writing        = Trim($_POST['Eng_Writing'.$cnt]);
                  $varA44 = "Eng_Writing_".$cnt;
                  $$varA44 = $Eng_Writing;
				  $Tam_Comprehension  = Trim($_POST['Tam_Comprehension'.$cnt]);
                  $varA45 = "Tam_Comprehension_".$cnt;
                  $$varA45 = $Tam_Comprehension;
				  $Tam_Speaking       = Trim($_POST['Tam_Speaking'.$cnt]);
                  $varA46 = "Tam_Speaking_".$cnt;
                  $$varA46 = $Tam_Speaking;
				  $Tam_Reading        = Trim($_POST['Tam_Reading'.$cnt]);
                  $varA47 = "Tam_Reading_".$cnt;
                  $$varA47 = $Tam_Reading;
				  $Tam_Writing        = Trim($_POST['Tam_Writing'.$cnt]);
                  $varA48 = "Tam_Writing_".$cnt;
                  $$varA48 = $Tam_Writing;
				  
				  $Mat_Comprehension  = Trim($_POST['Mat_Comprehension'.$cnt]);
                  $varA49 = "Mat_Comprehension_".$cnt;
                  $$varA49 = $Mat_Comprehension;
				  $Scn_Comprehension  = Trim($_POST['Scn_Comprehension'.$cnt]);
                  $varA50 = "Scn_Comprehension_".$cnt;
                  $$varA50 = $Scn_Comprehension;
                  $Admission_No1  = Trim($_POST['Admission_No'.$cnt]);
                  $varA51 = "Admission_No_".$cnt;
                  $$varA51 = $Admission_No1;
                  $Name  = Trim($_POST['Name'.$cnt]);
                  $varA52 = "Name_".$cnt;
                  $$varA52 = $Name;
                  $Dcode   = Trim($_POST['Dcode'.$cnt]);
                  							 
			      $j=$cnt+1;
				 
		          //Empty Field Check The Field Value Empty Means The error Messages
                  //Will Be Displayed
     
                  
                if($Class > 2 ) {
				 $dummy = NumCheck($Spoken_English,$errmsg,$errflag,"Spoken English".$cnt);
                 $dummy = NumCheck($Writing_Neatness,$errmsg,$errflag,"Writing Neatness".$cnt);
                 $dummy = NumCheck($Responsibilty,$errmsg,$errflag,"Responsibilty".$cnt);
                 $dummy = NumCheck($Sports_Games,$errmsg,$errflag,"Sports Games".$cnt);
                 $dummy = NumCheck($Art_Craft,$errmsg,$errflag,"Art Craft".$cnt);
                 $dummy = NumCheck($Computer_Skills,$errmsg,$errflag,"Computer Skills".$cnt);
                 $dummy = NumCheck($Projects,$errmsg,$errflag,"Projects".$cnt);
                 $dummy = NumCheck($English_Classwrk,$errmsg,$errflag,"English Classwrk".$cnt);
                 $dummy = NumCheck($English_Examgrade,$errmsg,$errflag,"English Examgrade".$cnt);
                 $dummy = NumCheck($English_Totalgrade,$errmsg,$errflag,"English Totalgrade".$cnt);
                 $dummy = NumCheck($Tamil_Classwrk,$errmsg,$errflag,"Tamil Classwrk".$cnt);
                 $dummy = NumCheck($Tamil_Examgrade,$errmsg,$errflag,"Tamil Examgrade".$cnt);
				 $dummy = NumCheck($Tamil_Totalgrade,$errmsg,$errflag,"Tamil Totalgrade".$cnt);
				 $dummy = NumCheck($Maths_Classwrk,$errmsg,$errflag,"Maths Classwrk".$cnt);
				 $dummy = NumCheck($Maths_Examgrade,$errmsg,$errflag,"Maths Examgrade".$cnt);
				 $dummy = NumCheck($Maths_Totalgrade,$errmsg,$errflag,"Maths Totalgrade".$cnt);
		    	 $dummy = NumCheck($Science_Classwrk,$errmsg,$errflag,"Science Classwrk".$cnt);
		    	 $dummy = NumCheck($Science_Examgrade,$errmsg,$errflag,"Science Examgrade".$cnt);
		    	 $dummy = NumCheck($Science_Totalgrade,$errmsg,$errflag,"Science Totalgrade".$cnt);
                 $dummy = NumCheck($Computer_Classwrk,$errmsg,$errflag,"Computer Classwrk".$cnt);
				 $dummy = NumCheck($Computer_Examgrade,$errmsg,$errflag,"Computer Examgrade".$cnt);
				 $dummy = NumCheck($Computer_Totalgrade,$errmsg,$errflag,"Computer Totalgrade".$cnt);
				 $dummy = NumCheck($Project_Classwrk,$errmsg,$errflag,"Project Classwrk".$cnt);
				 $dummy = NumCheck($Project_Examgrade,$errmsg,$errflag,"Project Examgrade".$cnt);
				 $dummy = NumCheck($Project_Totalgrade,$errmsg,$errflag,"Project Totalgrade".$cnt);
				    }
			     $dummy = NumCheck($Enthusiam_Energy,$errmsg,$errflag,"Enthusiam Energy".$cnt);
			     $dummy = NumCheck($Dicipline,$errmsg,$errflag,"Dicipline".$cnt);
				 $dummy = NumCheck($Attention_Span,$errmsg,$errflag,"Attention Span".$cnt);
		    	 $dummy = NumCheck($Punctuality,$errmsg,$errflag,"Punctuality".$cnt);
			     $dummy = NumCheck($Social_Interaction,$errmsg,$errflag,"Social Interaction".$cnt);
			     $dummy = NumCheck($Hygiene,$errmsg,$errflag,"Hygiene".$cnt);
				 $dummy = NumCheck($Noday_Attendance,$errmsg,$errflag,"Noday Attendance".$cnt);
                if($Class <= 2 ) {
     	         $dummy = NumCheck($Participation,$errmsg,$errflag,"Participation".$cnt);
                 $dummy = NumCheck($Fine_Motor_Skills,$errmsg,$errflag,"Fine Motor Skills".$cnt);
                 $dummy = NumCheck($Gross_Motor_Skills,$errmsg,$errflag,"Gross Motor Skills".$cnt);
                 $dummy = NumCheck($Eng_Comprehension,$errmsg,$errflag,"English Comprehension".$cnt);
                 $dummy = NumCheck($Eng_Speaking,$errmsg,$errflag,"English Speaking".$cnt);
                 $dummy = NumCheck($Eng_Reading,$errmsg,$errflag,"English Reading".$cnt);
                 $dummy = NumCheck($Eng_Writing,$errmsg,$errflag,"English Writing".$cnt);
                 $dummy = NumCheck($Tam_Comprehension,$errmsg,$errflag,"Tamil Comprehension".$cnt);
                 $dummy = NumCheck($Tam_Speaking,$errmsg,$errflag,"Tamil Speaking".$cnt);
                 $dummy = NumCheck($Tam_Reading,$errmsg,$errflag,"Tamil Reading".$cnt);
                 $dummy = NumCheck($Tam_Writing,$errmsg,$errflag,"Tamil Writing".$cnt);
                 $dummy = NumCheck($Mat_Comprehension,$errmsg,$errflag,"Maths Comprehension".$cnt);
                 $dummy = NumCheck($Scn_Comprehension,$errmsg,$errflag,"Science Comprehension".$cnt);
                   }
                if($Class > 4 ) {
                 $dummy = NumCheck($Sscience_Classwrk,$errmsg,$errflag,"Sscience Classwrk".$cnt); 
			     $dummy = NumCheck($Sscience_Examgrade,$errmsg,$errflag,"Sscience Examgrade".$cnt); 
				 $dummy = NumCheck($Sscience_Totalgrade,$errmsg,$errflag,"Sscience Totalgrade".$cnt);
                  }
				
               
			 }
        	   if($errflag==0)
		      {
			  	mssql_free_result($result);
	            $query = mssql_init('sp_Assesment_Header',$mssql);
				mssql_bind($query,'@Ass_Header_Code',$Ass_Header_Code,SQLINT4,false,false,5);
				mssql_bind($query,'@Term_Code ',$Term_Code,SQLINT4,false,false,5);		
				mssql_bind($query,'@Class_Code ',$Class,SQLINT4,false,false,5);
				mssql_bind($query,'@Section_code ',$Section,SQLINT4,false,false,5);	     
               // mssql_bind($query,'@Term_Date',$Term_Date, SQLVARCHAR,false,false,20); 
                if(strlen($Term_Date)==10) 
                $Term_Date1 = date('Y-m-d', strtotime($Term_Date));                                               
				mssql_bind($query,'@Term_Date',$Term_Date1,SQLVARCHAR,false,false,20);                
				mssql_bind($query,'@Teacher_Code ',$TeacherName,SQLINT4,false,false,5);	
				mssql_bind($query,'@Working_Days ',$WorkingDays,SQLINT4,false,false,5);
				mssql_bind($query,'@Period_Code ',$PeriodName,SQLINT4,false,false,5);
                mssql_bind($query,'@School_Id ',$Location,SQLINT4,false,false,5);
				mssql_bind($query,'@Ass_Header_Code1',$Ass_Header_Code1,SQLINT4,true);	
				$result = @mssql_execute($query);	
				mssql_free_statement($query);
                if($result){
                for($cnt=1;$cnt<=$scount;$cnt++) 
			   {
                  $Ass_Header_Code    = Trim($_POST['Ass_Header_Code'.$cnt]);				                    
                  $Student_Id         = Trim($_POST['Student_Id'.$cnt]);
                  
				  $Spoken_English     = Trim($_POST['Spoken_English'.$cnt]);
				  $Writing_Neatness	  = Trim($_POST['Writing_Neatness'.$cnt]);
				  $Enthusiam_Energy	  = Trim($_POST['Enthusiam_Energy'.$cnt]);	
				  $Responsibilty      = Trim($_POST['Responsibilty'.$cnt]);
				  $Dicipline      	  = Trim($_POST['Dicipline'.$cnt]);
				  $Attention_Span  	  = Trim($_POST['Attention_Span'.$cnt]);
				  $Punctuality        = Trim($_POST['Punctuality'.$cnt]);
				  $Social_Interaction = Trim($_POST['Social_Interaction'.$cnt]);
				  $Hygiene	          = Trim($_POST['Hygiene'.$cnt]);
				  $Group_Work         = Trim($_POST['Group_Work'.$cnt]);
				  $Sports_Games   	  = Trim($_POST['Sports_Games'.$cnt]);
				  $Art_Craft      	  = Trim($_POST['Art_Craft'.$cnt]);
				  $Computer_Skills    = Trim($_POST['Computer_Skills'.$cnt]);
				  $Projects	          = Trim($_POST['Projects'.$cnt]);
				  $Participation	  = Trim($_POST['Participation'.$cnt]);
				  $Fine_Motor_Skills  = Trim($_POST['Fine_Motor_Skills'.$cnt]);
			      $Gross_Motor_Skills = Trim($_POST['Gross_Motor_Skills'.$cnt]);
		     	  $Noday_Attendance	  = Trim($_POST['Noday_Attendance'.$cnt]);
				  $CommentCode	      = Trim($_POST['EnglishComment_1'.$cnt]);
                  $English_Classwrk	  = Trim($_POST['English_Classwrk'.$cnt]);
				  $English_Examgrade  = Trim($_POST['English_Examgrade'.$cnt]);
				  $English_Totalgrade = Trim($_POST['English_Totalgrade'.$cnt]);
				  $Tamil_Classwrk 	  = Trim($_POST['Tamil_Classwrk'.$cnt]);
				  $Tamil_Examgrade    = Trim($_POST['Tamil_Examgrade'.$cnt]);
				  $Tamil_Totalgrade	  = Trim($_POST['Tamil_Totalgrade'.$cnt]);
				  $Maths_Classwrk	  = Trim($_POST['Maths_Classwrk'.$cnt]);
				  $Maths_Examgrade    = Trim($_POST['Maths_Examgrade'.$cnt]);
				  $Maths_Totalgrade	  = Trim($_POST['Maths_Totalgrade'.$cnt]);
				  $Science_Classwrk	  = Trim($_POST['Science_Classwrk'.$cnt]);
				  $Science_Examgrade  = Trim($_POST['Science_Examgrade'.$cnt]);
				  $Science_Totalgrade = Trim($_POST['Science_Totalgrade'.$cnt]);
				  $Sscience_Classwrk  = Trim($_POST['Sscience_Classwrk'.$cnt]);
				  $Sscience_Examgrade = Trim($_POST['Sscience_Examgrade'.$cnt]);
				  $Sscience_Totalgrade= Trim($_POST['Sscience_Totalgrade'.$cnt]);
				  $Computer_Classwrk  = Trim($_POST['Computer_Classwrk'.$cnt]);
				  $Computer_Examgrade = Trim($_POST['Computer_Examgrade'.$cnt]);
				  $Computer_Totalgrade= Trim($_POST['Computer_Totalgrade'.$cnt]);
				  $Project_Classwrk	  = Trim($_POST['Project_Classwrk'.$cnt]);
			 	  $Project_Examgrade  = Trim($_POST['Project_Examgrade'.$cnt]);
                  $Project_Totalgrade = Trim($_POST['Project_Totalgrade'.$cnt]);
				  $Eng_Comprehension  = Trim($_POST['Eng_Comprehension'.$cnt]);
				  $Eng_Speaking       = Trim($_POST['Eng_Speaking'.$cnt]);
				  $Eng_Reading        = Trim($_POST['Eng_Reading'.$cnt]);
				  $Eng_Writing        = Trim($_POST['Eng_Writing'.$cnt]);
				  $Tam_Comprehension  = Trim($_POST['Tam_Comprehension'.$cnt]);
				  $Tam_Speaking       = Trim($_POST['Tam_Speaking'.$cnt]);
				  $Tam_Reading        = Trim($_POST['Tam_Reading'.$cnt]);
				  $Tam_Writing        = Trim($_POST['Tam_Writing'.$cnt]);
				  $Mat_Comprehension  = Trim($_POST['Mat_Comprehension'.$cnt]);
				  $Scn_Comprehension  = Trim($_POST['Scn_Comprehension'.$cnt]);
                  $Dcode              = Trim($_POST['Dcode'.$cnt]);
                  mssql_free_result($result);
			      $query = mssql_init('sp_Assesment_Detail',$mssql);
                  If($Ass_Header_Code>0) $Ass_Header_Code1 =$Ass_Header_Code; 
                  	
				  mssql_bind($query,'@Ass_Header_Code',$Ass_Header_Code1,SQLINT4,false,false,5);
				  mssql_bind($query,'@Student_Id',$Student_Id,SQLINT4,false,false,5);		
			                       
				  mssql_bind($query,'@Spoken_English',$Spoken_English,SQLINT4,false,false,5);	
				  mssql_bind($query,'@Writing_Neatness',$Writing_Neatness,SQLINT4,false,false,5);
				  mssql_bind($query,'@Enthusiam_Energy',$Enthusiam_Energy,SQLINT4,false,false,5);	
				  mssql_bind($query,'@Responsibilty',$Responsibilty,SQLINT4,false,false,5);
				  mssql_bind($query,'@Dicipline',$Dicipline,SQLINT4,false,false,5);		
				  mssql_bind($query,'@Attention_Span',$Attention_Span,SQLINT4,false,false,5);
				  mssql_bind($query,'@Punctuality',$Punctuality,SQLINT4,false,false,5);		
				  mssql_bind($query,'@Social_Interaction',$Social_Interaction,SQLINT4,false,false,5);
				  mssql_bind($query,'@Hygiene',$Hygiene,SQLINT4,false,false,5);		
			      mssql_bind($query,'@Group_Work',$Group_Work,SQLINT4,false,false,5);
				  mssql_bind($query,'@Sports_Games',$Sports_Games,SQLINT4,false,false,5);		
				  mssql_bind($query,'@Art_Craft',$Art_Craft,SQLINT4,false,false,5);
				  mssql_bind($query,'@Computer_Skills',$Computer_Skills,SQLINT4,false,false,5);	
     			  mssql_bind($query,'@Projects',$Projects,SQLINT4,false,false,5);
				  mssql_bind($query,'@Participation',$Participation,SQLINT4,false,false,5);
                  mssql_bind($query,'@Fine_Motor_Skills',$Fine_Motor_Skills,SQLINT4,false,false,5);
				  mssql_bind($query,'@Gross_Motor_Skills',$Gross_Motor_Skills,SQLINT4,false,false,5);	
				  mssql_bind($query,'@Noday_Attendance',$Noday_Attendance,SQLINT4,false,false,5);
				  mssql_bind($query,'@English_Classwrk',$English_Classwrk,SQLINT4,false,false,5);	
     			  mssql_bind($query,'@English_Examgrade',$English_Examgrade,SQLINT4,false,false,5);
                  mssql_bind($query,'@CommentCode',$CommentCode,SQLINT4,false,false,5);	
     	     	  mssql_bind($query,'@English_Totalgrade',$English_Totalgrade,SQLINT4,false,false,5);
                  mssql_bind($query,'@Tamil_Classwrk',$Tamil_Classwrk,SQLINT4,false,false,5);
				  mssql_bind($query,'@Tamil_Examgrade',$Tamil_Examgrade,SQLINT4,false,false,5);	
				  mssql_bind($query,'@Tamil_Totalgrade',$Tamil_Totalgrade,SQLINT4,false,false,5);	
     			  mssql_bind($query,'@Maths_Classwrk',$Maths_Classwrk,SQLINT4,false,false,5);
				  mssql_bind($query,'@Maths_Examgrade',$Maths_Examgrade,SQLINT4,false,false,5);
                  mssql_bind($query,'@Maths_Totalgrade',$Maths_Totalgrade,SQLINT4,false,false,5);
				  mssql_bind($query,'@Science_Classwrk',$Science_Classwrk,SQLINT4,false,false,5);	
				  mssql_bind($query,'@Science_Examgrade',$Science_Examgrade,SQLINT4,false,false,5);
				  mssql_bind($query,'@Science_Totalgrade',$Science_Totalgrade,SQLINT4,false,false,5);
				  mssql_bind($query,'@Sscience_Classwrk',$Sscience_Classwrk,SQLINT4,false,false,5);
				  mssql_bind($query,'@Sscience_Examgrade',$Sscience_Examgrade,SQLINT4,false,false,5);	
				  mssql_bind($query,'@Sscience_Totalgrade',$Sscience_Totalgrade,SQLINT4,false,false,5);
				  mssql_bind($query,'@Computer_Classwrk',$Computer_Classwrk,SQLINT4,false,false,5);
				  mssql_bind($query,'@Computer_Examgrade',$Computer_Examgrade,SQLINT4,false,false,5);
				  mssql_bind($query,'@Computer_Totalgrade',$Computer_Totalgrade,SQLINT4,false,false,5);	
				  mssql_bind($query,'@Project_Classwrk',$Project_Classwrk,SQLINT4,false,false,5);
				  mssql_bind($query,'@Project_Examgrade',$Project_Examgrade,SQLINT4,false,false,5);
				  mssql_bind($query,'@Project_Totalgrade',$Project_Totalgrade,SQLINT4,false,false,5);
				  mssql_bind($query,'@Eng_Comprehension',$Eng_Comprehension,SQLINT4,false,false,5);	
				  mssql_bind($query,'@Eng_Speaking',$Eng_Speaking,SQLINT4,false,false,5);
				  mssql_bind($query,'@Eng_Reading',$Eng_Reading,SQLINT4,false,false,5);
				  mssql_bind($query,'@Eng_Writing',$Eng_Writing,SQLINT4,false,false,5);	
				  mssql_bind($query,'@Tam_Comprehension',$Tam_Comprehension,SQLINT4,false,false,5);
				  mssql_bind($query,'@Tam_Speaking',$Tam_Speaking,SQLINT4,false,false,5);
				  mssql_bind($query,'@Tam_Reading',$Tam_Reading,SQLINT4,false,false,5);	
				  mssql_bind($query,'@Tam_Writing',$Tam_Writing,SQLINT4,false,false,5);
                  mssql_bind($query,'@Mat_Comprehension',$Mat_Comprehension,SQLINT4,false,false,5);
				  mssql_bind($query,'@Scn_Comprehension',$Scn_Comprehension,SQLINT4,false,false,5);
                  mssql_bind($query,'@Dcode',$Dcode,SQLINT4,false,false,5);
                  $result = @mssql_execute($query);
				  mssql_free_statement($query);
            
				  if(!$result)					
				  {
                    $errmsg1.=mssql_get_last_message();
                    If ($errcnt == 0) { $errcnt = 1;} else { $errcnt = $errcnt + 1; }
                    if ($errval == "") { $errval = $scount[$cnt];	 } else { $errval = $errval.",".$scount[$cnt]; } 
                  }
                }
                
                } 
                else { $errmsg1.=mssql_get_last_message(); $errflag=1; $warn=1; }
			}
            
			else {
				If ($errcnt == 0) { $errcnt = 1;} else { $errcnt = $errcnt + 1; }
				if ($errval == "") { $errval = $scount[$cnt];	 } else { $errval = $errval.",".$scount[$cnt]; } 
				}
			//Error Message String Length Must 150 Characters
			if($errflag==1) {
                if($warn==1)
                    echo "<P CLASS='error'>".$errmsg1."</p>";
                else {
				if(strlen($errmsg)<150)
					echo $errlbl.$errmsg;
			    else
					echo "<P CLASS='error'>Please fill all the fields</p>";
                }
			} 
            
            else if($errcnt>0) 
                    echo $errmsg1;
            else echo "<p class='mesg'>Record has been saved</p>";
}            
    if($_POST['Go'] =='Go')	
	{   
	   $Location        =   Trim($_POST['Location']);
	   $Class           =   Trim($_POST['Class']);	
	   $Section         =   Trim($_POST['Section']);
	   $Admission_No	=	Trim($_POST['Admission_No']);
       $Term	        =	Trim($_POST['Term']);
       if($Term>0) $Term_Code = $Term;
       //The Field Value Should be Empty Means The Error Message Displayed
	   $dummy = ZeroCheck($Location,$errmsg,$errflag,"Location"); 
	   $dummy = ZeroCheck($Class,$errmsg,$errflag,"Class"); 
	   $dummy = ZeroCheck($Section,$errmsg,$errflag,"Section"); 

    if($errflag==1) {
	   echo $errlbl.$errmsg;
       $errflag=2;
	}
}	?>
	<body>
    <form id="myform" name="myform" method="post" action="newreport.php" >
    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr><td align="center">
     <!--<div style= "display:block;width:100%; overflow:auto" id="scholarshipfr" align="center">-->
        <div id="scholarshipfr" align="center" style="width: 100%;">
        <table border="0"  cellpadding="1" cellspacing="1" width="100%" align="center">
        <tr>
                    <?php 	//SHOW Location  DROPDOWN
					mssql_free_result($result);
						$query = mssql_init('[sp_selectschoolname]',$mssql);
                        mssql_bind($query, '@School_id', $_SESSION['SchoolId'], SQLINT4, false, false, 5);
						$result = @mssql_execute($query);
						mssql_free_statement($query);	?>
                   <td width="100" align="right" style="width:100px;" >School<?php echo $mand; ?> </td>
                   <td width="130" align="left" ><div id ="Location">
                   <select id="Location" name="Location" style="width:125px" >
                        <?php echo $Select;?>
                        <?php while($field = mssql_fetch_array($result))   { 	   ?>
	                        <option value="<?php echo $field['School_id']?>" <?php if($Location==$field['School_id']) echo "Selected"; ?>>
                                                    <?php echo $field['SchoolName']?>                           </option>
                        <?php } ?>
                    </select></div>                    </td>
                    <?php 	//SHOW CLASS DROPDOWN
		mssql_free_result($result);			
		$query = mssql_init('sp_GetClass',$mssql);
		$result = @mssql_execute($query);
		mssql_free_statement($query);	?>
                    <td width="100" align="right" style="width:100px;">Class<?php echo $mand; ?> </td>
                    <td width="54" align="right"><div id ="Class">
                        <select id="Class" name="Class">
                          <?php echo $Select;?>
                          <?php while($field = mssql_fetch_array($result)) 	{  ?>
                          <option value="<?php echo $field['Class_Code']?>" <?php if( $Class==$field['Class_Code']) echo "Selected"; ?>> <?php echo $field['Class']?></option>
                          <?php } ?>
                        </select>
                      </div>				    </td>
<?php 	//SHOW SECTION DROPDOWN
		mssql_free_result($result);		
		$query = mssql_init('sp_GetSection',$mssql);
		$result = @mssql_execute($query);
		mssql_free_statement($query);	?>
                    <td width="100" align="right" style="width:100px;">Section<?php echo $mand; ?> </td>
<td width="143" align="left"><div id ="Section">
                        <select id="Section" name="Section">
                          <?php echo $Select;?>
                          <?php while($field = mssql_fetch_array($result)) 
   {  ?>
                          <option value="<?php echo $field['Section_Code']?>" <?php if($Section==$field['Section_Code']) echo "selected"; ?>> <?php echo $field['Section']?></option>
                          <?php } ?>
                        </select>
                    </div></td>
                    <td width="93" align="right">Adm. No.</td>
                    <td width="152" align="left"><div id ="Admission_No"></div>
                      <input type="text" name="Admission_No" id="Admission_No" maxlength="15" value="<?php echo $Admission_No ?>" onkeydown="return numberonly('Admission_No')" /></td>
          
<?php 	//SHOW TERM DROPDOWN
mssql_free_result($result);		
		$query = mssql_init('sp_GetTerm',$mssql);
		$result = mssql_execute($query);
		mssql_free_statement($query);	?>
                    <td width="32" align="right">Term</td>
                <td width="150" align="left"><div id ="Term">
                        <select id="Term" name="Term">
                          <?php echo $Select;?>
                          <?php while($field = mssql_fetch_array($result))    {  ?>
                          <option value="<?php echo $field['Term_Code']?>" <?php if($Term==$field['Term_Code']) echo "selected"; ?>> <?php echo $field['Term']?></option>
                          <?php } ?>
                        </select>
                    </div></td>
                    <td width="162" align="right"><input name="Go"  type="submit" value="Go" class="winbutton_go" /></td>
            </tr>
                </table></div></td></tr>
   <?php   
   if(($_POST['Go'] =='Go' and $errflag==0) or ($_POST['Save'] =='Save' or $errflag==1) )
    {   
        //Particular Fields Only Search  sp_NewReport sp_NewStudentReport
        If($errflag==0) {
		mssql_free_result($Rptresult);		
    	   	$query = mssql_init('sp_Reportcardview',$mssql);
            mssql_bind($query,'@School_id',$Location,SQLINT4,false,false,5);          
            mssql_bind($query,'@Section_Code',$Section,SQLINT4,false,false,5);           
            mssql_bind($query,'@Class_Code',$Class,SQLINT4,false,false,5);         
    		if(strlen($Admission_No)==0) $Admission_No=0;
            mssql_bind($query,'@Admission_No',$Admission_No,SQLVARCHAR,false,false,25);
            mssql_bind($query,'@Term_Code',$Term,SQLINT4,false,false,5);
            $Rptresult = @mssql_execute($query);
            mssql_free_statement($query);
            if($Rptresult)
            {
                if(mssql_num_rows($Rptresult)==0) {?>
              <p class="error">No Records Found...</p>  
<?php           exit(); 
            }  else 
            {
            $Count = mssql_num_rows($Rptresult);
            $scount = $Count;
            if($Count > 0)
   	        {
        		$colorflag = 0;
        		$i = 0;	  $cnt=0;    ?>
<?php       while($field = mssql_fetch_array($Rptresult))
			{ 
				$i  +=1;	$colorflag+=1;
				$tot_rec = $i; $cnt +=1;	
				//VALUE GET FROM DB
                
                  $Term	          =	 Trim($field['Term_Code']);                    
                  $Term_Date      =  Trim($field['Term_date']);
                  $TeacherName    =  Trim($field['Teacher_code']);
                  $WorkingDays	  =	 Trim($field['Working_Days']);
                  $PeriodName   	  =	 Trim($field['Period_Code']);
				  $Ass_Header_Code = $field['Ass_Header_Code']; 
                                   
                  if(strlen($Term_Date)>0) {
                    $Term_Date = str_replace("/","-",$Term_Date);
                    $Term_Date = date('d-m-Y', strtotime($Term_Date));
                  }

                  $Ass_Header_Code = $field['Ass_Header_Code']; 
                  $Spoken_English     = $field['Spoken_English'];
                  $varA1 = "Spoken_English_".$cnt;
                  $$varA1 = $Spoken_English;
                  $Writing_Neatness	  = Trim($field['Writing_Neatness']);
                  $varA2 = "Writing_Neatness_".$cnt;
                  $$varA2 = $Writing_Neatness;
				  $Enthusiam_Energy	  = Trim($field['Enthusiam_Energy']);	
                  $varA3 = "Enthusiam_Energy_".$cnt;
                  $$varA3 = $Enthusiam_Energy;
				  $Responsibilty      = Trim($field['Responsibilty']);
                  $varA4 = "Responsibilty_".$cnt;
                  $$varA4 = $Responsibilty;
				  $Dicipline      	  = Trim($field['Dicipline']);
                  $varA5 = "Dicipline_".$cnt;
                  $$varA5 = $Dicipline;
				  $Attention_Span  	  = Trim($field['Attention_Span']);
                  $varA6 = "Attention_Span_".$cnt;
                  $$varA6 = $Attention_Span;
				  $Punctuality        = Trim($field['Punctuality']);
                  $varA7 = "Punctuality_".$cnt;
                  $$varA7 = $Punctuality;
				  $Social_Interaction = Trim($field['Social_Interaction']);
                  $varA8 = "Social_Interaction_".$cnt;
                  $$varA8 = $Social_Interaction;
				  $Hygiene	          = Trim($field['Hygiene']);
                  $varA9 = "Hygiene_".$cnt;
                  $$varA9 = $Hygiene;
				  $Group_Work         = Trim($field['Group_Work']);
                  $varA10 = "Group_Work_".$cnt;
                  $$varA10 = $Group_Work;
				  $Sports_Games   	  = Trim($field['Sports_Games']);
                  $varA11 = "Sports_Games_".$cnt;
                  $$varA11 = $Sports_Games;
				  $Art_Craft      	  = Trim($field['Art_Craft']);
                  $varA12 = "Art_Craft_".$cnt;
                  $$varA12 = $Art_Craft;
				  $Computer_Skills    = Trim($field['Computer_Skills']);
                  $varA13 = "Computer_Skills_".$cnt;
                  $$varA13 = $Computer_Skills;
				  $Projects	          = Trim($field['Projects']);
                  $varA14 = "Projects_".$cnt;
                  $$varA14 = $Projects;
				  $Participation	  = Trim($field['Participation']);
                  $varA15 = "Participation_".$cnt;
                  $$varA15 = $Participation;
				  $Fine_Motor_Skills  = Trim($field['Fine_Motor_Skills']);
                  $varA16 = "Fine_Motor_Skills_".$cnt;
                  $$varA16 = $Fine_Motor_Skills;
			      $Gross_Motor_Skills = Trim($field['Gross_Motor_Skills']);
                  $varA17 = "Gross_Motor_Skills_".$cnt;
                  $$varA17 = $Gross_Motor_Skills;
		     	  $Noday_Attendance	  = Trim($field['Noday_Attendance']);
                  $varA18 = "Noday_Attendance_".$cnt;
                  $$varA18 = $Noday_Attendance;
				  $CommentCode	      = Trim($field['CommentCode']);
                  $varA19 = "CommentCode_".$cnt;
                  $$varA19 = $CommentCode;
				  $English_Classwrk	  = Trim($field['English_Classwrk']);
                  $varA20 = "English_Classwrk_".$cnt;
                  $$varA20 = $English_Classwrk;
				  $English_Examgrade  = Trim($field['English_Examgrade']);
                  $varA21 = "English_Examgrade_".$cnt;
                  $$varA21 = $English_Examgrade;
				  $English_Totalgrade = Trim($field['English_Totalgrade']);
                  $varA22 = "English_Totalgrade_".$cnt;
                  $$varA22 = $English_Totalgrade;
				  $Tamil_Classwrk 	  = Trim($field['Tamil_Classwrk']);
                  $varA23 = "Tamil_Classwrk_".$cnt;
                  $$varA23 = $Tamil_Classwrk;
				  $Tamil_Examgrade    = Trim($field['Tamil_Examgrade']);
                  $varA24 = "Tamil_Examgrade_".$cnt;
                  $$varA24 = $Tamil_Examgrade;
				  $Tamil_Totalgrade	  = Trim($field['Tamil_Totalgrade']);
                  $varA25 = "Tamil_Totalgrade_".$cnt;
                  $$varA25 = $Tamil_Totalgrade;
				  $Maths_Classwrk	  = Trim($field['Maths_Classwrk']);
                  $varA26 = "Maths_Classwrk_".$cnt;
                  $$varA26 = $Maths_Classwrk;
				  $Maths_Examgrade    = Trim($field['Maths_Examgrade']);
                  $varA27 = "Maths_Examgrade_".$cnt;
                  $$varA27 = $Maths_Examgrade;
				  $Maths_Totalgrade	  = Trim($field['Maths_Totalgrade']);
                  $varA28 = "Maths_Totalgrade_".$cnt;
                  $$varA28 = $Maths_Totalgrade;
				  $Science_Classwrk	  = Trim($field['Science_Classwrk']);
                  $varA29 = "Science_Classwrk_".$cnt;
                  $$varA29 = $Science_Classwrk;
				  $Science_Examgrade  = Trim($field['Science_Examgrade']);
                  $varA30 = "Science_Examgrade_".$cnt;
                  $$varA30 = $Science_Examgrade;
				  $Science_Totalgrade = Trim($field['Science_Totalgrade']);
                  $varA31 = "Science_Totalgrade_".$cnt;
                  $$varA31 = $Science_Totalgrade;
				  $Sscience_Classwrk  = Trim($field['Sscience_Classwrk']);
                  $varA32 = "Sscience_Classwrk_".$cnt;
                  $$varA32 = $Sscience_Classwrk;
				  $Sscience_Examgrade = Trim($field['Sscience_Examgrade']);
                  $varA33 = "Sscience_Examgrade_".$cnt;
                  $$varA33 = $Sscience_Examgrade;
				  $Sscience_Totalgrade= Trim($field['Sscience_Totalgrade']);
                  $varA34 = "Sscience_Totalgrade_".$cnt;
                  $$varA34 = $Sscience_Totalgrade;
				  $Computer_Classwrk  = Trim($field['Computer_Classwrk']);
                  $varA35 = "Computer_Classwrk_".$cnt;
                  $$varA35 = $Computer_Classwrk;
				  $Computer_Examgrade = Trim($field['Computer_Examgrade']);
                  $varA36 = "Computer_Examgrade_".$cnt;
                  $$varA36 = $Computer_Examgrade;
				  $Computer_Totalgrade= Trim($field['Computer_Totalgrade']);
                  $varA37 = "Computer_Totalgrade_".$cnt;
                  $$varA37 = $Computer_Totalgrade;
				  $Project_Classwrk	  = Trim($field['Project_Classwrk']);
                  $varA38 = "Project_Classwrk_".$cnt;
                  $$varA38 = $Project_Classwrk;
			 	  $Project_Examgrade  = Trim($field['Project_Examgrade']);
                  $varA39 = "Project_Examgrade_".$cnt;
                  $$varA39 = $Project_Examgrade;
                  $Project_Totalgrade = Trim($field['Project_Totalgrade']);
                  $varA40 = "Project_Totalgrade_".$cnt;
                  $$varA40 = $Project_Totalgrade;
				  $Eng_Comprehension  = Trim($field['Eng_Comprehension']);
                  $varA41 = "Eng_Comprehension_".$cnt;
                  $$varA41 = $Eng_Comprehension;
				  $Eng_Speaking       = Trim($field['Eng_Speaking']);
                  $varA42 = "Eng_Speaking_".$cnt;
                  $$varA42 = $Eng_Speaking;
				  $Eng_Reading        = Trim($field['Eng_Reading']);
                  $varA43 = "Eng_Reading_".$cnt;
                  $$varA43 = $Eng_Reading;
				  $Eng_Writing        = Trim($field['Eng_Writing']);
                  $varA44 = "Eng_Writing_".$cnt;
                  $$varA44 = $Eng_Writing;
				  $Tam_Comprehension  = Trim($field['Tam_Comprehension']);
                  $varA45 = "Tam_Comprehension_".$cnt;
                  $$varA45 = $Tam_Comprehension;
				  $Tam_Speaking       = Trim($field['Tam_Speaking']);
                  $varA46 = "Tam_Speaking_".$cnt;
                  $$varA46 = $Tam_Speaking;
				  $Tam_Reading        = Trim($field['Tam_Reading']);
                  $varA47 = "Tam_Reading_".$cnt;
                  $$varA47 = $Tam_Reading;
				  $Tam_Writing        = Trim($field['Tam_Writing']);
                  $varA48 = "Tam_Writing_".$cnt;
                  $$varA48 = $Tam_Writing;
				  $Mat_Comprehension  = Trim($field['Mat_Comprehension']);
                  $varA49 = "Mat_Comprehension_".$cnt;
                  $$varA49 = $Mat_Comprehension;
				  $Scn_Comprehension  = Trim($field['Scn_Comprehension']);
				  $varA50 = "Scn_Comprehension_".$cnt;
                  $$varA50 = $Scn_Comprehension;
                  $varA51= "Admission_No_".$cnt;
                  $$varA51 = $field['Admission_No'];
                  $varA52= "Name_".$cnt;
                  $$varA52 = $field['Name'];
                  $varA53= "Student_Id".$cnt;
                  $$varA53 = $field['Student_Id'];
                  
	           }
            }
          }
            }
        }   ?>
    <tr><td height="10">&nbsp;</td></tr>
    <tr><td>
    <div style= "display:block;width:100%; overflow:auto" id="scholarshipfr" align="center">
      <table align="center" cellpadding="3" cellspacing="1" width="100%">
        <tr>
          <?php 	//SHOW TEACHER DROPDOWN
		  mssql_free_result($result);	
		$query = mssql_init('sp_FetchTerm',$mssql);
        mssql_bind($query,'@Class_Code',$Class,SQLINT4,false,false,5);
        mssql_bind($query,'@Section_Code',$Section,SQLINT4,false,false,5);
        mssql_bind($query,'@School_Id',$Location,SQLINT4,false,false,5); 
        mssql_bind($query,'@Term',$Term,SQLINT4,false,false,5);       
		$result = mssql_execute($query);
       mssql_free_statement($query);?>
          <td width="100" align="right" style="width:100px;">Term<?php echo $mand; ?></td>
          <td width="77" align="left" readonly="readonly"><div id ="Term2" >
              <select id="Term3" name="Term2"
    <?php if($Ass_Header_Code>0){?>
  disabled="disabled" <?php } ?> >
                <?php echo $Select;?>
                <?php while($field1 = mssql_fetch_array($result)) 
            
   {  ?>
                <option value="<?php echo $field1['Term_Code']?>" <?php if($Term_Code==$field1['Term_Code']) echo "selected"; ?>> <?php echo $field1['Term']?> </option>
                <?php } ?>
              </select>
              <?php if($Ass_Header_Code>0) {?>
              <input type="hidden" name="Term2" value="$Term" />
            <?php } ?>
          </div></td>
          <td width="89" align="right"  >TermDate<?php echo $mand; ?></td>
          <td width="125" align="right"><a href="javascript:NewCal('Term_Date','ddmmyyyy')">
            <input type="text" name="Term_Date" id="Term_Date"readonly="readonly" style="width:125px" size="10" maxlength="50" value="<?php echo $Term_Date ?>" />
          </a></td>
          <?php 	//SHOW TEACHER DROPDOWN
		  mssql_free_result($result);	
		$query  = mssql_init('sp_SelectTeacher',$mssql);
		$result = mssql_execute($query);
		mssql_free_statement($query);	?>
          <td width="56"  align="right">Teacher<?php echo $mand; ?></td>
          <td width="125" align="left"><div id ="TeacherName">
              <select id="TeacherName" name="TeacherName" style="width:125px">
                <?php echo $Select;?>
                <?php while($field1 = mssql_fetch_array($result)) 
   {  ?>
                <option value="<?php echo $field1['Teacher_Code']?>" <?php if($TeacherName==$field1['Teacher_Code']) echo "selected"; ?>> <?php echo $field1['Teacher_Name']?></option>
                <?php } ?>
              </select>
          </div></td>
          <td width="102"  align="right">WorkingDays<?php echo $mand; ?></td>
          <td width="144" align="left"><div id ="Working_Days">
              <input type="text" name="Working_Days" id="Working_Days"  maxlength="3" value="<?php echo $WorkingDays ?>" 
	                      onkeydown="return numberonly('Working_Days')" />
          </div></td>
          <?php 	//SHOW TEACHER DROPDOWN
		  mssql_free_result($result);	
		$query  = mssql_init('sp_GetPeriod',$mssql);
		$result = mssql_execute($query);
		mssql_free_statement($query);	?>
          <td width="40"  align="right">Period<?php echo $mand; ?></td>
          <td width="209" align="left"><div id ="PeriodName">
              <select id="PeriodName" name="PeriodName" style="width:125px" <?php if($Ass_Header_Code>0){?>
  disabled="disabled" <?php } ?>>
                <?php echo $Select;?>
                <?php while($field1 = mssql_fetch_array($result)) 
   {  ?>
                <option value="<?php echo $field1['Period_Code']?>" <?php if($PeriodName==$field1['Period_Code']) echo "selected"; ?>> <?php echo $field1['Period']?></option>
                <?php } ?>
              </select>
              <?php if($Ass_Header_Code>0) {?>
              <input type="hidden" name="PeriodName" value="$PeriodName" />
            <?php } ?>
          </div></td>
          <td width="84" align="left">&nbsp;</td>
        </tr>
      </table>
    </div>
 </td></tr>   
  <!-- Print Student Details -->   
   <div style="width: 1200px;overflow:-moz-scrollbars-vertical;overflow-y:auto;">
   <!--<div style="height: 450px;width:1250px;overflow:-moz-scrollbars-vertical;overflow-y:auto;">-->
            <table align="center" cellpadding="3" cellspacing="1" width="100%">
            <tr class=<?php if($colorflag%2==0) { echo "row1"; } else { echo "row2"; } ?> valign="center">
            <td align="left"></td>
            
            
           <!-- <div style="height: 450px;width:1250px;overflow:-moz-scrollbars-vertical;overflow-y:auto;">-->
            
            <table align="center" cellpadding="3" cellspacing="1" width="100%">
              <tr align="center"> </tr>
              <thead>
              </thead>
  <th align="left">S.No.</th>
      <th>ADM. No.</th>
    <th>NAME</th>
<?php if($Class>2) 
                { 
                    $loopcnt = 15;
                    $loopspan=2;
                }
                 else {  
                    $loopcnt = 11;
                    $loopspan=3;
                 }
                    for($hcnt=1;$hcnt<=$loopcnt;$hcnt++)
                    echo "<th>&nbsp;</th>";  ?>
      <?php if($Class > 2 ){ ?>
      <th colspan="6">English Comments</th>
  <?php } 
  for($i=1;$i<=$scount;$i++)
    {   ?>
  <input type="hidden" name="Student_Id<?php echo $i?>" id="Student_Id<?php echo $i?>" value="<?php echo ${Student_Id.$i}  ?>"/>
  <input type="hidden" name="Ass_Header_Code" id="Ass_Header_Code"value="<?php echo $Ass_Header_Code ?>"/>

  <input type="hidden" name="scount" id="scount" value="<?php echo $scount ?>" />
  
  <tr class=<?php if($colorflag%2==0) { echo "row1"; } else { echo "row2"; } ?> valign="center">
    <td align="center" rowspan=<?php echo $loopspan; ?>><?php echo $i ?> </td>
    <td rowspan="<?php echo $loopspan; ?>"><input type="text" name="Admission_No<?php echo $i ?>" id="Admission_No2"  size="10" maxlength="10" style="width:80px;" value="<?php echo ${Admission_No_.$i} ?>" readonly="readonly"/>    </td>
    <td rowspan="<?php echo $loopspan;?>"><input type="text" name="Name<?php echo $i ?>" id="Name<?php echo $i;?>"  size="10" maxlength="25" style="width:100px;" value="<?php echo ${Name_.$i} ?> " readonly="readonly"	/>    </td>
    <?php if($Class > 2 ){ ?>
    <td align="left" ><div id ="Spoken_English<?php echo $i;?>" >
      <input type="text" name="Spoken_English<?php echo $i ?>" id="Spoken_English_<?php echo $i ?>" size="3" maxlength="3" value="<?php echo ${Spoken_English_.$i} ?>"  onkeyup="markcheck('<?php echo "#Spoken_English_".$i ?>')" onkeydown="return numberonly('Spoken_English<?php echo $i ?>')"  onfocus="showtitle('Spoken_English_<?php echo $i ?>','SPOKEN ENGLISH')" onblur="emptychk('<?php echo "#Spoken_English_".$i ?>')"style="width:25px;" />
    </div></td>
    <?php } ?>
    <td align="left"><div id ="Enthusiam_Energy<?php echo $i;?>">
      <input type="text" name="Enthusiam_Energy<?php echo $i ?>" 
                               id="Enthusiam_Energy_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"
                               value="<?php echo ${Enthusiam_Energy_.$i} ?>"onkeyup="markcheck('<?php echo "#Enthusiam_Energy_".$i ?>')" onkeydown="return numberonly('Enthusiam_Energy<?php echo $i ?>')" onfocus="showtitle('Enthusiam_Energy_<?php echo $i ?>','ENTHUSIASM & ENERGY')" onblur="emptychk('<?php echo "#Enthusiam_Energy_".$i ?>')" />
    </div>
        <div id="lbl1"></div></td>
    <?php if($Class > 2 ){ ?>
    <td align="left"><div id ="Writing_Neatness<?php echo $i;?>">
      <input type="text" name="Writing_Neatness<?php echo $i ?>" id="Writing_Neatness_<?php echo $i ?>" size="3" maxlength="3" style="width:25px" value="<?php echo ${Writing_Neatness_.$i} ?>"onkeyup="markcheck('<?php echo "#Writing_Neatness_".$i ?>')" onkeydown="return numberonly('Writing_Neatness<?php echo $i ?>')" onfocus="showtitle('Writing_Neatness_<?php echo $i ?>','WRITING NEATNESS')" onblur="emptychk('<?php echo "#Writing_Neatness_".$i ?>')"/>
    </div></td>
    <?php } ?>
    <?php if($Class > 2 ){ ?>
    <td align="left"><div id ="Responsibilty<?php echo $i;?>">
      <input type="text" name="Responsibilty<?php echo $i ?>" id="Responsibilty_<?php echo $i ?>" size="3" maxlength="3" style="width:25px" value="<?php echo ${Responsibilty_.$i} ?>" onkeyup="markcheck('<?php echo "#Responsibilty_".$i ?>')"  onkeydown="return numberonly('Responsibilty<?php echo $i ?>')" onfocus="showtitle('Responsibilty_<?php echo $i ?>','RESPONSIBILITY')" onblur="emptychk('<?php echo "#Responsibilty_".$i ?>')"/>
    </div></td>
    <?php } ?>
    <td align="left"><div id ="Dicipline<?php echo $i;?>">
      <input type="text" name="Dicipline<?php echo $i ?>" id="Dicipline_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo ${Dicipline_.$i} ?>" onkeyup="markcheck('<?php echo "#Dicipline_".$i ?>')" onkeydown="return numberonly('Dicipline<?php echo $i ?>')" onfocus="showtitle('Dicipline_<?php echo $i ?>','DICIPLINE')" onblur="emptychk('<?php echo "#Dicipline_".$i ?>')"/>
    </div></td>
    <td align="left"><div id ="Attention_Span<?php echo $i;?>">
      <input type="text" name="Attention_Span<?php echo $i ?>" id="Attention_Span_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo ${Attention_Span_.$i} ?>" onkeyup="markcheck('<?php echo "#Attention_Span_".$i ?>')" onkeydown="return numberonly('Attention_Span<?php echo $i ?>')" onfocus="showtitle('Attention_Span_<?php echo $i ?>','ATTENTION SPAN')" onblur="emptychk('<?php echo "#Attention_Span_".$i ?>')"/>
    </div></td>
    <td align="left"><div id ="Punctuality<?php echo $i;?>">
      <input type="text" name="Punctuality<?php echo $i ?>" id="Punctuality_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo ${Punctuality_.$i} ?>" onkeyup="markcheck('<?php echo "#Punctuality_".$i ?>')" onkeydown="return numberonly('Punctuality<?php echo $i ?>')" onfocus="showtitle('Punctuality_<?php echo $i ?>','PUNCTUALITY')" onblur="emptychk('<?php echo "#Punctuality_".$i ?>')"/>
    </div></td>
    <td align="left"><div id ="Social_Interaction<?php echo $i;?>">
      <input type="text" name="Social_Interaction<?php echo $i ?>" id="Social_Interaction_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo ${Social_Interaction_.$i} ?>" onkeyup="markcheck('<?php echo "#Social_Interaction_".$i ?>')" onkeydown="return numberonly('Social_Interaction<?php echo $i ?>')" 
							   onfocus="showtitle('Social_Interaction_<?php echo $i ?>','SOCIAL INTERACTION')" onblur="emptychk('<?php echo "#Social_Interaction_".$i ?>')"/>
    </div></td>
    <td align="left"><div id ="Hygiene<?php echo $i;?>">
      <input type="text" name="Hygiene<?php echo $i ?>" id="Hygiene_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo ${Hygiene_.$i} ?>"  onkeyup="markcheck('<?php echo "#Hygiene_".$i ?>')" onkeydown="return numberonly('Hygiene<?php echo $i ?>')"onfocus="showtitle('Hygiene_<?php echo $i ?>','HYGIENE')" onblur="emptychk('<?php echo "#Hygiene_".$i ?>')"/>
    </div></td>
    <?php if($Class > 2 ){ ?>
    <td align="left"><div id ="Group_Work<?php echo $i;?>">
      <input type="text" name="Group_Work<?php echo $i ?>" id="Group_Work_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo ${Group_Work_.$i} ?>"  onkeyup="markcheck('<?php echo "#Group_Work_".$i ?>')" onkeydown="return numberonly('Group_Work<?php echo $i ?>')"  onfocus="showtitle('Group_Work_<?php echo $i ?>','GROUP WORK')" onblur="emptychk('<?php echo "#Group_Work_".$i ?>')"/>
    </div></td>
    <?php } ?>
    <?php if($Class > 2 ){ ?>
    <td align="left"><div id ="Sports_Games<?php echo $i;?>">
      <input type="text" name="Sports_Games<?php echo $i ?>" id="Sports_Games_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo ${Sports_Games_.$i} ?>" onkeyup="markcheck('<?php echo "#Sports_Games_".$i ?>')" onkeydown="return numberonly('Sports_Games<?php echo $i ?>')"
							   onfocus="showtitle('Sports_Games_<?php echo $i ?>','SPORTS AND GAMES')" onblur="emptychk('<?php echo "#Sports_Games_".$i ?>')"/>
    </div></td>
    <?php } ?>
    <?php if($Class > 2 ){ ?>
    <td align="left"><div id ="Art_Craft<?php echo $i;?>">
      <input type="text" name="Art_Craft<?php echo $i ?>" id="Art_Craft_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo ${Art_Craft_.$i} ?>" onkeyup="markcheck('<?php echo "#Art_Craft_".$i ?>')" onkeydown="return numberonly('Art_Craft<?php echo $i ?>')" onfocus="showtitle('Art_Craft_<?php echo $i ?>','ART AND CRAFT')" onblur="emptychk('<?php echo "#Art_Craft_".$i ?>')"/>
    </div></td>
    <?php } ?>
    <?php if($Class > 2 ){ ?>
    <td align="left"><div id ="Computer_Skills<?php echo $i;?>">
      <input type="text" name="Computer_Skills<?php echo $i ?>" id="Computer_Skills_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo ${Computer_Skills_.$i} ?>" onkeyup="markcheck('<?php echo "#Computer_Skills_".$i ?>')" onkeydown="return numberonly('Computer_Skills<?php echo $i ?>')" onfocus="showtitle('Computer_Skills_<?php echo $i ?>','COMPUTER SKILLS')" onblur="emptychk('<?php echo "#Computer_Skills_".$i ?>')"/>
    </div></td>
    <?php } ?>
    <?php if($Class > 2 ){ ?>
    <td align="left"><div id ="Projects<?php echo $i;?>">
      <input type="text" name="Projects<?php echo $i ?>" id="Projects_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo ${Projects_.$i} ?>" onkeyup="markcheck('<?php echo "#Projects_".$i ?>')" onkeydown="return numberonly('Projects<?php echo $i ?>')" 
							   onfocus="showtitle('Projects_<?php echo $i ?>','PROJECTS')" onblur="emptychk('<?php echo "#Projects_".$i ?>')"/>
    </div></td>
    <?php } ?>
    <?php if($Class <= 2 ){ ?>
    <td align="left"><div id ="Participation<?php echo $i;?>">
      <input type="text" name="Participation<?php echo $i ?>" id="Participation_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo ${Participation_.$i} ?>"  onkeyup="markcheck('<?php echo "#Participation_".$i ?>')" onkeydown="return numberonly('Participation<?php echo $i ?>')"
							   onfocus="showtitle('Participation_<?php echo $i ?>','PARTICIPATION')" onblur="emptychk('<?php echo "#Participation_".$i ?>')"/>
    </div></td>
    <?php } ?>
    <?php if($Class <= 2 ){ ?>
    <td align="left"><div id ="Gross_Motor_Skills<?php echo $i;?>">
      <input type="text" name="Gross_Motor_Skills<?php echo $i ?>" id="Gross_Motor_Skills_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo ${Gross_Motor_Skills_.$i} ?>"  onkeyup="markcheck('<?php echo "#Gross_Motor_Skills_".$i ?>')" onkeydown="return numberonly('Gross_Motor_Skills<?php echo $i ?>')"
							   onfocus="showtitle('Gross_Motor_Skills_<?php echo $i ?>','GROSS MOTOR SKILLS')" onblur="emptychk('<?php echo "#Gross_Motor_Skills_".$i ?>')"/>
    </div></td>
    <?php } ?>
    <?php if($Class <= 2 ){ ?>
    <td align="left"><div id ="Fine_Motor_Skills<?php echo $i;?>">
      <input type="text" name="Fine_Motor_Skills<?php echo $i ?>" id="Fine_Motor_Skills_<?php echo $i ?>" size="3" 
                  maxlength="3" style="width:25px"value="<?php echo ${Fine_Motor_Skills_.$i} ?>"  onkeyup="markcheck('<?php echo "#Fine_Motor_Skills_".$i ?>')" 
                  onkeydown="return numberonly('Fine_Motor_Skills<?php echo $i ?>')"
		          onfocus="showtitle('Fine_Motor_Skills_<?php echo $i ?>','FINE MOTOR SKILLS')" onblur="emptychk('<?php echo "#Fine_Motor_Skills_".$i ?>')"/>
    </div></td>
    <?php } ?>
    <td align="left"><div id ="Noday_Attendance<?php echo $i;?>">
      <input type="text" name="Noday_Attendance<?php echo $i ?>" id="Noday_Attendance_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo ${Noday_Attendance_.$i} ?>" onkeyup="markcheck('<?php echo "#Noday_Attendance_".$i ?>')" onkeydown="return numberonly('Noday_Attendance<?php echo $i ?>')" 
							   onfocus="showtitle('Noday_Attendance_<?php echo $i ?>','NO OF DAYS ATTENDANCE')" onblur="emptychk('<?php echo "#Noday_Attendance_".$i ?>')"/>
    </div></td>
    <?php 	//SHOW COMMENT DROPDOWN
						  mssql_free_result($result);	
		                  $query = mssql_init('sp_FetchComment',$mssql);
	                      $result = mssql_execute($query);
		                  mssql_free_statement($query);	?>
    <td width="128" align="right" colspan="3"><div id ="EnglishComment">
      <select id="EnglishComment_1<?php echo $i ?>" 
                         name="EnglishComment_1<?php echo $i?>" style="width:125px">
        <?php echo $Select;?>
        <?php while($field = mssql_fetch_array($result))                                                                        	{   ?>
        <option value="<?php echo $field['CommentCode']?>" 
                          <?php if(${CommentCode_.$i}==$field['CommentCode']) echo "Selected"; ?>> <?php echo $field['EnglishComment']?></option>
        <?php } ?>
      </select>
    </div></td>
  </tr>
  <tr class=<?php if($colorflag%2==0) { echo "row1"; } else { echo "row2"; } ?> valign="center">
    <?php if($Class > 2 ){ ?>
    <td align="left"><?php echo $field['English_Classwrk'] ?>
        <div id ="English_Classwrk<?php echo $i;?>">
          <input type="text" name="English_Classwrk<?php echo $i ?>" id="English_Classwrk_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo ${English_Classwrk_.$i} ?>" onkeyup="markcheck('<?php echo "#English_Classwrk_".$i ?>')" onkeydown="return numberonly('English_Classwrk<?php echo $i ?>')"   onfocus="showtitle('English_Classwrk_<?php echo $i ?>','ENGLISH CLASSWORK')" onblur="emptychk('<?php echo "#English_Classwrk_".$i ?>')"/>
      </div></td>
    <td align="left"><?php echo $field['English_Examgrade'] ?>
        <div id ="English_Examgrade<?php echo $i;?>">
          <input type="text" name="English_Examgrade<?php echo $i ?>" id="English_Examgrade_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo ${English_Examgrade_.$i} ?>"  onkeyup="markcheck('<?php echo "#English_Examgrade_".$i ?>')" onkeydown="return numberonly('English_Examgrade<?php echo $i ?>')"
							  onfocus="showtitle('English_Examgrade_<?php echo $i ?>','ENGLISH EXAMGRADE')" onblur="emptychk('<?php echo "#English_Examgrade_".$i ?>')" />
      </div></td>
    <td align="left"><?php echo $field['English_Totalgrade'] ?>
        <div id ="English_Totalgrade<?php echo $i;?>">
          <input type="text" name="English_Totalgrade<?php echo $i ?>" id="English_Totalgrade_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo ${English_Totalgrade_.$i} ?>" onkeyup="markcheck('<?php echo "#English_Totalgrade_".$i ?>')" onkeydown="return numberonly('English_Totalgrade<?php echo $i ?>')" 
						onfocus="showtitle('English_Totalgrade_<?php echo $i ?>','ENGLISH TOTALGRADE')" onblur="emptychk('<?php echo "#English_Totalgrade_".$i ?>')" />
      </div></td>
    <td align="left"><?php echo $field['Tamil_Classwrk'] ?>
        <div id ="Tamil_Classwrk<?php echo $i;?>">
          <input type="text" name="Tamil_Classwrk<?php echo $i ?>" id="Tamil_Classwrk_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo ${Tamil_Classwrk_.$i} ?>" onkeyup="markcheck('<?php echo "#Tamil_Classwrk_".$i ?>')" onkeydown="return numberonly('Tamil_Classwrk<?php echo $i ?>')" onfocus="showtitle('Tamil_Classwrk_<?php echo $i ?>','TAMIL CLASSWORK')" onblur="emptychk('<?php echo "#Tamil_Classwrk_".$i ?>')" />
      </div></td>
    <td align="left"><?php echo $field['Tamil_Examgrade'] ?>
        <div id ="Tamil_Examgrade<?php echo $i;?>">
          <input type="text" name="Tamil_Examgrade<?php echo $i ?>" id="Tamil_Examgrade_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo ${Tamil_Examgrade_.$i} ?>"  onkeyup="markcheck('<?php echo "#Tamil_Examgrade_".$i ?>')" onkeydown="return numberonly('Tamil_Examgrade<?php echo $i ?>')" onfocus="showtitle('Tamil_Examgrade_<?php echo $i ?>','TAMIL EXAMGRADE')" onblur="emptychk('<?php echo "#Tamil_Examgrade_".$i ?>')" />
      </div></td>
    <td align="left"><?php echo $field['Tamil_Totalgrade'] ?>
        <div id ="Tamil_Totalgrade<?php echo $i;?>">
          <input type="text" name="Tamil_Totalgrade<?php echo $i ?>" id="Tamil_Totalgrade_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo ${Tamil_Totalgrade_.$i} ?>" onkeyup="markcheck('<?php echo "#Tamil_Totalgrade_".$i ?>')" onkeydown="return numberonly('Tamil_Totalgrade<?php echo $i ?>')" onfocus="showtitle('Tamil_Totalgrade_<?php echo $i ?>','TAMIL TOTALGRADE')" onblur="emptychk('<?php echo "#Tamil_Totalgrade_".$i ?>')"/>
      </div></td>
    <td align="left"><?php echo $field['Maths_Classwrk'] ?>
        <div id ="Maths_Classwrk<?php echo $i;?>">
          <input type="text" name="Maths_Classwrk<?php echo $i ?>" id="Maths_Classwrk_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo ${Maths_Classwrk_.$i} ?>" onkeyup="markcheck('<?php echo "#Maths_Classwrk_".$i ?>')" onkeydown="return numberonly('Maths_Classwrk<?php echo $i ?>')" onfocus="showtitle('Maths_Classwrk_<?php echo $i ?>','MATHS CLASSWORK')" onblur="emptychk('<?php echo "#Maths_Classwrk_".$i ?>')"/>
      </div></td>
    <td align="left"><div id ="Maths_Examgrade<?php echo $i;?>">
      <input type="text" name="Maths_Examgrade<?php echo $i ?>" id="Maths_Examgrade_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo ${Maths_Examgrade_.$i} ?>" onkeyup="markcheck('<?php echo "#Maths_Examgrade_".$i ?>')" onkeydown="return numberonly('Maths_Examgrade<?php echo $i ?>')" onfocus="showtitle('Maths_Examgrade_<?php echo $i ?>','MATHS EXAMGRADE')" onblur="emptychk('<?php echo "#Maths_Examgrade_".$i ?>')"/>
    </div></td>
    <td align="left"><?php echo $field['Maths_Totalgrade'] ?>
        <div id ="Maths_Totalgrade<?php echo $i;?>">
          <input type="text" name="Maths_Totalgrade<?php echo $i ?>" id="Maths_Totalgrade_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo ${Maths_Totalgrade_.$i} ?>" onkeyup="markcheck('<?php echo "#Maths_Totalgrade_".$i ?>')" onkeydown="return numberonly('Maths_Totalgrade<?php echo $i ?>')" 
							   onfocus="showtitle('Maths_Totalgrade_<?php echo $i ?>','MATHS TOTALGRADE')" onblur="emptychk('<?php echo "#Maths_Totalgrade_".$i ?>')"/>
      </div></td>
    <td align="left"><?php echo $field['Science_Classwrk'] ?>
        <div id ="Science_Classwrk<?php echo $i;?>">
          <input type="text" name="Science_Classwrk<?php echo $i ?>" id="Science_Classwrk_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo ${Science_Classwrk_.$i} ?>" onkeyup="markcheck('<?php echo "#Science_Classwrk_".$i ?>')" onkeydown="return numberonly('Science_Classwrk<?php echo $i ?>')" onfocus="showtitle('Science_Classwrk_<?php echo $i ?>','SCIENCE CLASSWORK')" onblur="emptychk('<?php echo "#Science_Classwrk_".$i ?>')"/>
      </div></td>
    <td align="left"><?php echo $field['Science_Examgrade'] ?>
        <div id ="Science_Examgrade<?php echo $i;?>">
          <input type="text" name="Science_Examgrade<?php echo $i ?>" id="Science_Examgrade_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo ${Science_Examgrade_.$i} ?>" onkeyup="markcheck('<?php echo "#Science_Examgrade_".$i ?>')" onkeydown="return numberonly('Science_Examgrade<?php echo $i ?>')"
							 onfocus="showtitle('Science_Examgrade_<?php echo $i ?>','SCIENCE EXAMGRADE')" onblur="emptychk('<?php echo "#Science_Examgrade_".$i ?>')"/>
      </div></td>
    <td align="left"><?php echo $field['Science_Totalgrade'] ?>
        <div id ="Science_Totalgrade<?php echo $i;?>">
          <input type="text" name="Science_Totalgrade<?php echo $i ?>" id="Science_Totalgrade_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo ${Science_Totalgrade_.$i} ?>"onkeyup="markcheck('<?php echo "#Science_Totalgrade_".$i ?>')"  onkeydown="return numberonly('Science_Totalgrade<?php echo $i ?>')" onfocus="showtitle('Science_Totalgrade_<?php echo $i ?>','SCIENCE TOTALGRADE')" onblur="emptychk('<?php echo "#Science_Totalgrade_".$i ?>')"/>
      </div></td>
    <?php } ?>
    <?php if($Class > 4 ){ ?>
    <td align="left"><?php echo $field['Sscience_Classwrk'] ?>
        <div id ="Sscience_Classwrk<?php echo $i;?>">
          <input type="text" name="Sscience_Classwrk<?php echo $i ?>" id="Sscience_Classwrk_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo ${Sscience_Classwrk_.$i} ?>" onkeyup="markcheck('<?php echo "#Sscience_Classwrk_".$i ?>')" onkeydown="return numberonly('Sscience_Classwrk<?php echo $i ?>')"
							  onfocus="showtitle('Sscience_Classwrk_<?php echo $i ?>','SSCIENCE CLASSWORK')" onblur="emptychk('<?php echo "#Sscience_Classwrk_".$i ?>')"/>
      </div></td>
    <?php } ?>
    <?php if($Class > 4 ){ ?>
    <td align="left"><?php echo $field['Sscience_Examgrade'] ?>
        <div id ="Sscience_Examgrade<?php echo $i;?>">
          <input type="text" name="Sscience_Examgrade<?php echo $i ?>" id="Sscience_Examgrade_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo ${Sscience_Examgrade_.$i} ?>"  onkeyup="markcheck('<?php echo "#Sscience_Examgrade_".$i ?>')" onkeydown="return numberonly('Sscience_Examgrade<?php echo $i ?>')"
							  onfocus="showtitle('Sscience_Examgrade_<?php echo $i ?>','SSCIENCE EXAMGRADE')" onblur="emptychk('<?php echo "#Sscience_Examgrade_".$i ?>')"/>
      </div></td>
    <?php } ?>
    <?php if($Class > 4 ){ ?>
    <td align="left"><?php echo $field['Sscience_Totalgrade'] ?>
        <div id ="Sscience_Totalgrade<?php echo $i;?>">
          <input type="text" name="Sscience_Totalgrade<?php echo $i ?>" id="Sscience_Totalgrade_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo ${Sscience_Totalgrade_.$i} ?>" onkeyup="markcheck('<?php echo "#Sscience_Totalgrade_".$i ?>')" onkeydown="return numberonly('Sscience_Totalgrade<?php echo $i ?>')" 
							  onfocus="showtitle('Sscience_Totalgrade_<?php echo $i ?>','SSCIENCE TOTALGRADE')" onblur="emptychk('<?php echo "#Sscience_Totalgrade_".$i ?>')"/>
      </div></td>
    <?php } ?>
    <?php if($Class > 2 ){ ?>
    <td align="left"><?php echo $field['Computer_Classwrk'] ?>
        <div id ="Computer_Classwrk<?php echo $i;?>">
          <input type="text" name="Computer_Classwrk<?php echo $i ?>" id="Computer_Classwrk_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo ${Computer_Classwrk_.$i} ?>" onkeyup="markcheck('<?php echo "#Computer_Classwrk_".$i ?>')" onkeydown="return numberonly('Computer_Classwrk<?php echo $i ?>')"
							  onfocus="showtitle('Computer_Classwrk_<?php echo $i ?>','COMPUTER CLASSWORK')" onblur="emptychk('<?php echo "#Computer_Classwrk_".$i ?>')"/>
      </div></td>
    <?php } ?>
    <?php if($Class > 2 ){ ?>
    <td align="left"><?php echo $field['Computer_Examgrade'] ?>
        <div id ="Computer_Examgrade<?php echo $i;?>">
          <input type="text" name="Computer_Examgrade<?php echo $i ?>" id="Computer_Examgrade_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo ${Computer_Examgrade_.$i} ?>" onkeyup="markcheck('<?php echo "#Computer_Examgrade_".$i ?>')" onkeydown="return numberonly('Computer_Examgrade<?php echo $i ?>')" 
							  onfocus="showtitle('Computer_Examgrade_<?php echo $i ?>','COMPUTER EXAMGARDE')" onblur="emptychk('<?php echo "#Computer_Examgrade_".$i ?>')"/>
      </div></td>
    <?php } ?>
    <?php if($Class > 2 ){ ?>
    <td align="left"><?php echo $field['Computer_Totalgrade'] ?>
        <div id ="Computer_Totalgrade<?php echo $i;?>">
          <input type="text" name="Computer_Totalgrade<?php echo $i ?>" id="Computer_Totalgrade_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo ${Computer_Totalgrade_.$i} ?>" onkeyup="markcheck('<?php echo "#Computer_Totalgrade_".$i ?>')" onkeydown="return numberonly('Computer_Totalgrade<?php echo $i ?>')" 
							   onfocus="showtitle('Computer_Totalgrade_<?php echo $i ?>','COMPUTER TOTALGRADE')" onblur="emptychk('<?php echo "#Computer_Totalgrade_".$i ?>')"/>
      </div></td>
    <?php } ?>
    <?php if($Class > 2 ){ ?>
    <td align="left"><?php echo $field['Project_Classwrk'] ?>
        <div id ="Project_Classwrk<?php echo $i;?>">
          <input type="text" name="Project_Classwrk<?php echo $i ?>" id="Project_Classwrk_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo ${Project_Classwrk_.$i} ?>" onkeyup="markcheck('<?php echo "#Project_Classwrk_".$i ?>')" onkeydown="return numberonly('Project_Classwrk<?php echo $i ?>')"  onfocus="showtitle('Project_Classwrk_<?php echo $i ?>','PROJECT CLASSWORK')" onblur="emptychk('<?php echo "#Project_Classwrk_".$i ?>')"/>
      </div></td>
    <?php } ?>
    <?php if($Class > 2 ){ ?>
    <td align="left"><?php echo $field['Project_Examgrade'] ?>
        <div id ="Project_Examgrade<?php echo $i;?>">
          <input type="text" name="Project_Examgrade<?php echo $i ?>" id="Project_Examgrade_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo ${Project_Examgrade_.$i} ?>" onkeyup="markcheck('<?php echo "#Project_Examgrade_".$i ?>')" onkeydown="return numberonly('Project_Examgrade<?php echo $i ?>')" 
							  onfocus="showtitle('Project_Examgrade_<?php echo $i ?>','PROJECT EXAMGRADE')" onblur="emptychk('<?php echo "#Project_Examgrade_".$i ?>')"/>
      </div></td>
    <?php } ?>
    <?php if($Class > 2 ){ ?>
    <td align="left"><?php echo $field['Project_Totalgrade'] ?>
        <div id ="Project_Totalgrade<?php echo $i;?>">
          <input type="text" name="Project_Totalgrade<?php echo $i ?>" id="Project_Totalgrade_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo ${Project_Totalgrade_.$i} ?>" onkeyup="markcheck('<?php echo "#Project_Totalgrade_".$i ?>')" onkeydown="return numberonly('Project_Totalgrade<?php echo $i ?>')" 
							  onfocus="showtitle('Project_Totalgrade_<?php echo $i ?>','PROJECT TOTALGRADE')" onblur="emptychk('<?php echo "#Project_Totalgrade_".$i ?>')"/>
      </div></td>
    <?php } ?>
  </tr>
  <tr>
  <?php if($Class <= 2 ){ ?>
  <td align="left"><?php echo $field['Eng_Comprehension'] ?>
          <div id ="Eng_Comprehension<?php echo $i;?>">
            <input type="text" name="Eng_Comprehension<?php echo $i ?>" id="Eng_Comprehension_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo ${Eng_Comprehension_.$i} ?>"  onkeyup="markcheck('<?php echo "#Eng_Comprehension_".$i ?>')" onkeydown="return numberonly('Eng_Comprehension<?php echo $i ?>')"
							   onfocus="showtitle('Eng_Comprehension_<?php echo $i ?>','ENGLISH COMPREHENSION')" onblur="emptychk('<?php echo "#Eng_Comprehension_".$i ?>')" />
        </div></td>
      <?php } ?>
      <?php if($Class <= 2 ){ ?>
      <td align="left"><?php echo $field['Eng_Speaking'] ?>
          <div id ="Eng_Speaking<?php echo $i;?>">
            <input type="text" name="Eng_Speaking<?php echo $i ?>" id="Eng_Speaking_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo ${Eng_Speaking_.$i} ?>"  onkeyup="markcheck('<?php echo "#Eng_Speaking_".$i ?>')" onkeydown="return numberonly('Eng_Speaking<?php echo $i ?>')"
							   onfocus="showtitle('Eng_Speaking_<?php echo $i ?>','ENGLISH SPEAKING')" onblur="emptychk('<?php echo "#Eng_Speaking_".$i ?>')" />
        </div></td>
    <?php } ?>
      <?php if($Class <= 2 ){ ?>
      <td align="left"><?php echo $field['Eng_Reading'] ?>
          <div id ="Eng_Reading<?php echo $i;?>">
            <input type="text" name="Eng_Reading<?php echo $i ?>" id="Eng_Reading_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo ${Eng_Reading_.$i} ?>"  onkeyup="markcheck('<?php echo "#Eng_Reading_".$i ?>')" onkeydown="return numberonly('Eng_Reading<?php echo $i ?>')"
							   onfocus="showtitle('Eng_Reading_<?php echo $i ?>','ENGLISH READING')" onblur="emptychk('<?php echo "#Eng_Reading_".$i ?>')" />
        </div></td>
    <?php } ?>
      <?php if($Class <= 2 ){ ?>
      <td align="left"><?php echo $field['Eng_Writing'] ?>
          <div id ="Eng_Writing<?php echo $i;?>">
            <input type="text" name="Eng_Writing<?php echo $i ?>" id="Eng_Writing_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo ${Eng_Writing_.$i} ?>"  onkeyup="markcheck('<?php echo "#Eng_Writing_".$i ?>')" onkeydown="return numberonly('Eng_Writing<?php echo $i ?>')"
							   onfocus="showtitle('Eng_Writing_<?php echo $i ?>','ENGLISH WRITING')" onblur="emptychk('<?php echo "#Eng_Writing_".$i ?>')" />
        </div></td>
    <?php } ?>
      <?php if($Class <= 2 ){ ?>
      <td align="left"><?php echo $field['Tam_Comprehension'] ?>
          <div id ="Tam_Comprehension<?php echo $i;?>">
            <input type="text" name="Tam_Comprehension<?php echo $i ?>" id="Tam_Comprehension_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo ${Tam_Comprehension_.$i} ?>"  onkeyup="markcheck('<?php echo "#Tam_Comprehension_".$i ?>')" onkeydown="return numberonly('Tam_Comprehension<?php echo $i ?>')"
							   onfocus="showtitle('Tam_Comprehension_<?php echo $i ?>','TAMIL COMPREHENSION')" onblur="emptychk('<?php echo "#Tam_Comprehension_".$i ?>')" />
        </div></td>
    <?php } ?>
      <?php if($Class <= 2 ){ ?>
      <td align="left"><?php echo $field['Tam_Speaking'] ?>
          <div id ="Tam_Speaking<?php echo $i;?>">
            <input type="text" name="Tam_Speaking<?php echo $i ?>" id="Tam_Speaking_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo ${Tam_Speaking_.$i} ?>"  onkeyup="markcheck('<?php echo "#Tam_Speaking_".$i ?>')" onkeydown="return numberonly('Tam_Speaking<?php echo $i ?>')"
							   onfocus="showtitle('Tam_Speaking_<?php echo $i ?>','TAMIL SPEAKING')" onblur="emptychk('<?php echo "#Tam_Speaking_".$i ?>')" />
        </div></td>
    <?php } ?>
      <?php if($Class <= 2 ){ ?>
      <td align="left"><?php echo $field['Tam_Reading'] ?>
          <div id ="Tam_Reading<?php echo $i;?>">
            <input type="text" name="Tam_Reading<?php echo $i ?>" id="Tam_Reading_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo ${Tam_Reading_.$i} ?>"  onkeyup="markcheck('<?php echo "#Tam_Reading_".$i ?>')" onkeydown="return numberonly('Tam_Reading<?php echo $i ?>')"
							   onfocus="showtitle('Tam_Reading_<?php echo $i ?>','TAMIL READING')" onblur="emptychk('<?php echo "#Tam_Reading_".$i ?>')" />
        </div></td>
    <?php } ?>
      <?php if($Class <= 2 ){ ?>
      <td align="left"><?php echo $field['Tam_Writing'] ?>
          <div id ="Tam_Writing<?php echo $i;?>">
            <input type="text" name="Tam_Writing<?php echo $i ?>" id="Tam_Writing_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo ${Tam_Writing_.$i} ?>"  onkeyup="markcheck('<?php echo "#Tam_Writing_".$i ?>')" onkeydown="return numberonly('Tam_Writing<?php echo $i ?>')"
							   onfocus="showtitle('Tam_Writing_<?php echo $i ?>','TAMIL WRITING')" onblur="emptychk('<?php echo "#Tam_Writing_".$i ?>')"/>
        </div></td>
    <?php } ?>
      <?php if($Class <= 2 ){ ?>
      <td align="left"><?php echo $field['(Mat_Comprehension'] ?>
          <div id ="Mat_Comprehension<?php echo $i;?>">
            <input type="text" name="Mat_Comprehension<?php echo $i ?>" id="Mat_Comprehension_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo ${Mat_Comprehension_.$i} ?>"  onkeyup="markcheck('<?php echo "#Mat_Comprehension_".$i ?>')" onkeydown="return numberonly('Mat_Comprehension<?php echo $i ?>')"
							   onfocus="showtitle('Mat_Comprehension_<?php echo $i ?>','MATHS COMPREHENSION')" onblur="emptychk('<?php echo "#Mat_Comprehension_".$i ?>')"/>
        </div></td>
        
    <?php } ?>
      <?php if($Class <= 2 ){ ?>
      <td align="left"><?php echo $field['Scn_Comprehension'] ?>
          <div id ="Scn_Comprehension<?php echo $i;?>">
            <input type="text" name="Scn_Comprehension<?php echo $i ?>" id="Scn_Comprehension_<?php echo $i ?>" size="3" maxlength="3" style="width:25px"value="<?php echo ${Scn_Comprehension_.$i} ?>"  onkeyup="markcheck('<?php echo "#Scn_Comprehension_".$i ?>')" onkeydown="return numberonly('Scn_Comprehension<?php echo $i ?>')"
							   onfocus="showtitle('Scn_Comprehension_<?php echo $i ?>','SCIENCE COMPREHENSION')" onblur="emptychk('<?php echo "#Scn_Comprehension_".$i ?>')"/>
        </div></td>
        </tr>
    <?php } ?> 
  </tr>
   <?php }?>
  <tr>
    <td colspan="5" align="left"><table width="50%">
    <table>
      <tr>
        <td><input type="Submit" name="Save" value="Save" id="Save" class="winbutton_go" /></td>
        <td><input type="Submit" name="Cancel" value="Cancel" class="winbutton_go"/>        </td>
      </tr>
      </table></td>
  </tr>   
  </table> 
  </table></div>
    <?php }     ?>
<br /><br />
    </div>  
  <input type="hidden" name="tot_rec" value="<?php echo $tot_rec ?>" />
    </td></tr>    
    </div></table>
    </table>
    </form>
    </body>
</html>
<?php include("../includes/copyright.php"); ?>