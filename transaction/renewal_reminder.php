<?php
$msg="<table width='88%' class='normal_text'  align='center'><tr><td colspan='5' align='justify'>Dear <span style='color:#00486b;'>".$Title." ".$name.",</span></td></tr>
								<tr><td >&nbsp;</td></tr>
								<tr><td >Greetings from Isha Vidhya!</td></tr>
								<tr><td >&nbsp;</td></tr>
								<tr><td >Thank you for taking the time to read this email.</td></tr>
								<tr><td >&nbsp;</td></tr>
								<tr><td >We express our warmest gratitude to you for the generous donation that has helped us educate underprivileged rural children through your scholarship support. We hope you have recently received the Profile and Thank You letter from the child(ren) you have sponsored.</td></tr>
								<tr><td >&nbsp;</td></tr>
								<tr><td >As you are aware we now have 8 schools with 5250 children studying, of whom almost 3000 are dependent on the scholarships that donors like you provide, to continue their education. We seek your continued support and renewal of the sponsorship for the academic year 2013-2014 which has commenced in June 2013. You may make this renewal at your earliest convenience. Your sponsorship will go to help the same child(ren) you had sponsored in the previous academic year.</td></tr>
								<tr><td >&nbsp;</td></tr>
								<tr><td >Please accept our sincere gratitude if you have already renewed the same. We assure you that you are making an invaluable investment by gifting these children a future.</td></tr>
								<tr><td >&nbsp;</td></tr>
								<tr><td >You can continue to support our children with Rs.10,000 / $240 / �150 donation towards Scholarship which includes tuition, note-books and books or with Rs. 20000/$480/ �300 towards Full Educational Support which additionally covers uniforms, transport and noon-meal as well.</td></tr>
								<tr><td >&nbsp;</td></tr>
								<tr><td >(If you are presently supporting Scholarships, you could also help to support our children to reach school safely by sponsoring their transport with Rs.5,250 / $130 / �80 per child per year or help them have a healthy, nutritious meal every day  by sponsoring their noon meal with Rs.3,000 / $72 /�45 per child per year.)</td></tr>
								<tr><td >&nbsp;</td></tr>
								<tr><td ><span style='text-decoration:underline;font-weight:bolder; font-size:20px'>For donations in India/USA:</span> (eligible for tax benefits under Sec 80G & Sec 501c(3))</td></tr>
								<tr><td >&nbsp;</td></tr>
								<tr><td ><span style='text-decoration:underline;font-weight:bolder; font-size:20px'>Online:</span> Donate through Credit/Debit Card at:  </td></tr>
								<tr><td><a href='http://www.ishavidhya.org/donate-now/financial-donation.html'>http://www.ishavidhya.org/donate-now/financial-donation.html</a></td></tr>
								<tr><td>(If you are unable to open the link, please copy-paste it on the web address bar of your browser)</td></tr>
								<tr><td>&nbsp;</td></tr>
								<tr><td><span style='text-decoration:underline;font-weight:bolder; font-size:20px'>Offline:</span>
								<tr><td valign='top'>
<table width='99%' border='1' cellspacing='1' cellpadding='1'  bgcolor='#FFFFFF' style='border-collapse:collapse; margin:5px; border:#999999 solid 1px; padding:3px; '>
	<tr>
		<td colspan='2'><span style='font-weight:bolder; font-size:20px'>Cheque in favor of:</span>Isha Education [for India], Isha Foundation Inc.[for USA & Canada] and send to:  </td>
		
	</tr>
	<tr>
		<td style='font-weight:bolder; font-size:20px' align='center'>India</td>
		<td style='font-weight:bolder; font-size:20px' align='center'>USA & CANADA</td>
	</tr>
	<tr>
		<td align='center'>Isha Education,<br>
14, CA Thevar Layout,<br>
Kannapiran Mills Road, Udayampalayam,<br>
Coimbatore, 641 028, INDIA.<br>
donations@ishavidhya.org,<br>
Ph: +91 94425-44458
</td>
		<td align='center'>Isha Foundation Inc.,<br>
Attn: Isha Vidhya<br>
951 Isha Lane, McMinnville,<br>
TN 37110, USA.<br>
Sathish.Raj@ishavidhya.org,<br>
Ph: +1 813-434-3515
</td>
	</tr>
	<tr>
		<td colspan='2'>Besides details of your donation amount, date and mode of donation, please also email us personal details including name, address, phone and email ID to <span style='font-weight:500; font-size:16px'>donations@ishavidhya.org</span>, so that we may confirm the receipt of funds and also provide feedback and updates regularly. </td>
		
	</tr>
</table>
<table width='100%'><tr><td style='text-decoration:underline;font-weight:bolder; font-size:18px'>For donations in the UK:</td></tr>
<tr><td>
<table width='99%' border='1' cellspacing='1' cellpadding='1'  bgcolor='#FFFFFF' style='border-collapse:collapse; margin:5px; border:#999999 solid 1px; padding:3px; '>
	<tr>
		<td style='font-size:18px; font-weight:bolder'  align='center'>Online account transfer</td>
		<td style='font-weight:bolder; font-size:18px' align='center'>Cheque donations</td>
	</tr>
	<tr>
		<td >Barclays Bank<br>
Name: Isha Institute of Inner Sciences<br>
Sort Code: 20-21-78<br>
Account No- 53398226<br>
Please mention your Isha Vidhya Reference No/ SOM ID:".$som_id." to help us track your donation.
</td>
<td>Isha Institute of Inner Sciences<br>
10 Thorpland Avenue<br>
Ickenham<br>
Uxbridge<br>
UB 10 8TW<br>
United Kingdom<br>
Ph:+44-7837060625<br>
Email: uk@ishavidhya.org
</td>
	</tr>
	<tr>
		<td>Please send the signed donation form to us either by post or as a scanned copy by email to: uk@ishavidhya.org & donations@ishavidhya.org</td>
		<td>Cheque donations should be made payable to �Isha Institute of Inner Sciences�- Please post cheque, together with the enclosed form.</td>
	</tr>
</table>
</td></tr>
</table>
</td></tr>
<tr><td>&nbsp; </td></tr>
<tr><td>For more details on how your donations work, please visit our website: www.ishavidhya.org or email us at donations@ishavidhya.org.</td></tr>
<tr><td>&nbsp; </td></tr>
<tr><td>Overseas donors, Kindly click the link below and please fill the form, sign and send the scanned form back to us.</td></tr>
<tr><td>&nbsp; </td></tr>
<tr><td style='font-weight:bolder;font-size:15px'><a href=www.ishadb.com/ishavidhya/transaction/Overseas_donor_form.doc>Overseas Donors Form</a></td></tr>
<tr><td>&nbsp; </td></tr>
<tr><td>We assure you that your love and support will go a long way in transforming the lives of underprivileged rural children.</td></tr>
<tr><td>&nbsp; </td></tr>
<tr><td>Thank you.</td></tr>
<tr><td> Sincerely,</td></tr>
<tr><td>Isha Vidhya Team</td></tr>
<tr><td><a href='http://www.ishavidhya.org'>www.ishavidhya.org</a></td></tr>
<tr><td><a href='http://facebook.com/ishavidhya'>https://www.facebook.com/ishavidhya</a></td></tr>
</td></tr>
</table>";	
?>