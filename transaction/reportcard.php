	<?php session_start();	 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php include("../includes/header.php");
title('Student Management','Manual Report Card Entry',2,1,2); ?>
<head>
<style type="text/css">
.paging
{
color: #666666;
font-style:italic;
font-weight:bold;
font-size:14px; 
align:center;
padding-left:300px;

}
.paging1
{
color:#666666;
font-weight:bold;
font-size:14px; 
align:center;

}
</style>
<script>
function navi(obj){

	$('#Page_Code').val(obj);
	$('#Save').val('Save');
	$('#myform').submit();
}

function subject(obj,obj1)
{
url = "../includes/ajax.php?listcode=123&Class_Code="+obj+"&Subject_Code="+obj1;
	var ct='#subject';
		$(ct).load(url);
}

function maths(obj,obj1)
{
	$('#maths_spcl_'+obj1).hide();
	var parts = obj.split('$z1y2x3');
	if(parts[1].length>0)
		{
			
			if($("#maths_spcl_"+obj1).is(":hidden")) 
		  $("#maths_spcl_"+obj1).show();
		}
}
</script>
</head>
<?php
  $errmsg="";
  $errflag=0;
  $dummy=0;
  
function Pagination($num_rows)
{
	$rec_limit=10;
	$page=($num_rows)/($rec_limit);
    $page=ceil($page);
	if($page > 0 )
	{
		for($lcnt=1;$lcnt<=$page;$lcnt++)
		{
		$pa.="<a href='#' onclick='navi(".$lcnt.") '>         ".$lcnt."     </a>";
		}
		return $pa;
		
	}
	
}

function Assessment_Header_Save($Ass_Header_Code,$Term_Code,$Class,$Section,$Term_Date,$TeacherName,$WorkingDays,$Period_Code,$Location,$Subject_Code,$mssql)
{
//echo "test <br>".$Ass_Header_Code."=".$Term_Code."=".$Class."=".$Section."=".$Term_Date."=".$TeacherName."=".$WorkingDays."=".$Period_Code."=".$Location."= <br>".$Subject_Code;
	mssql_free_result($result);
	$query = mssql_init('sp_Assesment_Header',$mssql);

	mssql_bind($query,'@Ass_Header_Code',$Ass_Header_Code,SQLINT4,false,false,5);
	mssql_bind($query,'@Term_Code',$Term_Code,SQLINT4,false,false,5);		
	mssql_bind($query,'@Class_Code',$Class,SQLINT4,false,false,5);
	mssql_bind($query,'@Section_code',$Section,SQLINT4,false,false,5);	     
	if(strlen($Term_Date)==10) $Term_Date1 = date('Y-m-d', strtotime($Term_Date));                                               
	mssql_bind($query,'@Term_Date',$Term_Date1,SQLVARCHAR,false,false,20);                
	mssql_bind($query,'@Teacher_Code',$TeacherName,SQLVARCHAR,false,false,50);	
	mssql_bind($query,'@Working_Days',$WorkingDays,SQLINT4,false,false,5);
	mssql_bind($query,'@Period_Code',$Period_Code,SQLINT4,false,false,5);
	mssql_bind($query,'@School_Id',$Location,SQLINT4,false,false,5);
	mssql_bind($query,'@SubjectName',$Subject_Code,SQLINT4,false,false,5);
	mssql_bind($query,'@Ass_Header_Code1',$Ass_Header_Code1,SQLINT4,true);	
	$result = @mssql_execute($query);
	if(!$result)  echo mssql_get_last_message();	
	mssql_free_statement($query); 
	return $Ass_Header_Code1;
	}
	
	//echo "==".$_POST['Save']."==".$_POST['Page_Code'];			
if($_POST['Go']=="Go" or $_POST['Save']=="Save"  or $_POST['Page_Code'] > 0 )	
	{ 
	// if($Page_Code=="" )$Page_Code=1;
	 $Ass_Header_Code1="";
 	 $Location=$_GET['Location'];
	 $Class=$_GET['Class'];
	 $Section=$_GET['Section'];
	 $Term_Code=$_GET['Term_Code'];
	 $Term_Date=$_GET['Term_Date'];
	 $TeacherName=$_GET['TeacherName'];
	 $Term_Code=$_GET['Term_Code'];
	 $Term_Date=$_GET['Term_Date'];
	 $WorkingDays=$_GET['WorkingDays'];
	 $Period_Code=$_GET['Period_Code'];
	 $Subject_Code=$_GET['Subject_Code'];
	 $Admission_No=Trim($_POST['Admission_No']);
	 $Page_Code=Trim($_POST['Page_Code']);
	    
	if($_GET['frm']<>1) {
	
	   $Ass_Header_Code=trim($_POST['Ass_Header_Code']);
	   $Location        =   Trim($_POST['Location']);
	   $Class           =   Trim($_POST['Class']);	
	   $Section         =   Trim($_POST['Section']);
	   $Admission_No	=	Trim($_POST['Admission_No']);
	   $Term_Code	    =	Trim($_POST['Term_Code']);
	   $Term_Date		=	Trim($_POST['Term_Date']);
	   $TeacherName		=	Trim($_POST['TeacherName']);
	   $Subject_Code		=	Trim($_POST['Subject_Code']);
	   $WorkingDays		=	Trim($_POST['WorkingDays']);
	   $Period_Code		=	Trim($_POST['Period_Code']);
	   $Admission_No    =   Trim($_POST['Admission_No']);
	   $Page_Code=Trim($_POST['Page_Code']);
      }
	   
       //The Field Value Should be Empty Means The Error Message Displayed
	   $dummy = ZeroCheck($Location,$errmsg,$errflag,"Location"); 
	   $dummy = ZeroCheck($Class,$errmsg,$errflag,"Class"); 
	   $dummy = ZeroCheck($Section,$errmsg,$errflag,"Section");
	   $dummy = ZeroCheck($Term_Code,$errmsg,$errflag,"Term"); 
	   if($_SESSION['UserID']<>1 and $_SESSION['UserID']<>1000031)
	   {
	   $dummy = Strcheck($Term_Date,$errmsg,$errflag,"Term Date");
	   $dummy = Strcheck($TeacherName,$errmsg,$errflag,"Teacher Name");
	   $dummy = Strcheck($WorkingDays,$errmsg,$errflag,"Working Days");
	   }
	   $dummy = ZeroCheck($Period_Code,$errmsg,$errflag,"Period");
	   $dummy = ZeroCheck($Subject_Code,$errmsg,$errflag,"Subject");

	   if($errflag==1) {
	   echo $errlbl.$errmsg;
       $errflag=2;
	}
	
		}	
?>
<form name="myform" id="myform" action="reportcard.php" method="post">	
<input type="hidden" name="Location" value="<?php echo $Location; ?>" />
<input type="hidden" name="Class" value="<?php echo $Class; ?>" />
<input type="hidden" name="Section" value="<?php echo $Section; ?>" />
<input type="hidden" name="Term_Code" value="<?php echo $Term_Code; ?>" />
<input type="hidden" name="Term_Date" value="<?php echo $Term_Date; ?>" />
<input type="hidden" name="TeacherName" value="<?php echo $TeacherName; ?>" />
<input type="hidden" name="WorkingDays" value="<?php echo $WorkingDays; ?>" />
<input type="hidden" name="Period_Code" value="<?php echo $Period_Code; ?>" />
<input type="hidden" name="Subject_Code" value="<?php echo $Subject_Code; ?>" />
<input type="hidden" name="Admission_No" value="<?php echo $Admission_No; ?>" />
<input type="hidden" name="Page_Code" id="Page_Code" value="<?php echo $Page_Code; ?>" />
<input type="hidden" name="Ass_Header_Code" id="Ass_Header_Code" value="<?php echo $Ass_Header_Code1 ?>"/>

<table width="99%" border="0"  cellpadding="0" cellspacing="0">
<!--<tr><td  align="center"><span class="hr_message">Please Enter the Co Scholastic value at last</span> </td></tr>-->
<tr><td height="10"></td></tr>
		<tr>
			<td >
				<div id="scholarshipfr" align="center" style="width: 100%;">
				
				<table width="100%" border="0">
				<tr>
							<?php 	//SHOW Location  DROPDOWN
						mssql_free_result($result);
						$query = mssql_init('[sp_selectschoolname]',$mssql);
                        mssql_bind($query, '@School_id', $_SESSION['SchoolId'], SQLINT4, false, false, 5);
						$result = @mssql_execute($query);
						mssql_free_statement($query);	?>
							<td  style="width:100px;" >Location<?php echo $mand; ?></td>
							<td>
									<select id="Location" name="Location" style="width:105px" >
										<?php echo $Select;?>
										<?php while($field = mssql_fetch_array($result))   { 
										   ?>
										<option value="<?php echo $field['School_id']?>" <?php if($Location==$field['School_id']) echo "Selected"; ?>> <?php echo $field['SchoolName']?> </option>
										<?php } ?>
									</select>								</td>
							<?php 	//SHOW CLASS DROPDOWN
		mssql_free_result($result);			
		$query = mssql_init('sp_GetClass',$mssql);
		$result = @mssql_execute($query);
		mssql_free_statement($query);	?>
							<td>Class<?php echo $mand; ?> </td>
							<td>
									<select id="Class" name="Class" style="width:155px" onchange="subject(this.value,'<?php echo $Subject_Code;?>')">
										<?php echo $Select;?>
										<?php while($field = mssql_fetch_array($result)) 	{ 
										 ?>
										<option value="<?php echo $field['Class_Code']?>" <?php if( $Class==$field['Class_Code']) echo "Selected"; ?>> <?php echo $field['Class']?></option>
										<?php } ?>
									</select>								</td>
							<?php 	//SHOW SECTION DROPDOWN
		mssql_free_result($result);		
		$query = mssql_init('sp_GetSection',$mssql);
		$result = @mssql_execute($query);
		mssql_free_statement($query);	?>
							<td>Section<?php echo $mand; ?> </td>
							<td>
									<select id="Section" name="Section" style="width:100px">
										<?php while($field = mssql_fetch_array($result)) 
   {  ?>
										<option value="<?php echo $field['Section_Code']?>" <?php if($Section==$field['Section_Code']) echo "selected"; ?>> <?php echo $field['Section']?></option>
										<?php } ?>
									</select>								</td>
							<td>Adm. No.</td>
							<td>
								<input type="text" name="Admission_No" id="Admission_No" maxlength="15" style="width:138px" value="<?php echo $Admission_No ?>"  /></td>
							<?php 	//SHOW TERM DROPDOWN
mssql_free_result($result);		
		$query = mssql_init('sp_GetTerm',$mssql);
		 mssql_bind($query, '@School_id', $_SESSION['SchoolId'], SQLINT4, false, false, 5);
		$result = mssql_execute($query);
		mssql_free_statement($query);	?>
							<td>Term <?php echo $mand; ?></td>
							<td>
									<select id="Term_Code" name="Term_Code" style="width:125px">
										<?php while($field = mssql_fetch_array($result))    {  ?>
										<option value="<?php echo $field['Term_Code']?>" <?php if($Term_Code==$field['Term_Code']) echo "selected"; ?>> <?php echo $field['Term']?></option>
										<?php } ?>
									</select>								</td>
						</tr>
						<tr>
							
								
							<td>TermDate<?php echo $mand; ?></td>
							<td><a href="javascript:NewCal('Term_Date','ddmmyyyy')">
								<input type="text" name="Term_Date" id="Term_Date"readonly="readonly" style="width:100px" size="10" maxlength="50" value="<?php echo $Term_Date ?>" />
								</a></td>
							<?php 	//SHOW TEACHER DROPDOWN
		  mssql_free_result($result);	
		$query  = mssql_init('sp_SelectTeacher',$mssql);
		$result = mssql_execute($query);
		mssql_free_statement($query);	?>
							<td>Teacher Name<?php echo $mand; ?></td>
							<td><input type="text" name="TeacherName" id="TeacherName" value="<?php echo $TeacherName?>"  /></td>
							<td>WorkingDays<?php echo $mand; ?></td>
							<td>
									<input type="text" name="WorkingDays" id="WorkingDays"  style="width:95px"  maxlength="3" value="<?php echo $WorkingDays ?>" 
	                      onkeydown="return numberonly('WorkingDays')" />								</td>
							<?php 	//SHOW PERIOD DROPDOWN
		  mssql_free_result($result);	
		$query  = mssql_init('sp_GetPeriod',$mssql);
		$result = mssql_execute($query);
		mssql_free_statement($query);	?>
							<td>Period<?php echo $mand; ?></td>
							<td>
									<select id="Period_Code" name="Period_Code">

										<?php while($field1 = mssql_fetch_array($result)) 
   {  ?>
										<option value="<?php echo $field1['Period_Code']?>" <?php if($Period_Code==$field1['Period_Code']) echo "selected"; ?>> <?php echo $field1['Period']?></option>
										<?php } ?>
									</select>								</td>
								
								<td>Subject<?php echo $mand; ?></td>
								
							<td> <div id="subject">
									<select id="Subject_Code" name="Subject_Code" style="width:125px" >		
									<?php echo $Select;?>
									
									</select>	</div></td>	
                              <script>subject(<?php echo $Class ?>,<?php echo $Subject_Code?>) </script>					
                                   
						</tr>
				<tr>
				<td colspan="8"></td>
				<td  align="right"><input name="Go"  type="submit" value="Go" class="winbutton_go" /> </td>
				<td align="right"><input type="Submit" name="Cancel" value="Cancel" class="winbutton_go"/></td>
				</tr>
				</table>
			</div>
				</td>
				</tr>
				</table></form>
<br />
<?php 

if($errflag==0 and ($_POST['Go']=="Go" or $_POST['Save']=="Save" or $Page_Code > 0 ) )	
	{ 	?> 
<div id="scholarshipfr"  style="width: 100%;">
<?php 
if($Location>0)
{
if(($Class>2 and $Subject_Code==1) or ($Class>2 and $Subject_Code==4) or ($Class>2 and $Subject_Code==5) or ($Class>2 and $Subject_Code==6) or  ($Class>2 and $Subject_Code==7)) {
//include("coscholastic_entry.php");
include("reportcard_mark_entry.php"); 
}
else if($Class>2 and $Subject_Code==2 )
{
	include("coscholastic_entry.php"); 
}
else if($Class<=2 and $Subject_Code==3)
 {
include("kgs_mark_entry.php");
}
else if($Class<=2 and $Subject_Code==8)
 {
include("kgs_scholastic_entry.php");
}
else if($Subject_Code==9 or $Subject_Code==10 )
 {
	// echo "sasdfsdfsdgfs";
include("comments.php");
}
}?>

</div>
<?php }
 ?>
</body>
</html>
<?php include("../includes/copyright.php"); ?>
