<?php session_start();?>
<!DOCTYPE html PUBLIC “-//W3C//DTD XHTML 1.0 Transitional//EN” “http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd”>
<html xmlns=”http://www.w3.org/1999/xhtml”>
<head>
<?php include ("../includes/header1.php"); ?>
<meta http-equiv=”Content-Type” content=”text/html; charset=utf-8″ / >
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<title>Isha Vidhya</title>
<style type="text/css">
<!--
body
{
	margin-left: 00px;
	margin-top: 0px;
	margin-right: 00px;
	margin-bottom: 0px;
}
.bg
{

background:#CCCCCC;
color:#BD3D03;
font-size:14px;

}
.val
{
font-size:15px;
}
.bg1
{
font-size:14px;
}
.nor
{
color:#bd3d03;
font-size:14px;

}
.nor1
{
font-size:11px;
}
.style2 {color: #BD3D03}
.style3 {color: #FFFFFF}
-->
</style></head>

<body>	

<?php
$Location = $_GET['Location'];
 $Class = $_GET['Class'];
 $Section=$_GET['section'];
 $Adm_No=$_GET['Adm_No'];
 	 mssql_free_result($result);
    $query=mssql_init('sp_GenerateReportcard',$mssql);
    mssql_bind($query,'@Location',$Location,SQLINT4,false,false,5);
    mssql_bind($query,'@Class',$Class,SQLINT4,false,false,5);
    mssql_bind($query,'@Section',$Section,SQLINT4,false,false,5); 
    mssql_bind($query,'@Admission_No',$Adm_No,SQLVARCHAR,false,false,50);      
    $result=mssql_execute($query);
    mssql_free_statement($query);
	$rs_cnt = mssql_num_rows($result);
    if($rs_cnt==0)
       echo "<p align='center' style='color:red;'>No Records Found...</p>"; 
    else
	{
	
	
	while($rfld= mssql_fetch_array($result))
	   {
	   			$sname = $rfld['Name'];
				$sgender = $rfld['Gender'];
				if($sgender=="M")
				{
				 $gen="MALE";
				}
				else if($sgender=="F")
				{
				$gen="FEMALE";
				}
				
				$sguardname=$rfld['Guard_Name']; 
				$smothername=$rfld['Mother_Name'];
				$sguardmobile=$rfld['GuardMobile'];
				$saddress=$rfld['Guard_Res_Address'];
				$speriod = $rfld['Period'];
				$sclass = $rfld['Class'];
			    $ssection = $rfld['Section'];
				$steacher = $rfld['Teacher_Name'];
	            $Student_Id = $rfld['Student_Id'];
				$sdob = $rfld['DOB'];
           	 	$sdob=date('d-m-Y',strtotime($sdob));
  	             //$sdob = $rfld[(strtotime("m/d/y", 'DOB')]);
                 
                //strtotime("YYYY-MM-DD", $sdob);
                //echo $sdob;
              // $sdob= strtotime("d-m", $sdob);
                //echo $sdob;
				$sadmno = $rfld['Admission_No'];
				$sloc = $rfld['Location'];
	   
	   
	  
	

?>
<table width="100%" border="0" cellspacing="2" cellpadding="5" style="background:url(bg.jpg) repeat-x; ">
	<tr>
		<td width="46%" valign="top"><table width="100%" border="0" cellspacing="5" cellpadding="0">
		<tr>
		<td>
		
		<table width="100%" border="1" cellspacing="0" cellpadding="0">
	<tr>
		<td colspan="3" align="center" style=" font-size:14px; font-weight:bold">CLASS TEACHER'S COMMENT</td>
		
	</tr>
	<tr>
		<td width="66%">TERM 1</td>
		<td width="22%">Date:</td>
		<td width="12%">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3" height="70px;">&nbsp;</td>
		
	</tr>
	<tr>
		<td width="66%">TERM 2</td>
		<td width="22%">Date:</td>
		<td width="12%">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3" height="70px;">&nbsp;</td>
	</tr>
	<tr>
		<td width="66%">TERM 3</td>
		<td width="22%">Date:</td>
		<td width="12%">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3" height="70px;">&nbsp;</td>
	</tr>
</table>

		
		</td>
		
		
		</tr>
		
		
		
			<tr>
				<td><table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr align="center" style=" font-size:18px; font-weight:bold">
						<td >GRADING SYSTEM</td>
					</tr>
					<tr align="center" style=" font-size:14px; font-weight:bold">
						<td >Scholastic Areas</td>
					</tr>
				</table></td>
			</tr>
			<tr>
				<td> <table width="100%" border="1" cellspacing="0" cellpadding="0">
	<tr style=" font-size:14px; font-weight:bold" align="center">
		<td colspan="2">Scholastic - Grading system 9 point scale FA</td>
		<td colspan="2">Scholastic - Grading system 9 point scale SA</td>
		<td colspan="2">Scholastic - Grading system 9 point scale FA + SA</td>
		</tr>
	<tr style="font-size:14px; font-weight:bold" align="center">
		<td>Marks</td>
		<td>Grade</td>
		<td>Marks</td>
		<td>Grade</td>
		<td>Marks</td>
		<td>Grade</td>
	</tr>
	<tr align="center">
		<td>37 - 40</td>
		<td>A1</td>
		<td>55 - 60</td>
		<td>A1</td>
		<td>91 - 100</td>
		<td>A1</td>
	</tr>
	<tr align="center">
		<td>33 - 36</td>
		<td>A2</td>
		<td>49 - 54</td>
		<td>A2</td>
		<td>81 - 90</td>
		<td>A2</td>
	</tr>
	<tr align="center">
		<td>29 - 32</td>
		<td>B1</td>
		<td>43 - 48</td>
		<td>B1</td>
		<td>71 - 80</td>
		<td>B1</td>
	</tr>
	<tr align="center">
		<td>25 - 28</td>
		<td>B2</td>
		<td>37 - 42</td>
		<td>B2</td>
		<td>61 - 70</td>
		<td>B2</td>
	</tr>
	<tr align="center">
		<td>21 - 24</td>
		<td>C1</td>
		<td>31 - 36</td>
		<td>C1</td>
		<td>51 - 60</td>
		<td>C1</td>
	</tr>
	<tr align="center">
		<td>17 - 20</td>
		<td>C2</td>
		<td>25 - 30</td>
		<td>C2</td>
		<td>41 - 50</td>
		<td>C2</td>
	</tr>
	<tr align="center">
		<td>13 - 16</td>
		<td>D</td>
		<td>19 - 24</td>
		<td>D</td>
		<td>33 - 40</td>
		<td>D</td>
	</tr>
	<tr align="center">
		<td>9 - 12</td>
		<td>E1</td>
		<td>13 - 18</td>
		<td>E1</td>
		<td>21 - 32</td>
		<td>E1</td>
	</tr>
	<tr align="center">
		<td>8 &amp; Below</td>
		<td>E2</td>
		<td>12 &amp; Below</td>
		<td>E2</td>
		<td>20 &amp; Below</td>
		<td>E2</td>
	</tr>
</table></td>
			</tr>
			<tr>
				<td>Grade and Description for Physical Education and Co-scholastic Activities</td>
			</tr>
			<tr>
				<td><table width="100%" border="1" cellspacing="0" cellpadding="0">
					<tr align="center" style="font-size:14px; font-weight:bold">
						<td>Grade</td>
						<td>Description</td>
					</tr>
					<tr align="center">
						<td>A</td>
						<td>Excellent</td>
					</tr>
					<tr align="center">
						<td>B</td>
						<td>Very Good</td>
					</tr>
					<tr align="center">
						<td>C</td>
						<td>Good</td>
					</tr>
					<tr align="center">
						<td>D</td>
						<td>Satisfactory</td>
					</tr>
					<tr align="center">
						<td>E</td>
						<td>Needs Improvement</td>
					</tr>
				</table></td>
			</tr>
		</table></td>
		<td width="54%" valign="top">
		
		<table width="100%" border="0" cellspacing="1" cellpadding="0">
	<tr>
		<td><table width="100%" border="0" cellspacing="5" cellpadding="0">
			<tr align="center" style=" font-size:18px; font-weight:bold">
				<td align="center">ISHA VIDHYA MATRICULAION SCHOOL</td>
			</tr>
			<tr align="center" style=" font-size:14px; font-weight:bold">
				<td >Comprehensive Evaluation Report</td>
			</tr>
			<tr>
			<td align="center">
			
			<img src="../images/logo.jpg"/>		
			
			</td>
			
			</tr>
			<tr align="center" style=" font-size:14px; font-weight:bold">
				<td align="center">ACADEMIC YEAR 2012 - 2013</td>
			</tr>
			
		</table></td>
	</tr>
	<tr>
		<td><table width="100%" border="1" cellspacing="1" cellpadding="0">
		<tr align="center" style=" font-size:14px; font-weight:bold">
				<td colspan="3" align="center">STUDENT PROFILE</td>
			</tr>
			<tr>
				<td width="24%"> Name of the Student:</td>
				<td width="37%"><?php echo $sname?> </td>
				<td width="39%" rowspan="9"  align="center" >
					<img src='../Photos/<?php echo $Student_Id?>/<?php echo $Student_Id?>.jpg' width='180' height='180'/>
				</td>
				
			</tr>
			<tr>
				<td> Gender:</td>
				<td> <?php echo $gen;?></td>
				</tr>
			<tr>
				<td> Class &amp; Section:</td>
				<td><?php echo $sclass ?>  &amp; <?php echo $ssection ?></td>
				</tr>
			<tr>
				<td colspan="2"> 
				<table width="100%" border="1" cellspacing="0" cellpadding="0">
				<tr>
				<td width="39%">Roll Number:</td>
				<td width="21%">&nbsp;</td>
				<td width="15%">Admission No:</td>
				<td width="25%">&nbsp;</td>
				</tr>
				</table>
			</td>
			
	</tr>
			<tr>
				<td> Admission Number:</td>
				<td> <?php echo $sadmno ?></td>
				</tr>
			<tr>
				<td>Date of Birth:</td>
				<td> <?php echo $sdob?></td>
				</tr>
			<tr>
				<td> Father's Name:</td>
				<td><?php echo $sguardname ?></td>
				</tr>
			<tr>
				<td> Mother's Name:</td>
				<td><?php echo $smothername ?> </td>
				</tr>
			<tr>
				<td> Guardian's Name:</td>
				<td></td>
				</tr>
			<tr>
				<td> Residential Address:</td>
				<td><?php echo $saddress ?></td>
				<td rowspan="2" align="center" >School Seal</td>
				
				</tr>
			<tr>
				<td> Contact Number:</td>
				<td><?php echo $sguardmobile ?></td>
				</tr>
			<tr>
				<td> Height :</td>
				<td> Weight :</td>
				<td> Blood Group :</td>
			</tr>
		</table></td>
	</tr>
		<tr>
		<td><table width="100%" border="1" cellspacing="1" cellpadding="0">
			<tr>
				<td>Parents are requested to review this card each term, sign on Page 3 and return to the school within 7 days. Clarifications can be discussed during &quot;Open Day&quot; or by making an appointment through the Administrative office to meet the Principal.</td>
			</tr>
			<tr>
				<td style="font-size:8px;"><p><span class='nor1'>
                பெற்றோர்கள் இந்த மதிப்பீட்டு அறிக்கையை பார்த்தபின், மூன்றாம் பக்கத்தில் கையொப்பம் இட்டு ஏழு நாட்களுக்குள் திருப்பிக் கொடுக்குமாறு கேட்டுக் கொள்ளப்படுகிறார்கள். ஐயங்கள் ஏதேனும் இருந்தாலஅதை பெற்றோர் ஆசிரியர் சந்திப்பில் ஆசிரியரிடம் கலந்தாலோசிக்கவும் அல்லது பள்ளி நிர்வாகியின் மூலமாக  பள்ளி முதல்வரை சந்திக்கவும்.</span></p></td>
			</tr>
		</table></td>
	</tr>
</table>		</td>
	</tr>
	<tr>
	<td height="70px"></td>
	<td></td>
	</tr>
	<tr><td valign="top"><table width="100%" border="0" cellspacing="1	" cellpadding="0">
		
		<tr>
			<td><table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr align="center" style=" font-size:14px; font-weight:bold">
						<td>PART - 1 - ACADEMIC PERFORMANCE: SCHOLASTIC AREAS</td>
					</tr>
			</table></td>
		</tr>
		<tr>
			<td><table width="100%" border="1" cellspacing="0" cellpadding="0">
					<tr align="center" style=" font-size:14px; font-weight:bold">
						<td width="24%">SUBJECTS</td>
						<td colspan="3">TERM - I</td>
						<td colspan="3">TERM - II</td>
						<td colspan="3">TERM - III</td>
						<td width="13%" colspan="3">Year End Grade(*)</td>
					</tr>
					<tr>
						<td></td>
						<td width="5%">FA</td>
						<td width="4%">SA</td>
						<td width="9%">TOTAL</td>
						<td width="6%">FA</td>
						<td width="5%">SA</td>
						<td width="9%">TOTAL</td>
						<td width="7%">FA</td>
						<td width="7%">SA</td>
						<td width="11%">TOTAL</td>
						<td>&nbsp;</td>
					
					
					</tr>
					<tr>
						<td>TAMIL</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>ENGLISH</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>MATHS</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>SCIENCE/EVS</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>SOCIAL SCIENCE</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>Physical Edcuation</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
			</table></td>
		</tr>
		<tr align="center" style=" font-size:14px; font-weight:bold" >
			<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>FA - Formative Assessment</td>
		<td>SA - Summative Assessment</td>
	</tr>
</table>
</td>
		</tr>
		<tr align="center" style=" font-size:14px; font-weight:bold">
			<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="49%">Total = 40% (FA) + 60 % (SA)</td>
		<td width="51%">* - Average of three Terms</td>
	</tr>
</table>
</td>
		</tr>
		
		<tr align="center" style=" font-size:14px; font-weight:bold">
			<td>PART - 2 - CO-SCHOLASTIC AREAS</td>
		</tr>
		<tr>
			<td><table width="100%" border="1" cellspacing="0" cellpadding="0">
					<tr  align="center" style=" font-size:14px; font-weight:bold">
						<td width="43%">Area</td>
						<td width="17%">Term 1</td>
						<td width="15%">Term 2</td>
						<td width="12%">Term 3</td>
						<td width="13%">Year End Grade(*)</td>
					</tr>
					<tr>
						<td>Life Skills</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>Attitudes and Values</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>Wellness &amp; Yoga / Holistic Exercise</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>Co-Curricular Activities</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
			</table></td>
		</tr>
		
		<tr>
		<td>
		
		
		<table width="100%" border="1" cellspacing="0" cellpadding="0">
					<tr  align="center" style=" font-size:14px; font-weight:bold">
						<td width="43%">DETAILS</td>
						<td width="17%">TERM - 1</td>
						<td width="15%">TERM - 2</td>
						<td width="12%">TERM - 3</td>
						<td width="13%">Year End Grade(*)</td>
					</tr>
					<tr>
						<td>No. of Working Days</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>Student's attendance</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>Signature of the Class Teacher</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>Signature of the Principal</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>Signature of the Parent</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
				</table></td>
		
		</tr>
	</table></td>
	<td valign="top">
	<table width="100%" border="0" cellspacing="2" cellpadding="0">
			<tr align="center" style=" font-size:14px; font-weight:bold">
				<td align="center">CO-SCHOLASTIC AREAS</td>
			</tr>
			<tr align="center" style=" font-size:14px; font-weight:bold">
				<td align="center">Criteria for Assessment</td>
			</tr>
			<tr>
				<td><table width="100%" border="1" cellspacing="0" cellpadding="0">
					<tr  align="center" style=" font-size:14px; font-weight:bold">
						<td  colspan="2" >Life Skills</td>
						
					</tr>
					<tr>
						<td width="40%">Communication Skills</td>
						<td width="60%">&nbsp;</td>
					</tr>
					<tr>
						<td>Assertion/Refusal Skills</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>Interpersonal Skills</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>Team Work</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>Emotion Management Skills</td>
						<td>&nbsp;</td>
					</tr>
					
				</table></td>
			</tr>
			<tr>
				<td><table width="100%" border="1" cellspacing="0" cellpadding="0">
					<tr  align="center" style=" font-size:14px; font-weight:bold">
						<td colspan="2">Attitude and Values</td>
						
					</tr>
					<tr>
						<td width="40%">Respect for Nation &amp; Culture</td>
						<td width="60%">&nbsp;</td>
					</tr>
					<tr>
						<td>Respect for School/Community Property</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>Compliance with Safety &amp; Road Rules</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>Compassion for all living creature</td>
						<td>&nbsp;</td>
					</tr>
				</table></td>
			</tr>
			<tr>
				<td><table width="100%" border="1" cellspacing="0" cellpadding="0">
					<tr  align="center" style=" font-size:14px; font-weight:bold">
						<td colspan="2">Wellness &amp; Yoga</td>
						
					</tr>
					<tr>
						<td width="40%">Health &amp; Hygiene</td>
						<td width="60%">&nbsp;</td>
					</tr>
					<tr>
						<td>Yoga/Holistic Exercise</td>
						<td>&nbsp;</td>
					</tr>
					
				</table></td>
			</tr>
			
			<tr>
				<td><table width="100%" border="1" cellspacing="0" cellpadding="0">
					<tr  align="center" style=" font-size:14px; font-weight:bold">
						<td colspan="2">Co-Curricular Activities</td>
						
					</tr>
					<tr>
						<td width="40%">Initiative</td>
						<td width="60%">&nbsp;</td>
					</tr>
					<tr>
						<td>Involvement in Club activities</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>Regularity</td>
						<td>&nbsp;</td>
					</tr>
					
				</table></td>
			</tr>
			<tr>
				<td>
				<table width="100%" border="1" cellspacing="0" cellpadding="0">
					<tr  align="center" style=" font-size:14px; font-weight:bold">
						<td colspan="2">RESULT</td>
						
					</tr>
					<tr>
						<td width="40%">HPromoted to Class</td>
						<td width="60%"> _______</td>
					</tr>
					<tr>
						<td colspan="2">
						
						
						<table width="100%" border="1" cellspacing="0" cellpadding="0">
	<tr>
		<td width="40%">&nbsp;</td>
		<td width="27%">&nbsp;</td>
		<td width="33%">&nbsp;</td>
	</tr>
<tr>
		<td>Class Teacher</td>
		<td>Seal</td>
		<td>Principal</td>
	</tr>	
	
</table>

						</td>
						
						
					</tr>
					
				</table>
				
				</td>
			</tr>
		</table>	</td>
	</tr>
</table>
<br/>
<?php } }?>


</body>
</html>
