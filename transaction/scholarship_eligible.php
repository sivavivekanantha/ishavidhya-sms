<?php
session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<?php
include ("../includes/header1.php");
?>

<script>
function scholarshipfunc(obj)
{
		if(obj==1)
		{
			if($('#schl2').is(":hidden"))	
			$('#schl2').show();
		
		}
		else
		{
			$('#schl2').hide();
					
		}
}
function recommendationfunc()
{ 
		var r=$('#recommendation').val();
		if(r=='Y')
		{
			$('#recom1').show();
			$('#recom2').show();
			$('#recom3').show();
			$('#recom4').show();		
		}
		else
		{
			$('#recom1').hide();
			$('#recom2').hide();
			$('#recom3').hide();
			$('#recom4').hide();
		}
						
}


function disablefunc()
{
	
		var r=$("#child_eligible").val();
		if(r=='Y')
		{
			$('#reonsideration').attr('disabled',true);
		}
		else
		{
			$('#reonsideration').attr('disabled',false);		
		}
				
}



function disablefunc1()
{
		
		var r=$("#reonsideration").val();
		if(r=='Y')
		{
			$('#child_eligible').attr('disabled',true);
		}
		else
		{
			$('#child_eligible').attr('disabled',false);		
		}
		
}

</script>
<?php
$name = "";
$admn = "";
if($_GET["student_id"] != '') {
    $name = $_GET["student_id"];
} else {
    $name = $_POST["student_id"];
}
if($_GET["name"] != '') {
    $name = $_GET["name"];
} else {
    $name = $_POST["name"];
}
if($_GET["admn"] != '') {
    $admn = $_GET["admn"];
} else {
    $admn = $_POST["admn"];
}
if($_GET["student_id"] != '') {
    $student_id = $_GET["student_id"];
} else {
    $student_id = $_POST["student_id"];
}
if($_GET["class"] != '') {
    $class = $_GET["class"];
} else {
    $class = $_POST["class"];
}
$form_correct = "";
$child_eligible = "";
$recommendation = "";
$reonsideration = "";
$recommender_name = "";
$reason = "";
$scholarship_reason = "";
$scholarship_reason_code = "";
if($_POST['save'] == "save") {
    $student_id = trim($_POST["student_id"]);
    $form_correct = trim($_POST["form_correct"]);
    $child_eligible = trim($_POST["child_eligible"]);
    $recommendation = trim($_POST["recommendation"]);
	
	$checkBox1 = $_POST['Scholarshiptype'];

   for ($i = 0; $i < sizeof($checkBox1); $i++) 	{

    $Scholarshiptype = $Scholarshiptype . $checkBox1[$i] . ",";
   }
   $Scholarshiptype = "," . $Scholarshiptype;
   
   $checkBox2 = $_POST['School_Subsidy'];

   for ($i = 0; $i < sizeof($checkBox2); $i++) 	{

    $School_Subsidy = $School_Subsidy . $checkBox2[$i] . ",";
   }
   $School_Subsidy = "," . $School_Subsidy;
   
   
	if($recommendation=="Y"){	
    $recommender_name = trim($_POST["recommender_name"]);
    $reason = trim($_POST["reason"]);
	  
		}
	else
	{
	 $recommender_name ="";
   	 $reason ="";
	}
	$reonsideration = trim($_POST["reonsideration"]);
    $scholarship_reason_code = trim($_POST["scholarship_reason"]);
	if($scholarship_reason_code==1){
    $scholarship_reason = trim($_POST["scholarship_reason1"]);
	}
	else
	{
	$scholarship_reason="";
	}
    $errflag = 0;
    $dummy = Zerocheck($form_correct, $errmsg, $errflag, "Form Correct");
    $dummy = Zerocheck($child_eligible, $errmsg, $errflag, "Child Eligible");
    if($child_eligible == 'N') {
        $dummy = Zerocheck($reonsideration, $errmsg, $errflag, "Reconsideration");
    }
    if($recommendation == 'Y') {
        $dummy = Strcheck($recommender_name, $errmsg, $errflag, "Name");
        $dummy = Strcheck($reason, $errmsg, $errflag, "Reason");
    }
    $dummy = Zerocheck($scholarship_reason_code, $errmsg, $errflag,
        "Scholarship Reason");
    if($scholarship_reason_code == 1) {
        $dummy = Strcheck($scholarship_reason, $errmsg, $errflag, "Scholarship Reason");
    }
    if($errflag == 0) {
	//echo "child".$child_eligible;
		mssql_free_result($result);
        $query = mssql_init('[sp_ScholarshipEligible]', $mssql);
        mssql_bind($query, '@Student_Id', $student_id, SQLINT4, false, false, 5);
        mssql_bind($query, '@Form_Correct', $form_correct, SQLVARCHAR, false, false, 5);
        mssql_bind($query, '@Child_Eligible', $child_eligible, SQLVARCHAR, false, false,50);
        mssql_bind($query, '@Recommendation', $recommendation, SQLVARCHAR, false, false,
            25);
        mssql_bind($query, '@Reconsideration', $reonsideration, SQLVARCHAR, false, false,
            5);
        mssql_bind($query, '@Recommender_Name', $recommender_name, SQLVARCHAR, false, false,
            1);
        mssql_bind($query, '@Reason_Recommend', $reason, SQLVARCHAR, false, false, 5);
        mssql_bind($query, '@Scholarship_Reason_Code', $scholarship_reason_code, SQLINT4, false, false,
            1);
        mssql_bind($query, '@Scholarship_Reason', $scholarship_reason, SQLTEXT, false, false,strlen($scholarship_reason));
        
	    mssql_bind($query, '@Scholarshiptype', $Scholarshiptype, SQLVARCHAR, false, false, 50);
		mssql_bind($query, '@School_Subsidy', $School_Subsidy, SQLVARCHAR, false, false, 50);
           
        mssql_bind($query, '@Created_By', $_SESSION['UserID'], SQLINT4, false, false, 5);
		
        $result = @mssql_execute($query);
        mssql_free_statement($query);
        if($result == 1) echo
                "<p class='mesg'>Scholarship Eligible has been Updated</p>";
        else {
            $errmsg1 = mssql_get_last_message();
            $errflag = 2;
            if($errcnt == 0) {
                $errcnt = 1;
            } else {
                $errcnt = $errcnt + 1;
            }
            if($errval == "") {
                $errval = $editcnt[$i];
            } else {
                $errval = $errval . "," . $editcnt[$i];
            }
        }
    } else {
        if($errcnt == 0) {
            $errcnt = 1;
        } else {
            $errcnt = $errcnt + 1;
        }
        if($errval == "") {
            $errval = $editcnt[$i];
        } else {
            $errval = $errval . "," . $editcnt[$i];
        }
    }
}
if($errflag == 1) echo $errlbl . $errmsg;
if($errflag == 2) echo "<p class='error'>" . $errmsg1;
?>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="2">
<?php
mssql_free_result($result);
$query = mssql_init('sp_GetScholarshipStatus', $mssql);
mssql_bind($query, '@Student_Id', $student_id, SQLINT4, false, false, 5);
$result = mssql_execute($query);
mssql_free_statement($query);
$rs_cnt = mssql_num_rows($result);
$colorflag = 0;
$i = 0;
while ($field = mssql_fetch_array($result)) {
    $form_correct = $field["Form_Correct"];
    $child_eligible = $field["Child_Eligible"];
    $reonsideration = $field["Reconsideration"];
    $recommendation = $field["Recommendation"];
    $recommender_name = $field["Recommender_Name"];
    $reason = $field["Reason_Recommend"];
    $scholarship_reason_code = $field["Scholarship_Reason_code"];
    $scholarship_reason = $field["Scholrship_Reason"];
    $child_subsidy=$field["Child_Subsidy"];
	$School_Subsidy=$field["School_Subsidy"];
			
 ?>

  <tr><td align="center" >
    <div id="scholarshipfr" align="center" style="width:85%">
      <form name="myform" id="myform" method="post" action="scholarship_eligible.php">
        <input name="student_id" type="hidden" value="<?php   echo $student_id;
?>" />
        <input name="name" type="hidden" value="<?php    echo $name; ?>" />
        <input name="admn" type="hidden" value="<?php    echo $admn; ?>" />
        <input name="class" type="hidden" value="<?php     echo $class; ?>" />

        <table width="100%" border="0" align="center" cellpadding="1" cellspacing="3">
          <tr><td align="center">
               
               <table width="60%" border="0" style="padding:5px 5px 5px 5px;" class="top_bar" align="center" cellpadding="3" cellspacing="1">
                <tr><td><b>Student Name: &nbsp; <?php
    echo $name;
?> </b> </td>
                    <td><b>Admission No: &nbsp; <?php
    echo $admn;
?> </b> </td>
                    <td><b>Class: &nbsp; <?php
    echo $class
?> </b></td> 
              </tr></table>
            </td>
          </tr>
          
		 <tr><td height="5">&nbsp;</td></tr>
         
         <tr>
            <td align="left">
            <table width="90%" border="0" align="center" cellpadding="1" cellspacing="3">
            <tr>
                <td width="155" align="left">Form Correct<?php
    echo $mand;
?></td>
                <td width="58" align="center"><select name="form_correct" id="form_correct">
                    <?php
    echo $Select;
?>
                    <option value="Y" <?php
    if($form_correct == 'Y') echo "Selected";
?>>Yes</option>
                    <option value="N" <?php
    if($form_correct == 'N') echo "Selected";
?>>No</option>
                  </select></td>
                    
            <td width="330" align="right"><!-- <input name="child_eligible" type="checkbox" value="Y" onclick="disablefunc()">-->
              Child Eligible for scholarship<?php
    echo $mand;
?> </td>
            <td width="185" align="center"><select name="child_eligible" onchange="disablefunc()" id="child_eligible">
                <?php
    echo $Select;
?>
                <option value="Y" <?php
    if($child_eligible == 'Y') echo "Selected";
?>>Yes</option>
                <option value="N" <?php
    if($child_eligible == 'N') echo "Selected";
?>>No</option>
              </select></td>
          </tr>
			<tr><td height="5">&nbsp;</td></tr>
	          <tr>
                <td>Reconsideration</td>
                <td><select name="reonsideration" id="reonsideration" onchange="disablefunc1()">
				<?php
    echo $Select;
?>
                <option value="Y" <?php
    if($reonsideration == 'Y') echo "Selected";
?>>Yes</option>
                <option value="N" <?php
    if($reonsideration == 'N') echo "Selected";
?>>No</option>
              </select></td>
	        <td align="right" >Recommendation</td>
            <td ><select name="recommendation" id="recommendation" onchange="recommendationfunc()">
				<?php
    echo $Select;
?>
                <option value="Y" <?php
    if($recommendation == 'Y') echo "Selected";
?>>Yes</option>
                <option value="N" <?php
    if($recommendation == 'N') echo "Selected";
?>>No</option>
              </select></td>
            <td width="224" colspan="3" align="left"><table width="100%" border="0" cellspacing="5" cellpadding="0">
              <tr>              </tr>
            </table></td>
            </tr>
          <tr>
            <td colspan="5">&nbsp;</td>
          </tr>
          <tr align="left">
            <td colspan="5"><table width="100%" border="0" cellspacing="1" cellpadding="3">
  <tr>
    <td ><div id="recom1" style="display:none">Name<?php
    echo $mand;
?> </div></td>
    <td ><div id="recom2" style="display:none"><input name="recommender_name" type="text" value="<?php
    echo $recommender_name;
?>"></div></td>
    <td ><div id="recom3" style="display:none">Reason<?php
    echo $mand;
?> </div></td>
    <td ><div id="recom4" style="display:none;"><textarea name="reason" cols="10" rows="2" style="width:305px;"><?php
    echo $reason;
?></textarea>
              </div></td>
  </tr>
</table>              </td>
            </tr>
          <tr align="left">
            <td>Scholarship Reason
			<?php
    echo $mand;
?></td>
            <td><select name="scholarship_reason" id="scholarship_reason" onchange="scholarshipfunc(this.value)">
               <?php
    echo $Select;
?>
                <?php
				mssql_free_result($result);
    $query = mssql_init('[sp_GetScholarshipReason]', $mssql);
    $result = mssql_execute($query);
    mssql_free_statement($query);
    while ($field = mssql_fetch_array($result)) {
?>
                <option value="<?php
        echo $field['Scholr_Reason_Code'];
?>" <?php
        if($scholarship_reason_code == $field['Scholr_Reason_Code']) echo "selected";
?>>
				<?php
        echo $field['Reason'];
?></option>
                <?php
    }
?>
                </select>                </td>
			
				<td align="center" colspan="2"> 
			<div id="schl2" style="display:none;">
                <textarea name="scholarship_reason1" id="scholarship_reason1" cols="10" rows="2"  style="width:285px;"><?php
    				echo $scholarship_reason;
?></textarea> </div>              </td>
			  <?PHP 
				if($scholarship_reason_code >0){ 
					?>
				<script>scholarshipfunc(<?php echo $scholarship_reason_code ?>) </script>
				
			<?php } ?>
          </tr>
		  <tr>
		  <td>Subsidy from Donor</td>
		  <td colspan="3"><div id="Class" style="display:block;">
                                          <div style="overflow:auto;width:610px;height:50px;border:1px solid #336699;
                                          padding-left:5px;background: #FFFFFF; ">
                                            <table width="13%" border="0">
                                              <tr>
                                                <?php // SHOW RECORDS
												mssql_free_result($result);
$query = mssql_init('SP_GetScholarshiptype', $mssql);
$result = mssql_execute($query);
mssql_free_statement($query);
$rs_cnt = mssql_num_rows($result);
$i = 0;
$slcode = split(",", $child_subsidy); ?>
												<?php while ($field = mssql_fetch_array($result)) {
												
    $i += 1;
    $tot_rec = $i;
    $count++; ?>
                                                <td width="7%" valign="top" nowrap ><input  type="checkbox" style="width:15px"                                                   id="Scholarshiptype" name="Scholarshiptype[]"  value="<?php echo $field['Donation_Code'] ?>"
                                                  <?php         if (in_array( $field['Donation_Code'],$slcode)) echo "checked"; ?>/>                                                </td>
                                                <td width="93%" valign="top" nowrap ><?php  echo $field['Donation']; ?></td>
                                                <?php     if ($count > 4) {
        $count = 0; ?>
                                                </tr>
                                                <tr>
                                                <?php     }
} ?>
                                              </tr>
                                            </table>
                                            <p>&nbsp;</p>
                                          </div>
                                        </div></td>
		  </tr>
		  
		   <tr>
		  <td>Subsidy from School</td>
		  <td colspan="3"><div id="Class" style="display:block;">
                                          <div style="overflow:auto;width:610px;height:50px;border:1px solid #336699;
                                          padding-left:5px;background: #FFFFFF; ">
                                            <table width="13%" border="0">
                                              <tr>
                                                <?php // SHOW RECORDS
												mssql_free_result($result);
$query = mssql_init('SP_GetScholarship_School', $mssql);
$result = mssql_execute($query);
mssql_free_statement($query);
$rs_cnt = mssql_num_rows($result);
$i = 0;
$slcode1 = split(",", $School_Subsidy); ?>
												<?php while ($field = mssql_fetch_array($result)) {
												
    $i += 1;
    $tot_rec = $i;
    $count++; ?>
                                                <td width="7%" valign="top" nowrap ><input  type="checkbox" style="width:15px"                                                   id="School_Subsidy" name="School_Subsidy[]"  value="<?php echo $field['Scholarship_Code'] ?>"
                                                  <?php         if (in_array( $field['Scholarship_Code'],$slcode1)) echo "checked"; ?>/>                                                </td>
                                                <td width="93%" valign="top" nowrap ><?php  echo $field['Scholarship']; ?></td>
                                                <?php     if ($count > 4) {
        $count = 0; ?>
                                                </tr>
                                                <tr>
                                                <?php     }
} ?>
                                              </tr>
                                            </table>
                                            <p>&nbsp;</p>
                                          </div>
                                        </div></td>
		  </tr>
          <tr>
            <td colspan="3" align="right">&nbsp;</td>
            <td align="right"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td><input type="submit" name="save" value="save" class="winbutton_go"  <?php if(in_array($_SESSION['DeptCode'],$hide_save1))echo "style='visibility: hidden;'";?> /></td>
                <td><input type="submit" name="Close" value="Close" class="winbutton_go"/ onclick="self.close()"  <?php if(in_array($_SESSION['DeptCode'],$hide_save1))echo "style='visibility: hidden;'";?> /></td>
              </tr>
            </table>              </td>
          </tr>
        </table>
        
<?php
}
if($child_eligible == 'Y') echo "<script> disablefunc();</script>";
if($reonsideration == 'Y') echo "<script> disablefunc1();</script>";
if($recommendation == 'Y') echo "<script> recommendationfunc();</script>";

?>
       </form>
    </td>
  </tr>
</table>

</div>
</body></html>
