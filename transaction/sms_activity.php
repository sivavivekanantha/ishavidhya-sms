<?php
session_start();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php
include ("../includes/header.php");
title('Student Management', 'Activity', 2, 1, 2);
?>
<script type="text/javascript">
 $(function() {
            $(this).bind("contextmenu", function(e) {
                e.preventDefault();
            });
        }); 

</script>
<script>
function thank(obj)
{
if(obj==1){
		$('#act_date').attr("disabled", true);
		$('#activity').attr("disabled", true);
//		$('input[type="text"], textarea').attr('readonly','readonly');
		$('#desc').attr("disabled", true);
		//$("#desc").prop("disabled", true);
		//$('#activity').attr.Remove("readonly", true);
} else if(obj==2) {
	$('#activity').attr("disabled", false);
	$('#act_date').attr("disabled", false);
	$('#desc').attr("disabled", false);
	}	
}
</script>
<style type="text/css">
<!--
.style3 {
	font-size: 11px
}
-->
</style>
</head><body>
<?php
$errmsg = "";
$errflag = 0;
$dummy = 0;
if($_GET['Stu_Id'] != '') $Student_id = TRIM($_GET['Stu_Id']);
else  $Student_id = TRIM($_POST['Stu_Id']);
if(strlen($_GET['SName']) > 0) $SName =trim($_GET['SName']);
else  $SName = trim($_POST['Stu_Name']);
if($_GET['Adm_No'] != '') $Admission_No = $_GET['Adm_No'];
else  $Admission_No = $_POST['Adm_No'];
if($_GET['Class'] != '') $Class = $_GET['Class'];
else  $Class = $_POST['Class'];
if($_GET['Loc_Code'] != '') $Loc_Code = $_GET['Loc_Code'];
else  $Loc_Code = $_POST['Loc_Code'];
if($_GET['Class_Code'] != '') $Class_Code = $_GET['Class_Code'];
else  $Class_Code = $_POST['Class_Code'];
if($_GET['Sec_Code'] != '') $Sec_Code = $_GET['Sec_Code'];
else  $Sec_Code = $_POST['Sec_Code'];
if(strlen($_GET['Status']) > 0) $Status =trim($_GET['Status']);
else  $Status = trim($_POST['Status']);
if($_GET['stu_Name'] != '') $stu_Name = $_GET['stu_Name'];
else  $stu_Name = $_POST['Stu_Name'];
if($_POST['Save'] == 'Save') {
    if($_FILES['photo']['error'][0] == 0) {
        $count = count($_FILES['photo']['tmp_name']);
        for ($i = 0; $i < $count; $i++) {
            if(is_uploaded_file($_FILES['photo']['tmp_name'][$i])) {
            	$exts = array('gif', 'png', 'jpg', 'jpeg','pjpeg'); 
				$extension=end(explode("/",$_FILES['photo']['type']));
       			if(!in_array($extension, $exts)){
                    $file_error = 'YES';
                    echo "<script type='text/javascript'>alert('Not a valid image type!! Please Retry!');</script>";
                }
            } else {
                $file = $_FILES['photo']['tmp_name'][$i];
                $size = $_FILES['photo']['size'][$i];
                /*	if ($size > 504800)	{
                $file_error='YES';	
                echo '<div id="message"><center><span class=error>Image Size exceeds 107200 bytes. Please choose a new image!</span></center></div>';			}*/
            }
        }
    } else {
        $errflag = 1;
        $errmsg = "Upload Photo";
    }

    $action = 1;
    $no_of_photos = 0;
	$Others=trim($_POST['Others']);
    $act_date = trim($_POST['act_date']);
    $activity = trim($_POST['activity']);
    $desc = trim($_POST['desc']);
    $no_of_photos = count($_FILES['photo']['tmp_name']);
    $created_by = trim($_POST['created_by']);
    $created_date = trim($_POST['created_date']);
    $modify_by = trim($_POST['modify_by']);
    $modify_date = trim($_POST['modify_date']);
	
	if($Others>1){
	    $dummy = Strcheck($act_date, $errmsg, $errflag, "Activity Date");
    	$dummy = Strcheck($activity, $errmsg, $errflag, " Activity");
	    //$dummy = Strcheck(no_of_photos,$errmsg,$errflag,"Upload Photo");
    if($errflag == 0) {	
		mssql_free_result($result);
        $query = mssql_init('sp_activity', $mssql);
        mssql_bind($query, '@Act_Code', $act_code, SQLINT4, false, false, 5);
        mssql_bind($query, '@Student_Id', $Student_id, SQLINT4, false, false, 5);
        mssql_bind($query, '@Act_Date', date('Y-m-d', strtotime($act_date)), SQLVARCHAR, false, false,50);
        mssql_bind($query, '@Activity', $activity, SQLVARCHAR, false, false, 25);
        mssql_bind($query, '@Descrip', $desc, SQLVARCHAR, false, false, strlen($desc));
        mssql_bind($query, '@No_of_photos', $no_of_photos, SQLINT4, false, false, 5);
        mssql_bind($query, '@Created_by', $_SESSION['UserID'], SQLINT4, false, false, 1);
        mssql_bind($query, '@Action', $action, SQLINT4, false, false, 5);
        $result = @mssql_execute($query);
        mssql_free_statement($query);			
        if($result == 1) {
            echo "<p class='mesg'>Record Information has been Added</p>";
            if(!is_dir("../photos/".$Student_id ))
			 { 
				 mkdir("../photos/" . $Student_Id,0777,true); //photo folder create 			
			 }				  		
            for ($i = 0; $i < $count; $i++) {
                $path = "../photos/" . $Student_id . "/" . date('Y-m-d', strtotime($act_date)) . "_" . $j . ".jpg";
                move_uploaded_file($_FILES["photo"]["tmp_name"][$i], $path);
                $j++;
				$path="";
          	  }
        }
		 else {
            $errmsg1 = mssql_get_last_message();
            $errflag = 2;
            $inserr = 1;
       	 		}
			}
		}
		else
		{
	 		if(!is_dir("../photos/".$Student_id ))
			 { 	
				  mkdir("../photos/".$Student_id, 0700);				
			 }	
			    $path1 = "../photos/".$Student_id ."/Thank.jpg";				
                move_uploaded_file($_FILES["photo"]["tmp_name"][0], $path1);        
		}
}
	if($errflag == 1) echo "<p class='error'>Incomplete / Invalid entried for<br>".$errmsg;
	if($errflag == 2) echo "<p class='error'>" . $errmsg1;	?>
	<form name="myform" id="myform" method="post" action="sms_activity.php" enctype="multipart/form-data">
	  <input type="hidden" name="Stu_Id" id="Stu_Id" value="<?php  echo $Student_id; ?>"/>
	  <input type="hidden" name="Stu_Name" id="Stu_Name" value="<?php  echo $stu_Name; ?>"/>
	  <input type="hidden" name="Adm_No" id="Adm_No" value="<?php echo $Admission_No; ?>"/>
	  <input type="hidden" name="Class" id="Class" value="<?php echo $Class; ?>"/>
	  <input type="hidden" name="Loc_Code" id="Loc_Code" value="<?php echo $Loc_Code; ?>"/>
	  <input type="hidden" name="Class_Code" id="Class_Code" value="<?php echo $Class_Code; ?>"/>
	  <input type="hidden" name="Sec_Code" id="Sec_Code" value="<?php echo $Sec_Code; ?>"/>
  <table width="70%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr><td>
  <div style="width:850px;  border:thin;" id="scholarshipfr" align="center">
   <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
   <?php  titleheader(Activity, 0); ?>
      <tr><td align="center">&nbsp;</td></tr>
      <tr><td colspan="2">
      <table width="98%" height="30" border="0" style="padding:5px 5px 5px 5px;" align="center" cellpadding="3" cellspacing="1" bgcolor="#6FDCF2">
            <tr>
              <td width="13%"><strong>Student Name : </strong></td>
              <td width="25%"><strong><?php echo $stu_Name; ?></strong> </td>
              <td width="12%"><strong>Admission No : </strong></td>
              <td width="17%"><strong><?php echo $Admission_No; ?></strong></td>
              <td width="8%"><strong>Class : </strong></td>
              <td width="25%"><strong><?php echo $Class; ?></strong></td>
            </tr>
          </table>
       </td></tr>
       <tr><td height="10" colspan="2"></td></tr>
      <tr>
        <td align="center" colspan="2">
        	<table width="60%" align="center" cellpadding="3" cellspacing="1"> 
			<tr>
              <td width="49%" align="left">Activity Type</td>
			<td><table width="100%" border="0" cellspacing="1" cellpadding="0">
				<tr>
					                                           
					 <td  align="left"><input type="radio" name="Others" id="ThankLetter" value="1"  style="width:30px;" onclick="thank(this.value);"/>ThankLetter</td>
					<td><input type="radio" name="Others" id="Others" value="2" style="width:30px;" onclick="thank(this.value);"/>  Others</td>
				</tr>
			</table></td>
            </tr>
            <tr>
              <td width="49%" align="left">Activity Date <?php echo $mand; ?></td>
              <td width="51%" align="left"><a href="javascript:NewCal('act_date','ddmmyyyy')">
              <input type="text" name="act_date" id="act_date" readonly  style="width:100px;" size="20" maxlength="50" value="<?php echo $act_date;?>" ></a> </td>
            </tr>
            <tr>
              <td align="left" >Activity <?php
echo $mand;
?></td>
              <td align="left" ><input type="text" name="activity" id="activity" style="width:100px;" size="20" maxlength="50" value="<?php
echo $activity
?>"></td>
            </tr>
            <tr>
              <td align="left">Description</td>
              <td align="left"><textarea cols="30" rows="3" name="desc" id="desc"><?php
echo $desc
?></textarea></td>
            </tr>
            <tr>
              <td>Upload Photo<?php
echo $mand;
?></td>
              <td colspan="5" ><input type="file" name="photo[]" class="multi accept-gif|jpg" style="width:100px;" size=20 value="<?php
echo $_FILES['photo']['tmp_name']
?>"/>
            </td>
            </tr>
          </table></td>
      </tr>
      <tr>
     <td align="center" colspan="2">
        	<table width="20%" align="right" cellpadding="1" cellspacing="1"> 
            <tr><td>
			<input type="submit" name="Save" value="Save" class="winbutton_go"  
            <?php if($_SESSION['CatCode']==3)echo "style='visibility:hidden;'";?>  /></td><td>
            <a href="studentsearch.php?Loc_Code=<?php echo $Loc_Code; ?>&Class_Code=<?php echo $Class_Code; ?>&Sec_Code=<?php echo $Sec_Code;?>&Adm_No=<?php echo $Adm_No;?>&StudentName=<?php echo $SName ?>&Status=<?php echo $Status; ?>">
            <input type="button" name="Back" value="Back" class="winbutton_go" />
            </a> </td>
      </tr></table>
      </td></tr>
    </table>
  </div>
</td></tr></table>
</form>
</body>
</html>
<?php
include ("../includes/copyright.php");
?>
