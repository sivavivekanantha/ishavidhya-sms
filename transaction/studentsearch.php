<?php session_start(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<?php include ("../includes/header.php"); ?>
<?php title('Student Management', 'Search Student', 2, 1, 2); ?>
<script>
$(document).ready(function() {
    $('#example').dataTable( {
        "sScrollY": "400px",
        "bPaginate": false
    } );
} );	
function editval(val)
{	
	if($('#editcnt').val()!="") {
	 var el = $('#editcnt').val()+","+val;
	 $('#editcnt').val(el);
	}
	else { 
		$('#editcnt').val(val)
		}
}
</script>
</head>
<?php if($_GET['Loc_Code'] > 0) $Loc_Code = $_GET['Loc_Code'];
else  $Loc_Code = Trim($_POST['Loc_Code']);

        if($_GET['Class_Code'] > 0) $Class_Code = $_GET['Class_Code'];
        else  $Class_Code = Trim($_POST['Class_Code']);
        
        if($_GET['Sec_Code'] > 0) $Sec_Code = $_GET['Sec_Code'];
        else  $Sec_Code = Trim($_POST['Sec_Code']);
        
        if(strlen($_GET['Adm_No']) > 0) $Adm_No = $_GET['Adm_No'];
        else  $Adm_No = Trim($_POST['Adm_No']);

        if(strlen($_GET['StudentName']) > 0) 
		$StudentName = $_GET['StudentName'];
        else  $StudentName = Trim($_POST['StudentName']);
        
        if(strlen($_GET['Status']) > 0) $Status = $_GET['Status'];
        else  $Status = Trim($_POST['Status']);
        $errmsg = "";
        $errflag = 0;
        $dummy = 0;
        if($_POST['Search'] == 'Search' or $Loc_Code > 0 or $Class_Code > 0 or $Sec_Code >
            0 or strlen($Adm_No) > 0) {
            $Dummy = ZeroCheck($Loc_Code, $errmsg, $errflag, "Location");
            if($errflag == 1) echo "<p class='error'>Incomplete / Invalid entries for<br>" .
                    $errmsg;
            }
            if($_POST['Reset'] == 'Reset') {
                $Loc_Code = $Class_Code = $Sec_Code = $Adm_No = $Status = $StudentName="";
            } ?>
<body>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr><td><?php titleheader("Search Student", 0); ?></td></tr>
	<tr><td>
	<form id="myform" name="myform" method="post" action="studentsearch.php">
		<div id="scholarshipfr" align="left" style="width:99%;">
			<table width="98%" border="0" cellspacing="2" cellpadding="0" align="center">
		<tr>
		 <td width="9%" align="left">Location <?php echo $mand; ?></td>
		 <?php //SHOW Location  DROPDOWN
				  mssql_free_result($result);
				  $query = mssql_init('[sp_selectschoolname]', $mssql);
				  mssql_bind($query, '@School_id', $_SESSION['SchoolId'], SQLINT4, false, false, 5);
				  $result = mssql_execute($query);
				  mssql_free_statement($query); ?>
		 <td width="20%"><select name="Loc_Code" id="Location" style="width:125px" >
		   <?php echo $Select; ?>
		   <?php while ($field = mssql_fetch_array($result)) { ?>
		   <option value="<?php echo $field['School_id'] ?>"
				  <?php if($Loc_Code == $field['School_id']) echo "Selected"; ?>> <?php echo $field['SchoolName'] ?> </option>
		   <?php } ?>
		  </select>
		 </td>
		 <td width="8%"  align="right">Class</td>
		 <?php //SHOW Class  DROPDOWN
				  mssql_free_result($result);
					$query = mssql_init('[sp_GetClass]', $mssql);
					$result = mssql_execute($query);
					mssql_free_statement($query); ?>
		 <td width="6%"><select name="Class_Code" id="Class">
		   <?php echo $Select; ?>
		   <?php while ($field = mssql_fetch_array($result)) { ?>
		   <option value="<?php echo $field['Class_Code'] ?>" 
					<?php if($Class_Code == $field['Class_Code']) echo "Selected"; ?>> <?php echo $field['Class'] ?> </option>
		   <?php } ?>
		  </select></td>
		 <td width="6%" align="right">Section</td>
		 <?php //SHOW CITY DROPDOWN
					  mssql_free_result($result_section);
						$query_section = mssql_init('sp_GetSection', $mssql);
						$result_section = mssql_execute($query_section);
						mssql_free_statement($query_section); ?>
		 <td width="10%"><select name="Sec_Code" id="Section" >
		   <?php echo $Select; ?>
		   <?php while ($field = mssql_fetch_array($result_section)) { ?>
		   <option value="<?php echo $field['Section_Code'] ?>" 
					  <?php if($Sec_Code == $field['Section_Code']) echo "Selected"; ?>> <?php echo $field['Section'] ?> </option>
		   <?php  } ?>
		  </select></td>
		 <td width="10%" align="right">Admission No.</td>
		 <td width="31%" ><input type="text" name="Adm_No" id="Admission_No" maxlength="15" value="<?php echo $Adm_No ?>" /></td>
		</tr>
		<tr>
		 <td> StudentName</td>
		 <td><input type="text" name="StudentName" id="StudentName" size="20" onkeydown="return alphaonly('StudentName')"value="<?php echo $StudentName ?>" /></td>
		 <td width="4%" align="right">Status</td>
		 <td width="16%" ><select name="Status" id="Status">
		   <option value="1" <?php if($Status==1)echo "selected"; ?>>Existing</option>
		   <option value="2" <?php if($Status==2)echo "selected"; ?>>Passout</option>
		   <option value="3" <?php if($Status==3)echo "selected"; ?>>In-Process</option>
			<option value="4" <?php if($Status==4)echo "selected"; ?>>Disjoined</option>
			<option value="5" <?php if($Status==5)echo "selected"; ?>>Discontinue</option>
			<option value="6" <?php if($Status == 6) echo "selected"; ?>>Transfer Certificate</option>
		  </select></td>
		</tr>
		<tr><td colspan="11"><table align="right">
		   <tr>
			<td ><input type="submit" name="Search" value="Search" class="winbutton_go"/></td>
			<td><input type="submit" name="Reset" value="Reset" class="winbutton_go"/></td>
		   </tr>
		  </table></td></tr>
	   </table>
	   </div>
	   	<table width="100%" border="0" cellspacing="1" cellpadding="3" id="example">
		<thead>
  		<th>S.No</th>
		 <th>Admission No.</th>
         <th>EMIS No</th>
		 <th>StudentName</th>
         <th>Gender</th>
         <th>Employee<br/>Children</th>
		 <th>Apply<br/>Scholarship</th>
		 <th>Edit Forms</th>
		 <th>Scholarship<br/>Sanctioned</th>
         <th>Subsidy <br/>from School</th>
		 <th>Student <br/>Photo</th>
		 <th>Profile <br/>Photo</th>
		 <th>Thank <br/>Photo</th>
		 <th>Family <br/>Photo</th>
		 </thead>
		<tbody>
		 <?php     // UPDATE & SHOW RECORDS
	if($errflag == 0 and ($_POST['Search'] == 'Search' or $_POST['Search'] ==
		'Search' or $Loc_Code > 0 or $Class_Code > 0 or $Sec_Code > 0 or strlen($Adm_No) >
		0)) {
		mssql_free_result($Tresult);
		$query = mssql_init('sp_studentview', $mssql);
		mssql_bind($query, '@School_id', $Loc_Code, SQLINT4, false, false, 5);
		mssql_bind($query, '@Section_Code', $Sec_Code, SQLINT4, false, false, 5);
		mssql_bind($query, '@Class_Code', $Class_Code, SQLINT4, false, false, 5);
	//	if(strlen($Adm_No) == 0) $Adm_No = 0;
		mssql_bind($query, '@Admission_No', $Adm_No, SQLVARCHAR, false, false, 50);
		mssql_bind($query, '@StudentName',$StudentName, SQLVARCHAR, false, false, 25);
		mssql_bind($query, '@Status', $Status, SQLINT4, false, false, 5);
		$Tresult = @mssql_execute($query);
		echo mssql_get_last_message();
		mssql_free_statement($query);
		$rs_cnt = mssql_num_rows($Tresult);
		if($rs_cnt > 0) {
		$colorflag = 0;
		$i = 0;
		while($field = mssql_fetch_array($Tresult)) {
			$Class_Code1=$field['Class_Code'];
			$Child_Eligible=$field['Child_Eligible'];
			$C_Date=$field['Created_Date'];
			if(strlen($C_Date)>0) $Created_Date=date('d-m-Y', strtotime($C_Date));
			
			$i += 1;
			$colorflag += 1;
			$tot_rec = $i; ?>
		 <tr class=<?php if($colorflag % 2 == 0) { echo "row1";} else { echo "row2";} ?> valign="center">
		 <td align="center"><?php echo $i ?> </td>
		 <td><?php echo $field['Admission_No'] ?> </td>
         <td><?php echo $field['EMIS_No'] ?> </td>
		 <td><?php echo $field['Name'] ?> </td>
         <td align="center"><?php if($field['Gender'] =='M') echo "Male"; else  echo "Female"; ?>            </td>
           <td align="center"><?php if($field['Employee_Child'] =='Y') echo "Yes"; 							else  echo "No"; ?></td>          
		  <td align="center"><?php if($field['Scholarship'] =='Y') echo "Yes"; 							else  echo "No"; ?></td>
		  <td align="left">
			  <table cellpadding="5" cellspacing="0" bordercolor="#FFFFFF" style="border:solid 1px #FFFFFF; border-collapse:collapse; margin:5px;" border="1"><tr><td>
			  <a href="studentinfo.php?Stu_Id=<?php  echo $field['Student_Id'] ?>&Loc_Code=<?php echo $Loc_Code ?>&Class_Code=<?php echo $Class_Code ?>&Sec_Code=<?php echo $Sec_Code ?>&Adm_No=<?php echo $Adm_No ?>&SName=<?php echo $StudentName ?>&Status=<?php echo $Status ?>"> <img src="../images/edit.gif" title="Admission"   /></a>&nbsp;
			  </td><td><?php if($field['Scholarship'] == 'Y') { ?>
			   <a href="scholarship.php?Stu_Id=<?php echo $field['Student_Id'] ?>&Loc_Code=<?php echo $Loc_Code ?>&Class_Code=<?php echo $Class_Code ?>&Sec_Code=<?php echo $Sec_Code ?>&Adm_No=<?php echo $Adm_No ?>&SName=<?php echo $StudentName ?>&Status=<?php echo $Status ?>"> <img src="../images/edit.gif" title="Scholarship"/></a>
			   <?php                 }
									if($field['Scholarship'] == 'Y') { ?>
			   &nbsp;&nbsp;<span class=<?php if($field['Parents'] > 0) echo "fill";
										else  echo "notfill" ?> >P</span>&nbsp; <span class=<?php if($field['Volunteer'] > 0) echo "fill";
											else  echo "notfill" ?>>V</span>
			   <?php                 }  else echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";?>
			   </td><td><a href="newgr.php?Stu_Id=<?php  echo $field['Student_Id'] ?>&Loc_Code=<?php echo $Loc_Code ?>&Class_Code=<?php echo $Class_Code1 ?>&Sec_Code=<?php echo $Sec_Code ?>&Adm_No=<?php echo $Adm_No ?>&mtype=3"> <img src="../images/edit.gif" title="Reportcard"/></a>
			  </td><td><a href="sms_activity.php?Stu_Id=<?php echo $field['Student_Id'] ?>&SName=<?php  echo $StudentName; ?>&Adm_No=<?php echo $field['Admission_No'] ?>&Loc_Code=<?php echo $Loc_Code ?>&Class_Code=<?php echo $Class_Code ?>&Sec_Code=<?php echo $Sec_Code ?>&Status=<?php echo $Status; ?>&Class=<?php echo $field['Class']; ?>&stu_Name=<?php echo $field['Name']; ?>"><img src="../images/edit.gif" title="Activity" /></a></td>
			  </tr>
		  </table>
		  </td>
		  <td align="center"><?php if($Child_Eligible=='Y'){ echo "Yes";?> <br> <?php echo $Created_Date; }  elseif($Child_Eligible=='N')  echo "No"; else echo " ";  ?> </td>
          <td><?php echo $field['School_Subsidy']; ?></td>
		  <?php $studid=$field['Student_Id'];				
				 $profilephoto ='../photos/'.$studid.'/'.$studid.'.jpg';
				 $studphoto ='../photos/'.$studid.'/student.jpg';
				 $thankphoto ='../photos/'.$studid.'/Thank.jpg';
				 $familyphoto='../photos/'.$studid.'/Family_1.jpg';				  ?>
		  <td align="center"><?php if((file_exists($studphoto))){echo '<img src="../images/Yes.png" height="14px" width="14px"/>'; }
						   else {echo '<img src="../images/Delete.png" height="14px" width="14px" border="0"/>';}?></td>
          <td align="center"><?php if((file_exists($profilephoto))){echo '<img src="../images/Yes.png" height="14px" width="14px"/>'; }
						   else {echo '<img src="../images/Delete.png" height="14px" width="14px" border="0"/>';}?></td>
		  <td align="center"><?php if((file_exists($thankphoto))){echo '<img src="../images/Yes.png" height="14px" width="14px"/>'; }
						   else {echo '<img src="../images/Delete.png" height="14px" width="14px" border="0"/>';}?></td>
		  <td align="center"><?php if((file_exists($familyphoto))){echo '<img src="../images/Yes.png" height="14px" width="14px"/>'; }
						   else {echo '<img src="../images/Delete.png" height="14px" width="14px" border="0"/>';}?></td>
		 </tr>
		 <?php }	} 
		else { ?>
		 <tr><td colspan="11" align="center" class="error">No Records Found...</td></tr>
		 <?php } } ?>
		<input type="hidden" name="tot_rec" value="<?php echo $tot_rec ?>" />
		<?php     if($errcnt > 0) {
				  for ($i = 0; $i <= $errcnt - 1; $i = $i + 1) {
					$editcnt = split(',', $_POST['editcnt']); ?>
		<script>editdata(<?php    echo $editcnt[$i]; ?>);</script>
		<?php     } } ?>
		</tbody>
	   </table>
  	</form>

	</td></tr>
</body>
</html>
<?php include ("../includes/copyright.php"); ?>