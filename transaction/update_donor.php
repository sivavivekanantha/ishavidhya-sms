<?php session_start(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php include("../includes/header.php");
  title('Student Management','Update Donor Mapping',2,1,2);	?>
<script>
$(document).ready(function(){
    $('#scrollbar1').tinyscrollbar();	
});

function editdata(val)
{ 
    if($('#editcnt').val() >0 ) return false;
    $('#editcnt').val(val);
	$('#country_name_'+val).show();
	$('#short_name_'+val).show();
	$('#phone_code_'+val).show();
	$('#active_'+val).show();
	$('#disporder_'+val).show();	
	var $mode ='EDIT';
	$('#mode').val($mode);
}

</script>
</head>
<?php   //VARIABLE DECLARATION
			$errmsg="";
			$errflag=0;
			$dummy=0;
			// GET MODE VALUE FOR WHICH FUNCTION PERFORMED (ADD / EDIT/ DELETE)
			$mode		=	$_POST['mode'];
			$editcnt 	=	$_POST['editcnt'];		// ROW ID ( SERIAL NO) IS GETTING
			//RESET FORM
			if($_POST['Cancel']=="Cancel") { $mode=""; $mode1="";	}
			         //RECORD EDIT PART
			If($mode == "EDIT")
			{ 
			
					//EDIT ROW VALUES ARE GETTING
			$country_code	=	$_POST['country_code_'.$editcnt];   
			$country_name	=	TRIM($_POST['country_name_'.$editcnt]);
			$short_name 	=	TRIM($_POST['short_name_'.$editcnt]);
			$phone_code		=	TRIM($_POST['phone_code_'.$editcnt]);
			$active			=	$_POST['active_'.$editcnt];
			$disporder		=	TRIM($_POST['disporder_'.$editcnt]);
			//VALIDATE THE INPUTS
			$dummy = Strcheck($country_name,$errmsg,$errflag,"Country");
			$dummy = Strcheck($short_name,$errmsg,$errflag,"Short Name");
			$dummy = Numcheck($phone_code,$errmsg,$errflag,"Phone Code");
			
			//ALL INPUTS ARE CORRECTED, THEN GOTO SP
			if($errflag==0)
			{
			 mssql_free_result($result);
			$query = mssql_init('sp_Country',$mssql);
			mssql_bind($query,'@Country_Code',$country_code,SQLINT4,false,false,5);
			mssql_bind($query,'@Country_Name',$country_name,SQLVARCHAR,false,false,50);
			mssql_bind($query,'@Short_Name',$short_name,SQLVARCHAR,false,false,25);
			mssql_bind($query,'@Phone_Code',$phone_code,SQLVARCHAR,false,false,5);
			mssql_bind($query,'@Active',$active,SQLVARCHAR,false,false,1);
			mssql_bind($query,'@Disp_Order',$disporder,SQLINT4,false,false,5);
			mssql_bind($query,'@Action',$action,SQLINT4,false,false,1);
			$result = @mssql_execute($query);    // SP EXECUTE HERE
			mssql_free_statement($query);        // FREE THE $QUERY VARIABLE
            //SP EXECUTE SUCCESSFULLY
			if($result==1)
				echo "<p class='mesg'>Country has been Updated</p>";
			else 
            {
				$errmsg1=mssql_get_last_message();          //  ERROR MESSAGE GET FROM SP
				$errflag=2;
    			If ($errcnt == 0) { $errcnt = 1;} else { $errcnt = $errcnt + 1; }
    			if ($errval == "") { $errval = $editcnt[$i]; } 
			}
			}
			//INPUT VALUES ARE WRONG
			else 
			{
				If ($errcnt == 0) { $errcnt = 1;} else { $errcnt = $errcnt + 1; }
				if ($errval == "") { $errval = $editcnt[$i]; } 
			}
		}	

    else $inserr=1;
	}	  if($errflag==1) 
		  echo $errlbl.$errmsg."</p>";
		  if($errflag==2) 
		  echo "<p class='error'>".$errmsg1;        ?>
		  
		  <body style="margin:0;">
		  <form name="myform" id="myform" method="post" action="update_donor.php">
		  <?PHP //HIDDEN VALUES   ?>
		  <input type="hidden" name="editcnt" id="editcnt"/>
		  <input type="hidden" name="country_code" id="country_code"/>
		  <input type="hidden" name="mode" id="mode"/>	
		
		  <table width="100%" height="450" border="0" align="center" cellpadding="3" cellspacing="3">
		  <tr><td>
          <table width="65%" border="0" align="center" cellpadding="3" cellspacing="1">
          <?php titleheader(UpdateDonorMapping,0); ?>
          <tr><td>
          <thead><colgroup><col width=3%><col width=3%><col width=10%><col width=10%><col width=10%><col width=5%><col width=5%></colgroup>
          <th>&nbsp;</th><th>&nbsp;</th><th>Country</th><th>Short Name</th><th>Phone Code</th><th>Active</th><th>Display Order</th></thead>
            
          <?php   // New Record Insert
           $colorflag+=1; ?>
            <!-- NEW ICON-->
              	
          <?php 	// UPDATE & SHOW RECORDS
            mssql_free_result($result);
    		$query = mssql_init('sp_GetCountry',$mssql);
    		$result = mssql_execute($query);
    		mssql_free_statement($query);
    		$rs_cnt = mssql_num_rows($result);
    		$colorflag = 0; $i=0;
    		while($field = mssql_fetch_array($result))
    		{	$colorflag+=1; $i=$i+1;
    			$country_code = $field['Country_Code'];
    			$country_name = $field['Country'];
    			$short_name = $field['Short_name'];
    			$phone_code = $field['Phone_Code'];
    			$active = strtoupper($field['Active']);
    			$disp_order = $field['Display_Order'];       ?>
          <tr class=<?php if($colorflag%2==0) { echo "row1"; } else { echo "row2"; } ?>>
          <input type="hidden" name="country_code_<?php echo $i ?>" id ="country_code_<?php echo $i ?>" 
          value="<?php echo $country_code?>"	 />
            <!-- DELETE ICON-->
          <td align="center"><img src="../images/delete_d.gif" title="Remove" onclick="deldata('<?php echo $field['Country_Code'];?>');" />&nbsp;</td>	 
            <!-- EDIT ICON-->
          <td align="center"><img src="../images/edit.gif" title="Edit" onclick="editdata('<?php echo $i;?>');" />&nbsp;</td>
            <!-- COUNTRY-->
          <td>
		  <?php echo $field['Country'] ?>
    	  <div id ="country_name_<?php echo $i;?>" style="display:none;">
          <input type="text" name="country_name_<?php echo $i ?>" id="country_name_<?php echo $i ?>"  size="25" maxlength="50" value="<?php echo $country_name; ?>"  onkeydown="return alphaonly('country_name_<?php echo $i ?>')">
          </div>
          </td>
            <!-- SHORT NAME-->
          <td align="center"><?php echo $field['Short_name'] ?>
    	  <div id ="short_name_<?php echo $i;?>" style="display:none;">
          <input type="text" name="short_name_<?php echo $i ?>" id="short_name_<?php echo $i ?>"  size="5" maxlength="10" value="<?php echo $short_name; ?>"
    	  onkeydown="return alphaonly('short_name_<?php echo $i ?>')"/>
          </div>
          </td>
            <!-- PHONE CODE-->		
          <td align="center"><?php echo $field['Phone_Code'] ?>
    	  <div id ="phone_code_<?php echo $i ?>" style="display:none;"><input type="text" 
          name="phone_code_<?php echo $i ?>" id="phone_code_<?php echo $i ?>"  size="5" 
          maxlength="10" value="<?php echo $phone_code; ?>"  
          onkeydown="return phonecode('phone_code_<?php echo $i ?>')"/>
          </div>
          </td>
            <!-- ACTIVE-->
          <td align="center"><?php if(strtoupper($field['Active'])=="Y") echo "Yes"; else echo "No";?>
          <div id ="active_<?php echo $i;?>" style="display:none;">
          <select id="active_<?php echo $i ?>" name="active_<?php echo $i ?>">
          <option value="Y" <?php if($active=="Y")  echo "selected" ?>>Yes</option>
          <option value="N" <?php if($active=="N")  echo "selected" ?>>No</option>
          </select>
          </div>
          </td>
            <!-- DISPLAY ORDER-->
          <td align="center"><?php echo $field['Display_Order'] ?>
          <div id ="disporder_<?php echo $i;?>" style="display:none;">
          <input type="text" name="disporder_<?php echo $i ?>" id="disporder_<?php echo $i ?>"  size="5" maxlength="5" value="<?php echo $disp_order; ?>" onkeydown="return numberonly('disporder_<?php echo $i ?>')">
          </div>
          </td>
          </tr>
          <?php } 
          //IF ANY ERROR IN EDIT, SHOW THAT ROW
          if ($errcnt > 0) {  ?>
                <script>editdata(<?php echo $editcnt;?>);</script>
    	  <?php }  ?>
          <!-- bUTTONS-->	
          <tr>
          <td colspan="7" align="right">
          <input type="submit" name="save" value="Save" class="winbutton_go" />&nbsp;&nbsp;&nbsp;
          <input type="submit" name="Cancel" value="Cancel" class="winbutton_go"/>
          </td>
          </tr>
          <?php	//IF ANY ERROR IN ADD, SHOW THAT ROW
            if($inserr == 1) { ?><script>adddata();</script><?php } ?>
          </table>    
		  </td></tr></table>
		  </form>
		  <?php include("../includes/copyright.php"); ?>
		  </body>
		  </html>