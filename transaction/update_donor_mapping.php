<?php session_start(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>Isha Foundation - A Non-profit Organization</title>
<?php include("../includes/header.php");
 title('Student Management','Update Donor Mapping',2,1,2);	?>
<script>
$(document).ready(function() {
    $('#example').dataTable( {
        "sScrollY": "300",
        "bPaginate": false,
		"bSort": false
        //"bStateSave":true
		 
               
    } );
} );	


function editdata(val)
{  
//	if($('#editcnt').val() >0 ) return false;
    $('#editcnt').val(val);
	$('#admission_no_'+val).show();
	$('#name_'+val).show();
	$('#title_'+val).show();
	$('#donor_code_'+val).show();
	$('#first_name_'+val).show();
	$('#middle_name_'+val).show();
	$('#last_name_'+val).show();
	$('#email_id_'+val).show();
	$('#allocation_mail_'+val).show();
	$('#report_card_'+val).show();
	$('#renewal_mail_'+val).show();
	$('#renew_date_'+val).show();
	$('#donation_type_'+val).show();
	$('#showbutton_'+val).show();
		
	var $mode ='EDIT';
	$('#mode').val($mode);
}
</script>
</head>
<?php		$errmsg="";
			$errflag=0;
			$dummy=0;
			 $mode		=	$_POST['mode'];
			 $editcnt 	=	$_POST['editcnt'];
			if($_POST['Cancel']=="Cancel") $mode="";
			
			
			if($_POST['Search'] == 'Search' or strlen($Adm_No) > 0 and $errflag == 0 ) {
			$Adm_No = Trim($_POST['Adm_No']);
			$First_Name= Trim($_POST['First_Name']);
			$Email_Id=Trim($_POST['Email_Id']);
      		if(strlen($Adm_No)==0 and  strlen($Email_Id)==0 and strlen($First_Name)==0 )
			{
					$errflag=1;  $errmsg="Please fill any one";
			}
            if($errflag == 1) echo "<p class='error'>".$errmsg; }
            if($_POST['Reset'] == 'Reset') {
                $Adm_No ="";
				$First_Name="";
				$Email_Id="";
            } 
			$Adm_No = Trim($_POST['Adm_No']);
							$First_Name= Trim($_POST['First_Name']);
							$Email_Id=Trim($_POST['Email_Id']);
			 
			if($_POST['Save']=="Save" and $editcnt>0)
			{
			
			                $Adm_No = Trim($_POST['Adm_No']);
							$First_Name= Trim($_POST['First_Name']);
							$Email_Id=Trim($_POST['Email_Id']);
						    $map_id             =   Trim($_POST['map_id_'.$editcnt]);
						  	$admission_no	    =	Trim($_POST['admission_no_'.$editcnt]);
                            $name		    	=	Trim($_POST['name_'.$editcnt]);
                            $title		    	=	Trim($_POST['title_'.$editcnt]);
                            $donor_code	        =	Trim($_POST['donor_code_'.$editcnt]);
                            $title				=	Trim($_POST['title_'.$editcnt]);
                            $first_name	        =	Trim($_POST['first_name_'.$editcnt]);
                            $middle_name		=	Trim($_POST['middle_name_'.$editcnt]);
                            $last_name	        =	Trim($_POST['last_name_'.$editcnt]);
                            $email_id		   	=	Trim($_POST['email_id_'.$editcnt]);
                          	 $donation_type	    =	Trim($_POST['donation_type_'.$editcnt]);
                                                      
							$j=$i+1;
							
							$dummy = Strcheck($first_name,$errmsg,$errflag,"First Name");
							$dummy = Strcheck($email_id,$errmsg,$errflag,"Email Id");
							$dummy = Strcheck($donation_type,$errmsg,$errflag,"Donation Type");
							if($errflag==0)
							{	
															
								mssql_free_result($result);                            
								$query = mssql_init('sp_DonorMappingView',$mssql);
									mssql_bind($query,'@Map_Id',$map_id,SQLINT4,false,false,5);
									mssql_bind($query,'@Admission_No',$admission_no,SQLVARCHAR,false,false,50);
									mssql_bind($query,'@Name',$name,SQLVARCHAR,false,false,50);
									mssql_bind($query,'@Donor_Code',$donor_code,SQLVARCHAR,false,false,20);
									mssql_bind($query,'@Title',$title,SQLVARCHAR,false,false,10);
									mssql_bind($query,'@First_Name',$first_name,SQLVARCHAR,false,false,50);
									mssql_bind($query,'@Middle_Name',$middle_name,SQLVARCHAR,false,false,50);
									mssql_bind($query,'@Last_Name',$last_name,SQLVARCHAR,false,false,50);
									mssql_bind($query,'@Email_Id',$email_id,SQLVARCHAR,false,false,100);
									//mssql_bind($query,'@Allocation_Mail',$allocation_mail,SQLVARCHAR,false,false,1);
									//mssql_bind($query,'@Report_Card',$report_card,SQLVARCHAR,false,false,1);
									//mssql_bind($query,'@Renewal_Mail',$renewal_mail,SQLVARCHAR,false,false,1);
									//mssql_bind($query,'@Renew_Date',$renew_date,SQLVARCHAR,false,false,50);
									mssql_bind($query,'@Donation_Type',$donation_type,SQLINT4,false,false,5);
									$result = @mssql_execute($query);
									mssql_free_statement($query);
									if($result)
									{
										echo "<p class='mesg'>Record has been Updated</p>";
									}
									else 
									{
										$errmsg1=mssql_get_last_message();
										$errflag=2;
									}
							}
							else
							{	$errcnt=1;
								if($errflag==1) echo $errlbl.$errmsg."</p>";
		  						if($errflag==2)	echo "<p class='error'>".$errmsg1;      								
							}
					
			}

?>
<body style="margin:0;">
        <form name="myform" id="myform" method="post" action="update_donor_mapping.php">
        <input type="hidden" name="editcnt" id="editcnt"/>
        <input type="hidden" name="map_id" id="map_id"/>
       <input type="hidden" name="mode" id="mode"/>
	  
<table width="80%" align="center" style=" border:#999999 solid 1px">
<tr>
		<td >Admission No.</td>
		 <td  ><input type="text" name="Adm_No" id="Admission_No" maxlength="25" value="<?php echo $Adm_No ?>" /></td>
         
         <td >Email Id</td>
		 <td  ><input type="text" name="Email_Id" id="Email_Id" maxlength="100" value="<?php echo $Email_Id ?>" /></td>

	<td >First Name</td>
		 <td  ><input type="text" name="First_Name" id="First_Name" maxlength="100" value="<?php echo $First_Name ?>" /></td>
         
		 <td><input type="submit" name="Search" value="Search" /> <input type="submit" name="Reset" value="Reset" /></td>
		
</tr>
</table>
         <table width="95%" border="0" align="center" cellpadding="1" cellspacing="1" id="example" style=" border:#999999 solid 1px">
                <thead>
                <th>S.No</th>
				<th>&nbsp;</th>
				<th align="center">Admission <br /> Number</th>
                <th align="center">Student<br /> Name</th>
				<th align="center">Donor Code</th>
				<th align="center">Title</th>
				<th align="center">First Name</th>
				<th align="center"> Middle Name</th>
                <th align="center">Last Name</th>
				<th align="center">Email Id</th>
				<th align="center">Donation Type</th>
                </thead>
        	   <tbody>
				        	        
				<?php
				if(($_POST['Search'] == 'Search' and $errflag==0) or($_POST['Save'] == 'Save')) 
				{ 
				 	//UPDATE & SHOW RECORDS
                        mssql_free_result($result);
                        $query = mssql_init('sp_GetDonorMappingView',$mssql);
						mssql_bind($query, '@Admission_No', $Adm_No, SQLVARCHAR, false, false, 50);
						mssql_bind($query, '@Email_Id', $Email_Id, SQLVARCHAR, false, false, 100);
						mssql_bind($query, '@First_Name', $First_Name, SQLVARCHAR, false, false, 100);
                        $result = @mssql_execute($query);
						//echo mssql_get_last_message();
               			mssql_free_statement($query);
               			$rs_cnt = mssql_num_rows($result);
               			$colorflag = 0;
                		$i = 0;
                		while($field = mssql_fetch_array($result))
                		{	
                		$i  +=1;	$colorflag+=1;
                    	$tot_rec = $i;	?>
                  
                    <tr class=<?php if($colorflag%2==0) { echo "row1"; } else { echo "row2"; } ?> valign="center">
                    <input type="hidden" name="map_id_<?php echo $i ?>" id ="map_id_<?php echo $i ?>" value="<?php echo $field['Map_Id']?>"/>
                    <td ><?php echo $i;?></td>
                   <td align="center"><img src="../images/edit.gif" title="Edit" onclick="editdata('<?php echo $i;?>');" />&nbsp;</td>
                   <td ><?php echo $field['Admission_No'] ?></td>
                   <td ><?php echo $field['Name'] ?></td>
                   <td ><?php echo $field['Donor_Code'] ?>
                    <div id ="donor_code_<?php echo $i;?>" style="display:none;"><input type="text" name="donor_code_<?php echo $i ?>" id="donor_code_<?php echo $i ?>"  size="6" maxlength="25" value="<?php echo $field['Donor_Code']?>"></div></td>
                   <td ><?php echo $field['Title'] ?>
                    <div id ="title_<?php echo $i;?>" style="display:none;"><input type="text" name="title_<?php echo $i ?>" id="title_<?php echo $i ?>"  size="5" maxlength="5" value="<?php echo $field['Title']?>"></div></td>
                   <td ><?php echo $field['First_Name'] ?>
                    <div id ="first_name_<?php echo $i;?>" style="display:none;"><input type="text" name="first_name_<?php echo $i ?>" id="first_name_<?php echo $i ?>"  size="13" maxlength="100" value="<?php echo $field['First_Name']?>"></div></td>
                   <td ><?php echo $field['Middle_Name'] ?>
                    <div id ="middle_name_<?php echo $i;?>" style="display:none;"><input type="text" name="middle_name_<?php echo $i ?>" id="middle_name_<?php echo $i ?>"  size="7" maxlength="15" value="<?php echo $field['Middle_Name']?>"></div></td>
                   <td ><?php echo $field['Last_Name'] ?>
                    <div id ="last_name_<?php echo $i;?>" style="display:none;"><input type="text" name="last_name_<?php echo $i ?>" id="last_name_<?php echo $i ?>"  size="7" maxlength="25" value="<?php echo $field['Last_Name']?>"></div></td>
                   <td ><?php echo $field['Email_id'] ?>
                    <div id ="email_id_<?php echo $i;?>" style="display:none;"><input type="text" name="email_id_<?php echo $i ?>" id="email_id_<?php echo $i ?>"  size="25" maxlength="50" value="<?php echo $field['Email_id']?>"></div></td>
                   
			       <?php 	
		           mssql_free_result($result1);
				  $query = mssql_init('sp_GetDonation',$mssql);
				  $result1 = mssql_execute($query);
				  mssql_free_statement($query);	?>
			      <td align="center"><?php echo $field['Donation'] ?>
			      <div id="donation_type_<?php echo $i ?>" style="display:none;">
			      <select name="donation_type_<?php echo $i ?>" id="donation_type_<?php echo $i ?>" style="width:50px">
			      
		          <?php   while($field1 = mssql_fetch_array($result1)) {  ?>
			      <option value="<?php echo $field1['Donation_Code']?>"  <?php if($field['Donation_Type']==$field1['Donation_Code']) echo "Selected";?> >
			      <?php echo $field1['Donation']?></option>
				  <?php } ?>
				  </select></div>
				  </td> 
	              </tr>
							
                <?php 
                }
				
				} 
				?>
				 
				<?php
                if ($errcnt > 0) 
                {
                    for($i=0;$i<=1;$i=$i+1){
                    $editcnt = split(',',$_POST['editcnt']);?>
                    <script>editdata(<?php echo $editcnt[$i];?>);</script>
                <?php 
                   } 
			   } ?>
			   </tbody>
               
               </table>
                <input type="hidden" name="tot_rec" value="<?php echo $tot_rec ?>" />
                <?php if(($_POST['Search'] == 'Search') or $_POST['Save']=="Save") { ?>
				 <table >
				 <tr><td  align="right">
                <input type="submit" name="Save" value="Save" class="winbutton_go" />&nbsp;&nbsp;&nbsp;
                <input type="submit" name="Cancel" value="Cancel" class="winbutton_go"/></td></tr> 
                </table><?php } ?>

        </form>
 </body></html>
<?php include("../Includes/copyright.php"); ?>