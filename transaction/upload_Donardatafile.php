<?php session_start(); 
  set_time_limit(2000);
 ob_start(); ?>
 <html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="ta">
<?php
    include("../includes/header.php"); 
	 title('Student Management','Upload_Reportcard',2,1,2); 
	include ("Spreadsheet/Excel/reader.php");
	$x=2; ?>
<table width="60%" style="margin-left:230px; border:dashed 1px; background-color:	#F9F9F9;">
<tr><td><?php titleheader("Upload Donor",0)?></td></tr>
<tr><td>
<?php
	$errmsg="";
	$errflag=0;
	$dummy=0;

if ($_FILES["file"]["error"] > 0)
  {
	  echo "Error: " . $_FILES["file"]["error"] . "<br />";
  }
else
  {

  echo "<p class='cms_li'>Upload File name : " . $_FILES["file"]["name"] . "<br />";
  echo "File Type: " . $_FILES["file"]["type"] . "<br />";
  echo "File Size: " . ($_FILES["file"]["size"] / 1024) . " Kb<br />";
  echo "Stored in: " . $_FILES["file"]["tmp_name"];

	$filename = $_FILES["file"]["tmp_name"];
	$excel = new Spreadsheet_Excel_Reader();
	$excel->setUTFEncoder('iconv');
	$excel->setOutputEncoding('UTF-8');
	$excel->read($filename);

	$rowcount =$excel->sheets[0]['numRows']; // counting number of rows.
	$colcount =$excel->sheets[0]['numCols']; // counting number of cols.
	}
while($x<=$rowcount)
{
$AdmnNo = $excel->sheets[0]['cells'][$x][1];
$Donor_Code= $excel->sheets[0]['cells'][$x][2];
$Salutation = $excel->sheets[0]['cells'][$x][3];
$First_Name = $excel->sheets[0]['cells'][$x][4];
$Middle_Name = strtoupper($excel->sheets[0]['cells'][$x][5]);
$Last_Name = $excel->sheets[0]['cells'][$x][6];
$Email_id = $excel->sheets[0]['cells'][$x][7];
$Donation_Type = $excel->sheets[0]['cells'][$x][8];	
$Renewal_Date = $excel->sheets[0]['cells'][$x][9];	
$Donar_Update = $excel->sheets[0]['cells'][$x][10];	

//$School_Subsidy = $excel->sheets[0]['cells'][$x][11];	

 $dummy = Strcheck($AdmnNo, $errmsg, $errflag, "Admission No");
 $dummy = Strcheck($First_Name, $errmsg, $errflag, "First Name");
 if(strstr($Email_id,',')) {
  $Mailid = split(",", $Email_id);
  for ($mailcnt = 0; $mailcnt < sizeof($Mailid); $mailcnt++)
	{
	 $dummy = Emailcheck(TRIM($Mailid[$mailcnt]), $errmsg, $errflag, "Email id-".$mailcnt);
	}
  }
  else
  {		
		$dummy = Emailcheck($Email_id, $errmsg, $errflag, "Email id");
  } 
 //$dummy = Emailcheck($Email_id, $errmsg, $errflag, "Email_id");
 $dummy = Strcheck($Donation_Type, $errmsg, $errflag, "Donation Type");
 if(strlen($Donar_Update)==0) $Donar_Update='N';
 if($errflag == 0) 
 {	

	mssql_free_result($result);
	$query1 = mssql_init('[sp_DonorExsave]',$mssql);			
	mssql_bind($query1,'@Map_Id',$Map_Id,SQLINT4,false,false,5);
	mssql_bind($query1,'@Admission_No',$AdmnNo,SQLVARCHAR,false,false,25);
	mssql_bind($query1,'@Donor_Code',$Donor_Code,SQLVARCHAR,false,false,50);
	mssql_bind($query1,'@Salutation',$Salutation,SQLVARCHAR,false,false,50);
  	mssql_bind($query1,'@First_Name',$First_Name,SQLVARCHAR,false,false,5000);
	mssql_bind($query1,'@Middle_Name',$Middle_Name,SQLVARCHAR,false,false,5000);
	mssql_bind($query1,'@Last_Name',$Last_Name,SQLVARCHAR,false,false,5000);
	mssql_bind($query1,'@Email_id',$Email_id,SQLVARCHAR,false,false,strlen($Email_id));
	mssql_bind($query1,'@Donation_Type',$Donation_Type,SQLVARCHAR,false,false,50);
	if(strlen($Renewal_Date)>0) $Renewal_Date = date('Y-m-d', strtotime($Renewal_Date));
	mssql_bind($query1,'@Renew_Date', $Renewal_Date, SQLVARCHAR, false, true,20);
	mssql_bind($query1,'@Donar_Update',$Donar_Update,SQLVARCHAR,false,false,50);
	//mssql_bind($query1,'@School_Subsidy',$School_Subsidy,SQLVARCHAR,false,false,50);
	$Dresult = @mssql_execute($query1);	
	mssql_free_statement($query1); 
	if($Dresult) echo "<br /><p class='mesg'>Row".$x."Saved Successfully !!</p>";
	else echo"<br/><p class='error'>Row".$x." : ".mssql_get_last_message();	 
 } else echo"<br/><p class='error'>Row".$x." : ".$errmsg." Should not be blank";	 
 	$errmsg=""; $errflag=0;
	$x++;
}	?>
	</td>
	</tr>
	</table>
</html>