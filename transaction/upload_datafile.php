<?php session_start(); 
  set_time_limit(2000);
 ob_start(); ?>
 <html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="ta">
<?  include("../includes/header.php"); 
include ("Spreadsheet/Excel/reader.php");


//require('../reports/PHPExcel.php'); // found when you download the PHPExcel
/*$file = $_FILES["file"]["tmp_name"];
$Reader = PHPExcel_IOFactory::createReaderForFile($file);
$Reader->setReadDataOnly(true); // set this, to not read all excel properties, just data

 $excel = $Reader->load($file);

//$value = $excel->getSheet(0)->getCell('A4')->getValue();
// or to get calculated value if there is a formula, etc
//$value = $excel->getSheet(0)->getCell('A4')->getCalculatedValue();

$period =  $excel->getSheet(0)->getCell('A4')->getValue();
$teacher = $excel->getSheet(0)->getCell('B4')->getValue();
$class = $excel->getSheet(0)->getCell('C4')->getValue();
$section = $excel->getSheet(0)->getCell('D4')->getValue();
$date = $excel->getSheet(0)->getCell('E4')->getValue();
$term = $excel->getSheet(0)->getCell('F4')->getValue());
$location = $excel->getSheet(0)->getCell('G4')->getValue();
$wdays = $excel->getSheet(0)->getCell('H4')->getValue();
*/



if ($_FILES["file"]["error"] > 0)
  {
	  echo "Error: " . $_FILES["file"]["error"] . "<br />";
  }
else
  {
  echo "<p class='cms_li'>Upload File name : " . $_FILES["file"]["name"] . "<br />";
  echo "File Type: " . $_FILES["file"]["type"] . "<br />";
  echo "File Size: " . ($_FILES["file"]["size"] / 1024) . " Kb<br />";
  echo "Stored in: " . $_FILES["file"]["tmp_name"];
	
	$filename = $_FILES["file"]["tmp_name"];
	$excel = new Spreadsheet_Excel_Reader();
	$excel->setUTFEncoder('iconv');
	$excel->setOutputEncoding('UTF-8');
	$excel->read($filename);

$rowcount =$excel->sheets[0]['numRows']; // counting number of rows.
$colcount =$excel->sheets[0]['numCols']; // counting number of cols.

}
$x=4;
$period = $excel->sheets[0]['cells'][$x][1];
$teacher = $excel->sheets[0]['cells'][$x][2];
$class = strtoupper($excel->sheets[0]['cells'][$x][3]);
$section = $excel->sheets[0]['cells'][$x][4];
$date = $excel->sheets[0]['cells'][$x][5];
$term = $excel->sheets[0]['cells'][$x][6];
$location = $excel->sheets[0]['cells'][$x][7];
$wdays = $excel->sheets[0]['cells'][$x][8];

	
    $query = mssql_init('sp_Assesment_Header_Excel',$mssql);			
	mssql_bind($query,'@Ass_Header_Code',$Ass_Header_Code,SQLINT4,false,false,5);
	mssql_bind($query,'@Term_Code1',$term,SQLVARCHAR,false,false,50);
  	mssql_bind($query,'@Class_Code1',$class,SQLVARCHAR,false,false,50);
	mssql_bind($query,'@Section_code1',$section,SQLVARCHAR,false,false,50);
  
	mssql_bind($query,'@Term_Date',$date,SQLVARCHAR,false,false,50);
	mssql_bind($query,'@Teacher_Code1',$teacher,SQLVARCHAR,false,false,50);
	mssql_bind($query,'@Working_Days',$wdays,SQLINT4,false,false,5);   
	mssql_bind($query,'@Period_Code1',$period,SQLVARCHAR,false,false,50);
	mssql_bind($query,'@School_Id',$location,SQLVARCHAR,false,false,50);	
    mssql_bind($query,'@Ass_Header_Code1',$Ass_Header_Code1,SQLINT4,True);
    mssql_bind($query,'@Class_Code2',$Class_Code2,SQLINT4,True);
    
	$result = @mssql_execute($query);
	if($result and $Ass_Header_Code1>0)
    {  
      mssql_free_statement($query);
    if ($rowcount > 1)
    { 
		if(strtoupper($class)=='LKG' or strtoupper($class)=='UKG') $cval=1;
		else $cval=3;
		if($cval==3 and $colcount == 26) $gr=1;
elseif($cval==1 and $colcount == 27) $gr=1; 
else $gr=0;	

if ( $gr==1)
{ 
    echo "<table>";
    $x=7; // this means first row
    $count=0;
    $participant_id='0';
    echo "<tr>";
    echo "<td>";
}
$x=7;
while($x<=$rowcount) // loop for rows
{ 
 	if($excel->sheets[0]['cells'][$x][1] >0)
 	{
	$y=5; // i am setting the column to 2 since, for each row, it starts from 2nd column.
while($y<$colcount+1) // loop for columns
{ 
    $cell_val=$excel->sheets[0]['cells'][$x][$y];
	$Student_Id =$excel->sheets[0]['cells'][$x][4];
   
    if($Class_Code2<3 and $y>4 and $y<28) 
	{ 	   
		if($y==5) $Hygiene = $cell_val;
        if($y==6) $Fine_Motor_Skills = $cell_val;
       	if($y==7) $Gross_Motor_Skills = $cell_val;
       	if($y==8) $Attention_Span = $cell_val;	
		if($y==9) $Enthusiam_Energy = $cell_val;
		if($y==10) $Participation = $cell_val;
		if($y==11) $Social_Interaction = $cell_val;
		if($y==12) $Dicipline = $cell_val;
		if($y==13) $Punctuality = $cell_val;
		if($y==14) $Noday_Attendance = $cell_val;
		if($y==15) $Remarks = $cell_val;
		//if($y==16) $CommentCode = $cell_val;
		if($y==17) $Eng_Comprehension = $cell_val;
		if($y==18) $Eng_Speaking = $cell_val;
		if($y==19) $Eng_Reading = $cell_val;
		if($y==20) $Eng_Writing = $cell_val;
		if($y==21) $Tam_Comprehension = $cell_val;
		if($y==22) $Tam_Speaking = $cell_val;
		if($y==23) $Tam_Reading = $cell_val;
		if($y==24) $Tam_Writing = $cell_val;
		if($y==25) $Mat_Comprehension = $cell_val;
		if($y==26) $Scn_Comprehension = $cell_val;
        if($y==27) $Dcode = $cell_val;
	} else {
		if($y==5) $Tamil_Classwrk  = $cell_val;
		if($y==6) $Tamil_Examgrade = $cell_val;
		if($y==7) $Tamil_Totalgrade = $cell_val;
		if($y==8) $English_Classwrk = $cell_val;
		if($y==9) $English_Examgrade = $cell_val;
		if($y==10) $English_Totalgrade = $cell_val;
		if($y==11) $Maths_Classwrk = $cell_val;
		if($y==12) $Maths_Examgrade = $cell_val;
		if($y==13) $Maths_Totalgrade = $cell_val;
		if($y==14) $Science_Classwrk = $cell_val;
		if($y==15) $Science_Examgrade = $cell_val;
		if($y==16) $Science_Totalgrade = $cell_val;        
		if($y==17) $Sscience_Classwrk = $cell_val;
      	if($y==18) $Sscience_Examgrade = $cell_val;      
		if($y==19) $Sscience_Totalgrade = $cell_val; 
		if($y==20) $Sports_Games = $cell_val; 
		if($y==21) $Art_Craft = $cell_val; 
		if($y==22) $Social_Interaction = $cell_val; 
		if($y==23) $Projects = $cell_val; 
		if($y==24) $Participation = $cell_val; 
		if($y==25) $Noday_Attendance = $cell_val; 
		if($y==26) $Remarks = $cell_val; 
	}
	$y++; 
    }    mssql_free_result($result);
    	$query = mssql_init('sp_Assesment_Detail',$mssql);
          If($Ass_Header_Code>0) $Ass_Header_Code1 =$Ass_Header_Code; 	
		  mssql_bind($query,'@Ass_Header_Code',$Ass_Header_Code1,SQLINT4,false,false,5);		
		  mssql_bind($query,'@Student_Id',$Student_Id,SQLINT4,false,false,5);		
		  mssql_bind($query,'@Spoken_English',$Spoken_English,SQLVARCHAR,false,false,50);
          mssql_bind($query,'@Writing_Neatness',$Writing_Neatness,SQLVARCHAR,false,false,50);
		  mssql_bind($query,'@Enthusiam_Energy',$Enthusiam_Energy,SQLVARCHAR,false,false,50);	
		  mssql_bind($query,'@Responsibilty',$Responsibilty,SQLVARCHAR,false,false,50);
		  mssql_bind($query,'@Dicipline',$Dicipline,SQLVARCHAR,false,false,50);		
		  mssql_bind($query,'@Attention_Span',$Attention_Span,SQLVARCHAR,false,false,50);
		  mssql_bind($query,'@Punctuality',$Punctuality,SQLVARCHAR,false,false,50);		
		  mssql_bind($query,'@Social_Interaction',$Social_Interaction,SQLVARCHAR,false,false,50);
		  mssql_bind($query,'@Hygiene',$Hygiene,SQLVARCHAR,false,false,50);
          mssql_bind($query,'@Group_Work',$Group_Work,SQLVARCHAR,false,false,50);
		  mssql_bind($query,'@Sports_Games',$Sports_Games,SQLVARCHAR,false,false,50);		
		  mssql_bind($query,'@Art_Craft',$Art_Craft,SQLVARCHAR,false,false,50);
		  mssql_bind($query,'@Computer_Skills',$Computer_Skills,SQLVARCHAR,false,false,50);	
		  mssql_bind($query,'@Projects',$Projects,SQLVARCHAR,false,false,50);
		  mssql_bind($query,'@Participation',$Participation,SQLVARCHAR,false,false,50);
          mssql_bind($query,'@Fine_Motor_Skills',$Fine_Motor_Skills,SQLVARCHAR,false,false,50);
		  mssql_bind($query,'@Gross_Motor_Skills',$Gross_Motor_Skills,SQLVARCHAR,false,false,50);	
		  mssql_bind($query,'@Noday_Attendance',$Noday_Attendance,SQLINT4,false,false,5);
		  mssql_bind($query,'@English_Classwrk',$English_Classwrk,SQLINT4,false,false,5);	
		  mssql_bind($query,'@English_Examgrade',$English_Examgrade,SQLINT4,false,false,5);
          mssql_bind($query,'@Remarks',$Remarks,SQLVARCHAR,false,false,strlen($Remarks));	
		  mssql_bind($query,'@English_Totalgrade',$English_Totalgrade,SQLINT4,false,false,5);
          mssql_bind($query,'@Tamil_Classwrk',$Tamil_Classwrk,SQLINT4,false,false,5);
		  mssql_bind($query,'@Tamil_Examgrade',$Tamil_Examgrade,SQLINT4,false,false,5);	
		  mssql_bind($query,'@Tamil_Totalgrade',$Tamil_Totalgrade,SQLINT4,false,false,5);	
		  mssql_bind($query,'@Maths_Classwrk',$Maths_Classwrk,SQLINT4,false,false,5);
		  mssql_bind($query,'@Maths_Examgrade',$Maths_Examgrade,SQLINT4,false,false,5);
          mssql_bind($query,'@Maths_Totalgrade',$Maths_Totalgrade,SQLINT4,false,false,5);
		  mssql_bind($query,'@Science_Classwrk',$Science_Classwrk,SQLINT4,false,false,5);	
		  mssql_bind($query,'@Science_Examgrade',$Science_Examgrade,SQLINT4,false,false,5);
		  mssql_bind($query,'@Science_Totalgrade',$Science_Totalgrade,SQLINT4,false,false,5);
		  mssql_bind($query,'@Sscience_Classwrk',$Sscience_Classwrk,SQLINT4,false,false,5);
		  mssql_bind($query,'@Sscience_Examgrade',$Sscience_Examgrade,SQLINT4,false,false,5);	
		  mssql_bind($query,'@Sscience_Totalgrade',$Sscience_Totalgrade,SQLINT4,false,false,5);
		  mssql_bind($query,'@Computer_Classwrk',$Computer_Classwrk,SQLINT4,false,false,5);
		  mssql_bind($query,'@Computer_Examgrade',$Computer_Examgrade,SQLINT4,false,false,5);
		  mssql_bind($query,'@Computer_Totalgrade',$Computer_Totalgrade,SQLINT4,false,false,5);	
		  mssql_bind($query,'@Project_Classwrk',$Project_Classwrk,SQLINT4,false,false,5);
		  mssql_bind($query,'@Project_Examgrade',$Project_Examgrade,SQLINT4,false,false,5);
		  mssql_bind($query,'@Project_Totalgrade',$Project_Totalgrade,SQLINT4,false,false,5);
		  mssql_bind($query,'@Eng_Comprehension',$Eng_Comprehension,SQLINT4,false,false,5);	
		  mssql_bind($query,'@Eng_Speaking',$Eng_Speaking,SQLINT4,false,false,5);
		  mssql_bind($query,'@Eng_Reading',$Eng_Reading,SQLINT4,false,false,5);
		  mssql_bind($query,'@Eng_Writing',$Eng_Writing,SQLINT4,false,false,5);	
		  mssql_bind($query,'@Tam_Comprehension',$Tam_Comprehension,SQLINT4,false,false,5);
		  mssql_bind($query,'@Tam_Speaking',$Tam_Speaking,SQLINT4,false,false,5);
		  mssql_bind($query,'@Tam_Reading',$Tam_Reading,SQLINT4,false,false,5);	
		  mssql_bind($query,'@Tam_Writing',$Tam_Writing,SQLINT4,false,false,5);
          mssql_bind($query,'@Mat_Comprehension',$Mat_Comprehension,SQLINT4,false,false,5);
		  mssql_bind($query,'@Scn_Comprehension',$Scn_Comprehension,SQLINT4,false,false,5);
          mssql_bind($query,'@Dcode',$Dcode,SQLVARCHAR,false,false,50);
                
          $result = @mssql_execute($query);
		  mssql_free_statement($query);
  } 
            if ((!$result)) 
            {
                echo  "<br /> Row $x not inserted.";
                echo "<br /> Reason : Error!! ";
                echo "<br />";
                echo "<br />";
                echo "Error Message:  <br /><br />".mssql_get_lastmessage();
                echo "<br />";
                echo "<br />";
                $error_rows[$array_count]=$x;
                $array_count=$array_count+1;
                $error_count=$error_count+1;
            }  
          
            else
	       {
            echo "<br /> Row".$x."  Saved Successfully !!";
         	 $total_rows=$total_rows+1;
              
	       }
             
        $x++;      
  
}

   
}  } else  echo"<br/><p class='error' align='center'>".mssql_get_last_message()."</p>";?>
</html>