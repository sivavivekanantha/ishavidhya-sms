<?php session_start();  
include("../includes/connection.php");	?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<script src="../includes/highchart/highcharts.js" type="text/javascript"></script>
<script src="../includes/highchart/exporting.js" type="text/javascript"></script>

</head>
<body>

<form name="myform" id="myform" method="post" action="yearlystudentchart.php">
 <?php 
            $query = mssql_init('SP_Front_Screenchart',$mssql);
			mssql_bind($query, '@School_Id', $_SESSION['SchoolId'], SQLINT4, false, false, 5);
    		$result = mssql_execute($query);
    		mssql_free_statement($query);
    		$rs_cnt = mssql_num_rows($result);
 		
 //if($rs_cnt>0)
 {
 while ($fieldview2 = mssql_fetch_array($result)) 
				{
				if(strlen($school_name)>0) $school_name.=",'".$fieldview2['School_Name']."'"; 
				else $school_name="'".$fieldview2['School_Name']."'";
				 
				if(strlen($total)>0) $total=$total.",".$fieldview2['Total']; else $total=$fieldview2['Total'];
				
				if(strlen($boys)>0) $boys=$boys.",".$fieldview2['Boys']; else $boys=$fieldview2['Boys'];
				
				if(strlen($girls)>0) $girls=$girls.",".$fieldview2['Girls']; else $girls=$fieldview2['Girls'];
				
				if(strlen($scholarshiptotal)>0) $scholarshiptotal=$scholarshiptotal.",".$fieldview2['TotalScholarship']; else $scholarshiptotal=$fieldview2['TotalScholarship'];
				
				if(strlen($scholarshipboys)>0) $scholarshipboys=$scholarshipboys.",".$fieldview2['ScholarshipBoys']; else $scholarshipboys=$fieldview2['ScholarshipBoys'];
				
				if(strlen($scholarshipgirls)>0) $scholarshipgirls=$scholarshipgirls.",".$fieldview2['ScholarshipGirls']; else $scholarshipgirls=$fieldview2['ScholarshipGirls'];
				
				if(strlen($totdropout)>0) $totdropout=$totdropout.",".$fieldview2['TotalDropoutStudents']; else $totdropout=$fieldview2['TotalDropoutStudents'];
				
				if(strlen($dropout)>0) $dropout=$dropout.",".$fieldview2['DropoutStudents']; else $dropout=$fieldview2['DropoutStudents'];
				
				
				 }		 ?>  
				 
				 

<script>
$(function () {
        $('#container').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Ishavidhya'
            },
          
            xAxis: {
                  categories: [<?php echo $school_name;?>]
            },
            yAxis: {
                min: 0,
                title: {
                 text: 'No.of Students and No.of Scholarship Students'
                },
				 stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                     
                    }
                }
				
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
				   pointPadding: 0.3,
                    borderWidth: 0,
					
	              }
            },
			
			
            series: [{
               name: 'Total Child',
                 data: [<?php echo $total;?>],
				 dataLabels: {
                    enabled: true,
                    rotation: -90,
                    color: '#000',
                    align: 'left',
                    x: 4,
                    y: -5,
                    style: {
                        fontSize: '10px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
    
            }, {
                name: 'Boys',
                 data: [<?php echo $boys;?>],
				 dataLabels: {
                    enabled: true,
                    rotation: -90,
                    color: 'red',
                    align: 'left',
                    x: 4,
                    y: -5,
                    style: {
                        fontSize: '10px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
    
            }, {
                name: 'Girls',
                data: [<?php echo $girls;?>],
				 dataLabels: {
                    enabled: true,
                    rotation: -90,
                    color: 'blue',
                    align: 'left',
                   x: 4,
                    y: -5,
                    style: {
                        fontSize: '10px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
    
            }, 
			{
                name: 'Total Scholarship  Child',
                data: [<?php echo $scholarshiptotal;?>],
				 dataLabels: {
                    enabled: true,
                    rotation: -90,
                    color: '#000',
                    align: 'left',
                   x: 4,
                    y: -5,
                    style: {
                        fontSize: '10px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
    
            },
			{
                name: 'Scholarship Girls',
                data: [<?php echo $scholarshipgirls;?>],
				 dataLabels: {
                    enabled: true,
                    rotation: -90,
                    color: 'red',
                    align: 'left',
                    x: 4,
                    y: -5,
                    style: {
                        fontSize: '10px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
    
            },
			{
                name: 'Scholarship Boys',
                data: [<?php echo $scholarshipboys;?>],
				 dataLabels: {
                    enabled: true,
                    rotation: -90,
                    color: '#000',
                    align: 'left',
                    x: 4,
                    y: -5,
                    style: {
                        fontSize: '10px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
    
            },
			
			{
                name: 'Total Dropout ',
                data: [<?php echo $totdropout;?>],
				 dataLabels: {
                    enabled: true,
                    rotation: -90,
                    color: 'blue',
                    align: 'left',
                    x: 4,
                    y: -10,
                    style: {
                        fontSize: '10px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
    
            },
			
			{
                name: 'Dropout Scholarship',
                data: [<?php echo $dropout;?>],
				 dataLabels: {
                    enabled: true,
                    rotation: -90,
                    color: 'blue',
                    align: 'left',
                    x: 4,
                    y: -5,
                    style: {
                        fontSize: '10px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
    
            },
			
			]
        });
    });
</script>
<?php } ?>
<div id="container" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
</form>
</body>
</html>